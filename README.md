# Crave Library

The `Crave` library contains all basic needs for a new project, such as:
- Enums
- Constants
- Basic type extensions
- Base class for unit tests