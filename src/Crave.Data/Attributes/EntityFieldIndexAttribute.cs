﻿using System;

namespace Crave.Data.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class EntityFieldIndexAttribute : Attribute
    {
        public EntityFieldIndexAttribute(int fieldIndex)
        {
            this.FieldIndex = fieldIndex;
        }

        public int FieldIndex { get; }
    }
}
