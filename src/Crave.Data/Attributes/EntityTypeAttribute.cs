﻿using System;

namespace Crave.Data.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EntityTypeAttribute : Attribute
    {
        public EntityTypeAttribute(Type entityType)
        {
            this.EntityType = entityType;
        }

        public Type EntityType { get; }
    }
}
