﻿using System;

namespace Crave.Data.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ExcludeEntityFieldAttribute : Attribute
    {
    }
}
