﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Crave.Data
{
    public class CraveEntityException : CraveException
    {
        public CraveEntityException(Enum error, IEntity2 entity) : base(error, string.Empty)
        {
        }

        public CraveEntityException(Enum error, IEntity2 entity, string format, params object[] args) : base(error, format, args)
        {
        }
    }
}
