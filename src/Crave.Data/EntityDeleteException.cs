﻿using System;

namespace Crave.Data
{
    public class EntityDeleteException : ApplicationException
    {
        public EntityDeleteException(string message) : base(message)
        {
        }

        public EntityDeleteException(string message, params object[] args) : base(string.Format(message, args))
        {
        }

        public EntityDeleteException(Exception innerException, string message, params object[] args) : base(string.Format(message, args), innerException)
        {
        }
    }
}