﻿using System;

namespace Crave.Data
{
    public class EntitySaveException : ApplicationException
    {
        #region Constructors

        public EntitySaveException(string message) : base(message)
        {
        }

        public EntitySaveException(string message, params object[] args) : base(string.Format(message, args))
        {
        }

        public EntitySaveException(Exception innerException, string message, params object[] args) : base(string.Format(message, args), innerException)
        {
        }

        #endregion
    }
}