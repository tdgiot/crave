﻿using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Data.Interfaces
{
    public interface IAdapter
    {
        IDataAccessAdapter Adapter { get; set; }
    }
}
