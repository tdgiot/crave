﻿using System;

namespace Crave.Data.Interfaces
{
    /// <summary>
    /// Cache invalidator interface
    /// </summary>
    public interface ICacheInvalidator : IDisposable
    {
        /// <summary>
        /// Publish event to invalidate a cache key
        /// </summary>
        /// <param name="key">Caching key to invalidate</param>
        void InvalidateCache(string key);

        /// <summary>
        /// Subscribe to cache invalidator to receive a signal when
        /// </summary>
        /// <param name="callback">Action to call when message is received</param>
        /// <returns>Disposable for disposing the subscription when it's not needed anymore</returns>
        IDisposable Subscribe(Action<string> callback);
    }
}