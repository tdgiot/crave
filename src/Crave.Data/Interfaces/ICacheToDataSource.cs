﻿namespace Crave.Data.Interfaces
{
    /// <summary>
    /// Implement methods to allow saving to a data source
    /// </summary>
    /// <typeparam name="TKey">Key type to index the value</typeparam>
    /// <typeparam name="TValue">Value type to save</typeparam>
    public interface ICacheToDataSource<in TKey, in TValue>
    {
        /// <summary>
        /// Add object of type <typeparamref name="TValue"/> to data source.
        /// </summary>
        /// <param name="key">Key to link the model to</param>
        /// <param name="model">Actual object to save/cache</param>
        void Add(TKey key, TValue model);
    }
}