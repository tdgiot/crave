﻿using System.Collections.Generic;

namespace Crave.Data.Interfaces
{
    public interface IChildCategoryFetcher
    {
        List<int> GetChildCategoryIds(List<int> parentCategoryIds);
    }
}