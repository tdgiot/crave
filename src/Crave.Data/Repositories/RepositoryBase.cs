﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using Crave.Data.Interfaces;

namespace Crave.Data.Repositories
{
    /// <summary>
    /// Base repository class
    /// </summary>
    /// <typeparam name="TDataSource">Type of data source to use</typeparam>
    public class RepositoryBase<TDataSource>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase{TDataSource}" /> class.
        /// </summary>
        protected RepositoryBase(params TDataSource[] dataSources)
        {
            this.AddDataSources(dataSources);
        }
        
        #endregion

        #region Properties

        /// <summary>
        /// Gets data source interface structure.
        /// </summary>
        /// <remarks>
        /// This property should ONLY be used to get method signatures of the data source interface.
        /// </remarks>
        protected TDataSource DataSource { get; private set; }

        /// <summary>
        /// Gets the sorted list containing the data sources.
        /// </summary>
        protected SortedList<int, TDataSource> DataSources { get; } = new SortedList<int, TDataSource>();
        
        #endregion

        #region Methods

        /// <summary>
        /// Adds the specified data source to the repository.
        /// </summary>
        /// <param name="dataSource">The data source to add.</param>
        public void AddDataSource(TDataSource dataSource)
        {
            if (this.DataSources.Count == 0)
            {
                this.DataSource = dataSource;
                this.DataSources.Add(1, dataSource);
            }
            else
            {
                this.DataSources.Add(this.DataSources.Count + 1, dataSource);
            }
        }

        /// <summary>
        /// Adds the specified data sources to the repository.
        /// </summary>
        /// <param name="dataSources">The list of data sources to add.</param>
        public void AddDataSources(IEnumerable<TDataSource> dataSources)
        {
            foreach (TDataSource dataSource in dataSources)
            {
                this.AddDataSource(dataSource);
            }
        }

        /// <summary>
        /// Execute update or delete method on data source which will not return anything.
        /// </summary>
        /// <typeparam name="TRequestDto">The type of the request DTO.</typeparam>
        /// <param name="method">The method which has the same signature as the method to call on the data source.</param>
        /// <param name="requestDto">The request DTO which contains the parameters of the request.</param>
        protected void TryExecuteMethod<TRequestDto>(Action<TRequestDto> method, TRequestDto requestDto)
        {
            // Check whether data sources have been configured for this repository
            this.CheckDataSources();

            foreach (TDataSource dataSource in this.DataSources.Values)
            {
                try
                {
                    // Create a delegate for the method to call on the data source
                    Delegate methodOnDataSource = this.GetDelegate(method.GetType(), dataSource, method.Method);
                    methodOnDataSource.DynamicInvoke(requestDto);
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e);

#if DEBUG
                    // Re-throw when debugging
                    throw;
#endif
                }
            }
        }

        /// <summary>
        /// Tried to get a value from the data source of the repository
        /// When using multiple data sources and the value does not exists in a high priority data soruce
        /// the return value will automatically be progated from the lower to high priority data sources.
        /// </summary>
        /// <typeparam name="TResult">The type of the value to return.</typeparam>
        /// <param name="method">The method which has the same signature as the method to call on the data source.</param>
        /// <param name="value">The instance which holds the value to fill in this method.</param>
        /// <returns>True and an instance of <typeparamref name="TResult" /> if the value could be retrieved from the data sources, otherwise false.</returns>
        protected bool TryExecuteMethod<TResult>(Func<TResult> method, out TResult value)
        {
            return this.InvokeMethodOnDataSources(method.GetType(), method.Method, out value);
        }

        /// <summary>
        /// Tries to get a value from the data sources of the repository.
        /// When using multiple data sources and the value does not exists in a high priority data source
        /// the return value will automatically be progated from the lower to high priority data sources.
        /// </summary>
        /// <typeparam name="TResult">The type of the value to return.</typeparam>
        /// <typeparam name="TRequestDto">The type of the request DTO.</typeparam>
        /// <param name="method">The method which has the same signature as the method to call on the data source.</param>
        /// <param name="requestDto">The request DTO which contains the parameters of the request.</param>
        /// <param name="value">The instance which holds the value to fill in this method.</param>
        /// <returns>True and an instance of <typeparamref name="TResult" /> if the value could be retrieved from the data sources, otherwise false.</returns>
        protected bool TryExecuteMethod<TRequestDto, TResult>(Func<TRequestDto, TResult> method, TRequestDto requestDto, out TResult value)
        {
            return this.InvokeMethodOnDataSources(method.GetType(), method.Method, requestDto, out value);
        }

        /// <summary>
        /// Tries to get a value by executing a method on the data sources of the repository.
        /// When using multiple data sources and the value does not exists in a high priority data source
        /// the return value will NOT!!!! be cached.
        /// </summary>
        /// <typeparam name="TResult">The type of the value to return.</typeparam>
        /// <typeparam name="T1">The type of the first argument.</typeparam>
        /// <typeparam name="T2">The type of the second argument.</typeparam>
        /// <param name="method">The method which has the same signature as the method to call on the data source.</param>
        /// <param name="value">The instance which holds the value to fill in this method.</param>
        /// <param name="arg1">The first argument.</param>
        /// <param name="arg2">The second argument.</param>
        /// <returns>An <see cref="TResult" /> instance if the value could be retrieved from the data sources.</returns>
        protected bool TryExecuteMethod<T1, T2, TResult>(Func<T1, T2, TResult> method, T1 arg1, T2 arg2, out TResult value)
        {
            return this.InvokeMethodOnDataSources(method.GetType(), method.Method, arg1, arg2, out value);
        }

        /// <summary>
        /// Tries to get a value by executing a method on the data sources of the repository.
        /// When using multiple data sources and the value does not exists in a high priority data source
        /// the return value will NOT!!!! be cached.
        /// </summary>
        /// <typeparam name="TResult">The type of the value to return.</typeparam>
        /// <typeparam name="T1">The type of the first argument.</typeparam>
        /// <typeparam name="T2">The type of the second argument.</typeparam>
        /// <typeparam name="T3">The type of the third argument.</typeparam>
        /// <param name="method">The method which has the same signature as the method to call on the data source.</param>
        /// <param name="value">The instance which holds the value to fill in this method.</param>
        /// <param name="arg1">The first argument.</param>
        /// <param name="arg2">The second argument.</param>
        /// <param name="arg3">The third argument.</param>
        /// <returns>An <see cref="TResult" /> instance if the value could be retrieved from the data sources.</returns>
        protected bool TryExecuteMethod<T1, T2, T3, TResult>(Func<T1, T2, T3, TResult> method, T1 arg1, T2 arg2, T3 arg3, out TResult value)
        {
            return this.InvokeMethodOnDataSources(method.GetType(), method.Method, arg1, arg2, arg3, out value);
        }

        /// <summary>
        /// Invokes a method on the data source in this repository.
        /// </summary>
        /// <typeparam name="TReturn">The type of the value to return.</typeparam>
        /// <param name="methodType">Method type used for creating delegate</param>
        /// <param name="methodInfo">Method information holding the name of the method used to create delegate</param>
        /// <param name="value">The instance which holds the value to fill in this method.</param>
        /// <returns>True and an instance of <typeparamref name="TReturn" /> if the value could be retrieved from the data sources, otherwise false.</returns>
        private bool InvokeMethodOnDataSources<TReturn>(Type methodType, MethodInfo methodInfo, out TReturn value)
        {
            bool success = false;
            value = default(TReturn);

            // Check whether we have data sources
            this.CheckDataSources();

            // Walk through the data sources and try to get the value from them
            foreach (TDataSource dataSource in this.DataSources.Values)
            {
                // Create a delegate for the method to call on the data source
                Delegate methodOnDataSource = this.GetDelegate(methodType, dataSource, methodInfo);
                value = (TReturn)methodOnDataSource.DynamicInvoke();

                if (value != null)
                {
                    success = true;
                    break;
                }
            }

            return success;
        }

        /// <summary>
        /// Invokes a method on the data source in this repository.
        /// </summary>
        /// <typeparam name="TReturn">The type of the value to return.</typeparam>
        /// <typeparam name="T1">The type of the request DTO.</typeparam>
        /// <param name="methodType">Method type used for creating delegate</param>
        /// <param name="methodInfo">Method information holding the name of the method used to create delegate</param>
        /// <param name="arg">The request DTO which contains the parameters of the request.</param>
        /// <param name="value">The instance which holds the value to fill in this method.</param>
        /// <returns>True and an instance of <typeparamref name="T1" /> if the value could be retrieved from the data sources, otherwise false.</returns>
        protected virtual bool InvokeMethodOnDataSources<T1, TReturn>(Type methodType, MethodInfo methodInfo, T1 arg, out TReturn value)
        {
            bool success = false;

            value = default(TReturn);

            // Check whether data sources have been configured for this repository
            this.CheckDataSources();

            List<TDataSource> valueMissingDataSources = new List<TDataSource>();

            // Walk through the data sources and try to get the value from them
            foreach (TDataSource dataSource in this.DataSources.Values)
            {
                Stopwatch sw = Stopwatch.StartNew();

                // Create a delegate for the method to call on the data source
                Delegate methodOnDataSource = this.GetDelegate(methodType, dataSource, methodInfo);
                value = (TReturn)methodOnDataSource.DynamicInvoke(arg);

                sw.Stop();

                if (value != null)
                {
                    Debug.WriteLine($"Value was successfully retrieved from datasource '{dataSource.GetType().Name}' in {sw.ElapsedMilliseconds} ms.");
                    success = true;
                }
                else
                {
                    Debug.WriteLine($"Value could not be retrieved from datasource '{dataSource.GetType().Name}' in {sw.ElapsedMilliseconds} ms.");
                    valueMissingDataSources.Add(dataSource);
                }

                if (value != null)
                {
                    // This break statement has intentionally been placed 
                    // in a separate if statement for testing purposes
                    break;
                }
            }

            if (success)
            {
                // Write value back to data sources for caching
                this.SaveValue(arg, value, valueMissingDataSources);
            }

            return success;
        }

        /// <summary>
        /// Invokes a method on the data source in this repository.
        /// </summary>
        /// <typeparam name="TReturn">The type of the value to return.</typeparam>
        /// <typeparam name="T1">The type of the first argument.</typeparam>
        /// <typeparam name="T2">The type of the second argument.</typeparam>
        /// <param name="methodType">Method type used for creating delegate</param>
        /// <param name="methodInfo">Method information holding the name of the method used to create delegate</param>
        /// <param name="value">The instance which holds the value to fill in this method.</param>
        /// <param name="arg1">The first argument.</param>
        /// <param name="arg2">The second argument.</param>
        /// <returns>True and an instance of <typeparamref name="TReturn" /> if the value could be retrieved from the data sources, otherwise false.</returns>
        private bool InvokeMethodOnDataSources<T1, T2, TReturn>(Type methodType, MethodInfo methodInfo, T1 arg1, T2 arg2, out TReturn value)
        {
            bool success = false;
            value = default(TReturn);

            // Check whether we have data sources
            this.CheckDataSources();

            // Walk through the data sources and try to get the value from them
            foreach (TDataSource dataSource in this.DataSources.Values)
            {
                // Create a delegate for the method to call on the data source
                Delegate methodOnDataSource = this.GetDelegate(methodType, dataSource, methodInfo);
                value = (TReturn)methodOnDataSource.DynamicInvoke(arg1, arg2);

                if (value != null)
                {
                    success = true;
                    break;
                }
            }

            return success;
        }

        /// <summary>
        /// Invokes a method on the data source in this repository.
        /// </summary>
        /// <typeparam name="TReturn">The type of the value to return.</typeparam>
        /// <typeparam name="T1">The type of the first argument.</typeparam>
        /// <typeparam name="T2">The type of the second argument.</typeparam>
        /// <typeparam name="T3">The type of the third argument.</typeparam>
        /// <param name="methodType">Method type used for creating delegate</param>
        /// <param name="methodInfo">Method information holding the name of the method used to create delegate</param>
        /// <param name="value">The instance which holds the value to fill in this method.</param>
        /// <param name="arg1">The first argument.</param>
        /// <param name="arg2">The second argument.</param>
        /// <param name="arg3">The third argument.</param>
        /// <returns>True and an instance of <typeparamref name="TReturn" /> if the value could be retrieved from the data sources, otherwise false.</returns>
        private bool InvokeMethodOnDataSources<T1, T2, T3, TReturn>(Type methodType, MethodInfo methodInfo, T1 arg1, T2 arg2, T3 arg3, out TReturn value)
        {
            bool success = false;
            value = default(TReturn);

            // Check whether we have data sources
            this.CheckDataSources();

            // Walk through the data sources and try to get the value from them
            foreach (TDataSource dataSource in this.DataSources.Values)
            {
                try
                {
                    // Create a delegate for the method to call on the data source
                    Delegate methodOnDataSource = this.GetDelegate(methodType, dataSource, methodInfo);
                    value = (TReturn)methodOnDataSource.DynamicInvoke(arg1, arg2, arg3);

                    if (value != null)
                    {
                        success = true;
                        break;
                    }
                }
                catch (NotImplementedException)
                {
                    // To catch the NotImplementedException which is mostly thrown in MemoryDataSources and RedisDataSources
                }
            }

            return success;
        }

        /// <summary>
        /// Save value to data sources which implement the <see cref="ICacheToDataSource{TKey,TValue}" /> interface
        /// </summary>
        /// <typeparam name="TKey">Key type</typeparam>
        /// <typeparam name="TValue">Value type</typeparam>
        /// <param name="key">Key to save value to</param>
        /// <param name="value">Value to save</param>
        /// <param name="dataSources">
        /// List of data sources to save value to (if it implements
        /// <see cref="ICacheToDataSource{TKey,TValue}" /> interface)
        /// </param>
        protected void SaveValue<TKey, TValue>(TKey key, TValue value, IEnumerable<TDataSource> dataSources)
        {
            Type interfaceType = typeof(ICacheToDataSource<TKey, TValue>);

            foreach (TDataSource dataSource in dataSources)
            {    
                if (interfaceType.IsInstanceOfType(dataSource))
                {
                    ICacheToDataSource<TKey, TValue> ds = (ICacheToDataSource<TKey, TValue>)dataSource;
                    ds.Add(key, value);
                }
            }
        }

        /// <summary>
        /// Checks whether data sources have been configured for this repository.
        /// </summary>
        protected void CheckDataSources()
        {
            // Check whether data sources have been configured for this repository
            if (this.DataSources.IsNullOrEmpty())
            {
                throw new ApplicationException($"No data sources have been configured for repository '{this.GetType().Name}'!");
            }
        }

        /// <summary>
        /// Gets the delegate for the specified method.
        /// </summary>
        /// <param name="methodType">The type of the method.</param>
        /// <param name="target">The target to invoke the method on.</param>
        /// <param name="methodInfo">The method info for the method to get the delegate for.</param>
        /// <returns>A <see cref="Delegate"/> instance.</returns>
        protected Delegate GetDelegate(Type methodType, object target, MethodInfo methodInfo)
        {
            return Delegate.CreateDelegate(methodType, target, methodInfo.Name);
        }

#endregion
    }
}