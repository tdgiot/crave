﻿namespace Crave
{
    public class ApplicationCode
    {
        public const string Emenu = "CraveEmenuAndroidTablet";
        public const string Console = "CraveConsoleAndroidTablet";
        public const string Agent = "CraveAgentAndroid";
        public const string SupportTools = "CraveSupportToolsAndroid";
        public const string MessagingService = "CraveMessagingServiceAndroid";

        public const string OnsiteAgent = "CraveOnsiteAgent";
        public const string OnsiteServer = "CraveOnsiteServer";
    }
}
