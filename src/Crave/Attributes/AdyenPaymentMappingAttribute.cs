﻿using System;
using Crave.Enums;

namespace Crave.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class AdyenPaymentMappingAttribute : Attribute
    {
        #region Constructors

        public AdyenPaymentMappingAttribute(string mappingName, params AdyenPaymentMethodBrand[] brands)
        {
            this.MappingName = mappingName;
            this.Brands = brands;
        }

        #endregion

        #region Properties

        public string MappingName { get; }
        public AdyenPaymentMethodBrand[] Brands { get; }

        #endregion
    }
}
