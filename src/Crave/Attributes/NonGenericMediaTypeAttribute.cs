﻿using System;
using Crave.Enums;

namespace Crave.Attributes
{
    public class NonGenericMediaTypeAttribute : Attribute
    {
        public NonGenericMediaTypeAttribute(MediaType nonGenericMediaType)
        {
            this.NonGenericMediaType = nonGenericMediaType;
        }

        public MediaType NonGenericMediaType { get; private set; }
    }
}
