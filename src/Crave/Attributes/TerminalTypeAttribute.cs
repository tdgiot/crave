﻿using Crave.Enums;
using System;
using System.Collections.Generic;

namespace Crave.Attributes
{
    public class TerminalTypeAttribute : Attribute
    {
        public TerminalTypeAttribute(params TerminalType[] terminalTypes)
        {
            this.TerminalTypes = terminalTypes;
        }

        public IEnumerable<TerminalType> TerminalTypes { get; set; }
    }
}
