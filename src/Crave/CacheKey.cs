﻿using System;
using System.Collections.Generic;

namespace Crave
{
    /// <summary>
    /// Class which represents a key used for caching.
    /// </summary>
    public class CacheKey
    {
        #region Fields

        /// <summary>The data type of the cache key.</summary>
        private readonly string dataType;

        /// <summary>The dictionary holding the cache key parameters.</summary>
        private readonly Dictionary<string, CacheKeyParameter> parameters = new Dictionary<string, CacheKeyParameter>();

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CacheKey"/> class.
        /// </summary>
        /// <param name="dataType">The data type of the cache key.</param>
        public CacheKey(string dataType)
        {
            this.dataType = dataType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CacheKey"/> class.
        /// </summary>
        /// <param name="dataType">The data type of the cache key.</param>
        /// <param name="parameters">The cache key parameters.</param>
        public CacheKey(string dataType, params CacheKeyParameter[] parameters) : this(dataType)
        {
            foreach (CacheKeyParameter parameter in parameters)
            {
                this.Add(parameter.Name, parameter.Value);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a version for the cache key.
        /// </summary>
        /// <remarks>This property is optional.</remarks>
        public string Version { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Adds the specified key and value to the dictionary of cache key parameters. 
        /// </summary>
        /// <param name="name">The name of the cache key parameter.</param>
        /// <param name="value">The value of the cache key parameter.</param>
        public void Add(string name, object value)
        {
            if (!this.parameters.ContainsKey(name))
            {
                this.parameters.Add(name, new CacheKeyParameter { Name = name, Value = value });
            }
        }

        /// <summary>
        /// Removes the cache key parameter with the specified name.
        /// </summary>
        /// <param name="name">The name of the cache key parameter to remove.</param>
        public void Remove(string name)
        {
            if (this.parameters.ContainsKey(name))
            {
                this.parameters.Remove(name);
            }
        }

        /// <summary>
        /// Returns a string that represents the current cache key.
        /// </summary>
        /// <returns>A <see cref="string"/> instance.</returns>
        public override string ToString()
        {
            return $"{this.dataType}_{string.Join("_", this.parameters.Values)}";
        }

        #endregion

        /// <summary>
        /// Class which represents a parameter in a cache key.
        /// </summary>
        public class CacheKeyParameter
        {
            #region Properties

            /// <summary>
            /// Gets or sets the name of the cache key parameter.
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Gets or sets the value of the cache key parameter.
            /// </summary>
            public object Value { get; set; }

            #endregion

            #region Methods

            /// <summary>
            /// Returns a string that represents the current cache key parameter.
            /// </summary>
            /// <returns>A <see cref="string"/> instance.</returns>
            public override string ToString()
            {
                return $"{this.Name}-{this.Value}";
            }

            #endregion
        }
    }
}
