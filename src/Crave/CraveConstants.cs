using System;
using System.IO;

namespace Crave
{
    /// <summary>
    /// Class which contains constant values that are used throughout the whole Crave platform.
    /// </summary>
    public static class CraveConstants
    {
        /// <summary>
        /// The threshold in seconds which is used to determine whether a device is offline
        /// </summary>
        public const int OfflineThresholdSeconds = 90;

        /// <summary>
        /// The threshold in percentage which is used to determine whether a device is offline due to it's battery.
        /// </summary>
        public const int OutOfBatteryThreshold = 6;

        /// <summary>
        /// The threshold in percentage which is used to determine whether a device is low on battery.
        /// </summary>
        public const int LowBatteryThreshold = 10;

        /// <summary>
        /// The current API version.
        /// </summary>
        /// <remarks>
        /// DO NOT UPDATE THIS VALUE BY HAND!
        /// THIS VALUE IS UPDATED WHEN A NEW VERSION OF THE API IS GENERATED.
        /// </remarks>
        public const string ApiVersion = "v1";

        /// <summary>
        /// The current heartbeat API version.
        /// </summary>
        /// <remarks>
        /// DO NOT UPDATE THIS VALUE BY HAND!
        /// THIS VALUE IS UPDATED WHEN A NEW VERSION OF THE API IS GENERATED.
        /// </remarks>
        public const string HEARTBEAT_API_VERSION = "v1";

        /// <summary>
        /// Configuration entries from web.config AppSettings
        /// </summary>
        public static class AppSettings
        {
            /// <summary>
            /// Messaging service base url
            /// </summary>
            public const string MessagingBaseUrl = "Messaging.BaseUrl";
        }

        /// <summary>
        /// Cloud constants
        /// </summary>
        public static class Cloud
        {
            /// <summary>
            /// Cloud container in which to save weather files
            /// </summary>
            public const string WEATHER_CONTAINER = "weather";

            /// <summary>
            /// Cloud container in which to save entertainment blobs
            /// </summary>
            public const string ENTERTAINMENT_CONTAINER = "entertainment";


            public const string FILENAME_FORMAT_TICKS = "[ticks]"; // MUST BE LOWER CASE!

            /// <summary>
            /// Cloud container in which to save published Api results
            /// </summary>
            public const string APIRESULT_CONTAINER = "published";

            /// <summary>
            /// Cloud container in which to save delivery times
            /// </summary>
            public const string DELIVERYTIME_CONTAINER = "deliverytime";
        }

        public static class AzureCloudStorage
        {
            public const string AzureBlobStorageUrlFormat = "https://{0}.blob.core.windows.net/";

            public static readonly string PrimaryAzureStorageAccount = "cravefiles";
            public static readonly string PrimaryAzureCdnBaseUrl = "https://cravefiles.blob.core.windows.net/"; // Was http://cloudstorage.crave-emenu.com/ but SSL is not supported with custom domains on Azure Storage
            public static readonly string PrimaryAzureCdnBaseUrlFiles = PrimaryAzureCdnBaseUrl.CombinePaths(Media.CLOUD_CONTAINER);

            public static readonly string DevelopmentAzureStorageAccount = "cravefilesdevelopment";
            public static readonly string DevelopmentAzureCdnBaseUrl = "https://cravefilesdevelopment.blob.core.windows.net/"; // Was http://cloudstorage-dev.crave-emenu.com/ but SSL is not supported with custom domains on Azure Storage
            public static readonly string DevelopmentAzureCdnBaseUrlFiles = DevelopmentAzureCdnBaseUrl.CombinePaths(Media.CLOUD_CONTAINER);

            public static readonly string TestAzureStorageAccount = "cravefilestest";
            public static readonly string TestAzureCdnBaseUrl = "https://cravefilestest.blob.core.windows.net/"; // Was http://cloudstorage-test.crave-emenu.com/ but SSL is not supported with custom domains on Azure Storage
            public static readonly string TestAzureCdnBaseUrlFiles = TestAzureCdnBaseUrl.CombinePaths(Media.CLOUD_CONTAINER);

            public static readonly string DemoAzureStorageAccount = "cravefilesdemo";
            public static readonly string DemoAzureCdnBaseUrl = "https://cravefilesdemo.blob.core.windows.net/"; // Was http://cloudstorage-demo.crave-emenu.com/ but SSL is not supported with custom domains on Azure Storage
            public static readonly string DemoAzureCdnBaseUrlFiles = DemoAzureCdnBaseUrl.CombinePaths(Media.CLOUD_CONTAINER);
        }

        public static class AmazonCloudStorage
        {
            public const string AmazonBlobStorageUrlFormat = "https://s3-eu-west-1.amazonaws.com/{0}/{1}";
            public const string AmazonStorageUrlFormat = "https://s3-eu-west-1.amazonaws.com/{0}/";

            public const string AmazonS3AwsCompanyUsername = "{0}-{1}"; // {0} = Environment prefix, {1} = CompanyId        
            public const string AmazonS3AwsCompanyContainer = "companies/" + AmazonS3AwsCompanyUsername;
            public const string AmazonS3AwsGroupname = "companies";
            public const string AmazonS3AwsPolicyName = "companies-policy";

            public static readonly string PrimaryAmazonRootContainer = "cravecloud-prod";
            public static readonly string PrimaryAmazonCdnBaseUrl = AmazonStorageUrlFormat.FormatSafe(PrimaryAmazonRootContainer);
            public static readonly string PrimaryAmazonCdnBaseUrlFiles = PrimaryAmazonCdnBaseUrl.CombinePaths(Media.CLOUD_CONTAINER);

            public static readonly string DevelopmentAmazonRootContainer = "cravecloud-dev";
            public static readonly string DevelopmentAmazonCdnBaseUrl = AmazonStorageUrlFormat.FormatSafe(DevelopmentAmazonRootContainer);
            public static readonly string DevelopmentAmazonCdnBaseUrlFiles = DevelopmentAmazonCdnBaseUrl.CombinePaths(Media.CLOUD_CONTAINER);

            public static readonly string TestAmazonRootContainer = "cravecloud-test";
            public static readonly string TestAmazonCdnBaseUrl = AmazonStorageUrlFormat.FormatSafe(TestAmazonRootContainer);
            public static readonly string TestAmazonCdnBaseUrlFiles = TestAmazonCdnBaseUrl.CombinePaths(Media.CLOUD_CONTAINER);

            public static readonly string DemoAmazonRootContainer = "cravecloud-demo";
            public static readonly string DemoAmazonCdnBaseUrl = AmazonStorageUrlFormat.FormatSafe(DemoAmazonRootContainer);
            public static readonly string DemoAmazonCdnBaseUrlFiles = DemoAmazonCdnBaseUrl.CombinePaths(Media.CLOUD_CONTAINER);
        }

        /// <summary>
        /// Media constants
        /// </summary>
        public static class Media
        {
            /// <summary>
            /// Default JPG quality level
            /// </summary>
            public const int DEFAULT_JPG_QUALITY = 80;

            /// <summary>
            /// Cloud container in which the original media files are saved.
            /// </summary>
            public const string CLOUD_CONTAINER_ORIGINAL = "original";

            /// <summary>
            /// Cloud container in which the original media files are saved.
            /// </summary>
            public const string CLOUD_CONTAINER_GENERIC = "generic";
            
            /// <summary>
            /// Cloud container in which the original media files are saved.
            /// </summary>
            public const string CLOUD_CONTAINER_BRAND = "brand";

            /// <summary>
            /// Cloud container in which media files are saved
            /// </summary>
            public const string CLOUD_CONTAINER = "files";

            /// <summary>
            /// Cloud container in which manual file uploads are saved.
            /// </summary>
            public const string CLOUD_CONTAINER_FILE_UPLOAD = "companies";

            /// <summary>
            /// Media storage cache time (1 day)
            /// </summary>
            public const int MEDIA_STORAGE_CACHE_TIME = 86400;
        }

        /// <summary>
        /// Constants for communication with messaging service
        /// </summary>
        public static class Messaging
        {
            /// <summary>
            /// Internal salt used to hash messages
            /// </summary>
            public const string InternalSalt = "3e722b834e99091b3bb986be5e54d8bec366689cdc3937bb71f96adf9a361c95";

            /// <summary>
            /// Identifier for service apps (API, ControlCenter) to connect with
            /// </summary>
            public const string ServiceIdentifier = "SERVICE_9550706e522ff0567ee8456f405d599e6f2bfdde";

            /// <summary>
            /// Authentication token for service apps (API, ControlCenter)
            /// </summary>
            public const string ServiceAuthToken = "SERVICE_TOKEN_aa8734c4febbb5b5bf3a43a30f885fbde7101506";

            #region Old Messaging

            /// <summary>
            /// Messaging identifier for API
            /// </summary>
            public const string IdentifierApi = "WS_916ba459e02049d97e4582b98cef0fcd631329d0";
            
            /// <summary>
            /// Messaging identifier for Control Center
            /// </summary>
            public const string IdentifierCms = "CMS_103b24abd1d2cd2df282b5fd8d4de4e323a6127c";

            /// <summary>
            /// Messaging identifier for NOC
            /// </summary>
            public const string IdentifierNoc = "NOC_33b2a382454f6327e9ada0dda9712c99cb68715d";

            /// <summary>
            /// Messaging identifier for services
            /// </summary>
            public const string IdentifierServices = "SVC_544955976eab58e623ed086b62427837b62b4d87";

            #endregion

            /// <summary>
            /// Query string keys
            /// </summary>
            public static class QueryString
            {
                /// <summary>
                /// Identifier query string key 
                /// </summary>
                public const string Identifier = "identifier";

                /// <summary>
                /// Access token query string key
                /// </summary>
                public const string AccessToken = "access_token";
            }
        }
    }
}
