﻿using System;

namespace Crave
{
    /// <summary>
    /// Class which is used to throw exceptions from within the Crave platform 
    /// containing enumerator values for the exception codes. 
    /// </summary>
    public class CraveException : Exception
    {
        #region Constructors

        public CraveException()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CraveException"/> class.
        /// </summary>
        /// <param name="error">The code of the exception using an enumerator value.</param>
        public CraveException(Enum error) : base()
        {
            this.Error = error;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CraveException"/> class.
        /// </summary>
        /// <param name="error">The code of the exception using an enumerator value.</param>
        /// <param name="exception">The real exception.</param>
        public CraveException(Enum error, Exception exception) : base(exception.Message, exception)
        {
            this.Error = error;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CraveException"/> class.
        /// </summary>
        /// <param name="error">The code of the exception using an enumerator value.</param>
        /// <param name="format">The format of the message for the exception.</param>
        /// <param name="args">The arguments of the message for the exception.</param>
        public CraveException(Enum error, string format, params object[] args) : base(string.Format(format, args))
        {
            this.Error = error;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the error of the exception using an enumerator value.
        /// </summary>
        public Enum Error { get; }

        /// <summary>
        /// Gets the type of the error enumerator as a <see cref="string"/>. 
        /// </summary>
        public string ErrorType
        {
            get
            {
                return this.Error != null ? this.Error.GetType().Name : "(null)"; 
            }
        }

        /// <summary>
        /// Gets the text of the error enumerator as a <see cref="string"/>. 
        /// </summary>
        public string ErrorText
        {
            get
            {
                return this.Error != null ? this.Error.ToString() : "(null)";
            }
        }

        /// <summary>
        /// Gets the code of the error enumerator as an <see cref="int"/>. 
        /// </summary>
        public int ErrorCode
        {
            get
            {
                int errorCode = -1;

                if (this.Error != null)
                {
                    try
                    {
                        errorCode = (int)(object)this.Error;
                    }
                    catch
                    {
                    }
                }

                return errorCode;
            }
        }

        /// <summary>
        /// Gets the message from the base exception.
        /// </summary>
        public string BaseMessage
        {
            get { return this.Message; }
        }

        #endregion
    }
}
