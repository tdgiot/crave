﻿namespace Crave.Enums
{
    public enum ActionType
    {
        InternalLink = 0,
        ExternalLink = 1,
        PhoneCall = 2,
        Download = 3,
        EmbeddedLink = 4
    }
}
