﻿using Crave.Attributes;

namespace Crave.Enums
{
    public enum AdyenPaymentMethod
    {
        [AdyenPaymentMapping("scheme", AdyenPaymentMethodBrand.MasterCard, AdyenPaymentMethodBrand.Visa, AdyenPaymentMethodBrand.AmericanExpress)]
        CreditCard = 1,

        [AdyenPaymentMapping("paywithgoogle", AdyenPaymentMethodBrand.MasterCard, AdyenPaymentMethodBrand.Visa, AdyenPaymentMethodBrand.AmericanExpress)]
        GooglePay = 2,

        [AdyenPaymentMapping("applepay", AdyenPaymentMethodBrand.MasterCard, AdyenPaymentMethodBrand.Visa, AdyenPaymentMethodBrand.AmericanExpress)]
        ApplePay = 3,

        [AdyenPaymentMapping("ideal")]
        IDeal = 4,

        [AdyenPaymentMapping("paymaya_wallet")]
        PayMaya = 5,

        [AdyenPaymentMapping("doku_ovo")]
        DokuOVO = 6
    }
}
