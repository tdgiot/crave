﻿using System;
using System.Collections.Generic;
using System.Text;
using Crave.Attributes;

namespace Crave.Enums
{
    public enum AdyenPaymentMethodBrand
    {
        [AdyenPaymentMapping("mc")]
        MasterCard = 1,

        [AdyenPaymentMapping("visa")]
        Visa = 2,

        [AdyenPaymentMapping("amex")]
        AmericanExpress = 3,

        [AdyenPaymentMapping("discover")]
        Discover = 4,

        [AdyenPaymentMapping("bancontact")]
        Bancontact = 5,

        [AdyenPaymentMapping("cartesbancaires")]
        CartesBancaires = 6,

        [AdyenPaymentMapping("chinaunionpay")]
        ChinaUnionPay = 7,

        [AdyenPaymentMapping("dankort")]
        Dankort = 8,

        [AdyenPaymentMapping("diners")]
        Diners = 9,

        [AdyenPaymentMapping("elo")]
        Elo = 10,

        [AdyenPaymentMapping("hipercard")]
        Hipercard = 11,

        [AdyenPaymentMapping("jcb")]
        JCB = 12,

        [AdyenPaymentMapping("maestro")]
        Maestro = 13,

        [AdyenPaymentMapping("uatp")]
        UATP = 14,

        [AdyenPaymentMapping("visaelectron")]
        VisaElectron = 15,

        [AdyenPaymentMapping("vpay")]
        VPay = 16
    }
}
