﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Alert type
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum AlertType
    {
        /// <summary>
        /// Something else
        /// </summary>
        Other = 0,
        /// <summary>
        /// All systems green
        /// </summary>
        Good = 1,
        /// <summary>
        /// Something might break very soon
        /// </summary>
        Warning = 2,
        /// <summary>
        /// Shits going down yo
        /// </summary>
        Danger = 3
    }
}