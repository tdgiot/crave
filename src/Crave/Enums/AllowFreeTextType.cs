﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Allow free text type
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum AllowFreeTextType
    {
        /// <summary>
        /// Inherit
        /// </summary>
        Inherit = 0,
        /// <summary>
        /// Yes
        /// </summary>
        Yes = 1,
        /// <summary>
        /// No
        /// </summary>
        No = 2
    }
}