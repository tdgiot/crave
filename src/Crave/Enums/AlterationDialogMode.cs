﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the variant of the alteration dialog.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum AlterationDialogMode
    {
        /// <summary>
        /// Version 1 (alteration wizard)
        /// </summary>
        v1 = 0,

        /// <summary>
        /// Versions 2 (options within options)
        /// </summary>
        v2 = 1
    }
}
