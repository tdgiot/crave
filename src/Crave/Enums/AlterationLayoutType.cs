﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the layout type of the alteration
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum AlterationLayoutType : int
    {
        /// <summary>
        /// Text on top
        /// </summary>
        TextOnTop = 0,

        /// <summary>
        /// Image and text on top
        /// </summary>
        ImageAndTextOnTop = 1,

        /// <summary>
        /// Image and text on left
        /// </summary>
        ImageAndTextOnLeft = 2,
    }
}
