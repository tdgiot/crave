﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of the alteration
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum AlterationType : int
    {
        /// <summary>
        /// TO DO.
        /// </summary>
        Options = 1,

        /// <summary>
        /// A date and time have to be selected.  
        /// </summary>
        DateTime = 2,

        /// <summary>
        /// A date has to be selected.
        /// </summary>
        Date = 3,

        /// <summary>
        /// An email address has to be specified.
        /// </summary>
        Email = 4,

        /// <summary>
        /// TO DO.
        /// </summary>
        Text = 5,

        /// <summary>
        /// TO DO.
        /// </summary>
        Numeric = 6,

        /// <summary>
        /// TO DO.
        /// </summary>
        Package = 7,

        /// <summary>
        /// TO DO.
        /// </summary>
        Category = 8,

        /// <summary>
        /// TO DO.
        /// </summary>
        Summary = 9,

        /// <summary>
        /// TO DO.
        /// </summary>
        Page = 10,

        /// <summary>
        ///  TO DO.
        /// </summary>
        Notes = 11,

        /// <summary>
        /// TO DO.
        /// </summary>
        Image = 12,

        /// <summary>
        /// TO DO.
        /// </summary>
        Instruction = 13,
        
        /// <summary>
        /// TO DO.
        /// </summary>
        Form = 14        
    }
}
