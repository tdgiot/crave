﻿namespace Crave.Enums
{
    public enum AlterationVersion : int
    {
        Packages = 1,
        OptionsWithinOptions = 2
    }
}
