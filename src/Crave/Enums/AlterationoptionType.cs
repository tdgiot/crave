﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of an alteration option.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum AlterationoptionType : int
    {
        /// <summary>
        /// TO DO.
        /// </summary>
        Option = 1,

        /// <summary>
        /// A date and time have to be selected.  
        /// </summary>
        DateTime = 2,

        /// <summary>
        /// A date has to be selected.
        /// </summary>
        Date = 3,

        /// <summary>
        /// An email address has to be specified.
        /// </summary>
        Email = 4,

        /// <summary>
        /// TO DO.
        /// </summary>
        Text = 5,

        /// <summary>
        /// TO DO.
        /// </summary>
        Numeric = 6
    }
}
