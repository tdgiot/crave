﻿namespace Crave.Enums
{
    public enum AnalyticsEnvironment
    {
        None = 0,
        Demo = 1,
        Production = 2
    }
}
