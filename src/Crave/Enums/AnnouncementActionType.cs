﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Announcement action
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum AnnouncementActionType
    {
        /// <summary>
        /// Category
        /// </summary>
        Category = 1,
        /// <summary>
        /// Entertainment
        /// </summary>
        Entertainment = 2,
        /// <summary>
        /// Product
        /// </summary>
        Product = 3        
    }
}