﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the variant of the announcement dialog 
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum AnnouncementDialogType : int
    {
        /// <summary>
        /// Dialog with a single OK button.
        /// </summary>
        OkDialog = 1,

        /// <summary>
        /// TO DO.
        /// </summary>
        NotificationDialog = 2,

        /// <summary>
        ///  TO DO.
        /// </summary>
        ReorderDialog = 3,

        /// <summary>
        /// Dialog with a Yes and a No button.
        /// </summary>
        YesNoDialog = 4
    }
}