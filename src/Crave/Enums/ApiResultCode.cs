﻿namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the result code of an API call. 
    /// </summary>
    public enum ApiResultCode
    {
        /// <summary>
        /// The API call was successful.
        /// </summary>
        Success = 100,

        /// <summary>
        /// The API call failed.
        /// </summary>
        Failure = 200,

        /// <summary>
        /// The API call request failed validation. 
        /// </summary>
        RequestValidationError = 300,

        /// <summary>
        /// The version specified was invalid.
        /// </summary>
        InvalidVersion = 400
    }
}
