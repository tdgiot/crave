﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the interval of clearing third party app data.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum AppDataClearInterval
    {
        /// <summary>
        /// Clear the third party app data when the Emenu starts. 
        /// </summary>
        EmenuStart = 0,

        /// <summary>
        /// Never clear the third party app data.
        /// </summary>
        Never = 1,

        /// <summary>
        /// Clear the third party app data when the app closes. 
        /// </summary>
        AppClose = 2,

        /// <summary>
        /// Clear the third party app data one hour after the app was closed. 
        /// </summary>
        AfterOneHour = 10,

        /// <summary>
        /// Clear the third party app data six hours after the app was closed. 
        /// </summary>
        AfterSixHours = 11,

        /// <summary>
        /// Clear the third party app data twelve hours after the app was closed. 
        /// </summary>
        AfterTwelveHours = 12,

        /// <summary>
        /// Clear the third party app data one day after the app was closed. 
        /// </summary>
        AfterOneDay = 13
    }
}
