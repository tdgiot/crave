﻿using System.ComponentModel;

namespace Crave.Enums
{
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ApplessFeatureFlags
    {
        ReadOnly = 0,
        Maps = 1,
        PwaDisabled = 2,
        Zingle = 3,
        DesktopMode = 4,
        Discounts = 10,
        EatOutToHelpOutDiscount = 999
    }
}
