﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of the attachment
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum AttachmentType
    {
        /// <summary>
        /// Attachment type has not been set
        /// </summary>
        NotSet = 0,

        /// <summary>
        /// A document attachment (PDF)
        /// </summary>
        Document = 1,

        /// <summary>
        /// An attachment that links into a website
        /// </summary>
        Website = 2,

        /// <summary>
        /// An attachment that links to a video agent
        /// </summary>
        AffiliateVideoAgent = 4,

        /// <summary>
        /// An attachment that links to an affiliate website with prices.
        /// </summary>
        AffiliatePrices = 5,

        /// <summary>
        /// An attachment that links to a youtube video
        /// </summary>
        YouTube = 6,

        /// <summary>
        /// An attachment that contains a telephone number
        /// </summary>
        Telephone = 7,
    }
}