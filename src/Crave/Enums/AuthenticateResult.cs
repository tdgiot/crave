﻿namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the result of validating the credentials of an API call.
    /// </summary>
    public enum AuthenticateResult
    {
        /// <summary>
        /// User was successfully authenticated.
        /// </summary>
        Success = 100,

        /// <summary>
        /// Invalid credentials.
        /// </summary>
        InvalidCredentials = 200,

        /// <summary>
        /// Invalid company for the company owner.
        /// </summary>
        InvalidCompanyId = 201,

        /// <summary>
        /// Multiple users found with same username.
        /// </summary>
        MultipleUsersWithSameUsername = 202,

        /// <summary>
        /// No device found for identifier.
        /// </summary>
        NoDeviceFoundForIdentifier = 203,

        /// <summary>
        /// Multiple clients found for identifier.
        /// </summary>
        MultipleClientsFoundForDevice = 205,

        /// <summary>
        /// Multiple terminals found for identifier.
        /// </summary>
        MultipleTerminalsFoundForDevice = 206,

        /// <summary>
        /// No client, terminal or customer for the device.
        /// </summary>
        NoTerminalClientOrCustomerFoundForDevice = 207,

        /// <summary>
        /// No company found for the client or terminal of the device.
        /// </summary>
        NoCompanyFoundForClientOrTerminal = 208,

        /// <summary>
        /// Hash is invalid.
        /// </summary>
        RequestHashIsInvalid = 209,

        /// <summary>
        /// Timestamp is out of range.
        /// </summary>
        TimestampIsOutOfRange = 212,

        /// <summary>
        /// Multiple customers found for the device.
        /// </summary>
        MultipleCustomersFoundForDevice = 214,

        /// <summary>
        /// Customer is black listed. TODO device is black listed?
        /// </summary>
        CustomerIsBlacklisted = 215,

        /// <summary>
        /// No device token configured for device
        /// </summary>
        NoTokenFoundForDevice = 216,

        /// <summary>
        /// No company salt configured for device
        /// </summary>
        NoCompanySaltFoundForDevice = 217,
    }
}
