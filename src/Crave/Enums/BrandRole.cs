﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// User roles in a brand
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum BrandRole
    {
        /// <summary>
        /// Owner
        /// </summary>
        Owner = 1000,

        /// <summary>
        /// Editor
        /// </summary>
        Editor = 500,

        /// <summary>
        /// Viewer
        /// </summary>
        Viewer = 100
    }
}