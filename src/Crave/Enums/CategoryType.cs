﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of the category (
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum CategoryType : int
    {
        /// <summary>
        /// Inherit from parent category.
        /// </summary>
        Inherit = 0,

        /// <summary>
        /// A category containing orderable products.
        /// </summary>
        Normal = 1,

        /// <summary>
        /// A category containing service items. Items in categories of this type are sent directly without adding them to the order basket first. 
        /// </summary>
        ServiceItems = 2,

        /// <summary>
        /// A category containing non-orderable directory items.
        /// </summary>
        Directory = 3
    }
}
