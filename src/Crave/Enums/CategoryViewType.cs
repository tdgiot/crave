﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of the category (
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum CategoryViewType
    {
        // Inherit from parent category.
        Inherit = 0,

        // Default display configuration with all products/categories expanded.
        Standard = 1,

        // Shows the category cascaded on the AppLess client.
        Cascading = 2
    }
}
