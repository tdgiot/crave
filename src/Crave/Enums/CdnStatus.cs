﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the media cdn status.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum CdnStatus
    {
        /// <summary>
        /// Not Available.
        /// </summary>
        NotAvailable,

        /// <summary>
        /// Out Of Date
        /// </summary>
        OutOfDate,

        /// <summary>
        /// Up To Date
        /// </summary>
        UpToDate,

        /// <summary>
        /// Not Applicable.
        /// </summary>
        NotApplicable
    }
}
