﻿namespace Crave.Enums
{
    /// <summary>
    /// Enumeration of charge types.
    /// </summary>
    public enum ChargeType
    {
        /// <summary>
        /// Charge type none.
        /// </summary>
        None = 0,

        /// <summary>
        /// Charge type for fixed prices.
        /// </summary>
        Fixed = 1,

        /// <summary>
        /// Charge type for price percentages
        /// </summary>
        Percentage = 2,
    }
}
