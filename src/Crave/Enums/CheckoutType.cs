﻿namespace Crave.Enums
{
    /// <summary>
    /// Enumeration of checkout method types.
    /// </summary>
    public enum CheckoutType
    {
        /// <summary>
        /// Charge to guest room.
        /// </summary>
        ChargeToRoom = 0,

        /// <summary>
        /// Pay via a payment provider.
        /// </summary>
        PaymentProvider = 1,

        /// <summary>
        /// Pay later, 'Put it on my tab!'
        /// </summary>
        PayLater = 2,

        /// <summary>
        /// Free Of Charge, checkout total of zero.
        /// </summary>
        FreeOfCharge = 3,

        Emenu = 9999
    }
}
