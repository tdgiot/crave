﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the command to send to clients.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ClientCommand
    {
        /// <summary>
        /// Do not use. Does nothing.
        /// </summary>
        DoNothing = 0,

        /// <summary>
        /// Restarts the application.
        /// </summary>
        RestartApplication = 1,

        /// <summary>
        /// Restarts the tablet device.
        /// </summary>
        RestartDevice = 2,

        /// <summary>
        /// Restarts the tablet device in recovery mode.
        /// </summary>
        RestartInRecovery = 3,

        /// <summary>
        /// Sends the log to Crave. 
        /// </summary>
        SendLogToWebservice = 4,

        /// <summary>
        /// Performs the daily order reset.
        /// </summary>
        DailyOrderReset = 5,

        /// <summary>
        /// Downloads an update for the E-menu application.
        /// </summary>
        DownloadEmenuUpdate = 6,

        /// <summary>
        /// Downloads an update for the Agent application.
        /// </summary>
        DownloadAgentUpdate = 7,

        /// <summary>
        /// Downloads an update for the SupportTools application.
        /// </summary>
        DownloadSupportToolsUpdate = 8,

        /// <summary>
        /// Installs an update for the E-menu application.
        /// </summary>
        InstallEmenuUpdate = 9,

        /// <summary>
        /// Installs an update for the Agent application.
        /// </summary>
        InstallAgentUpdate = 10,

        /// <summary>
        /// Installs an update for the SupportTools application.
        /// </summary>
        InstallSupportToolsUpdate = 11,

        /// <summary>
        /// Clears the browser cache.
        /// </summary>
        ClearBrowserCache = 12,

        /// <summary>
        /// Downloads an update for CraveOS.
        /// </summary>
        DownloadOSUpdate = 13,

        /// <summary>
        /// Installs an update for CraveOS. 
        /// </summary>
        InstallOSUpdate = 14,

        /// <summary>
        /// Clears the messages that were sent to a device and are current displayed on the screen of a device. 
        /// </summary>
        ClearSendMessages = 15
    }
}
