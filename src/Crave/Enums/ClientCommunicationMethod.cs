﻿using System;
using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the types of communication (web service, SignalR)
    /// </summary>
    /// <remarks>
    /// VALUES NEED TO BE POWERS OF TWO (0,1,2,4,8,16,etc..)
    /// </remarks>
    [Flags]
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ClientCommunicationMethod
    {
        /// <summary>
        /// Do not use. Internally used to fetch 0 values
        /// </summary>
        None = 0,

        /// <summary>
        /// Force web service communication
        /// </summary>
        /// <remarks>
        /// DEFAULT IN DATABASE!! IF YOU CHANGE THIS ALSO CHANGE THE LastCommunicationMethod FIELD ON Terminal AND Client TABLE!!
        /// </remarks>
        Webservice = 1,

        /// <summary>
        /// SignalR comet connection
        /// </summary>
        SignalR = 4,

        /// <summary>
        /// All communication method i.e. web service and SignalR
        /// </summary>
        All = (Webservice | SignalR)
    }
}
