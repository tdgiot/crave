﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of log message for clients
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ClientLogType : int
    {
        /// <summary>
        ///  Do not use. Unknown.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// A generic message with free-form text.
        /// </summary>
        GenericMessage = 5,

        /// <summary>
        /// The application started.
        /// </summary>
        ApplicationStarted = 10,

        /// <summary>
        /// The application stopped.
        /// </summary>
        ApplicationStopped = 15,

        /// <summary>
        /// Client wake up set
        /// </summary>        
        ClientWakeUpSet = 20,

        /// <summary>
        /// Service version changed
        /// </summary>        
        ServiceVersionChanged = 25,

        /// <summary>
        /// Device is being rebooted
        /// </summary>
        DeviceReboot = 50,

        /// <summary>
        /// Device is being shutdown
        /// </summary>
        DeviceShutdown = 55,

        /// <summary>
        /// The client went to kiosk mode due to unprocessed orders.
        /// </summary>
        FailToKioskForUnprocessedOrders = 100,

        /// <summary>
        /// The client went to kiosk mode due to no terminal being alone. TODO.
        /// </summary>
        FailToKioskNoTerminalOnline = 101,

        /// <summary>
        /// The client went to kiosk mode due to unprocessable orders.
        /// </summary>
        FailToKioskForUnprocessableOrders = 102,

        /// <summary>
        /// The client went to ordering mode.
        /// </summary>
        AutomaticReturnToOrdering = 200,

        /// <summary>
        /// A log file from the client was received.
        /// </summary>
        ReceivedLog = 300,

        /// <summary>
        /// A log file was shipped to Crave.
        /// </summary>
        ShippedLog = 301,

        /// <summary>
        /// The device that was linked to the client is unlinked and another device was linked to this client.
        /// </summary>
        DeviceChanged = 305,

        /// <summary>
        /// The delivery point changed for this client.
        /// </summary>
        DeliverypointChanged = 306,

        /// <summary>
        /// TO DO.
        /// </summary>
        NonQualifiedException = 400,

        /// <summary>
        /// A download was started.
        /// </summary>
        DownloadStarted = 600,

        /// <summary>
        /// A download was completed.
        /// </summary>
        DownloadCompleted = 601,

        /// <summary>
        /// A download failed.
        /// </summary>
        DownloadFailed = 602,

        /// <summary>
        /// The installation of an update was started.
        /// </summary>
        InstallationStarted = 603,

        /// <summary>
        /// The installation of an update completed.
        /// </summary>
        InstallationCompleted = 604,

        /// <summary>
        /// The installation of an update failed.
        /// </summary>
        InstallationFailed = 605,

        /// <summary>
        /// The backup of a configuration file was restored on the device.
        /// </summary>
        ConfigurationBackupRestored = 606,

        /// <summary>
        /// CraveOS was updated on the device.
        /// </summary>
        OsUpdated = 607,

        /// <summary>
        /// TO DO.
        /// </summary>
        FreezeLog = 608,

        /// <summary>
        /// TO DO.
        /// </summary>
        CometMessage = 700,

        /// <summary>
        /// The communication method changed.
        /// </summary>
        CommunicationMethodChanged = 800,

        /// <summary>
        /// Room control has been connnected.
        /// </summary>
        RoomControlConnected = 910,

        /// <summary>
        /// Room control has been disconnected.
        /// </summary>
        RoomControlDisconnected = 920
    }
}
