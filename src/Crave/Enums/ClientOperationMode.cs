﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the operation mode for clients
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ClientOperationMode : int
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Ordering mode.
        /// </summary>
        Ordering = 1,

        /// <summary>
        /// Manually set to kiosk mode.  
        /// </summary>
        KioskManual = 200,

        /// <summary>
        /// Automatically set to kiosk mode due to unprocessed orders.
        /// </summary>
        KioskAutomaticUnprocessedOrders = 201,

        /// <summary>
        /// Automatically set to kiosk mode due to unprocessable orders.
        /// </summary>
        KioskAutomaticUnprocessableOrders = 202,

        /// <summary>
        /// Automatically set to kiosk mode due to no terminal being online.
        /// </summary>
        KioskAutomaticNoTerminalOnline = 203,

        /// <summary>
        /// Automatically set to kiosk mode due to no delivery point being configured.
        /// </summary>
        KioskNoDeliverypointConfigured = 204,

        /// <summary>
        /// Automatically set to kiosk mode due to being disconnected from wi-fi.
        /// </summary>
        KioskDisconnectedFromWifi = 205,

        /// <summary>
        /// Automatically set to kiosk mode due to being disconnected from web service.
        /// </summary>
        KioskDisconnectedFromWebservice = 206,

        /// <summary>
        /// Automatically set to kiosk mode due to being disconnected from PokeIn.
        /// </summary>
        KioskDisconnectedFromPokeIn = 207,

        /// <summary>
        /// Client is disabled.
        /// </summary>
        Disabled = 300,
    }
}
