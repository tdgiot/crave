﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the status of clients
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ClientStatus : int
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        [Stylesheet("default")]
        Unknown = 0,

        /// <summary>
        /// Client is OK.
        /// </summary>
        [Stylesheet("success")]
        OK = 100,

        /// <summary>
        /// Client has a configuration error.
        /// </summary>
        [Stylesheet("warning")]
        ConfigurationError = 201,

        /// <summary>
        /// Client has an internet error.
        /// </summary>
        [Stylesheet("danger")]
        InternetError = 202,

        /// <summary>
        /// Client has a web service error.
        /// </summary>
        [Stylesheet("danger")]
        WebserviceError = 203,

        /// <summary>
        /// Client has a database error.
        /// </summary>
        [Stylesheet("danger")]
        DatabaseError = 204,

        /// <summary>
        /// The application was stopped on the client.
        /// </summary>
        [Stylesheet("danger")]
        ApplicationStopped = 206,
    }
}
