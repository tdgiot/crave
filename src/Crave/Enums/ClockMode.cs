﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the clock mode (24 hour or AM/PM)
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ClockMode
    {
        /// <summary>
        /// 24 hour clock.
        /// </summary>
        Hour24 = 0,

        /// <summary>
        /// 12 hour clock with AM and PM.
        /// </summary>
        AmPm = 1,
    }
}
