﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents a back-end application in the cloud.
    /// </summary>
    public enum CloudApplication
    {
        /// <summary>
        /// API
        /// </summary>
        API = 0,

        /// <summary>
        /// Management
        /// </summary>
        Management = 1,

        /// <summary>
        /// Messaging
        /// </summary>
        Messaging = 2,

        /// <summary>
        /// Mobile NOC
        /// </summary>
        NOC = 3,

        /// <summary>
        /// Services
        /// </summary>
        Services = 4,

        /// <summary>
        /// NOC Service
        /// </summary>
        NOCService = 5
    }
}
