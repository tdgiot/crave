﻿namespace Crave.Enums
{
    public enum CloudEnvironment
    {
        Production = 0,
        //ProductionSecondary = 1,
        //ProductionOverwrite = 2,
        Test = 3,
        Development = 4,
        //Demo = 5,
        Manual = 6,
        Staging = 7
    }
}