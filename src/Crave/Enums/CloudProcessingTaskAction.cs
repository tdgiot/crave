﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the action for a cloud processing task.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum CloudProcessingTaskAction
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Upload item to the cloud.
        /// </summary>
        Upload = 1,

        /// <summary>
        /// Delete item from the cloud.
        /// </summary>
        Delete = 2
    }
}