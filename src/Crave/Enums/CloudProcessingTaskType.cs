﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type for a cloud processing task.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum CloudProcessingTaskType
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Processing of entertainment items.
        /// </summary>
        Entertainment = 1,

        /// <summary>
        /// Processing of weather items.
        /// </summary>
        Weather = 2,

        /// <summary>
        /// Processing of releases.
        /// </summary>
        Release = 3,

        /// <summary>
        /// Processing of media.
        /// </summary>
        Media = 4,

        /// <summary>
        /// Processing of API results.
        /// </summary>
        ApiResult = 5,

        /// <summary>
        /// Processing of delivery time.
        /// </summary>
        DeliveryTime = 6
    }
}
