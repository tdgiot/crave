﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of the cloud storage account
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum CloudStorageAccountType : int
    {
        /// <summary>
        /// Amazon cloud storage.
        /// </summary>
        Amazon = 0,

        /// <summary>
        /// Azure cloud storage.
        /// </summary>
        Azure = 1
    }
}
