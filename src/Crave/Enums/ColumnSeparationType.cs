﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Column separation type
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ColumnSeparationType
    {
        /// <summary>
        /// Tabs
        /// </summary>
        Tabs = 0,
        /// <summary>
        /// Spaces
        /// </summary>
        Spaces = 1,
        /// <summary>
        /// Spaces
        /// </summary>
        Commas = 2
    }
}