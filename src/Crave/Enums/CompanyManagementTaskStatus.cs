﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the status for a company management task.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum CompanyManagementTaskStatus
    {
        /// <summary>
        /// The task is currently pending.
        /// </summary>
        Pending = 0,

        /// <summary>
        /// The task is started.
        /// </summary>
        Started = 10,

        /// <summary>
        /// The task is completed.
        /// </summary>
        Completed = 20,

        /// <summary>
        /// The task has failed.
        /// </summary>
        Failed = 99
    }
}
