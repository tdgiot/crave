﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of the company management task.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum CompanyManagementTaskType
    {
        /// <summary>
        /// Create a copy of a company.
        /// </summary>
        CopyCompany = 0,

        /// <summary>
        /// Export a company to a file.
        /// </summary>
        ExportCompany = 1,

        /// <summary>
        /// Import a company from a file.
        /// </summary>
        ImportCompany = 2,

        /// <summary>
        /// Create a copy of a point-of-interest.
        /// </summary>
        CopyPointOfInterest = 3,

        /// <summary>
        /// Export a point-of-interest to a file.
        /// </summary>
        ExportPointOfInterest = 4,

        /// <summary>
        /// Import a point-of-interest from a file.
        /// </summary>
        ImportPointOfInterest = 5
    }
}
