﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Condition operations
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ConditionOperator
    {
        /// <summary>
        /// Key value equals condition value
        /// </summary>
        Equals = 1,

        /// <summary>
        /// Key value does not equal condition value
        /// </summary>
        NotEquals = 2,

        /// <summary>
        /// Key value is greater than condition value
        /// </summary>
        GreaterThan = 3,

        /// <summary>
        /// Key value is less than condition value
        /// </summary>
        LessThan = 4,

        /// <summary>
        /// Key value is greater then or equal to condition value
        /// </summary>
        GreaterThanOrEqual = 5,

        /// <summary>
        /// Key value is less then or equal to condition value
        /// </summary>
        LessThanOrEqual = 6,
    }
}