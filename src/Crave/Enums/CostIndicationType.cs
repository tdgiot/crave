﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the cost indication for a company or point-of-interest.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum CostIndicationType : int
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        NotSet = 0,

        /// <summary>
        /// Free of charge.
        /// </summary>
        Free = 1,

        /// <summary>
        /// TO DO.
        /// </summary>
        Cheap = 2,

        /// <summary>
        /// TO DO.
        /// </summary>
        Average = 3,

        /// <summary>
        /// TO DO.
        /// </summary>
        Expensive = 4
    }
}
