﻿using System.ComponentModel;

namespace Crave.Enums
{
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum CoversType
    {
        DependentOnProducts = 0,
        AlwaysAsk = 1,
        NeverAsk = 2,
        Inherit = 3
    }
}
