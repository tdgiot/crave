﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the result of saving an order.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum CreateOrderResult
    {
        /// <summary>
        /// Unspecified result.
        /// </summary>
        Unspecified = 0,

        /// <summary>
        /// Order was successfully saved.
        /// </summary>
        Success = 100,

        /// <summary>
        /// Xml Serialize Error.
        /// </summary>
        XmlSerializeError = 201,

        /// <summary>
        /// Deliverypoint is unknown.
        /// </summary>
        UnknownDeliverypoint = 202,

        /// <summary>
        /// Product is unknown.
        /// </summary>
        UnknownProduct = 203,

        /// <summary>
        /// Product in invalid for company.
        /// </summary>
        InvalidProductForCompany = 204,

        /// <summary>
        /// Order failed to save.
        /// </summary>
        EntitySaveRecursiveFalse = 205,

        /// <summary>
        /// Order failed to save.
        /// </summary>
        EntitySaveRecursiveException = 206,

        /// <summary>
        /// User has an open tap in another company.
        /// </summary>
        OpenTransactionInOtherCompany = 207,

        /// <summary>
        /// User is blacklisted
        /// </summary>
        CustomerBlacklisted = 208,

        /// <summary>
        /// The previous order is still pending
        /// </summary>
        PreviousOrderIsPending = 209,

        /// <summary>
        /// Transaction Already Opened
        /// </summary>
        TransactionAlreadyOpened = 210,

        /// <summary>
        /// Transaction Already Closed
        /// </summary>
        TransactionAlreadyClosed = 211,

        /// <summary>
        /// Transaction is closing
        /// </summary>
        TransactionIsClosing = 212,

        /// <summary>
        /// An error occurred while executing the payment request
        /// </summary>
        PaymentRequestExecutionError = 213,

        /// <summary>
        /// The company is outside it's business hours
        /// </summary>
        CompanyIsClosed = 214,

        /// <summary>
        /// User is locked out
        /// </summary>
        CustomerLockedOut = 215,

        /// <summary>
        /// Paymentmethod not supported by company
        /// </summary>
        PaymentmethodNotSupported = 216,

        /// <summary>
        /// Change not allowed to an existing order
        /// </summary>
        ChangeNotAllowedToExistingOrder = 217,

        /// <summary>
        /// Product can not be ordered due to visibily or order schedule
        /// </summary>
        ProductOrderHoursNotAvailable = 219,

        /// <summary>
        /// Deliverypoint is unknown
        /// </summary>
        InvalidProducts = 220,

        MobileOrderingDisabled = 221,

        GeoFencingOutOfRange = 222,

        GeoFencingNoLocation = 223,

        CustomerMustBeSet = 224,

        /// <summary>
        /// The company is outside it's business hours
        /// </summary>
        NoTerminalConnectedToDeliverypoint = 315,

        /// <summary>
        /// Terminal offline while company is in business
        /// </summary>
        TerminalOffline = 316,

        /// <summary>
        /// Transaction without created date
        /// </summary>
        TransactionWithoutCreatedDate = 317,

        /// <summary>
        /// No order items for order
        /// </summary>
        NoOrderitemsForOrder = 318,

        /// <summary>
        /// Deliverypoint is unknown
        /// </summary>
        DeliverypointgroupDisabled = 321,

        /// <summary>
        /// No order items for order
        /// </summary>
        OrderIsNotValidatedByOrderProcessingHelper = 323,

        /// <summary>
        /// Overlapping exceptions found
        /// </summary>
        OverlappingBusinesshourexceptions = 324,

        /// <summary>
        /// No business hours found for company
        /// </summary>
        NoBusinesshoursAvailable = 325,

        /// <summary>
        /// Comet provider could not be set or found
        /// </summary>
        CometProviderNotSet = 500,

        /// <summary>
        /// Failed to save Netmessage object in the database
        /// </summary>
        FailedSavingNetmessage = 501,

        /// <summary>
        /// Guid not unique
        /// </summary>
        GuidNotUnique = 502,

        OrderIsNotNew = 900,

        AuthenticationError = 998,

        Failure = 999
    }
}
