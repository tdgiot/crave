﻿using System;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of the custom text
    /// </summary>
    public enum CustomTextType : int
    {
        #region Company (Range: 1 to 1000)

        //// [MultiLineTextBox]
        CompanyDescription = 1,

        CompanyDescriptionSingleLine = 2,

        //// [MultiLineTextBox]
        //// [EnableEditingOnDefaultTab]
        CompanyVenuePageDescription = 3,

        //// [EnableEditingOnDefaultTab]
        /// TODO This shouldnt be on company
        CompanyBrowserAgeVerificationTitle = 4,

        //// [MultiLineTextBox]
        //// [EnableEditingOnDefaultTab]
        /// TODO This shouldnt be on company
        CompanyBrowserAgeVerificationText = 5,

        #endregion

        #region Deliverypointgroup (Range: 1000 to 2000)

        DeliverypointgroupOrderProcessingNotificationTitle = 1000,

        DeliverypointgroupOrderProcessingNotificationText = 1001,

        DeliverypointgroupServiceProcessingNotificationTitle = 1002,

        DeliverypointgroupServiceProcessingNotificationText = 1003,

        DeliverypointgroupOrderProcessedNotificationTitle = 1004,

        DeliverypointgroupOrderProcessedNotificationText = 1005,

        DeliverypointgroupServiceProcessedNotificationTitle = 1006,

        DeliverypointgroupServiceProcessedNotificationText = 1007,

        DeliverypointgroupFreeformMessageTitle = 1008,

        DeliverypointgroupOutOfChargeTitle = 1009,

        DeliverypointgroupOutOfChargeText = 1010,

        DeliverypointgroupOrderProcessedTitle = 1011,

        DeliverypointgroupOrderProcessedText = 1012,

        DeliverypointgroupOrderFailedTitle = 1013,

        DeliverypointgroupOrderFailedText = 1014,

        DeliverypointgroupOrderSavingTitle = 1015,

        DeliverypointgroupOrderSavingText = 1016,

        DeliverypointgroupOrderCompletedTitle = 1017,

        DeliverypointgroupOrderCompletedText = 1018,

        DeliverypointgroupServiceSavingTitle = 1019,

        DeliverypointgroupServiceSavingText = 1020,

        DeliverypointgroupServiceProcessedTitle = 1021,

        DeliverypointgroupServiceProcessedText = 1022,

        DeliverypointgroupServiceFailedTitle = 1023,

        DeliverypointgroupServiceFailedText = 1024,

        DeliverypointgroupRatingSavingTitle = 1025,

        DeliverypointgroupRatingSavingText = 1026,

        DeliverypointgroupRatingProcessedTitle = 1027,

        DeliverypointgroupRatingProcessedText = 1028,

        DeliverypointgroupOrderingNotAvailableText = 1029,

        DeliverypointgroupPmsDeviceLockedTitle = 1030,

        DeliverypointgroupPmsDeviceLockedText = 1031,

        DeliverypointgroupPmsCheckinFailedTitle = 1032,

        DeliverypointgroupPmsCheckinFailedText = 1033,

        DeliverypointgroupPmsRestartTitle = 1034,

        DeliverypointgroupPmsRestartText = 1035,

        DeliverypointgroupPmsWelcomeTitle = 1036,

        DeliverypointgroupPmsWelcomeText = 1037,

        DeliverypointgroupPmsCheckoutCompleteTitle = 1038,

        DeliverypointgroupPmsCheckoutCompleteText = 1039,

        DeliverypointgroupPmsCheckoutApproveText = 1040,

        DeliverypointgroupChargerRemovedTitle = 1041,

        DeliverypointgroupChargerRemovedText = 1042,

        DeliverypointgroupChargerRemovedReminderTitle = 1043,

        DeliverypointgroupChargerRemovedReminderText = 1044,

        DeliverypointgroupTurnOffPrivacyTitle = 1045,

        DeliverypointgroupTurnOffPrivacyText = 1046,

        DeliverypointgroupItemCurrentlyUnavailableText = 1047,

        DeliverypointgroupAlarmSetWhileNotChargingTitle = 1048,

        DeliverypointgroupAlarmSetWhileNotChargingText = 1049,

        DeliverypointgroupHotelUrl1 = 1050,

        DeliverypointgroupHotelUrl1Caption = 1051,

        DeliverypointgroupHotelUrl2 = 1052,

        DeliverypointgroupHotelUrl2Caption = 1053,

        DeliverypointgroupHotelUrl3 = 1054,

        DeliverypointgroupHotelUrl3Caption = 1055,

        DeliverypointgroupDeliverypointCaption = 1056,

        //// [EnableEditingOnDefaultTab]
        DeliverypointgroupPrintingConfirmationTitle = 1057,

        //// [EnableEditingOnDefaultTab]
        DeliverypointgroupPrintingConfirmationText = 1058,

        //// [EnableEditingOnDefaultTab]
        DeliverypointgroupPrintingSucceededTitle = 1059,

        //// [EnableEditingOnDefaultTab]
        DeliverypointgroupPrintingSucceededText = 1060,

        //// [EnableEditingOnDefaultTab]
        DeliverypointgroupSuggestionsCaption = 1061,

        //// [EnableEditingOnDefaultTab]
        DeliverypointgroupPagesCaption = 1062,

        //// [EnableEditingOnDefaultTab]
        DeliverypointgroupRoomserviceChargeText = 1063,

        //// [EnableEditingOnDefaultTab]
        DeliverypointgroupTitle = 1064,

        //// [EnableEditingOnDefaultTab]
        DeliverypointgroupDescription = 1065,

        //// [EnableEditingOnDefaultTab]
        DeliverypointgroupDeliveryLocationMismatchTitle = 1066,

        //// [EnableEditingOnDefaultTab]
        DeliverypointgroupDeliveryLocationMismatchText = 1067,

        DeliverypointgroupBrowserAgeVerificationTitle = 1068,
        DeliverypointgroupBrowserAgeVerificationText = 1069,
        DeliverypointgroupBrowserAgeVerificationButton = 1070,

        DeliverypointgroupRestartApplicationDialogTitle = 1071,
        DeliverypointgroupRestartApplicationDialogText = 1072,

        DeliverypointgroupRebootDeviceDialogTitle = 1073,
        DeliverypointgroupRebootDeviceDialogText = 1074,

        DeliverypointgroupEstimatedDeliveryTimeDialogTitle = 1075,
        DeliverypointgroupEstimatedDeliveryTimeDialogText = 1076,

        DeliverypointgroupClearBasketTitle = 1078,
        DeliverypointgroupClearBasketText = 1079,

        #endregion

        #region Category (Range: 2000 to 3000)

        CategoryName = 2000,

        //// [MultiLineTextBox]
        CategoryDescription = 2001,

        //// [EnableEditingOnDefaultTab]
        CategoryOrderProcessedTitle = 2002,

        //// [EnableEditingOnDefaultTab]
        CategoryOrderProcessedText = 2003,

        //// [EnableEditingOnDefaultTab]
        CategoryOrderConfirmationTitle = 2004,

        //// [EnableEditingOnDefaultTab]
        CategoryOrderConfirmationText = 2005,

        //// [EnableEditingOnDefaultTab]
        CategoryButtonText = 2006,

        //// [EnableEditingOnDefaultTab]
        CategoryCustomizeButtonText = 2007,

        #endregion

        #region Product (Range: 3000 to 4000)

        ProductName = 3000,

        //// [MultiLineTextBox]
        ProductDescription = 3001,

        ProductButtonText = 3002,

        //// [EnableEditingOnDefaultTab]
        ProductOrderProcessedTitle = 3003,

        //// [EnableEditingOnDefaultTab]
        ProductOrderProcessedText = 3004,

        //// [EnableEditingOnDefaultTab]
        ProductOrderConfirmationTitle = 3005,

        //// [EnableEditingOnDefaultTab]
        ProductOrderConfirmationText = 3006,

        ProductCustomizeButtonText = 3007,

        ProductInstructionsTitle = 3008,

        ProductInstructionsSubtitle = 3009,

        ProductInstructionsText = 3010,

        ProductSuggestionsTitle = 3011,

        ProductSuggestionsSubtitle = 3012,

        ProductShortDescription = 3013,

        #endregion        

        #region ActionButton (Range: 4000 to 5000)

        ActionButtonName = 4000, // TODO

        #endregion

        #region Advertisement (Range: 5000 to 6000)

        AdvertisementName = 5000,

        //// [MultiLineTextBox]
        AdvertisementDescription = 5001,

        #endregion

        #region Alteration (Range: 6000 to 7000)

        AlterationName = 6000,

        //// [MultiLineTextBox]
        AlterationDescription = 6001,

        AlterationColumnTitle = 6002,

        AlterationColumnSubtitle = 6003,

        AlterationNoThanksOption = 6004,

        #endregion

        #region Alterationoption (Range: 7000 to 8000)

        AlterationoptionName = 7000,

        //// [MultiLineTextBox]
        AlterationoptionDescription = 7001,

        #endregion

        #region Amenity (Range: 8000 to 9000)

        AmenityName = 8000,

        #endregion

        #region Announcement (Range: 9000 to 10000) [Obsolete]

        [Obsolete("Not used anymore.")]
        AnnouncementTitle = 9000,

        [Obsolete("Not used anymore.")]
        AnnouncementText = 9001,

        #endregion

        #region Attachment (Range: 10000 to 11000)

        AttachmentName = 10000,

        #endregion

        #region Entertainmentcategory (Range: 11000 to 12000)

        EntertainmentcategoryName = 11000,

        #endregion

        #region Genericcategory (Range: 12000 to 13000)

        GenericcategoryName = 12000,

        #endregion

        #region Genericproduct (Range: 13000 to 14000)

        GenericproductName = 13000,

        //// [MultiLineTextBox]
        GenericproductDescription = 13001,

        #endregion

        #region Page (Range: 14000 to 15000)

        PageName = 14000,

        #endregion

        #region PageTemplate (Range: 15000 to 16000)

        PageTemplateName = 15000,

        #endregion

        #region PointOfInterest (Range: 16000 to 17000)

        //// [MultiLineTextBox]
        PointOfInterestDescription = 16000,

        PointOfInterestDescriptionSingleLine = 16001,

        //// [MultiLineTextBox]
        PointOfInterestVenuePageDescription = 16002,

        #endregion

        #region RoomControlArea (Range: 17000 to 18000)

        RoomControlAreaName = 17000,

        #endregion

        #region RoomControlComponent (Range: 18000 to 19000)

        RoomControlComponentName = 18000,

        #endregion

        #region RoomControlSection (Range: 19000 to 20000)

        RoomControlSectionName = 19000,

        #endregion

        #region RoomControlSectionItem (Range: 20000 to 21000)

        RoomControlSectionItemName = 20000,

        #endregion

        #region RoomControlWidget (Range: 21000 to 22000)

        RoomControlWidgetCaption = 21000,

        #endregion

        #region ScheduledMessage (Range: 22000 to 23000)

        ScheduledMessageTitle = 22000, // TODO

        ScheduledMessageMessage = 22001, // TODO

        #endregion

        #region Site (Range: 23000 to 24000)

        //// [MultiLineTextBox]
        SiteDescription = 23000,

        #endregion

        #region SiteTemplate (Range: 24000 to 25000)

        [Obsolete("Not being used anywhere.")]
        SiteTemplateName = 24000,

        #endregion

        #region Station (Range: 25000 to 26000)

        StationName = 25000,

        //// [MultiLineTextBox]
        StationDescription = 25001,

        StationSuccessMessage = 25002,

        #endregion

        #region UIFooterItem (Range: 26000 to 27000)

        UIFooterItemName = 26000,

        #endregion

        #region UITab (Range: 27000 to 28000)

        UITabCaption = 27000,

        UITabUrl = 27001,

        #endregion

        #region UIWidget (Range: 28000 to 29000)

        UIWidgetCaption = 28000,

        //// [DisableRendering]
        UIWidgetFieldValue1 = 28001,

        //// [DisableRendering]
        UIWidgetFieldValue2 = 28002,

        //// [DisableRendering]
        UIWidgetFieldValue3 = 28003,

        //// [DisableRendering]
        UIWidgetFieldValue4 = 28004,

        //// [DisableRendering]
        UIWidgetFieldValue5 = 28005,

        #endregion

        #region VenueCategory (Range: 29000 to 30000)

        VenueCategoryName = 29000,

        VenueCategoryNamePlural = 29001,

        #endregion

        #region Availability (Range: 30000 to 31000)

        AvailabilityName = 30000,

        AvailabilityStatus = 30001,

        #endregion

        #region ClientConfiguration (Range: 31000 to 32000)

        ClientConfigurationOrderProcessingNotificationTitle = 31000,

        ClientConfigurationOrderProcessingNotificationText = 31001,

        ClientConfigurationServiceProcessingNotificationTitle = 31002,

        ClientConfigurationServiceProcessingNotificationText = 31003,

        ClientConfigurationOrderProcessedNotificationTitle = 31004,

        ClientConfigurationOrderProcessedNotificationText = 31005,

        ClientConfigurationServiceProcessedNotificationTitle = 31006,

        ClientConfigurationServiceProcessedNotificationText = 31007,

        ClientConfigurationFreeformMessageTitle = 31008,

        ClientConfigurationOutOfChargeTitle = 31009,

        ClientConfigurationOutOfChargeText = 31010,

        ClientConfigurationOrderProcessedTitle = 31011,

        ClientConfigurationOrderProcessedText = 31012,

        ClientConfigurationOrderFailedTitle = 31013,

        ClientConfigurationOrderFailedText = 31014,

        ClientConfigurationOrderSavingTitle = 31015,

        ClientConfigurationOrderSavingText = 31016,

        ClientConfigurationOrderCompletedTitle = 31017,

        ClientConfigurationOrderCompletedText = 31018,

        ClientConfigurationServiceSavingTitle = 31019,

        ClientConfigurationServiceSavingText = 31020,

        ClientConfigurationServiceProcessedTitle = 31021,

        ClientConfigurationServiceProcessedText = 31022,

        ClientConfigurationServiceFailedTitle = 31023,

        ClientConfigurationServiceFailedText = 31024,

        ClientConfigurationOrderingNotAvailableText = 31025,

        ClientConfigurationPmsDeviceLockedTitle = 31026,

        ClientConfigurationPmsDeviceLockedText = 31027,

        ClientConfigurationPmsCheckinFailedTitle = 31028,

        ClientConfigurationPmsCheckinFailedText = 31029,

        ClientConfigurationPmsRestartTitle = 31030,

        ClientConfigurationPmsRestartText = 31031,

        ClientConfigurationPmsWelcomeTitle = 31032,

        ClientConfigurationPmsWelcomeText = 31033,

        ClientConfigurationPmsCheckoutCompleteTitle = 31034,

        ClientConfigurationPmsCheckoutCompleteText = 31035,

        ClientConfigurationPmsCheckoutApproveText = 31036,

        ClientConfigurationChargerRemovedTitle = 31037,

        ClientConfigurationChargerRemovedText = 31038,

        ClientConfigurationChargerRemovedReminderTitle = 31039,

        ClientConfigurationChargerRemovedReminderText = 31040,

        ClientConfigurationTurnOffPrivacyTitle = 31041,

        ClientConfigurationTurnOffPrivacyText = 31042,

        ClientConfigurationItemCurrentlyUnavailableText = 31043,

        ClientConfigurationAlarmSetWhileNotChargingTitle = 31044,

        ClientConfigurationAlarmSetWhileNotChargingText = 31045,

        ClientConfigurationDeliverypointCaption = 31046,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationPrintingConfirmationTitle = 31047,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationPrintingConfirmationText = 31048,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationPrintingSucceededTitle = 31049,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationPrintingSucceededText = 31050,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationSuggestionsCaption = 31051,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationRoomserviceChargeText = 31052,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationDeliveryLocationMismatchTitle = 31053,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationDeliveryLocationMismatchText = 31054,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationBrowserAgeVerificationTitle = 31055,

        //// [MultiLineTextBox]
        //// [EnableEditingOnDefaultTab]
        ClientConfigurationBrowserAgeVerificationText = 31056,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationBrowserAgeVerificationButton = 31057,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationRestartApplicationDialogTitle = 31058,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationRestartApplicationDialogText = 31059,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationRebootDeviceDialogTitle = 31060,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationRebootDeviceDialogText = 31061,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationEstimatedDeliveryTimeDialogTitle = 31062,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationEstimatedDeliveryTimeDialogText = 31063,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationClearBasketTitle = 31064,

        //// [EnableEditingOnDefaultTab]
        ClientConfigurationClearBasketText = 31065,

        #endregion

        #region Productgroup (Range: 32000 to 33000)

        ProductgroupName = 32000,
        ProductgroupColumnTitle = 32001,
        ProductgroupColumnSubtitle = 32002,

        #endregion

        #region Routestephandler (Range: 33000 to 34000)

        RoutestephandlerOnTheCaseCaption = 33000,
        RoutestephandlerCompleteCaption = 33001,
        RoutestephandlerManuallyProcessOrderCaption = 33002,

        #endregion

        #region InfraredCommand (Range: 34000 to 35000)

        InfraredCommandName = 34000,

        #endregion

        #region AppLess - carousel item (35000 to 36000)

        AppLessCarouselItemMessage = 35000,

        #endregion

        #region AppLess - navigation menu item (36000 to 37000)

        AppLessNavigationMenuItemText = 36000,

        #endregion

        #region AppLess - Widget Action Banner (37000 to 38000)

        AppLessWidgetActionBannerText = 37000,

        #endregion

        #region AppLess - Widget Action Button (38000 to 39000)

        AppLessWidgetActionButtonText = 38000,

        #endregion

        #region AppLess - Widget Language Switcher (39000 to 40000)

        AppLessWidgetLanguageSwitcherText = 39000,

        #endregion

        #region AppLess - Widget Page Title (40000 to 41000)

        AppLessWidgetPageTitle = 40000,

        #endregion

        #region Outlet (41000 to 42000)

        CheckoutInformationSectionTitle = 41000,

        CheckoutInformationSectionDescription = 41001,

        CustomerDetailsSectionTitle = 41002,

        CustomerDetailsDescription = 41003,

        CustomerDetailsPhoneLabel = 41004,

        CustomerDetailsEmailLabel = 41005,

        PaymentSectionTitle = 41006,

        OrderSummarySectionTitle = 41007,

        OrderSummaryItemsLabel = 41008,

        OrderSummaryTotalLabel = 41009,

        PlaceOrderButtonText = 41010,

        PlaceServiceRequestButtonText = 41011,

        OrderCompletedConfirmationTitle = 41012,

        OrderCompletedConfirmationDescription = 41013,

        OrderFailedConfirmationTitle = 41014,

        OrderFailedConfirmationDescription = 41015,

        ServiceTypeSectionTitle = 41016,

        TippingLabel = 41017,

        TippingSectionTitle = 41018,

        TippingDescription = 41019,

        CustomerDetailsPhonePlaceholder = 41020,

        CustomerDetailsEmailPlaceholder = 41021,

        OutletClosedText = 41022,

        OutletOrderingDisabledText = 41023,

        CustomerDetailsNameLabel = 41024,

        CustomerDetailsNamePlaceholder = 41025,

        ViewBasketButtonText = 41026,

        BackToBasketButtonText = 41027,

        BackToHomepageButtonText = 41028,

        CheckoutTitle = 41029,

        PaymentIsProcessingMessage = 41030,

        PaymentViewPopupPaymentNotValidTitle = 41031,

        PaymentViewPopupPaymentNotValidMessage = 41032,

        CheckoutPopupFailedTitle = 41033,

        CheckoutPopupFailedMessage = 41034,

        CheckoutPopupFailedClose = 41035,

        // AcceptOurTermsLabel = 41036,

        // TermsAndConditionsText = 41037,

        BasketTotalPrice = 41038,

        BasketTitle = 41039,

        CheckoutButtonText = 41040,

        PaymentViewPopupPaymentNotValidClose = 41041,

        PaymentPendingTitle = 41042,

        PaymentPendingMessage = 41043,

        PaymentCancelledTitle = 41045,

        PaymentCancelledMessage = 41046,

        BasketCheckoutMinimalOrderValue = 41047,

        PaymentServiceInvalidInformation = 41048,
        ProductsAvailableAgain = 41049,

        OptInCheckboxLabel = 41050,

        OrderSummarySubtotalLabel = 41051,

        OptInRadioButtonDescription = 41052,

        OptInRadioButtonYesLabel = 41053,

        OptInRadioButtonNoLabel = 41054,

        PaymentConfigurationInvalid = 41055,

        CoversDefaultOptionText = 41056,

        CoversOtherOptionText = 41057,

        CoversNumericPlaceholderText = 41058,

        CheckoutFailedMultipleRevenueCentersMessage = 41059,

        #endregion

        #region Service method (42000 to 43000)

        ServiceMethodLabel = 42000,

        ServiceMethodDescription = 42001,

        DeliverypointLabel = 42002,

        RoomEntryDescription = 42003,

        LastNameLabel = 42004,

        ServiceChargeLabel = 42005,

        ConfirmationText = 42006,

        OrderNumberLabel = 42007,

        OrderCompletedText = 42008,

        DeliveryPointGroupLabel = 42009,

        BuildingNameLabel = 42010,

        BuildingNamePlaceholder = 42011,

        AddressLabel = 42012,

        AddressPlaceholder = 42013,

        ZipCodeLabel = 42014,

        ZipCodePlaceholder = 42015,

        CityLabel = 42016,

        CityPlaceholder = 42017,

        InstructionsLabel = 42018,

        InstructionsPlaceholder = 42019, 
        
        AddressMissingText = 42020,
        
        CityRequiredText = 42021,
        
        PostalCodeRequiredText = 42022,
        
        AddressOutsideRangeText = 42023,
        
        HouseNumberMissingText = 42024,
        
        ServiceFreeLabel = 42025,
        
        DeliveryChargeLabel = 42026,
        
        AddressLookupLabel = 42027,

        MinimalOrderPriceText = 42028,

        CoversTitleText = 42029,

        CoversDescText = 42030,

        #endregion

        #region Checkout Method (43000 to 44000)

        CheckoutMethodLabel = 43000,

        CheckoutMethodDescription = 43001,

        CheckoutMethodConfirmationDescription = 43002,

        CheckoutMethodDeliverypointLabel = 43003,

        CheckoutMethodLastNameLabel = 43004,

        CheckoutMethodDeliveryPointGroupLabel = 43005,

        CheckoutMethodTermsAndConditionsLabel = 43006,

        CheckoutMethodTermsAndConditionsRequiredError = 43007,

        #endregion

        #region Appless - Widget Wait Time (44000 to 45000)

        AppLessWidgetWaitTimeText = 44000,

        #endregion

        #region Appless - Widget Opening Time (45000 to 46000)

        AppLessWidgetOpeningTimeTitle = 45000,

        AppLessWidgetOpeningTimeCurrentlyOpen = 45001,

        AppLessWidgetOpeningTimeClosed = 45002,

        #endregion

        #region Appless - Widget Markdown (46000 to 47000)

        AppLessWidgetMarkdownTitle = 46000,

        AppLessWidgetMarkdownText = 46001,

        #endregion

        #region AppLess - Application Configuration (47000 to 48000)

        ThisIsRequired = 47001,

        CookieAcceptButtonText = 47002,

        CookieTermsAndConditionsLabel = 47003,

        CookieTitleText = 47004,

        InvalidPhoneNumberText = 47005,

        InvalidEmailAddressText = 47006,

        DisclaimerLabel = 47007,

        SaveChangesButtonText = 47008,

        ShowMoreText = 47009,

        ShowLessText = 47010,

        PopupCancel = 47011,

        PaymentOrderTotal = 47012,

        PaymentChoosePaymentMethod = 47013,

        ProductRemoveItem = 47014,

        AlterationsMinRequired = 47015,

        AlterationsMaxRequired = 47016,

        AlterationsOptional = 47017,

        AlterationsMinOptions = 47018,

        AlterationsMaxOptions = 47019,

        AlterationsAddAnother = 47020,

        AlterationsChosenOptionsLabel = 47021,

        AlterationsRequired = 47022,

        ProductViewPopupRemoveProductTitle = 47023,

        ProductViewPopupRemoveProductMessage = 47024,

        ProductViewPopupRemoveProductCancel = 47025,

        ProductViewPopupRemoveProductCta = 47026,

        AlterationOptionsAdd = 47027,

        RadioButtonsOptions = 47028,

        PwaAddToHomeScreenCta = 47029,

        PwaAddToHomeScreenText = 47030,

        ValidationErrorRequestInvalid = 47031,
        ValidationErrorOutletInvalid = 47032,
        ValidationErrorOutletOperationalStateInvalid = 47033,
        ValidationErrorOutletClosedDueBusinessHours = 47034,
        ValidationErrorServiceMethodInvalid = 47035,
        ValidationErrorServiceMethodNotActive = 47036,
        ValidationErrorServiceChargeRequired = 47037,
        ValidationErrorServiceChargeNotRequired = 47038,
        ValidationErrorServiceChargeInvalid = 47039,
        ValidationErrorCheckoutMethodInvalid = 47040,
        ValidationErrorCheckoutMethodNotActive = 47041,
        ValidationErrorPaymentDataInvalid = 47042,
        ValidationErrorPaymentDataUnsupportedPaymentMethod = 47043,
        ValidationErrorCustomerLastNameInvalid = 47044,
        ValidationErrorCustomerEmailInvalid = 47045,
        ValidationErrorCustomerEmailRequired = 47046,
        ValidationErrorCustomerPhoneInvalid = 47047,
        ValidationErrorCustomerPhoneRequired = 47048,
        ValidationErrorTipRequired = 47049,
        ValidationErrorTipNotRequired = 47050,
        ValidationErrorTipInvalidMinimum = 47051,
        ValidationErrorServiceMethodDeliveryPointRequired = 47052,
        ValidationErrorServiceMethodDeliveryPointNotRequired = 47053,
        ValidationErrorServiceMethodDeliveryPointInvalid = 47054,
        ValidationErrorCheckoutMethodDeliveryPointRequired = 47055,
        ValidationErrorCheckoutMethodDeliveryPointNotRequired = 47056,
        ValidationErrorCheckoutMethodDeliveryPointInvalid = 47057,
        ValidationErrorProductInvalid = 47058,
        ValidationErrorProductPriceInvalid = 47059,
        ValidationErrorProductUnavailable = 47060,
        ValidationErrorAlterationInvalid = 47061,
        ValidationErrorAlterationPriceInvalid = 47062,
        ValidationErrorAlterationTimeInvalid = 47063,
        ValidationErrorOrderIntakeDisabled = 47064,
        ValidationErrorDeliveryLocationInvalid = 47065,
        ValidationErrorDeliveryStreetAddressMissing = 47066,
        ValidationErrorDeliveryZipCodeMissing = 47067,
        ValidationErrorDeliveryCityMissing = 47068,
        ValidationErrorDeliveryAddressInvalid = 47069,
        ValidationErrorDeliveryGeoLocationInvalid = 47070,
        ValidationErrorDeliveryInformationMissing = 47071,
        ValidationErrorDeliveryInformationNotRequired = 47072,
        ValidationErrorDeliveryRateInvalid = 47073,
        ValidationErrorDeliveryLocationOutsideDeliveryArea = 47074,
        ValidationErrorNoDeliveryDistanceSpecifiedForServiceMethod = 47075,
        ValidationErrorUnableToRetrieveDistanceBetweenOutletAndDeliveryLocation = 47076,
        ValidationErrorCompanyInvalid = 47077,
        ValidationErrorCategoryInvalid = 47078,
        ValidationErrorCheckoutMethodReceiptTemplateMissing = 47079,
        ValidationErrorInvalidOrderState = 47080,
        ValidationErrorPaymentStateInvalid = 47081,

        TaxBreakdownRate = 47082,
        TaxBreakdownNetto = 47083,
        TaxBreakdownVAT = 47084,
        ProductUnavailable = 47085,

        #endregion

        #region AppLess - Landing Page (48000 to 49000)

        ApplessPageName = 48000,

        #endregion
    }
}
