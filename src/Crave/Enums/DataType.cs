﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Data type
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum DataType
    {
        /// <summary>
        /// String
        /// </summary>
        String = 0, 
        /// <summary>
        /// Integer
        /// </summary>
        Integer = 1,
        /// <summary>
        /// DateTime
        /// </summary>
        DateTime = 2
    }
}