﻿namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents a day or days filter.
    /// </summary>
    public enum DayFilter
    {
        /// <summary>
        /// Filter on today.
        /// </summary>
        Today = 0,

        /// <summary>
        /// Filter on yesterday.
        /// </summary>
        Yesterday = 1,

        /// <summary>
        /// Filter on the past 7 days.
        /// </summary>
        Past7Days = 2,

        /// <summary>
        /// Filter on the past 30 days.
        /// </summary>
        Past30Days = 3
    }
}