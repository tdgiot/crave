﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which specifies how orders are delivered.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum DeliveryLocationType
    {
        /// <summary>
        /// Inherit from parent category.
        /// </summary>
        Inherit = 0,

        /// <summary>
        /// Orders are delivered to the room.
        /// </summary>
        Room = 1,

        /// <summary>
        /// Orders are not delivered to the room.
        /// </summary>
        NoDelivery = 999
    }
}
