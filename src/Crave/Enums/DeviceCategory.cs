﻿using System.ComponentModel;

namespace Crave.Enums
{
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum DeviceCategory : int
    {
        Archos = 1,

        InRoomTablet = 2,

        TvBox = 3,

        Otoucho = 4,
        
        MobileAndroid = 5,
        
        MobileIphone = 6,

        Ipad = 7,

        Misc = 8
    }
}
