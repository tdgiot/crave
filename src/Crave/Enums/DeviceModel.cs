﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the model of a device.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum DeviceModel
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Archos70 tablet.
        /// </summary>
        Archos70 = 2,

        /// <summary>
        /// Samsung Galaxy Tab.
        /// </summary>
        SamsungP4Wifi = 3,

        /// <summary>
        /// Sony Z1.
        /// </summary>
        SonySGP311 = 4,

        /// <summary>
        /// Samsung Galaxy Tab 2
        /// </summary>
        SamsungP5110 = 5,

        /// <summary>
        /// Sony Z2.
        /// </summary>
        SonySGP511 = 6,

        /// <summary>
        /// Android TV box.
        /// </summary>
        AndroidTvBox = 30,

        /// <summary>
        /// Crave T1.
        /// </summary>
        MtkEsky27 = 50,

        /// <summary>
        /// Crave T2.
        /// </summary>
        IntelAnzhen4 = 51,

        /// <summary>
        /// Crave T-Mini
        /// </summary>
        RockchipRK312x = 70,

        /// <summary>
        /// Crave T3
        /// </summary>
        RockchipRK3288_10inch = 80,
        
        /// <summary>
        /// Crave T3-Mini
        /// </summary>
        RockchipRK3288_8inch = 90
    }
}
