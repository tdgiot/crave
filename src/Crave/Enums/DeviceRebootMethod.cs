﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the device reboot method.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum DeviceRebootMethod
    {
        /// <summary>
        /// TO DO.
        /// </summary>
        Default = 1,

        /// <summary>
        /// TO DO.
        /// </summary>
        RebootAfter15Minutes = 2
    }
}
