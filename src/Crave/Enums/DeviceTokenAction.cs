﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Device token action
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum DeviceTokenAction
    {
        /// <summary>
        /// Renew
        /// </summary>
        Renew = 0,
        /// <summary>
        /// Revoked
        /// </summary>
        Revoke = 1
    }
}