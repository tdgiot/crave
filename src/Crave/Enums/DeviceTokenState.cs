﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Device token state
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum DeviceTokenState
    {
        /// <summary>
        /// Queued
        /// </summary>
        Queued = 0,
        /// <summary>
        /// Waiting to be retrieved by device
        /// </summary>
        WaitingToBeRetrieved = 1,
        /// <summary>
        /// Acknowledged
        /// </summary>
        Acknowledged = 2
    }
}