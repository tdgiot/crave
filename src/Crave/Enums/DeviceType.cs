﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a device.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum DeviceType
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// PartnerTech EM-200 tablet
        /// </summary>
        EM200 = 1,

        /// <summary>
        /// Archos 70 tablet
        /// </summary>
        Archos70 = 2,

        /// <summary>
        /// Generic tablet with 1280x800 display
        /// </summary>
        Tablet1280x800 = 3,

        /// <summary>
        /// iPhone 3GS
        /// </summary>
        iPhone = 10,

        /// <summary>
        /// iPhone 4, 4S, 5
        /// </summary>
        iPhoneRetina = 11,

        /// <summary>
        /// iPhone 6
        /// </summary>
        iPhoneRetinaHD = 12,

        /// <summary>
        /// iPhone 6 Plus
        /// </summary>
        iPhoneRetinaHDPlus = 13,

        /// <summary>
        /// WindowsPC (on-site Server)
        /// </summary>
        OnSiteServer = 20,

        /// <summary>
        /// Android Smart TV Box
        /// </summary>
        AndroidTvBox = 30,

        /// <summary>
        /// Small density phones
        /// </summary>
        PhoneSmall = 40,

        /// <summary>
        /// Normal density phones
        /// </summary>
        PhoneNormal = 41,

        /// <summary>
        /// Large density phones
        /// </summary>
        PhoneLarge = 42,

        /// <summary>
        /// XLarge density phones
        /// </summary>
        PhoneXLarge = 43,

        /// <summary>
        /// Small density tablets
        /// </summary>
        TabletSmall = 50,

        /// <summary>
        /// Normal density tablets
        /// </summary>
        TabletNormal = 51,

        /// <summary>
        /// Large density tablets
        /// </summary>
        TabletLarge = 52,

        /// <summary>
        /// XLarge density tablets
        /// </summary>
        TabletXLarge = 53,

        /// <summary>
        /// iPad tablet.
        /// </summary>
        iPad = 60,

        /// <summary>
        /// iPad Retina tablet.
        /// </summary>
        iPadRetina = 61
    }
}