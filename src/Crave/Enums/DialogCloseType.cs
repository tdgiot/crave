﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Dialog close type
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum DialogCloseType
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Ok button
        /// </summary>
        OkButton = 1,
        /// <summary>
        /// Yes button
        /// </summary>
        YesButton = 2,
        /// <summary>
        /// No button
        /// </summary>
        NoButton = 3,

        /// <summary>
        /// Time out
        /// </summary>
        TimeOut = 4,

        /// <summary>
        /// Clear manually
        /// </summary>
        ClearManually = 5
    }
}