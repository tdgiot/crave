﻿namespace Crave.Enums
{
    /// <summary>
    /// Result enums used in discovery service
    /// </summary>
    public enum DiscoveryServiceResult
    {
        RegisterServiceMissingParameters = 201,
        RegisterServiceInvalidParameters = 202,

        RemoveServiceMissingParameters = 301,
    }
}