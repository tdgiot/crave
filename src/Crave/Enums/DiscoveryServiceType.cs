﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Types of services
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum DiscoveryServiceType
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Webservice / API
        /// </summary>
        Api = 1,

        /// <summary>
        /// Control center
        /// </summary>
        ControlCenter = 2,

        /// <summary>
        /// Messaging service
        /// </summary>
        Messaging = 3,

        /// <summary>
        /// Background worker
        /// </summary>
        BackgroundWorker = 4,
    }
}