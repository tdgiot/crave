﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the types of entertainment items
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum EntertainmentType
    {
        /// <summary>
        /// Web page.
        /// </summary>
        Web = 1,

        /// <summary>
        /// Android app.
        /// </summary>
        Android = 2,

        // Deprecated, keeping for future reference
        //// Socialmedia = 4,

        /// <summary>
        /// Survey
        /// </summary>
        //// Survey = 6,

        /// <summary>
        /// Product rating.
        /// </summary>
        //// ProductRating = 7,

        /// <summary>
        /// A browser.
        /// </summary>
        Browser = 8,

        /// <summary>
        /// Restricted browser.
        /// </summary>
        RestrictedBrowser = 9,

        /// <summary>
        /// Content Management System.
        /// </summary>
        Cms = 10,

        /// <summary>
        /// A bill.
        /// </summary>
        Bill = 11,

        /// <summary>
        /// iPad browser.
        /// </summary>
        IpadBrowser = 12,

        /// <summary>
        /// Custom browser.
        /// </summary>
        CustomBrowser = 13,

        /// <summary>
        /// A site.
        /// </summary>
        Site = 14,
    }
}
