﻿using System;
using System.Collections.Generic;
using System.Text;
using Crave.Attributes;

namespace Crave.Enums.Extensions
{
    public static class AdyenPaymentMethodBrandExtensions
    {
        public static string ToAdyenBrand(this AdyenPaymentMethodBrand adyenPaymentMethodBrand)
        {
            AdyenPaymentMappingAttribute adyenPaymentMappingAttribute = adyenPaymentMethodBrand.GetAttribute<AdyenPaymentMappingAttribute>();
            return adyenPaymentMappingAttribute != null ? adyenPaymentMappingAttribute.MappingName : string.Empty;
        }
    }
}
