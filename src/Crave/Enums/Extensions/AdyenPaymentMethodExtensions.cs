﻿using System;
using System.Collections.Generic;
using System.Text;
using Crave.Attributes;

namespace Crave.Enums.Extensions
{
    public static class AdyenPaymentMethodExtensions
    {
        public static string ToAdyenType(this AdyenPaymentMethod adyenPaymentMethod)
        {
            AdyenPaymentMappingAttribute adyenPaymentMappingAttribute = adyenPaymentMethod.GetAttribute<AdyenPaymentMappingAttribute>();
            return adyenPaymentMappingAttribute != null ? adyenPaymentMappingAttribute.MappingName : string.Empty;
        }
    }
}
