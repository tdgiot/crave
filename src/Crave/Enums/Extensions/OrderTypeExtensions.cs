﻿using System;
using Crave.Attributes;

// ReSharper disable once CheckNamespace
namespace Crave.Enums
{
    public static class OrderTypeExtensions
    {
        public static bool DoesNotRequireOrderitems(this OrderType type)
        {
            return type.GetAttribute<DoesNotRequireOrderitemsAttribute>() != null;
        }

        public static bool IsNotUsedForPrinting(this OrderType type)
        {
            return type.GetAttribute<NoPrintingAttribute>() != null;
        }
    }
}
