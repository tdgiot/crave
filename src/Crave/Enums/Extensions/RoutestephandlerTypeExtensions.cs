﻿using Crave.Attributes;
using System;
using System.Collections.Generic;

namespace Crave.Enums
{
    public static class RoutestephandlerTypeExtensions
    {
        public static bool IsAutomaticallyHandled(this RoutestephandlerType type)
        {
            return type.GetAttribute<AutomaticallyHandledAttribute>() != null;
        }

        public static bool IsNotHandledByTerminal(this RoutestephandlerType type)
        {
            return type.GetAttribute<NoTerminalAttribute>() != null;
        }

        public static IEnumerable<TerminalType> GetTerminalTypes(this RoutestephandlerType type)
        {
            IEnumerable<TerminalType> terminalTypes = new List<TerminalType>();

            TerminalTypeAttribute terminalTypeAttribute = type.GetAttribute<TerminalTypeAttribute>();
            if (terminalTypeAttribute != null && !terminalTypeAttribute.TerminalTypes.IsNullOrEmpty())
            {
                terminalTypes = terminalTypeAttribute.TerminalTypes;
            }

            return terminalTypes;
        }
    }
}
