﻿namespace Crave.Enums
{
    public enum ExternalProductType
    {
        Product = 1,
        ModifierGroup = 2,
        Modifier = 3,
        Bundle = 4
    }
}
