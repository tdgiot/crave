﻿namespace Crave.Enums
{
    public enum ExternalSystemEnvironment
    {
        Live = 1,
        Test = 2
    }
}
