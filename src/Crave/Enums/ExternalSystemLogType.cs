﻿namespace Crave.Enums
{
    public enum ExternalSystemLogType
    {
        ChannelStatusUpdate = 1,
        MenuUpdate = 2,
        OrderStatusUpdate = 3,
        SnoozeProductsUpdate = 4,
        DispatchOrder = 5,
        BusyMode = 6
    }
}
