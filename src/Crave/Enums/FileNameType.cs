﻿namespace Crave.Enums
{
    public enum FileNameType
    {
        Cdn,

        Format,

        DeletionWildcard
    }
}
