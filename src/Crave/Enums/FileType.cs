﻿namespace Crave.Enums
{
    public enum FileType
    {
        Unknown = 0,

        Pdf,

        Word,

        Image,

        Video,

        Excel,

        Text,

        Powerpoint,

        Archive
    }
}
