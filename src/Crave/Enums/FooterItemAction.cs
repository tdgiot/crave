﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the action for a footer item.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum FooterItemAction
    {
        /// <summary>
        /// Turn the screen off.
        /// </summary>
        TurnScreenOff = 0,

        /// <summary>
        /// Displays the estimated delivery time.
        /// </summary>
        EstimatedDeliveryTime = 1
    }
}