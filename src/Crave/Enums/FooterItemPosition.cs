﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the position of a footer item.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum FooterItemPosition
    {
        /// <summary>
        /// Footer item is displayed on the left.
        /// </summary>
        Left = -1,

        /// <summary>
        /// Footer item is displayed in the center.
        /// </summary>
        Center = 0,

        /// <summary>
        /// Footer item is displayed on the right.
        /// </summary>
        Right = 1
    }
}
