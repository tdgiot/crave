﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a footer item.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum FooterItemType
    {
        /// <summary>
        /// An icon.
        /// </summary>
        Icon = 1,

        /// <summary>
        /// A button.
        /// </summary>
        Button = 2,

        // TextView = 3
    }
}