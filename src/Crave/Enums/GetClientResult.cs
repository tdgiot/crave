﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the result of the GetClient API call.
    /// </summary>
    public enum GetClientResult
    {
        NoClientConfigurationSetOnClientOrDeliverypointgroup = 201,
        ClientDoesNotExist = 202
    }
}
