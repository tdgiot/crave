﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the result of the GetCompanySynopsis API call.
    /// </summary>
    public enum GetCompanySynopsisResult
    {
        InvalidCredentials = 201
    }
}
