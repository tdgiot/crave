﻿using System;

namespace Crave.Enums
{
    /// <summary>
    /// Enum containg html input type attributes
    /// </summary>
    [Flags]
    public enum HtmlInputType
    {
        /// <summary>
        /// Defines a clickable button (mostly used with a JavaScript to activate a script)
        /// </summary>
        Button = 0,

        /// <summary>
        /// Defines a checkbox
        /// </summary>
        Checkbox = 1,

        /// <summary>
        /// Defines a color picker
        /// </summary>
        Color = 2,

        /// <summary>
        /// Defines a date control (year, month and day (no time))
        /// </summary>
        Date = 3,

        /// <summary>
        /// The input type datetime has been removed from the HTML standard. Use datetime-local instead.
        /// </summary>
        Datetime = 4,

        /// <summary>
        /// Defines a field for an e-mail address
        /// </summary>
        Email = 5,

        /// <summary>
        /// Defines a file-select field and a "Browse..." button (for file uploads)
        /// </summary>
        File = 6,

        /// <summary>
        /// Defines a hidden input field
        /// </summary>
        Hidden = 7,

        /// <summary>
        /// Defines an image as the submit button
        /// </summary>
        Image = 8,

        /// <summary>
        /// Defines a month and year control (no time zone)
        /// </summary>
        Month = 9,

        /// <summary>
        /// Defines a field for entering a number
        /// </summary>
        Number = 10,

        /// <summary>
        /// Defines a password field (characters are masked)
        /// </summary>
        Password = 11,

        /// <summary>
        /// Defines a radio button
        /// </summary>
        Radio = 12,

        /// <summary>
        /// Defines a control for entering a number whose exact value is not important (like a slider control)
        /// </summary>
        Range = 13,

        /// <summary>
        /// Defines a reset button (resets all form values to default values)
        /// </summary>
        Reset = 14,

        /// <summary>
        /// Defines a text field for entering a search string
        /// </summary>
        Search = 15,

        /// <summary>
        /// Defines a submit button
        /// </summary>
        Submit = 16,

        /// <summary>
        /// Defines a field for entering a telephone number
        /// </summary>
        Tel = 17,

        /// <summary>
        /// Defines a single-line text field (default width is 20 characters)
        /// </summary>
        Text = 18,

        /// <summary>
        /// Defines a control for entering a time (no time zone)
        /// </summary>
        Time = 19,

        /// <summary>
        /// Defines a field for entering a URL
        /// </summary>
        Url = 20,

        /// <summary>
        /// Defines a week and year control (no time zone)
        /// </summary>
        Week = 21
    }
}
