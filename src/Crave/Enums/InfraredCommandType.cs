﻿namespace Crave.Enums
{
    public enum InfraredCommandType
    {
        Power = 1,
        Mute = 2,
        ChannelUp = 3,
        ChannelDown = 4,
        VolumeUp = 5,
        VolumeDown = 6,
        CursorUp = 7,
        CursorRight = 8,
        CursorDown = 9,
        CursorLeft = 10,
        Enter = 11,
        Number0 = 12,
        Number1 = 13,
        Number2 = 14,
        Number3 = 15,
        Number4 = 16,
        Number5 = 17,
        Number6 = 18,
        Number7 = 19,
        Number8 = 20,
        Number9 = 21,
        Guide = 22,
        Dash = 23,
        CustomButton1 = 101,
        CustomButton2 = 102,
        CustomButton3 = 103,
        Custom = 99999
    }
}