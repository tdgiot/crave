﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the action for a media processing task.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum MediaProcessingTaskAction
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Upload a media item.
        /// </summary>
        Upload = 1,

        /// <summary>
        /// Delete a media item.
        /// </summary>
        Delete = 2
    }
}