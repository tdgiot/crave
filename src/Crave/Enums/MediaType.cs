﻿using System.ComponentModel;
using Crave.Attributes;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of media entity.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum MediaType : int
    {
        None = -1,

        /// <summary>
        /// Do not use. Default.
        /// </summary>
        NoMediaTypeSpecified = 0,

        /// <summary>
        /// Media type not specified for media type group device combination.
        /// </summary>
        MediaTypeNotSpecifiedForMediaTypeGroupDeviceCombination = 1,

        #region Otoucho - 1280x800 - OBSOLETE

        /// <summary>
        /// Big photo which is displayed when a user clicks the branding photo
        /// </summary>
        ProductPhoto = 100,

        /// <summary>
        /// Small photo which is displayed when product is displayed
        /// </summary>
        ProductBranding = 200,

        /// <summary>
        /// Button image of a product
        /// </summary>
        ProductButton = 300,

        /// <summary>
        /// Button image of a category
        /// </summary>
        CategoryButton = 400,

        /// <summary>
        /// Image of an advertisement
        /// </summary>
        Advertisement = 500,

        /// <summary>
        /// Big image of an advertisement
        /// </summary>
        AdvertisementPhoto = 600,

        /// <summary>
        /// Button image of an entertainment item
        /// </summary>
        EntertainmentButton = 700,

        /// <summary>
		/// Generic product button.
        /// </summary>
        [NonGenericMediaType(ProductButton)]
        GenericproductButton = 601,

        /// <summary>
		/// Generic product branding.
        /// </summary>
        [NonGenericMediaType(ProductBranding)]
        GenericproductBranding = 701,

        /// <summary>
        /// Eye-catcher of the age check view
        /// </summary>
        AgeCheck = 1000,

        /// <summary>
        /// Eye-catcher of the homepage view
        /// </summary>
        Homepage = 1100,

        /// <summary>
        /// Generic category button.
        /// </summary>
        [NonGenericMediaType(CategoryButton)]
        GenericcategoryButton = 1200,

        #endregion

        #region Archos - 800x480 - OBSOLETE

        /// <summary>
        /// Crave notification dialog
        /// </summary>
        NotificationDialog800x480 = 1300,

        /// <summary>
        /// Button image of a product
        /// </summary>
        ProductButton800x480 = 1400,

        /// <summary>
        /// Crave product button placeholder
        /// </summary>
        ProductButtonPlaceholder800x480 = 1401,

        /// <summary>
        /// Branding image of a product
        /// </summary>
        ProductBranding800x480 = 1500,

        /// <summary>
        /// Crave product button placeholder
        /// </summary>
        ProductBrandingPlaceholder800x480 = 1501,

        /// <summary>
        /// Generic product button.
        /// </summary>
        [NonGenericMediaType(ProductButton800x480)]
        GenericProductButton800x480 = 1700,

        /// <summary>
        /// Generic product branding.
        /// </summary>
        [NonGenericMediaType(ProductBranding800x480)]
        GenericProductBranding800x480 = 1800,

        /// <summary>
        /// Eye-catcher of the homepage view
        /// </summary>
        Homepage800x480 = 1900,

        /// <summary>
        /// Button image on an entertainment item
        /// </summary>
        EntertainmentButton800x480 = 2000,

        /// <summary>
        /// Button image on an entertainment item with out an image
        /// </summary>
        EntertainmentButtonPlaceholder800x480 = 2001,

        /// <summary>
        /// Branding image of a social medium
        /// </summary>
        SocialmediaBranding800x480 = 2100,

        /// <summary>
        /// Branding image of the customer data capture
        /// </summary>
        CustomerDataCaptureBranding800x480 = 2200,

        /// <summary>
        /// Photo of a category
        /// </summary>
        CategoryPhoto800x480 = 2300,

        /// <summary>
        /// Placeholder for category photo
        /// </summary>
        CategoryPhotoPlaceholder800x480 = 2301,

        /// <summary>
        /// Branding image of a category
        /// </summary>
        CategoryBranding800x480 = 2400,

        /// <summary>
        /// Homepage full screen
        /// </summary>
        HomepageFullscreen800x480 = 2500,

        /// <summary>
        /// Image next to an alteration
        /// </summary>
        AlterationImage800x480 = 2600,

        /// <summary>
        /// Branding image of a product on mobile devices
        /// </summary>
        CraveProductBrandingMobile = 2700,

        /// <summary>
        /// Image to show with the battery out of charge message
        /// </summary>
        OutOfCharge800x480 = 2800,

        /// <summary>
        /// Branding image of a generic product on mobile devices
        /// </summary>
        [NonGenericMediaType(CraveProductBrandingMobile)]
        CraveGenericProductBrandingMobile = 3000,

        /// <summary>
        /// Image to show with the order processed message
        /// </summary>
        OrderProcessed800x480 = 3100,

        /// <summary>
        /// Image to show with the service request processed message
        /// </summary>
        ServiceProcessed800x480 = 3200,

        /// <summary>
        /// Image to show with the rating processed message
        /// </summary>
        RatingProcessed800x480 = 3300,

        /// <summary>
        /// Image to show with the social media processed message
        /// </summary>
        SocialmediaProcessed800x480 = 3400,

        /// <summary>
        /// Image to show with the survey processed message
        /// </summary>
        SurveyProcessed800x480 = 3500,

        /// <summary>
        /// Image to show with the form processed message
        /// </summary>
        FormProcessed800x480 = 3600,

        /// <summary>
        /// Crave basket image
        /// </summary>
        Basket800x480 = 3700,

        /// <summary>
        /// Crave advertisement image
        /// </summary>
        Advertisement800x480 = 3800,

        /// <summary>
        /// Crave console logo
        /// </summary>
        CraveConsoleLogo = 3900,

        /// <summary>
        /// Crave Product Added
        /// </summary>
        ProductAdded800x480 = 4000,

        /// <summary>
        /// Advertisement half next to category (Crave)
        /// </summary>
        AdvertisementHalf800x480 = 6800,

        #endregion

        #region Samsung - 1280x800

        /// <summary>
        /// Branding image of a product
        /// </summary>
        ProductBranding1280x800 = 4100,

        /// <summary>
        /// Product branding placeholder
        /// </summary>
        ProductBrandingPlaceholder1280x800 = 4101,

        /// <summary>
        /// Product photo horizontal
        /// </summary>
        ProductPagePhotoHorizontal1280x800 = 4110,

        /// <summary>
        /// Product photo horizontal placeholder
        /// </summary>
        ProductPagePhotoHorizontalPlaceholder1280x800 = 4111,

        /// <summary>
        /// Product photo vertical
        /// </summary>
        ProductPagePhotoVertical1280x800 = 4120,

        /// <summary>
        /// Product photo vertical placeholder
        /// </summary>
        ProductPagePhotoVerticalPlaceholder1280x800 = 4121,

        /// <summary>
        /// Button image of a product
        /// </summary>
        ProductButton1280x800 = 4200,

        /// <summary>
        /// Product button placeholder
        /// </summary>
        ProductButtonPlaceholder1280x800 = 4201,

        /// <summary>
        /// Product icon
        /// </summary>
        ProductIcon1280x800 = 4210,

        /// <summary>
        /// Product icon placeholder
        /// </summary>
        ProductIconPlaceholder1280x800 = 4211,

        /// <summary>
        /// Generic product button.
        /// </summary>
        GenericProductButton1280x800 = 4300,

        /// <summary>
        /// Generic product icon
        /// </summary>
        GenericProductIcon1280x800 = 4310,

        /// <summary>
        /// Generic product branding.
        /// </summary>
        GenericProductBranding1280x800 = 4400,

        /// <summary>
        /// Generic product photo horizontal
        /// </summary>
        GenericProductPagePhotoHorizontal1280x800 = 4410,

        /// <summary>
        /// Generic product photo vertical
        /// </summary>        
        GenericProductPagePhotoVertical1280x800 = 4420,

        /// <summary>
        /// Eye-catcher of the homepage view
        /// </summary>
        Homepage1280x800 = 4500,

        /// <summary>
        /// Button image on an entertainment item
        /// </summary>
        EntertainmentButton1280x800 = 4600,

        /// <summary>
        /// Entertainment button placeholder
        /// </summary>
        EntertainmentButtonPlaceholder1280x800 = 4601,

        /// <summary>
        /// Branding image of a social medium
        /// </summary>
        SocialmediaBranding1280x800 = 4700,

        /// <summary>
        /// Branding image of the customer data capture
        /// </summary>
        CustomerDataCaptureBranding1280x800 = 4800,

        /// <summary>
        /// Photo of a category
        /// </summary>
        CategoryPhoto1280x800 = 4900,

        /// <summary>
        /// Photo of a horizontal category page
        /// </summary>
        CategoryPagePhotoHorizontal1280x800 = 4910,

        /// <summary>
        /// Photo of a vertical category page
        /// </summary>
        CategoryPagePhotoVertical1280x800 = 4920,

        /// <summary>
        /// Category photo placeholder
        /// </summary>
        CategoryPhotoPlaceholder1280x800 = 4901,

        /// <summary>
        /// Category page horizontal photo placeholder
        /// </summary>
        CategoryPagePhotoHorizontalPlaceholder1280x800 = 4911,

        /// <summary>
        /// Category page photo placeholder
        /// </summary>
        CategoryPagePhotoVerticalPlaceholder1280x800 = 4921,

        /// <summary>
        /// Category page photo full page
        /// </summary>
        CategoryPagePhotoFullPage1280x800 = 4930,

        /// <summary>
        /// Branding image of a category
        /// </summary>
        CategoryBranding1280x800 = 5000,

        /// <summary>
        /// Homepage full screen
        /// </summary>
        HomepageFullscreen1280x800 = 5100,

        /// <summary>
        /// Image to show with the battery out of charge message
        /// </summary>
        OutOfCharge1280x800 = 5200,

        /// <summary>
        /// Image to show with the order processed message
        /// </summary>
        OrderProcessed1280x800 = 5300,

        /// <summary>
        /// Image to show with the service request processed message
        /// </summary>
        ServiceProcessed1280x800 = 5400,

        /// <summary>
        /// Image to show with the rating processed message
        /// </summary>
        RatingProcessed1280x800 = 5500,

        /// <summary>
        /// Image to show with the social media processed message
        /// </summary>
        SocialmediaProcessed1280x800 = 5600,

        /// <summary>
        /// Image to show with the survey processed message
        /// </summary>
        SurveyProcessed1280x800 = 5700,

        /// <summary>
        /// Image to show with the form processed message
        /// </summary>
        FormProcessed1280x800 = 5800,

        /// <summary>
        /// Basket image
        /// </summary>
        Basket1280x800 = 5900,

        /// <summary>
        /// Advertisement image
        /// </summary>
        Advertisement1280x800 = 6000,

        /// <summary>
        /// Product Added Small
        /// </summary>
        ProductAddedSmall1280x800 = 6099,

        /// <summary>
        /// Product Added
        /// </summary>
        ProductAdded1280x800 = 6100,

        /// <summary>
        /// Notification dialog
        /// </summary>
        NotificationDialog1280x800 = 6200,

        /// <summary>
        /// Image next to an alteration
        /// </summary>
        AlterationImage1280x800 = 6300,

        /// <summary>
        /// Image on top of printed receipts
        /// </summary>
        CraveReceiptLogo = 6400,

        /// <summary>
        /// Image in the company image gallery
        /// </summary>
        Gallery = 6500,

        /// <summary>
        /// Image advertisements front page e-menu
        /// </summary>
        AdvertisementSmall1280x800 = 6600,

        /// <summary>
        /// Advertisement half next to category (1280x800)
        /// </summary>
        AdvertisementHalf1280x800 = 6700,

        /// <summary>
        /// Survey page bottom (1280x800)
        /// </summary>
        SurveyPageBottom1280x800 = 6900,

        /// <summary>
        /// Survey page right (1280x800)
        /// </summary>
        SurveyPageRight1280x800 = 7000,

        /// <summary>
        /// Survey page logo (1280x800)
        /// </summary>
        SurveyPageLogo1280x800 = 7100,

        /// <summary>
        /// Survey page right half (1280x800)
        /// </summary>
        SurveyPageRightHalf1280x800 = 7200,

        /// <summary>
        /// Charger removed dialog (1280x800)
        /// </summary>
        ChargerRemovedDialog1280x800 = 7300,

        /// <summary>
        /// Charger removed reminder dialog (1280x800)
        /// </summary>
        ChargerRemovedReminderDialog1280x800 = 7400,

        /// <summary>
        /// Clear basket dialog
        /// </summary>
        ClearBasketDialog1280x800 = 7401,

        /// <summary>
        /// Survey answer required (1280x800)
        /// </summary>
        SurveyAnswerRequired1280x800 = 7500,

        /// <summary>
        /// PMS Message Dialog (1280x800)
        /// </summary>
        PmsMessage1280x800 = 7600,

        /// <summary>
        /// PMS Bill logo (1280x800)
        /// </summary>
        PmsBill1280x800 = 7700,

        /// <summary>
        /// PMS Checkout approval (1280x800)
        /// </summary>
        PmsCheckoutApproval1280x800 = 7800,

        /// <summary>
        /// Alarm Set While Not Charging (1280x800)
        /// </summary>
        AlarmSetWhileNotCharging1280x800 = 7801,

        SitePageIcon1280x800 = 7900,
        SiteFullPageImage1280x800 = 7910,
        SiteFullPageImageDirectory1280x800 = 7911,
        SiteTwoThirdsPageImage1280x800 = 7920,
        SiteTwoThirdsPageImageDirectory1280x800 = 7921,
        SiteHdWideScreen1280x800 = 7930,
        SiteCinemaWideScreen1280x800 = 7940,
        SitePointOfInterestSummaryPage1280x800 = 7950,
        SiteCompanySummaryPage1280x800 = 7951,
        SitePointOfInterestSummaryPageMicrosite1280x800 = 7952,
        SiteCompanySummaryPageMicrosite1280x800 = 7953,
        SiteBranding1280x800 = 7954,

        TemplateSitePageIcon1280x800 = 8000,
        TemplateSiteFullPageImage1280x800 = 8010,
        TemplateSiteFullPageImageDirectory1280x800 = 8011,
        TemplateSiteTwoThirdsPageImage1280x800 = 8020,
        TemplateSiteTwoThirdsPageImageDirectory1280x800 = 8021,

        // Widget media types
        AdvertisementWidgetSmall1280x800 = 8101,
        AdvertisementWidgetMedium1280x800 = 8102,
        WidgetIconSmall1280x800 = 8103,
        WidgetIconMedium1280x800 = 8104,
        Widget1x1Background1280x800 = 8105,
        Widget1x2Background1280x800 = 8106,
        Widget2x1Background1280x800 = 8107,
        Widget2x2Background1280x800 = 8108,
        AdvertisementWidgetSmallFull1280x800 = 8109,
        AdvertisementWidgetMediumFull1280x800 = 8110,
        AdvertisementWidget1x1Full1280x800 = 8111,
        WidgetPdf1280x800 = 8112,
        WidgetAvailability1280x800 = 8113,
        Widget2x4Background1280x800 = 8114,

        // App customizations
        ScreenBackground1280x800 = 8200,
        FooterLogo1280x800 = 8201,
        FooterBrightnessIcon1280x800 = 8202,
        FooterWarningIcon1280x800 = 8203,
        FooterPendingOrdersIcon1280x800 = 8204,
        FooterGuestInformationIcon1280x800 = 8205,
        FooterCustomIcon1280x800 = 8206,
        ScreensaverBackground1280x800 = 8207,

        RoomControlSectionIcon1280x800 = 8300,
        RoomControlSectionItemIcon1280x800 = 8301,
        RoomControlStation1280x800 = 8302,

        AlterationDialogPage1280x800 = 8303,
        AlterationDialogColumn1280x800 = 8304,

        BrowserAgeVerificationImageSmall1280x800 = 8305,
        BrowserAgeVerificationBackground1280x800 = 8306,
        BrowserAgeVerificationImageBig1280x800 = 8307,

        RestartApplicationDialog1280x800 = 8308,
        RebootDeviceDialog1280x800 = 8309,

        CategoryConfirmation1280x800 = 8310,
        ProductConfirmation1280x800 = 8311,
        CategoryProcessed1280x800 = 8312,
        ProductProcessed1280x800 = 8313,

        GallerySmall = 8399,
        GalleryLarge = 8400,

        ProductDialogAlterationColumn1280x800 = 8500,
        ProductDialogProductgroupColumn1280x800 = 8501,

        SiteMenuHeader1280x800 = 8600,
        SiteMenuIcon1280x800 = 8601,
        SitePageFull1280x800 = 8602,
        SitePageTwoThirds1280x800 = 8603,
        SitePageTwoThirdsPointOfInterest1280x800 = 8604,
        SitePageTwoThirdsCompany1280x800 = 8605,

        MapMenuItem1280x800 = 8606,
        MapMarkerPointOfInterest1280x800 = 8607,
        MapMarkerCompany1280x800 = 8608,

        QrCodeLogo1280x800 = 8609,

        #endregion

        #region Android Smart Tv Box - 1280x720 - OBSOLETE

        /// <summary>
        /// Branding image of a product
        /// </summary>
        ProductBranding1280x720 = 10100,

        /// <summary>
        /// Product branding placeholder
        /// </summary>
        ProductBrandingPlaceholder1280x720 = 10101,

        /// <summary>
        /// Button image of a product
        /// </summary>
        ProductButton1280x720 = 10200,

        /// <summary>
        /// Product button placeholder
        /// </summary>
        ProductButtonPlaceholder1280x720 = 10201,

        /// <summary>
        /// Basisproduct knop
        /// </summary>
        [NonGenericMediaType(ProductButton1280x720)]
        GenericProductButton1280x720 = 10300,

        /// <summary>
        /// Basisproduct branding
        /// </summary>
        [NonGenericMediaType(ProductBranding1280x720)]
        GenericProductBranding1280x720 = 10400,

        /// <summary>
        /// Eye-catcher of the homepage view
        /// </summary>
        Homepage1280x720 = 10500,

        /// <summary>
        /// Button image on an entertainment item
        /// </summary>
        EntertainmentButton1280x720 = 10600,

        /// <summary>
        /// Entertainment button placeholder
        /// </summary>
        EntertainmentButtonPlaceholder1280x720 = 10601,

        /// <summary>
        /// Branding image of a social medium
        /// </summary>
        SocialmediaBranding1280x720 = 10700,

        /// <summary>
        /// Photo of a category
        /// </summary>
        CategoryPhoto1280x720 = 10900,

        /// <summary>
        /// Category photo placeholder
        /// </summary>
        CategoryPhotoPlaceholder1280x720 = 10901,

        /// <summary>
        /// Branding image of a category
        /// </summary>
        CategoryBranding1280x720 = 11000,

        /// <summary>
        /// Image to show with the battery out of charge message
        /// </summary>
        OutOfCharge1280x720 = 11200,

        /// <summary>
        /// Image to show with the order processed message
        /// </summary>
        OrderProcessed1280x720 = 11300,

        /// <summary>
        /// Image to show with the service request processed message
        /// </summary>
        ServiceProcessed1280x720 = 11400,

        /// <summary>
        /// Image to show with the rating processed message
        /// </summary>
        RatingProcessed1280x720 = 11500,

        /// <summary>
        /// Image to show with the social media processed message
        /// </summary>
        SocialmediaProcessed1280x720 = 11600,

        /// <summary>
        /// Image to show with the survey processed message
        /// </summary>
        SurveyProcessed1280x720 = 11700,

        /// <summary>
        /// Image to show with the form processed message
        /// </summary>
        FormProcessed1280x720 = 11800,

        /// <summary>
        /// Basket image
        /// </summary>
        Basket1280x720 = 11900,

        /// <summary>
        /// Advertisement image
        /// </summary>
        Advertisement1280x720 = 12000,

        /// <summary>
        /// Product Added
        /// </summary>
        ProductAdded1280x720 = 12100,

        /// <summary>
        /// Notification dialog
        /// </summary>
        NotificationDialog1280x720 = 12200,

        /// <summary>
        /// Image next to an alteration
        /// </summary>
        AlterationImage1280x720 = 12300,

        /// <summary>
        /// Image advertisements frontpage emenu
        /// </summary>
        AdvertisementSmall1280x720 = 12600,

        /// <summary>
        /// Advertisement half next to category (1280x720)
        /// </summary>
        AdvertisementHalf1280x720 = 12700,

        /// <summary>
        /// Survey page bottom (1280x720)
        /// </summary>
        SurveyPageBottom1280x720 = 12900,

        /// <summary>
        /// Survey page right (1280x720)
        /// </summary>
        SurveyPageRight1280x720 = 13000,

        /// <summary>
        /// Survey page logo (1280x720)
        /// </summary>
        SurveyPageLogo1280x720 = 13100,

        /// <summary>
        /// Survey page right half (1280x720)
        /// </summary>
        SurveyPageRightHalf1280x720 = 13200,

        /// <summary>
        /// Survey answer required (1280x720)
        /// </summary>
        SurveyAnswerRequired1280x720 = 13300,

        /// <summary>
        /// PMS Message Dialog (1280x720)
        /// </summary>
        PmsMessage1280x720 = 13400,

        /// <summary>
        /// PMS Bill logo (1280x720)
        /// </summary>
        PmsBill1280x720 = 13500,

        /// <summary>
        /// PMS Checkout approval (1280x720)
        /// </summary>
        PmsCheckoutApproval1280x720 = 13600,

        /// <summary>
        /// Homepage Fullscreen (1280x720)
        /// </summary>
        HomepageFullscreen1280x720 = 13700,

        SitePageIcon1280x720 = 13800,
        SiteFullPageImage1280x720 = 13810,
        SiteFullPageImageDirectory1280x720 = 13811,
        SiteTwoThirdsPageImage1280x720 = 13820,
        SiteTwoThirdsPageImageDirectory1280x720 = 13821,
        SiteHdWideScreen1280x720 = 13830,
        SiteCinemaWideScreen1280x720 = 13840,
        SitePointOfInterestSummaryPage1280x720 = 13850,
        SiteCompanySummaryPage1280x720 = 13851,
        SiteCompanySummaryPageMicrosite1280x720 = 13852,
        SitePointOfInterestSummaryPageMicrosite1280x720 = 13853,

        /*SiteBranding1280x720 = 13854,*/

        [NonGenericMediaType(SitePageIcon1280x720)]
        TemplateSitePageIcon1280x720 = 13900,

        [NonGenericMediaType(SiteFullPageImage1280x720)]
        TemplateSiteFullPageImage1280x720 = 13910,

        [NonGenericMediaType(SiteFullPageImageDirectory1280x720)]
        TemplateSiteFullPageImageDirectory1280x720 = 13911,

        [NonGenericMediaType(SiteTwoThirdsPageImage1280x720)]
        TemplateSiteTwoThirdsPageImage1280x720 = 13920,

        [NonGenericMediaType(SiteTwoThirdsPageImageDirectory1280x720)]
        TemplateSiteTwoThirdsPageImageDirectory1280x720 = 13921,

        #endregion

        #region Small (phone)

        /// <summary>
        /// Product button small (phone)
        /// </summary>
        ProductButtonPhoneSmall = 1011100,

        /// <summary>
        /// Generic product button small (phone)
        /// </summary>
        [NonGenericMediaType(ProductButtonPhoneSmall)]
        GenericProductButtonPhoneSmall = 1011101,

        /// <summary>
        /// Product button placeholder small (phone)
        /// </summary>
        ProductButtonPlaceholderPhoneSmall = 1011102,

        /// <summary>
        /// Product branding small (phone)
        /// </summary>
        ProductBrandingPhoneSmall = 1011200,

        /// <summary>
        /// Generic product branding small (phone)
        /// </summary>
        [NonGenericMediaType(ProductBrandingPhoneSmall)]
        GenericProductBrandingPhoneSmall = 1011201,

        /// <summary>
        /// Product branding placeholder small (phone)
        /// </summary>
        ProductBrandingPlaceholderPhoneSmall = 1011202,

        /// <summary>
        /// Category button small (phone)
        /// </summary>
        CategoryButtonPhoneSmall = 1011300,

        /// <summary>
        /// Generic category button small (phone)
        /// </summary>
        [NonGenericMediaType(CategoryButtonPhoneSmall)]
        GenericCategoryButtonPhoneSmall = 1011301,

        /// <summary>
        /// Category button placeholder small (phone)
        /// </summary>
        CategoryButtonPlaceholderPhoneSmall = 1011302,

        /// <summary>
        /// Category branding small (phone)
        /// </summary>
        CategoryBrandingPhoneSmall = 1011400,

        /// <summary>
        /// Generic category branding small (phone)
        /// </summary>
        [NonGenericMediaType(CategoryBrandingPhoneSmall)]
        GenericCategoryBrandingPhoneSmall = 1011401,

        /// <summary>
        /// Category branding placeholder small (phone)
        /// </summary>
        CategoryBrandingPlaceholderPhoneSmall = 1011402,

        /// <summary>
        /// Company list small (phone)
        /// </summary>
        CompanyListPhoneSmall = 1011500,

        /// <summary>
        /// Homepage small (phone)
        /// </summary>
        HomepagePhoneSmall = 1011600,

        /// <summary>
        /// PointOfInterest list small (phone)
        /// </summary>
        PointOfInterestListPhoneSmall = 1011700,

        /// <summary>
        /// PointOfInterest homepage small (phone)
        /// </summary>
        PointOfInterestHomepagePhoneSmall = 1011800,

        /// <summary>
        /// Map icon small (phone)
        /// </summary>
        MapIconPhoneSmall = 1011900,

        /// <summary>
        /// PointOfInterest map icon small (phone)
        /// </summary>
        PointOfInterestMapIconPhoneSmall = 1012000,

        SitePageIconPhoneSmall = 1011905,

        /*
        //SiteFullPageImagePhoneSmall = 1011910,
        //SiteTwoThirdsPageImagePhoneSmall = 1011920,
        //SiteHdWideScreenPhoneSmall = 1011930,
        //SiteCinemaWideScreenPhoneSmall = 1011940,
        */

        SiteMobileHeaderImagePhoneSmall = 1011950,
        SitePointOfInterestSummaryPagePhoneSmall = 1011955,
        SiteCompanySummaryPagePhoneSmall = 1011956,
        SiteBrandingPhoneSmall = 1011957,

        [NonGenericMediaType(SitePageIconPhoneSmall)]
        TemplateSitePageIconPhoneSmall = 1012005,

        [NonGenericMediaType(SiteMobileHeaderImagePhoneSmall)]
        TemplateSiteMobileHeaderImagePhoneSmall = 1012050,

        #endregion

        #region Normal (phone)

        /// <summary>
        /// Product button normal (phone)
        /// </summary>
        ProductButtonPhoneNormal = 1012100,

        /// <summary>
        /// Generic product button normal (phone)
        /// </summary>
        [NonGenericMediaType(ProductButtonPhoneNormal)]
        GenericProductButtonPhoneNormal = 1012101,

        /// <summary>
        /// Product button placeholder normal (phone)
        /// </summary>
        ProductButtonPlaceholderPhoneNormal = 1012102,

        /// <summary>
        /// Product branding normal (phone)
        /// </summary>
        ProductBrandingPhoneNormal = 1012200,

        /// <summary>
        /// Generic product branding normal (phone)
        /// </summary>
        [NonGenericMediaType(ProductBrandingPhoneNormal)]
        GenericProductBrandingPhoneNormal = 1012201,

        /// <summary>
        /// Product branding placeholder normal (phone)
        /// </summary>
        ProductBrandingPlaceholderPhoneNormal = 1012202,

        /// <summary>
        /// Category button normal (phone)
        /// </summary>
        CategoryButtonPhoneNormal = 1012300,

        /// <summary>
        /// Generic category button normal (phone)
        /// </summary>
        [NonGenericMediaType(CategoryButtonPhoneNormal)]
        GenericCategoryButtonPhoneNormal = 1012301,

        /// <summary>
        /// Category button placeholder normal (phone)
        /// </summary>
        CategoryButtonPlaceholderPhoneNormal = 1012302,

        /// <summary>
        /// Category branding normal (phone)
        /// </summary>
        CategoryBrandingPhoneNormal = 1012400,

        /// <summary>
        /// Generic category branding normal (phone)
        /// </summary>
        GenericCategoryBrandingPhoneNormal = 1012401,

        /// <summary>
        /// Category branding placeholder normal (phone)
        /// </summary>
        CategoryBrandingPlaceholderPhoneNormal = 1012402,

        /// <summary>
        /// Company list normal (phone)
        /// </summary>
        CompanyListPhoneNormal = 1012500,

        /// <summary>
        /// Homepage normal (phone)
        /// </summary>
        HomepagePhoneNormal = 1012600,

        /// <summary>
        /// PointOfInterest list normal (phone)
        /// </summary>
        PointOfInterestListPhoneNormal = 1012700,

        /// <summary>
        /// PointOfInterest homepage normal (phone)
        /// </summary>
        PointOfInterestHomepagePhoneNormal = 1012800,

        /// <summary>
        /// Map icon normal (phone)
        /// </summary>
        MapIconPhoneNormal = 1012900,

        SitePageIconPhoneNormal = 1012905,

        /*
        //SiteFullPageImagePhoneNormal = 1012910,
        //SiteTwoThirdsPageImagePhoneNormal = 1012920,
        //SiteHdWideScreenPhoneNormal = 1012930,
        //SiteCinemaWideScreenPhoneNormal = 1012940,
        */

        SiteMobileHeaderImagePhoneNormal = 1012950,
        SitePointOfInterestSummaryPagePhoneNormal = 1012955,
        SiteCompanySummaryPagePhoneNormal = 1012956,
        SiteBrandingPhoneNormal = 1012957,

        [NonGenericMediaType(SitePageIconPhoneNormal)]
        TemplateSitePageIconPhoneNormal = 1013005,

        [NonGenericMediaType(SiteMobileHeaderImagePhoneNormal)]
        TemplateSiteMobileHeaderImagePhoneNormal = 1013050,

        NotificationIconNormal = 1012990,

        /// <summary>
        /// PointOfInterest map icon normal (phone)
        /// </summary>
        PointOfInterestMapIconPhoneNormal = 1013000,

        #endregion

        #region Large (phone)

        /// <summary>
        /// Product button large (phone)
        /// </summary>
        ProductButtonPhoneLarge = 1013100,

        /// <summary>
        /// Generic product button large (phone)
        /// </summary>
        [NonGenericMediaType(ProductButtonPhoneLarge)]
        GenericProductButtonPhoneLarge = 1013101,

        /// <summary>
        /// Product button placeholder large (phone)
        /// </summary>
        ProductButtonPlaceholderPhoneLarge = 1013102,

        /// <summary>
        /// Product branding large (phone)
        /// </summary>
        ProductBrandingPhoneLarge = 1013200,

        /// <summary>
        /// Generic product branding large (phone)
        /// </summary>
        [NonGenericMediaType(ProductBrandingPhoneLarge)]
        GenericProductBrandingPhoneLarge = 1013201,

        /// <summary>
        /// Product branding placeholder large (phone)
        /// </summary>
        ProductBrandingPlaceholderPhoneLarge = 1013202,

        /// <summary>
        /// Category button large (phone)
        /// </summary>
        CategoryButtonPhoneLarge = 1013300,

        /// <summary>
        /// Generic category button large (phone)
        /// </summary>
        [NonGenericMediaType(CategoryButtonPhoneLarge)]
        GenericCategoryButtonPhoneLarge = 1013301,

        /// <summary>
        /// Category button placeholder large (phone)
        /// </summary>
        CategoryButtonPlaceholderPhoneLarge = 1013302,

        /// <summary>
        /// Category branding large (phone)
        /// </summary>
        CategoryBrandingPhoneLarge = 1013400,

        /// <summary>
        /// Generic category branding large (phone)
        /// </summary>
        [NonGenericMediaType(CategoryBrandingPhoneLarge)]
        GenericCategoryBrandingPhoneLarge = 1013401,

        /// <summary>
        /// Category branding placeholder large (phone)
        /// </summary>
        CategoryBrandingPlaceholderPhoneLarge = 1013402,

        /// <summary>
        /// Company list large (phone)
        /// </summary>
        CompanyListPhoneLarge = 1013500,

        /// <summary>
        /// Homepage large (phone)
        /// </summary>
        HomepagePhoneLarge = 1013600,

        /// <summary>
        /// PointOfInterest list large (phone)
        /// </summary>
        PointOfInterestListPhoneLarge = 1013700,

        /// <summary>
        /// PointOfInterest homepage large (phone)
        /// </summary>
        PointOfInterestHomepagePhoneLarge = 1013800,

        /// <summary>
        /// Map icon large (phone)
        /// </summary>
        MapIconPhoneLarge = 1013900,

        /// <summary>
        /// PointOfInterest map icon large (phone)
        /// </summary>
        PointOfInterestMapIconPhoneLarge = 1014000,

        SitePageIconPhoneLarge = 1013905,

        /*
        //SiteFullPageImagePhoneLarge = 1013910,
        //SiteTwoThirdsPageImagePhoneLarge = 1013920,
        //SiteHdWideScreenPhoneLarge = 1013930,
        //SiteCinemaWideScreenPhoneLarge = 1013940,\
        */

        SiteMobileHeaderImagePhoneLarge = 1013950,
        SitePointOfInterestSummaryPagePhoneLarge = 1013955,
        SiteCompanySummaryPagePhoneLarge = 1013956,
        SiteBrandingPhoneLarge = 1013957,

        [NonGenericMediaType(SitePageIconPhoneLarge)]
        TemplateSitePageIconPhoneLarge = 1014005,

        [NonGenericMediaType(SiteMobileHeaderImagePhoneLarge)]
        TemplateSiteMobileHeaderImagePhoneLarge = 1014050,

        #endregion

        #region XLarge (phone)

        /// <summary>
        /// Product button xlarge (phone)
        /// </summary>
        ProductButtonPhoneXLarge = 1014100,

        /// <summary>
        /// Generic product button xlarge (phone)
        /// </summary>
        [NonGenericMediaType(ProductButtonPhoneXLarge)]
        GenericProductButtonPhoneXLarge = 1014101,

        /// <summary>
        /// Product button placeholder xlarge (phone)
        /// </summary>
        ProductButtonPlaceholderPhoneXLarge = 1014102,

        /// <summary>
        /// Product branding xlarge (phone)
        /// </summary>
        ProductBrandingPhoneXLarge = 1014200,

        /// <summary>
        /// Generic product branding xlarge (phone)
        /// </summary>
        [NonGenericMediaType(ProductBrandingPhoneXLarge)]
        GenericProductBrandingPhoneXLarge = 1014201,

        /// <summary>
        /// Product branding placeholder xlarge (phone)
        /// </summary>
        ProductBrandingPlaceholderPhoneXLarge = 1014202,

        /// <summary>
        /// Category button xlarge (phone)
        /// </summary>
        CategoryButtonPhoneXLarge = 1014300,

        /// <summary>
        /// Generic category button xlarge (phone)
        /// </summary>
        [NonGenericMediaType(CategoryButtonPhoneXLarge)]
        GenericCategoryButtonPhoneXLarge = 1014301,

        /// <summary>
        /// Category button placeholder xlarge (phone)
        /// </summary>
        CategoryButtonPlaceholderPhoneXLarge = 1014302,

        /// <summary>
        /// Category branding xlarge (phone)
        /// </summary>
        CategoryBrandingPhoneXLarge = 1014400,

        /// <summary>
        /// Generic category branding xlarge (phone)
        /// </summary>
        [NonGenericMediaType(CategoryBrandingPhoneXLarge)]
        GenericCategoryBrandingPhoneXLarge = 1014401,

        /// <summary>
        /// Category branding placeholder xlarge (phone)
        /// </summary>
        CategoryBrandingPlaceholderPhoneXLarge = 1014402,

        /// <summary>
        /// Company list xlarge (phone)
        /// </summary>
        CompanyListPhoneXLarge = 1014500,

        /// <summary>
        /// Homepage xlarge (phone)
        /// </summary>
        HomepagePhoneXLarge = 1014600,

        /// <summary>
        /// PointOfInterest list xlarge (phone)
        /// </summary>
        PointOfInterestListPhoneXLarge = 1014700,

        /// <summary>
        /// PointOfInterest homepage xlarge (phone)
        /// </summary>
        PointOfInterestHomepagePhoneXLarge = 1014800,

        /// <summary>
        /// Map icon xlarge (phone)
        /// </summary>
        MapIconPhoneXLarge = 1014900,

        /// <summary>
        /// PointOfInterest map icon xlarge (phone)
        /// </summary>
        PointOfInterestMapIconPhoneXLarge = 1015000,

        SitePageIconPhoneXLarge = 1014905,

        /*
        //SiteFullPageImagePhoneXLarge = 1014910,
        //SiteTwoThirdsPageImagePhoneXLarge = 1014920,
        //SiteHdWideScreenPhoneXLarge = 1014930,
        //SiteCinemaWideScreenPhoneXLarge = 1014940,
        */

        SiteMobileHeaderImagePhoneXLarge = 1014950,
        SitePointOfInterestSummaryPagePhoneXLarge = 1014955,
        SiteCompanySummaryPagePhoneXLarge = 1014956,
        SiteBrandingPhoneXLarge = 1014957,

        [NonGenericMediaType(SitePageIconPhoneXLarge)]
        TemplateSitePageIconPhoneXLarge = 1015005,

        [NonGenericMediaType(SiteMobileHeaderImagePhoneXLarge)]
        TemplateSiteMobileHeaderImagePhoneXLarge = 1015050,

        #endregion

        #region Small (tablet)

        /// <summary>
        /// Product button small (tablet)
        /// </summary>
        ProductButtonTabletSmall = 2011100,

        /// <summary>
        /// Generic product button small (tablet)
        /// </summary>
        [NonGenericMediaType(ProductButtonTabletSmall)]
        GenericProductButtonTabletSmall = 2011101,

        /// <summary>
        /// Product button placeholder small (tablet)
        /// </summary>
        ProductButtonPlaceholderTabletSmall = 2011102,

        /// <summary>
        /// Product branding small (tablet)
        /// </summary>
        ProductBrandingTabletSmall = 2011200,

        /// <summary>
        /// Generic product branding small (tablet)
        /// </summary>
        [NonGenericMediaType(ProductBrandingTabletSmall)]
        GenericProductBrandingTabletSmall = 2011201,

        /// <summary>
        /// Product branding placeholder small (tablet)
        /// </summary>
        ProductBrandingPlaceholderTabletSmall = 2011202,

        /// <summary>
        /// Category button small (tablet)
        /// </summary>
        CategoryButtonTabletSmall = 2011300,

        /// <summary>
        /// Generic category button small (tablet)
        /// </summary>
        [NonGenericMediaType(CategoryButtonTabletSmall)]
        GenericCategoryButtonTabletSmall = 2011301,

        /// <summary>
        /// Category button placeholder small (tablet)
        /// </summary>
        CategoryButtonPlaceholderTabletSmall = 2011302,

        /// <summary>
        /// Category branding small (tablet)
        /// </summary>
        CategoryBrandingTabletSmall = 2011400,

        /// <summary>
        /// Generic category branding small (tablet)
        /// </summary>
        [NonGenericMediaType(CategoryBrandingTabletSmall)]
        GenericCategoryBrandingTabletSmall = 2011401,

        /// <summary>
        /// Category branding placeholder small (tablet)
        /// </summary>
        CategoryBrandingPlaceholderTabletSmall = 2011402,

        /// <summary>
        /// Company list small (tablet)
        /// </summary>
        CompanyListTabletSmall = 2011500,

        /// <summary>
        /// Homepage small (tablet)
        /// </summary>
        HomepageTabletSmall = 2011600,

        /// <summary>
        /// PointOfInterest list small (tablet)
        /// </summary>
        PointOfInterestListTabletSmall = 2011700,

        /// <summary>
        /// PointOfInterest homepage small (tablet)
        /// </summary>
        PointOfInterestHomepageTabletSmall = 2011800,

        /// <summary>
        /// Map icon small (tablet)
        /// </summary>
        MapIconTabletSmall = 2011900,

        /// <summary>
        /// PointOfInterest map icon small (tablet)
        /// </summary>
        PointOfInterestMapIconTabletSmall = 2012000, // GK wrong sequence

        SitePageIconTabletSmall = 2011910,
        SiteFullPageImageTabletSmall = 2011915,
        SiteTwoThirdsPageImageTabletSmall = 2011920,
        SiteHdWideScreenTabletSmall = 2011925,
        SiteCinemaWideScreenTabletSmall = 2011930,
        SiteMobileHeaderImageTabletSmall = 2011935,
        SitePointOfInterestSummaryPageTabletSmall = 2011955,
        SiteCompanySummaryPageTabletSmall = 2011956,
        SiteBrandingTabletSmall = 2011957,

        [NonGenericMediaType(SitePageIconTabletSmall)]
        TemplateSitePageIconTabletSmall = 2012010,

        #endregion

        #region Normal (tablet)

        /// <summary>
        /// Product button normal (tablet)
        /// </summary>
        ProductButtonTabletNormal = 2012100,

        /// <summary>
        /// Generic product button normal (tablet)
        /// </summary>
        [NonGenericMediaType(ProductButtonTabletNormal)]
        GenericProductButtonTabletNormal = 2012101,

        /// <summary>
        /// Product button placeholder normal (tablet)
        /// </summary>
        ProductButtonPlaceholderTabletNormal = 2012102,

        /// <summary>
        /// Product branding normal (tablet)
        /// </summary>
        ProductBrandingTabletNormal = 2012200,

        /// <summary>
        /// Generic product branding normal (tablet)
        /// </summary>
        [NonGenericMediaType(ProductBrandingTabletNormal)]
        GenericProductBrandingTabletNormal = 2012201,

        /// <summary>
        /// Product branding placeholder normal (tablet)
        /// </summary>
        ProductBrandingPlaceholderTabletNormal = 2012202,

        /// <summary>
        /// Category button normal (tablet)
        /// </summary>
        CategoryButtonTabletNormal = 2012300,

        /// <summary>
        /// Generic category button normal (tablet)
        /// </summary>
        [NonGenericMediaType(CategoryButtonTabletNormal)]
        GenericCategoryButtonTabletNormal = 2012301,

        /// <summary>
        /// Category button placeholder normal (tablet)
        /// </summary>
        CategoryButtonPlaceholderTabletNormal = 2012302,

        /// <summary>
        /// Category branding normal (tablet)
        /// </summary>
        CategoryBrandingTabletNormal = 2012400,

        /// <summary>
        /// Generic category branding normal (tablet)
        /// </summary>
        [NonGenericMediaType(CategoryBrandingTabletNormal)]
        GenericCategoryBrandingTabletNormal = 2012401,

        /// <summary>
        /// Category branding placeholder normal (tablet)
        /// </summary>
        CategoryBrandingPlaceholderTabletNormal = 2012402,

        /// <summary>
        /// Company list normal (tablet)
        /// </summary>
        CompanyListTabletNormal = 2012500,

        /// <summary>
        /// Homepage normal (tablet)
        /// </summary>
        HomepageTabletNormal = 2012600,

        /// <summary>
        /// PointOfInterest list normal (tablet)
        /// </summary>
        PointOfInterestListTabletNormal = 2012700,

        /// <summary>
        /// PointOfInterest homepage normal (tablet)
        /// </summary>
        PointOfInterestHomepageTabletNormal = 2012800,

        /// <summary>
        /// Map icon normal (tablet)
        /// </summary>
        MapIconTabletNormal = 2012900,

        /// <summary>
        /// PointOfInterest map icon normal (tablet)
        /// </summary>
        PointOfInterestMapIconTabletNormal = 2013000, // gk wrong sequence

        SitePageIconTabletNormal = 2012910,
        SiteFullPageImageTabletNormal = 2012915,
        SiteTwoThirdsPageImageTabletNormal = 2012920,
        SiteHdWideScreenTabletNormal = 2012925,
        SiteCinemaWideScreenTabletNormal = 2012930,
        SiteMobileHeaderImageTabletNormal = 2012935,
        SitePointOfInterestSummaryPageTabletNormal = 2012955,
        SiteCompanySummaryPageTabletNormal = 2012956,
        SiteBrandingTabletNormal = 2012957,

        [NonGenericMediaType(SitePageIconTabletNormal)]
        TemplateSitePageIconTabletNormal = 2013010,

        #endregion

        #region Large (tablet)

        /// <summary>
        /// Product button large (tablet)
        /// </summary>
        ProductButtonTabletLarge = 2013100,

        /// <summary>
        /// Generic product button large (tablet)
        /// </summary>
        [NonGenericMediaType(ProductButtonTabletLarge)]
        GenericProductButtonTabletLarge = 2013101,

        /// <summary>
        /// Product button placeholder large (tablet)
        /// </summary>
        ProductButtonPlaceholderTabletLarge = 2013102,

        /// <summary>
        /// Product branding large (tablet)
        /// </summary>
        ProductBrandingTabletLarge = 2013200,

        /// <summary>
        /// Generic product branding large (tablet)
        /// </summary>
        [NonGenericMediaType(ProductBrandingTabletLarge)]
        GenericProductBrandingTabletLarge = 2013201,

        /// <summary>
        /// Product branding placeholder large (tablet)
        /// </summary>
        ProductBrandingPlaceholderTabletLarge = 2013202,

        /// <summary>
        /// Category button large (tablet)
        /// </summary>
        CategoryButtonTabletLarge = 2013300,

        /// <summary>
        /// Generic category button large (tablet)
        /// </summary>
        [NonGenericMediaType(CategoryButtonTabletLarge)]
        GenericCategoryButtonTabletLarge = 2013301,

        /// <summary>
        /// Category button placeholder large (tablet)
        /// </summary>
        CategoryButtonPlaceholderTabletLarge = 2013302,

        /// <summary>
        /// Category branding large (tablet)
        /// </summary>
        CategoryBrandingTabletLarge = 2013400,

        /// <summary>
        /// Generic category branding large (tablet)
        /// </summary>
        [NonGenericMediaType(CategoryBrandingTabletLarge)]
        GenericCategoryBrandingTabletLarge = 2013401,

        /// <summary>
        /// Category branding placeholder large (tablet)
        /// </summary>
        CategoryBrandingPlaceholderTabletLarge = 2013402,

        /// <summary>
        /// Company list large (tablet)
        /// </summary>
        CompanyListTabletLarge = 2013500,

        /// <summary>
        /// Homepage large (tablet)
        /// </summary>
        HomepageTabletLarge = 2013600,

        /// <summary>
        /// PointOfInterest list large (tablet)
        /// </summary>
        PointOfInterestListTabletLarge = 2013700,

        /// <summary>
        /// PointOfInterest homepage large (tablet)
        /// </summary>
        PointOfInterestHomepageTabletLarge = 2013800,

        /// <summary>
        /// Map icon large (tablet)
        /// </summary>
        MapIconTabletLarge = 2013900,

        /// <summary>
        /// PointOfInterest map icon large (tablet)
        /// </summary>
        PointOfInterestMapIconTabletLarge = 2014000, // gk wrong sequence

        SitePageIconTabletLarge = 2013910,
        SiteFullPageImageTabletLarge = 2013915,
        SiteTwoThirdsPageImageTabletLarge = 2013920,
        SiteHdWideScreenTabletLarge = 2013925,
        SiteCinemaWideScreenTabletLarge = 2013930,
        SiteMobileHeaderImageTabletLarge = 2013935,
        SitePointOfInterestSummaryPageTabletLarge = 2013955,
        SiteCompanySummaryPageTabletLarge = 2013956,
        SiteBrandingTabletLarge = 2013957,

        [NonGenericMediaType(SitePageIconTabletLarge)]
        TemplateSitePageIconTabletLarge = 2014010,

        #endregion

        #region XLarge (tablet)

        /// <summary>
        /// Product button xlarge (tablet)
        /// </summary>
        ProductButtonTabletXLarge = 2014100,

        /// <summary>
        /// Generic product button xlarge (tablet)
        /// </summary>
        [NonGenericMediaType(ProductButtonTabletXLarge)]
        GenericProductButtonTabletXLarge = 2014101,

        /// <summary>
        /// Product button placeholder xlarge (tablet)
        /// </summary>
        ProductButtonPlaceholderTabletXLarge = 2014102,

        /// <summary>
        /// Product branding xlarge (tablet)
        /// </summary>
        ProductBrandingTabletXLarge = 2014200,

        /// <summary>
        /// Generic product branding xlarge (tablet)
        /// </summary>
        [NonGenericMediaType(ProductBrandingTabletXLarge)]
        GenericProductBrandingTabletXLarge = 2014201,

        /// <summary>
        /// Product branding placeholder xlarge (tablet)
        /// </summary>
        ProductBrandingPlaceholderTabletXLarge = 2014202,

        /// <summary>
        /// Category button xlarge (tablet)
        /// </summary>
        CategoryButtonTabletXLarge = 2014300,

        /// <summary>
        /// Generic category button xlarge (tablet)
        /// </summary>
        [NonGenericMediaType(CategoryButtonTabletXLarge)]
        GenericCategoryButtonTabletXLarge = 2014301,

        /// <summary>
        /// Category button placeholder xlarge (tablet)
        /// </summary>
        CategoryButtonPlaceholderTabletXLarge = 2014302,

        /// <summary>
        /// Category branding xlarge (tablet)
        /// </summary>
        CategoryBrandingTabletXLarge = 2014400,

        /// <summary>
        /// Generic category branding xlarge (tablet)
        /// </summary>
        [NonGenericMediaType(CategoryBrandingTabletXLarge)]
        GenericCategoryBrandingTabletXLarge = 2014401,

        /// <summary>
        /// Category branding placeholder xlarge (tablet)
        /// </summary>
        CategoryBrandingPlaceholderTabletXLarge = 2014402,

        /// <summary>
        /// Company list xlarge (tablet)
        /// </summary>
        CompanyListTabletXLarge = 2014500,

        /// <summary>
        /// Homepage xlarge (tablet)
        /// </summary>
        HomepageTabletXLarge = 2014600,

        /// <summary>
        /// PointOfInterest list xlarge (tablet)
        /// </summary>
        PointOfInterestListTabletXLarge = 2014700,

        /// <summary>
        /// PointOfInterest homepage xlarge (tablet)
        /// </summary>
        PointOfInterestHomepageTabletXLarge = 2014800,

        /// <summary>
        /// Map icon xlarge (tablet)
        /// </summary>
        MapIconTabletXLarge = 2014900,

        /// <summary>
        /// PointOfInterest map icon xlarge (tablet)
        /// </summary>
        PointOfInterestMapIconTabletXLarge = 2015000,

        SitePageIconTabletXLarge = 2014910,
        SiteFullPageImageTabletXLarge = 2014915,
        SiteTwoThirdsPageImageTabletXLarge = 2014920,
        SiteHdWideScreenTabletXLarge = 2014925,
        SiteCinemaWideScreenTabletXLarge = 2014930,
        SiteMobileHeaderImageTabletXLarge = 2014935,
        SitePointOfInterestSummaryPageTabletXLarge = 2014955,
        SiteCompanySummaryPageTabletXLarge = 2014956,
        SiteBrandingTabletXLarge = 2014957,

        [NonGenericMediaType(SitePageIconTabletXLarge)]
        TemplateSitePageIconTabletXLarge = 2015010,

        #endregion

        #region iPhone

        /// <summary>
        /// Product button iPhone
        /// </summary>
        ProductButtoniPhone = 3011100,

        /// <summary>
        /// Generic product button iPhone
        /// </summary>
        [NonGenericMediaType(ProductButtoniPhone)]
        GenericProductButtoniPhone = 3011101,

        /// <summary>
        /// Product button placeholder iPhone
        /// </summary>
        ProductButtonPlaceholderiPhone = 3011102,

        /// <summary>
        /// Product branding iPhone
        /// </summary>
        ProductBrandingiPhone = 3011200,

        /// <summary>
        /// Generic product branding iPhone
        /// </summary>
        [NonGenericMediaType(ProductBrandingiPhone)]
        GenericProductBrandingiPhone = 3011201,

        /// <summary>
        /// Product branding placeholder iPhone
        /// </summary>
        ProductBrandingPlaceholderiPhone = 3011202,

        /// <summary>
        /// Category button iPhone
        /// </summary>
        CategoryButtoniPhone = 3011300,

        /// <summary>
        /// Generic category button iPhone
        /// </summary>
        [NonGenericMediaType(CategoryButtoniPhone)]
        GenericCategoryButtoniPhone = 3011301,

        /// <summary>
        /// Category button placeholder iPhone
        /// </summary>
        CategoryButtonPlaceholderiPhone = 3011302,

        /// <summary>
        /// Category branding iPhone
        /// </summary>
        CategoryBrandingiPhone = 3011400,

        /// <summary>
        /// Generic category branding iPhone
        /// </summary>
        [NonGenericMediaType(CategoryBrandingiPhone)]
        GenericCategoryBrandingiPhone = 3011401,

        /// <summary>
        /// Category branding placeholder iPhone
        /// </summary>
        CategoryBrandingPlaceholderiPhone = 3011402,

        /// <summary>
        /// Alteration Dialog Page iPhone
        /// </summary>
        AlterationDialogPageiPhone = 3011403,

        /// <summary>
        /// Company list iPhone
        /// </summary>
        CompanyListiPhone = 3011500,

        /// <summary>
        /// Deliverypointgroup list iPhone
        /// </summary>
        DeliverypointgroupListiPhone = 3011501,

        /// <summary>
        /// Homepage iPhone
        /// </summary>
        HomepageiPhone = 3011600,

        /// <summary>
        /// PointOfInterest list iPhone
        /// </summary>
        PointOfInterestListiPhone = 3011700,

        /// <summary>
        /// PointOfInterest homepage iPhone
        /// </summary>
        PointOfInterestHomepageiPhone = 3011800,

        /// <summary>
        /// Map icon iPhone
        /// </summary>
        MapIconiPhone = 3011900,

        /// <summary>
        /// PointOfInterest map icon iPhone
        /// </summary>
        PointOfInterestMapIconiPhone = 3012000,

        SitePageIconiPhone = 3011910,

        /*
        //SiteFullPageImageiPhone = 3011915,
        //SiteTwoThirdsPageImageiPhone = 3011920,
        //SiteHdWideScreeniPhone = 301125,
        //SiteCinemaWideScreeniPhone = 3011930,
        */

        SiteMobileHeaderImageiPhone = 3011935,
        SitePointOfInterestSummaryPageiPhone = 3011955,
        SiteCompanySummaryPageiPhone = 3011956,
        SiteBrandingiPhone = 3011957,

        [NonGenericMediaType(SitePageIconiPhone)]
        TemplateSitePageIconiPhone = 3012010,

        [NonGenericMediaType(SiteMobileHeaderImageiPhone)]
        TemplateSiteMobileHeaderImageiPhone = 3012035,

        AttachmentThumbnailiPhone = 3012020,

        #endregion

        #region iPhone Retina

        /// <summary>
        /// Product button iPhone Retina
        /// </summary>
        ProductButtoniPhoneRetina = 3012100,

        /// <summary>
        /// Generic product button iPhone Retina
        /// </summary>
        [NonGenericMediaType(ProductButtoniPhoneRetina)]
        GenericProductButtoniPhoneRetina = 3012101,

        /// <summary>
        /// Product button placeholder iPhone Retina
        /// </summary>
        ProductButtonPlaceholderiPhoneRetina = 3012102,

        /// <summary>
        /// Product branding iPhone Retina
        /// </summary>
        ProductBrandingiPhoneRetina = 3012200,

        /// <summary>
        /// Generic product branding iPhone Retina
        /// </summary>
        [NonGenericMediaType(ProductBrandingiPhoneRetina)]
        GenericProductBrandingiPhoneRetina = 3012201,

        /// <summary>
        /// Product branding placeholder iPhone Retina
        /// </summary>
        ProductBrandingPlaceholderiPhoneRetina = 3012202,

        /// <summary>
        /// Category button iPhone Retina
        /// </summary>
        CategoryButtoniPhoneRetina = 3012300,

        /// <summary>
        /// Generic category button iPhone Retina
        /// </summary>
        [NonGenericMediaType(CategoryButtoniPhoneRetina)]
        GenericCategoryButtoniPhoneRetina = 3012301,

        /// <summary>
        /// Category button placeholder iPhone Retina
        /// </summary>
        CategoryButtonPlaceholderiPhoneRetina = 3012302,

        /// <summary>
        /// Category branding iPhone Retina
        /// </summary>
        CategoryBrandingiPhoneRetina = 3012400,

        /// <summary>
        /// Generic category branding iPhone Retina
        /// </summary>
        [NonGenericMediaType(CategoryBrandingiPhoneRetina)]
        GenericCategoryBrandingiPhoneRetina = 3012401,

        /// <summary>
        /// Category branding placeholder iPhone Retina
        /// </summary>
        CategoryBrandingPlaceholderiPhoneRetina = 3012402,

        /// <summary>
        /// Alteration dialog page iPhone Retina
        /// </summary>
        AlterationDialogPageiPhoneRetina = 3012403,

        /// <summary>
        /// Company list iPhone Retina
        /// </summary>
        CompanyListiPhoneRetina = 3012500,

        /// <summary>
        /// Deliverypointgroup list iPhone Retina
        /// </summary>
        DeliverypointgroupListiPhoneRetina = 3012501,

        /// <summary>
        /// Homepage iPhone Retina
        /// </summary>
        HomepageiPhoneRetina = 3012600,

        /// <summary>
        /// PointOfInterest list iPhone Retina
        /// </summary>
        PointOfInterestListiPhoneRetina = 3012700,

        /// <summary>
        /// PointOfInterest homepage iPhone Retina
        /// </summary>
        PointOfInterestHomepageiPhoneRetina = 3012800,

        /// <summary>
        /// Map icon iPhone Retina
        /// </summary>
        MapIconiPhoneRetina = 3012900,

        /// <summary>
        /// PointOfInterest map icon iPhone Retina
        /// </summary>
        PointOfInterestMapIconiPhoneRetina = 3013000,

        SitePageIconiPhoneRetina = 3012910,

        /*
        //SiteFullPageImageiPhoneRetina = 3012915,
        //SiteTwoThirdsPageImageiPhoneRetina = 3012920,
        //SiteHdWideScreeniPhoneRetina = 301225,
        //SiteCinemaWideScreeniPhoneRetina = 3012930,
        */

        SiteMobileHeaderImageiPhoneRetina = 3012935,
        SitePointOfInterestSummaryPageiPhoneRetina = 3012955,
        SiteCompanySummaryPageiPhoneRetina = 3012956,
        SiteBrandingiPhoneRetina = 3012957,

        [NonGenericMediaType(SitePageIconiPhoneRetina)]
        TemplateSitePageIconiPhoneRetina = 3013010,

        [NonGenericMediaType(SiteMobileHeaderImageiPhoneRetina)]
        TemplateSiteMobileHeaderImageiPhoneRetina = 3013035,

        AttachmentThumbnailiPhoneRetina = 3013020,

        #endregion

        #region iPad

        /// <summary>
        /// Product button iPad
        /// </summary>
        ProductButtoniPad = 3013100,

        /// <summary>
        /// Generic product button iPad
        /// </summary>
        [NonGenericMediaType(ProductButtoniPad)]
        GenericProductButtoniPad = 3013101,

        /// <summary>
        /// Product button placeholder iPad
        /// </summary>
        ProductButtonPlaceholderiPad = 3013102,

        /// <summary>
        /// Product branding iPad
        /// </summary>
        ProductBrandingiPad = 3013200,

        /// <summary>
        /// Generic product branding iPad
        /// </summary>
        [NonGenericMediaType(ProductBrandingiPad)]
        GenericProductBrandingiPad = 3013201,

        /// <summary>
        /// Product branding placeholder iPad
        /// </summary>
        ProductBrandingPlaceholderiPad = 3013202,

        /// <summary>
        /// Category button iPad
        /// </summary>
        CategoryButtoniPad = 3013300,

        /// <summary>
        /// Generic category button iPad
        /// </summary>
        [NonGenericMediaType(CategoryButtoniPad)]
        GenericCategoryButtoniPad = 3013301,

        /// <summary>
        /// Category button placeholder iPad
        /// </summary>
        CategoryButtonPlaceholderiPad = 3013302,

        /// <summary>
        /// Category branding iPad
        /// </summary>
        CategoryBrandingiPad = 3013400,

        /// <summary>
        /// Generic category branding iPad
        /// </summary>
        [NonGenericMediaType(CategoryBrandingiPad)]
        GenericCategoryBrandingiPad = 3013401,

        /// <summary>
        /// Category branding placeholder iPad
        /// </summary>
        CategoryBrandingPlaceholderiPad = 3013402,

        /// <summary>
        /// Alteration dialog page iPad
        /// </summary>
        AlterationDialogPageiPad = 3013403,

        /// <summary>
        /// Company list iPad
        /// </summary>
        CompanyListiPad = 3013500,

        /// <summary>
        /// Deliverypointgroup list iPad
        /// </summary>
        DeliverypointgroupListiPad = 3013501,

        /// <summary>
        /// Homepage iPad
        /// </summary>
        HomepageiPad = 3013600,

        /// <summary>
        /// PointOfInterest list iPad
        /// </summary>
        PointOfInterestListiPad = 3013700,

        /// <summary>
        /// PointOfInterest homepage iPad
        /// </summary>
        PointOfInterestHomepageiPad = 3013800,

        /// <summary>
        /// Map icon iPad
        /// </summary>
        MapIconiPad = 3013900,

        /// <summary>
        /// PointOfInterest map icon iPad
        /// </summary>
        PointOfInterestMapIconiPad = 3014000,

        /// <summary>
        /// Homepage2 iPad 
        /// </summary>
        Homepage2iPad = 3014001,

        /// <summary>
        /// Homepage3 iPad 
        /// </summary>
        Homepage3iPad = 3014002,

        /// <summary>
        /// Homepage advert iPad 
        /// </summary>
        HomepageAdvertiPad = 3014202,

        SitePageIconiPad = 3013910,
        SiteFullPageImageiPad = 3013915,
        SiteTwoThirdsPageImageiPad = 3013920,
        SiteHdWideScreeniPad = 3013925,
        SiteCinemaWideScreeniPad = 3013930,

        /*SiteMobileHeaderImageiPad = 3013935,*/

        SitePointOfInterestSummaryPageiPad = 3013955,
        SiteCompanySummaryPageiPad = 3013956,
        SiteBrandingiPad = 3013957,

        [NonGenericMediaType(SitePageIconiPad)]
        TemplateSitePageIconiPad = 3014010,

        [NonGenericMediaType(SiteFullPageImageiPad)]
        TemplateSiteFullPageImageiPad = 3014015,

        [NonGenericMediaType(SiteTwoThirdsPageImageiPad)]
        TemplateSiteTwoThirdsPageImageiPad = 3014020,

        [NonGenericMediaType(SiteCinemaWideScreeniPad)]
        TemplateSiteCinemaWideScreeniPad = 3014030,

        AttachmentThumbnailiPad = 3014040,

        #endregion

        #region iPad Retina

        /// <summary>
        /// Product button iPad Retina
        /// </summary>
        ProductButtoniPadRetina = 3014100,

        /// <summary>
        /// Generic product button iPad Retina
        /// </summary>
        [NonGenericMediaType(ProductButtoniPadRetina)]
        GenericProductButtoniPadRetina = 3014101,

        /// <summary>
        /// Product button placeholder iPad Retina
        /// </summary>
        ProductButtonPlaceholderiPadRetina = 3014102,

        /// <summary>
        /// Product branding iPad Retina
        /// </summary>
        ProductBrandingiPadRetina = 3014200,

        /// <summary>
        /// Generic product branding iPad Retina
        /// </summary>
        [NonGenericMediaType(ProductBrandingiPadRetina)]
        GenericProductBrandingiPadRetina = 3014201,

        /// <summary>
        /// Product branding placeholder iPad Retina
        /// </summary>
        ProductBrandingPlaceholderiPadRetina = 3014203,

        /// <summary>
        /// Category button iPad Retina
        /// </summary>
        CategoryButtoniPadRetina = 3014300,

        /// <summary>
        /// Generic category button iPad Retina
        /// </summary>
        [NonGenericMediaType(CategoryButtoniPadRetina)]
        GenericCategoryButtoniPadRetina = 3014301,

        /// <summary>
        /// Category button placeholder iPad Retina
        /// </summary>
        CategoryButtonPlaceholderiPadRetina = 3014302,

        /// <summary>
        /// Category branding iPad Retina
        /// </summary>
        CategoryBrandingiPadRetina = 3014400,

        /// <summary>
        /// Generic category branding iPad Retina
        /// </summary>
        [NonGenericMediaType(CategoryBrandingiPadRetina)]
        GenericCategoryBrandingiPadRetina = 3014401,

        /// <summary>
        /// Category branding placeholder iPad Retina
        /// </summary>
        CategoryBrandingPlaceholderiPadRetina = 3014402,

        /// <summary>
        /// Alteration dialog page iPad Retina
        /// </summary>
        AlterationDialogPageiPadRetina = 3014403,

        /// <summary>
        /// Company list iPad Retina
        /// </summary>
        CompanyListiPadRetina = 3014500,

        /// <summary>
        /// Deliverypointgroup list iPad Retina
        /// </summary>
        DeliverypointgroupListiPadRetina = 3014501,

        /// <summary>
        /// Homepage iPad Retina
        /// </summary>
        HomepageiPadRetina = 3014600,

        /// <summary>
        /// PointOfInterest list iPad Retina
        /// </summary>
        PointOfInterestListiPadRetina = 3014700,

        /// <summary>
        /// PointOfInterest homepage iPad Retina
        /// </summary>
        PointOfInterestHomepageiPadRetina = 3014800,

        /// <summary>
        /// Map icon iPad Retina
        /// </summary>
        MapIconiPadRetina = 3014900,

        /// <summary>
        /// PointOfInterest map icon iPad Retina
        /// </summary>
        PointOfInterestMapIconiPadRetina = 3015000,

        /// <summary>
        /// Homepage2 iPad Retina
        /// </summary>
        Homepage2iPadRetina = 3015001,

        /// <summary>
        /// Homepage Advert iPad Retina
        /// </summary>
        HomepageAdvertiPadRetina = 3015002,

        /// <summary>
        /// Homepage3 iPad Retina
        /// </summary>
        Homepage3iPadRetina = 3015003,

        SitePageIconiPadRetina = 3014910,
        SiteFullPageImageiPadRetina = 3014915,
        SiteTwoThirdsPageImageiPadRetina = 3014920,
        SiteHdWideScreeniPadRetina = 3014925,
        SiteCinemaWideScreeniPadRetina = 3014930,

        /*SiteMobileHeaderImageiPadRetina = 3014935,*/

        SitePointOfInterestSummaryPageiPadRetina = 3014955,
        SiteCompanySummaryPageiPadRetina = 3014956,
        SiteBrandingiPadRetina = 3014957,

        [NonGenericMediaType(SitePageIconiPadRetina)]
        TemplateSitePageIconiPadRetina = 3015010,

        [NonGenericMediaType(SiteFullPageImageiPadRetina)]
        TemplateSiteFullPageImageiPadRetina = 3015015,

        [NonGenericMediaType(SiteTwoThirdsPageImageiPadRetina)]
        TemplateSiteTwoThirdsPageImageiPadRetina = 3015020,

        [NonGenericMediaType(SiteCinemaWideScreeniPadRetina)]
        TemplateSiteCinemaWideScreeniPadRetina = 3015030,

        AttachmentThumbnailiPadRetina = 3015040,

        #endregion

        #region Non Device Related

        RouteStepHandlerEmailResource = 4015000,

        AttachmentImage = 4015010,

        AttachmentPdf = 4015015,

        AttachmentThumbnail = 4015020,

        DownloadPageBranding = 4015025,

        #endregion        

        #region iPhone Retina HD

        /// <summary>
        /// Product button iPhone Retina HD
        /// </summary>
        ProductButtoniPhoneRetinaHD = 5011100,

        /// <summary>
        /// Generic product button iPhone Retina HD
        /// </summary>
        [NonGenericMediaType(ProductButtoniPhoneRetinaHD)]
        GenericProductButtoniPhoneRetinaHD = 5011101,

        /// <summary>
        /// Product button placeholder iPhone Retina HD
        /// </summary>
        ProductButtonPlaceholderiPhoneRetinaHD = 5011102,

        /// <summary>
        /// Product branding iPhone Retina HD
        /// </summary>
        ProductBrandingiPhoneRetinaHD = 5011200,

        /// <summary>
        /// Generic product branding iPhone Retina HD
        /// </summary>
        [NonGenericMediaType(ProductBrandingiPhoneRetinaHD)]
        GenericProductBrandingiPhoneRetinaHD = 5011201,

        /// <summary>
        /// Product branding placeholder iPhone Retina HD
        /// </summary>
        ProductBrandingPlaceholderiPhoneRetinaHD = 5011202,

        /// <summary>
        /// Category button iPhone Retina HD
        /// </summary>
        CategoryButtoniPhoneRetinaHD = 5011300,

        /// <summary>
        /// Generic category button iPhone Retina HD
        /// </summary>
        [NonGenericMediaType(CategoryButtoniPhoneRetinaHD)]
        GenericCategoryButtoniPhoneRetinaHD = 5011301,

        /// <summary>
        /// Category button placeholder iPhone Retina HD
        /// </summary>
        CategoryButtonPlaceholderiPhoneRetinaHD = 5011302,

        /// <summary>
        /// Category branding iPhone Retina HD
        /// </summary>
        CategoryBrandingiPhoneRetinaHD = 5011400,

        /// <summary>
        /// Generic category branding iPhone Retina HD
        /// </summary>
        [NonGenericMediaType(CategoryBrandingiPhoneRetinaHD)]
        GenericCategoryBrandingiPhoneRetinaHD = 5011401,

        /// <summary>
        /// Category branding placeholder iPhone Retina HD
        /// </summary>
        CategoryBrandingPlaceholderiPhoneRetinaHD = 5011402,

        /// <summary>
        /// Alteration dialog page iPhone Retina HD
        /// </summary>
        AlterationDialogPageiPhoneRetinaHD = 5011403,

        /// <summary>
        /// Company list iPhone Retina HD
        /// </summary>
        CompanyListiPhoneRetinaHD = 5011500,

        /// <summary>
        /// Deliverypointgroup list iPhone Retina HD
        /// </summary>
        DeliverypointgroupListiPhoneRetinaHD = 5011501,

        /// <summary>
        /// Homepage iPhone Retina HD
        /// </summary>
        HomepageiPhoneRetinaHD = 5011600,

        /// <summary>
        /// PointOfInterest list iPhone Retina HD
        /// </summary>
        PointOfInterestListiPhoneRetinaHD = 5011700,

        /// <summary>
        /// PointOfInterest homepage iPhone Retina HD
        /// </summary>
        PointOfInterestHomepageiPhoneRetinaHD = 5011800,

        /// <summary>
        /// Map icon iPhone Retina HD
        /// </summary>
        MapIconiPhoneRetinaHD = 5011900,

        /// <summary>
        /// PointOfInterest map icon iPhone Retina HD
        /// </summary>
        PointOfInterestMapIconiPhoneRetinaHD = 5012000,

        SitePageIconiPhoneRetinaHD = 5011910,

        /*
        //SiteFullPageImageiPhoneRetinaHD = 5011915,
        //SiteTwoThirdsPageImageiPhoneRetinaHD = 5011920,
        //SiteHdWideScreeniPhoneRetinaHD = 501125,
        //SiteCinemaWideScreeniPhoneRetinaHD = 5011930,
        */

        SiteMobileHeaderImageiPhoneRetinaHD = 5011935,
        SitePointOfInterestSummaryPageiPhoneRetinaHD = 5011955,
        SiteCompanySummaryPageiPhoneRetinaHD = 5011956,
        SiteBrandingiPhoneRetinaHD = 5011957,

        [NonGenericMediaType(SitePageIconiPhoneRetinaHD)]
        TemplateSitePageIconiPhoneRetinaHD = 5012010,

        [NonGenericMediaType(SiteMobileHeaderImageiPhoneRetinaHD)]
        TemplateSiteMobileHeaderImageiPhoneRetinaHD = 5011936,

        AttachmentThumbnailiPhoneRetinaHD = 5012020,

        #endregion

        #region iPhone Retina HD Plus

        /// <summary>
        /// Product button iPhone Retina HD Plus
        /// </summary>
        ProductButtoniPhoneRetinaHDPlus = 6012100,

        /// <summary>
        /// Generic product button iPhone Retina HD Plus
        /// </summary>
        [NonGenericMediaType(ProductButtoniPhoneRetinaHDPlus)]
        GenericProductButtoniPhoneRetinaHDPlus = 6012101,

        /// <summary>
        /// Product button placeholder iPhone Retina HD Plus
        /// </summary>
        ProductButtonPlaceholderiPhoneRetinaHDPlus = 6012102,

        /// <summary>
        /// Product branding iPhone Retina HD Plus
        /// </summary>
        ProductBrandingiPhoneRetinaHDPlus = 6012200,

        /// <summary>
        /// Generic product branding iPhone Retina HD Plus
        /// </summary>
        [NonGenericMediaType(ProductBrandingiPhoneRetinaHDPlus)]
        GenericProductBrandingiPhoneRetinaHDPlus = 6012201,

        /// <summary>
        /// Product branding placeholder iPhone Retina HD Plus
        /// </summary>
        ProductBrandingPlaceholderiPhoneRetinaHDPlus = 6012202,

        /// <summary>
        /// Category button iPhone Retina HD Plus
        /// </summary>
        CategoryButtoniPhoneRetinaHDPlus = 6012300,

        /// <summary>
        /// Generic category button iPhone Retina HD Plus
        /// </summary>
        [NonGenericMediaType(CategoryButtoniPhoneRetinaHDPlus)]
        GenericCategoryButtoniPhoneRetinaHDPlus = 6012301,

        /// <summary>
        /// Category button placeholder iPhone Retina HD Plus
        /// </summary>
        CategoryButtonPlaceholderiPhoneRetinaHDPlus = 6012302,

        /// <summary>
        /// Category branding iPhone Retina HD Plus
        /// </summary>
        CategoryBrandingiPhoneRetinaHDPlus = 6012400,

        /// <summary>
        /// Generic category branding iPhone Retina HD Plus
        /// </summary>
        [NonGenericMediaType(CategoryBrandingiPhoneRetinaHDPlus)]
        GenericCategoryBrandingiPhoneRetinaHDPlus = 6012401,

        /// <summary>
        /// Category branding placeholder iPhone Retina HD Plus
        /// </summary>
        CategoryBrandingPlaceholderiPhoneRetinaHDPlus = 6012402,

        /// <summary>
        /// Alteration dialog page iPhone Retina HD Plus
        /// </summary>
        AlterationDialogPageiPhoneRetinaHDPlus = 6012403,

        /// <summary>
        /// Company list iPhone Retina HD Plus
        /// </summary>
        CompanyListiPhoneRetinaHDPlus = 6012500,

        /// <summary>
        /// Deliverypointgroup list iPhone Retina HD Plus
        /// </summary>
        DeliverypointgroupListiPhoneRetinaHDPlus = 6012501,

        /// <summary>
        /// Homepage iPhone Retina HD Plus
        /// </summary>
        HomepageiPhoneRetinaHDPlus = 6012600,

        /// <summary>
        /// PointOfInterest list iPhone Retina HD Plus
        /// </summary>
        PointOfInterestListiPhoneRetinaHDPlus = 6012700,

        /// <summary>
        /// PointOfInterest homepage iPhone Retina HD Plus
        /// </summary>
        PointOfInterestHomepageiPhoneRetinaHDPlus = 6012800,

        /// <summary>
        /// Map icon iPhone Retina HD Plus
        /// </summary>
        MapIconiPhoneRetinaHDPlus = 6012900,

        /// <summary>
        /// PointOfInterest map icon iPhone Retina HD Plus
        /// </summary>
        PointOfInterestMapIconiPhoneRetinaHDPlus = 6013000,

        SitePageIconiPhoneRetinaHDPlus = 6012910,

        /*
        //SiteFullPageImageiPhoneRetinaHDPlus = 6012915,
        //SiteTwoThirdsPageImageiPhoneRetinaHDPlus = 6012920,
        //SiteHdWideScreeniPhoneRetinaHDPlus = 601225,
        //SiteCinemaWideScreeniPhoneRetinaHDPlus = 6012930,
        */

        SiteMobileHeaderImageiPhoneRetinaHDPlus = 6012935,
        SitePointOfInterestSummaryPageiPhoneRetinaHDPlus = 6012955,
        SiteCompanySummaryPageiPhoneRetinaHDPlus = 6012956,
        SiteBrandingiPhoneRetinaHDPlus = 6012957,

        [NonGenericMediaType(SitePageIconiPhoneRetinaHDPlus)]
        TemplateSitePageIconiPhoneRetinaHDPlus = 6013010,

        [NonGenericMediaType(SiteMobileHeaderImageiPhoneRetinaHDPlus)]
        TemplateSiteMobileHeaderImageiPhoneRetinaHDPlus = 6012936,

        AttachmentThumbnailiPhoneRetinaHDPlus = 6013020,

        #endregion

        #region Appless

        #region Appless small

        /// <summary>
        /// AppLess header logo (small)
        /// </summary>
        AppLessHeaderLogoSmall = 700001,

        /// <summary>
		/// AppLess hero (small)
		/// </summary>
        AppLessHeroSmall = 700002,

        /// <summary>
		/// AppLess carousel (small)
		/// </summary>
        AppLessCarouselSmall = 700003,

        /// <summary>
		/// AppLess action banner (small)
		/// </summary>
        AppLessActionBannerSmall = 700004,

        /// <summary>
        /// AppLess application icon (small)
        /// </summary>
        AppLessApplicationIconSmall = 700005,

        /// <summary>
        /// AppLess product banner (small)
        /// </summary>
        AppLessProductBannerSmall = 700006,

        /// <summary>
        /// AppLess category banner (small)
        /// </summary>
        AppLessCategoryBannerSmall = 700007,

        #endregion

        #region Appless medium

        /// <summary>
        /// AppLess header logo (medium)
        /// </summary>
        AppLessHeaderLogoMedium = 800001,

        /// <summary>
		/// AppLess hero (medium)
		/// </summary>
        AppLessHeroMedium = 800002,

        /// <summary>
		/// AppLess carousel (medium)
		/// </summary>
        AppLessCarouselMedium = 800003,

        /// <summary>
		/// AppLess action banner (medium)
		/// </summary>
        AppLessActionBannerMedium = 800004,

        /// <summary>
        /// AppLess application icon (medium)
        /// </summary>
        AppLessApplicationIconMedium = 800005,

        /// <summary>
        /// AppLess product banner (medium)
        /// </summary>
        AppLessProductBannerMedium = 800006,

        /// <summary>
        /// AppLess catgeory banner (medium)
        /// </summary>
        AppLessCategoryBannerMedium = 800007,

        #endregion

        #region Appless large

        /// <summary>
        /// AppLess header logo (large)
        /// </summary>
        AppLessHeaderLogoLarge = 900001,

        /// <summary>
		/// AppLess hero (large)
		/// </summary>
        AppLessHeroLarge = 900002,

        /// <summary>
		/// AppLess carousel (large)
		/// </summary>
        AppLessCarouselLarge = 900003,

        /// <summary>
		/// AppLess action banner (large)
		/// </summary>
        AppLessActionBannerLarge = 900004,

        /// <summary>
        /// AppLess application icon (large)
        /// </summary>
        AppLessApplicationIconLarge = 900005,

        /// <summary>
        /// AppLess product banner (large)
        /// </summary>
        AppLessProductBannerLarge = 900006,

        /// <summary>
        /// AppLess category banner (large)
        /// </summary>
        AppLessCategoryBannerLarge = 900007

        #endregion

        #endregion
    }
}