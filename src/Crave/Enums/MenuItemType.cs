﻿namespace Crave.Enums
{
    public enum MenuItemType
    {
        /// <summary>
        /// Category
        /// </summary>
        Category = 0,

        /// <summary>
        /// Product
        /// </summary>
        Product = 1,

        /// <summary>
        /// Alteration
        /// </summary>
        Alteration = 2,

        /// <summary>
        /// Alteration option
        /// </summary>
        Alterationoption = 3
    }
}
