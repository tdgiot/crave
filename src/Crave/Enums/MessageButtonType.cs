﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration type which represents the buttons on a message.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum MessageButtonType : int
    {
        /// <summary>
        /// OK button.
        /// </summary>
        Ok = 1,

        /// <summary>
        /// Yes and No button.
        /// </summary>
        YesNo = 2
    }
}