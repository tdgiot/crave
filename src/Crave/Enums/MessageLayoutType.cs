﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the layout type of a message.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum MessageLayoutType
    {
        /// <summary>
        /// AutoSize the message.
        /// </summary>
        AutoSize = 0,

        /// <summary>
        /// Small message.
        /// </summary>
        Small = 1,

        /// <summary>
        /// Medium message.
        /// </summary>
        Medium = 2,

        /// <summary>
        /// Large message.
        /// </summary>
        Large = 3,
    }
}
