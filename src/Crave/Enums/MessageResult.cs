﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration type which represents the message result of a recipient.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum MessageResult : int
    {
        /// <summary>
        /// Recipient clicked on OK.
        /// </summary>
        Ok = 1,

        /// <summary>
        /// Recipient clicked on YES.
        /// </summary>
        Yes = 2,
        
        /// <summary>
        /// Recipient clicked on NO.
        /// </summary>
        No = 3,

        /// <summary>
        /// Recipient did not interact with the message.
        /// </summary>
        TimeOut = 4,
        
        /// <summary>
        /// The message was cleared manually.
        /// </summary>
        ClearManually = 5
    }
}