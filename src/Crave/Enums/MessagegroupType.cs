﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Message group type
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum MessagegroupType
    {
        /// <summary>
        /// Custom
        /// </summary>
        Custom = 0,
        /// <summary>
        /// Check-in
        /// </summary>
        Checkin = 1,
        /// <summary>
        /// Check-out
        /// </summary>
        Checkout = 2,
        /// <summary>
        /// Group
        /// </summary>
        Group = 3
    }
}