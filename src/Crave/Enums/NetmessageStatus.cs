﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the status of a net message.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum NetmessageStatus
    {
        /// <summary>
        /// Message has been saved, but not yet delivered to the client.
        /// </summary>
        WaitingForVerification = 0,

        /// <summary>
        /// Message has successfully been delivered to the client.
        /// </summary>
        Delivered = 1,

        /// <summary>
        /// Message is cancelled because the client configuration changed (ClientValidator).
        /// </summary>
        CancelledNewConfiguration = 2,

        /// <summary>
        /// Message is timed out.
        /// </summary>
        TimedOut = 3,

        /// <summary>
        /// The app handling the message has received it
        /// </summary>
        Verified = 10,

        /// <summary>
        /// Message is completed.
        /// </summary>
        Completed = 1000,
    }
}
