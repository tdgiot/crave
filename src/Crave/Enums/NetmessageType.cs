﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a net message.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum NetmessageType
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Display a new message.
        /// </summary>
        AnnounceNewMessage = 2,

        /// <summary>
        /// The status of an order was updated.
        /// </summary>
        OrderStatusUpdated = 3,

        /// <summary>
        /// Mobile ordering status changed
        /// </summary>
        SetMobileOrdering = 4,

        /// <summary>
        /// Broadcast message.
        /// </summary>
        BroadcastMessage = 5,

        /// <summary>
        /// Social media message
        /// </summary>
        NewSocialMediaMessage = 6,

        /// <summary>
        /// Route step handler status updated
        /// </summary>
        RoutestephandlerStatusUpdated = 7,

        /// <summary>
        /// Checkout notification to E-menu
        /// </summary>
        SetPmsCheckedOut = 8,

        /// <summary>
        /// Get PMS folio from OSS.
        /// </summary>
        GetPmsFolio = 9,

        /// <summary>
        /// Set PMS folio to E-menu.
        /// </summary>
        SetPmsFolio = 10,

        /// <summary>
        /// Check in notification to E-menu.
        /// </summary>
        SetPmsCheckedIn = 11,

        /// <summary>
        /// Set PMS guest information to E-menu.
        /// </summary>
        SetPmsGuestInformation = 12,

        /// <summary>
        /// Get PMS Guest information from PMS via OSS.
        /// </summary>
        GetPmsGuestInformation = 13,

        /// <summary>
        /// Set forward terminal ID for a console
        /// </summary>
        SetForwardTerminal = 14,

        /// <summary>
        /// Get PMS Guest information from PMS via OSS.
        /// </summary>
        GetPmsExpressCheckout = 15,

        /// <summary>
        /// Get PMS Guest information from PMS via OSS.
        /// </summary>
        SetPmsExpressCheckedOut = 16,

        /// <summary>
        /// Authentication result
        /// </summary>
        AuthenticateResult = 17,

        /// <summary>
        /// Set network information like internal and external IP
        /// </summary>
        SetNetworkInformation = 18,

        /// <summary>
        /// Set the client type
        /// </summary>
        SetClientType = 19,

        /// <summary>
        /// Notifies the connected PokeIn clients that the company timestamp has been updated
        /// </summary>
        CompanyTimestampsUpdated = 20,

        /// <summary>
        /// Connect an external listener to an agent
        /// </summary>
        ConnectToAgent = 21,

        /// <summary>
        /// Command output returned by Agent
        /// </summary>
        AgentCommandResponse = 22,

        /// <summary>
        /// Send request to Agent
        /// </summary>
        AgentCommandRequest = 23,

        /// <summary>
        /// New event for external listeners
        /// </summary>
        NewEventExternalListener = 24,

        /// <summary>
        /// Push event to external listener
        /// </summary>
        PushEventToExternalListeners = 25,

        /// <summary>
        /// Set the Operation Mode of a Client
        /// </summary>
        SetClientOperationMode = 26,

        /// <summary>
        /// Get connected clients
        /// </summary>
        GetConnectedClients = 27,

        /// <summary>
        /// New survey result has been saved
        /// </summary>
        NewSurveyResult = 28,

        /// <summary>
        /// New survey result has been saved
        /// </summary>
        SetDeliverypointId = 29,

        /// <summary>
        /// Command without parameters for a Client
        /// </summary>
        ClientCommand = 30,

        /// <summary>
        /// Command without parameters for a Terminal
        /// </summary>
        TerminalCommand = 31,

        /// <summary>
        /// Execute command directly on device shell
        /// </summary>
        DeviceCommandExecute = 32,

        /// <summary>
        /// Application update available.
        /// </summary>
        UpdateAvailable = 33,

        /// <summary>
        /// Set Cloud Environment
        /// </summary>
        SetCloudEnvironment = 34,

        /// <summary>
        /// TO DO.
        /// </summary>
        SendInformation = 35,

        /// <summary>
        /// Device Unlinked
        /// </summary>
        DeviceUnlink = 36,

        /// <summary>
        /// TO DO.
        /// </summary>
        SandboxConfig = 37,

        /// <summary>
        /// TO DO.
        /// </summary>
        SetMasterTab = 38,

        /// <summary>
        /// TO DO.
        /// </summary>
        DownloadUpdate = 39,

        /// <summary>
        /// TO DO.
        /// </summary>
        InstallUpdate = 40,

        /// <summary>
        /// TO DO.
        /// </summary>
        SetPmsWakeUp = 41,

        /// <summary>
        /// TO DO.
        /// </summary>
        ClearPmsWakeUp = 42,

        /// <summary>
        /// TO DO.
        /// </summary>
        MenuUpdated = 43,

        /// <summary>
        /// Switch tab on client
        /// </summary>
        SwitchTab = 44,

        /// <summary>
        /// Send JSON object
        /// </summary>
        SendJsonObject = 45,

        /// <summary>
        /// Refresh content on client
        /// </summary>
        RefreshContent = 46,

        /// <summary>
        /// Message send to client when it's about to be removed from the server
        /// </summary>
        Disconnect = 1000,

        /// <summary>
        /// Set the active comet handler for the client
        /// </summary>
        SwitchCometHandler = 2000,

        /// <summary>
        /// Request connection state from services (web service, CMS, etc..) and total connected clients
        /// </summary>
        SystemState = 5000,

        /// <summary>
        /// Join one or more groups
        /// </summary>
        JoinGroups = 9990,

        /// <summary>
        /// Leave one or more groups
        /// </summary>
        LeaveGroups = 9991,

        /// <summary>
        /// Netmessage has been received by the server and will be processed
        /// </summary>
        VerifyNetmessage = 9992,

        /// <summary>
        /// Ping request.
        /// </summary>
        Ping = 9998,

        /// <summary>
        /// Pong response.
        /// </summary>
        Pong = 9999,

        /// <summary>
        /// TO DO.
        /// </summary>
        Test = 10000,
    }
}
