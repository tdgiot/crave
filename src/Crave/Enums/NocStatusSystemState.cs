﻿using System.ComponentModel;

namespace Crave.Enums
{
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum NocStatusSystemState
    {
        Normal = 0,
        Failing = 1
    }
}