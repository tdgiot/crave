﻿namespace Crave.Enums
{
    public enum OptInStatus
    {
        NoOptInAsked = 0,
        HardOptIn = 1,
        HardOptOut = 2,
        HardOptInExists = 3
    }
}
