﻿namespace Crave.Enums
{
    public enum OptInType
    {
        NoOptIn = 0,
        HardOptInCheckbox = 1,
        HardOptInRadioButtons = 2
    }
}
