﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents errors for orders. 
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum OrderErrorCode : int
    {
        /// <summary>
        /// No error.
        /// </summary>
        None = 0,

        /// <summary>
        /// Manually processed.
        /// </summary>
        ManuallyProcessed = 1,

        /// <summary>
        /// Unspecified error.
        /// </summary>
        UnspecifiedError = 2,

        #region At on-site- server 200 - 299

        /// <summary>
        /// Order could not be converted to a POS order
        /// </summary>
        OnsitePosConversionFailed = 200,

        /// <summary>
        /// Table queue is locked
        /// </summary>
        TableQueueuIsLocked = 201,

        /// <summary>
        /// No table for delivery point at OSS.
        /// </summary>
        NoTableForDeliverypointAtOss = 202,

        /// <summary>
        /// Failed to print
        /// </summary>
        PrinterFailure = 203,

        #endregion

        // GK > Refacorting could be use for splitting the NON-orderprocessing POS errors out of this file
        #region Pos (Connector) Related 500 - 1499

        #region Generic Pos Errors

        #region Non-Connector specific non-fatal order processing errors (500 - 549)

        /// <summary>
        /// An error happened processing to the POS that's not fatal
        /// </summary>
        PosErrorUnspecifiedNonFatalPosProblem = 500,

        /// <summary>
        /// The delivery point is locked on the POS
        /// </summary>
        PosErrorDeliverypointLocked = 501,

        /// <summary>
        /// A connection failure happened that could be reattempted
        /// </summary>
        PosErrorConnectivityProblem = 502,

        /// <summary>
        /// There's something not acceptable by the POS about the product(s) and/or alteration(s)
        /// </summary>
        PosErrorProductAndOrAlterationConfigurationInvalid = 503,

        #endregion

        #region Non-Connector specific fatal Errors (550 - 599)

        /// <summary>
        /// An error happened processing to the POS that's fatal
        /// </summary>
        PosErrorUnspecifiedFatalPosProblem = 550,

        /// <summary>
        /// Order could not be converted to POS order
        /// </summary>
        PosErrorCouldNotInitializeConnector = 551,

        /// <summary>
        /// Order could not be converted to POS order
        /// </summary>
        PosErrorUnknownPosItem = 552,

        /// <summary>
        /// API interface gave unknown error.
        /// </summary>
        PosErrorApiInterfaceException = 553,

        /// <summary>
        /// Could not print via POS
        /// </summary>
        PosErrorPrintingError = 554,

        /// <summary>
        /// Save order failed via POS
        /// </summary>
        PosErrorSaveOrderFailed = 555,

        /// <summary>
        /// Any conversion from one data type to another that goes wrong
        /// </summary>
        PosErrorNonParseableOrConvertableDataType = 556,

        /// <summary>
        /// Configuration Incomplete
        /// </summary>
        PosErrorConfigurationIncomplete = 557,

        /// <summary>
        /// Configuration Incorrect
        /// </summary>
        PosErrorConfigurationIncorrect = 558,

        /// <summary>
        /// Payment method not implemented.
        /// </summary>
        PosErrorPaymentmethodNotImplemented = 559,

        /// <summary>
        /// Incompatible POS product external id.
        /// </summary>
        IncompatiblePosProductExternalId = 560,

        /// <summary>
        /// Incompatible POS alteration external id.
        /// </summary>
        IncompatiblePosAlterationExternalId = 561,

        /// <summary>
        /// Incompatible POS alteration option external id.
        /// </summary>
        IncompatiblePosAlterationOptionExternalId = 562,

        #endregion

        #region Pos Synchronisation Errors (600 - 629)

        /// <summary>
        /// Could not retrieve POS Products
        /// </summary>
        PosErrorCouldNotGetPosProducts = 600,

        /// <summary>
        /// Could not retrieve POS Categories
        /// </summary>
        PosErrorCouldNotGetPosCategories = 601,

        /// <summary>
        /// Could not retrieve POS delivery point groups
        /// </summary>
        PosErrorCouldNotGetPosDeliverypointGroups = 602,

        /// <summary>
        /// Could not retrieve POS delivery points.
        /// </summary>
        PosErrorCouldNotGetPosDeliverypoints = 603,

        /// <summary>
        /// Could not retrieve POS delivery points.
        /// </summary>
        PosErrorCouldNotGetPosOrders = 604,

        #endregion

        #endregion

        #region Vectron 750-799
        /// <summary>
        /// An exception was thrown while trying to display the output of the Flexipos ordering.
        /// </summary>
        VectronCouldNotDisplayOut = 750,

        /// <summary>
        /// The clerk could not be opened!
        /// </summary>
        VectronClerkCouldNotBeOpened = 751,

        /// <summary>
        /// The Guest check could not be opened
        /// </summary>
        VectronGuestCheckCouldNotBeOpened = 753,

        /// <summary>
        /// Product could not be added to check
        /// </summary>
        VectronProductCouldNotBeAddedToCheck = 755,

        /// <summary>
        /// Could not close guest check
        /// </summary>
        VectronCouldNotCloseGuestCheck = 756,

        /// <summary>
        /// Could not close guest check
        /// </summary>
        VectronCouldNotCloseClerk = 757,

        /// <summary>
        /// Could not hide output
        /// </summary>
        VectronCouldNotHideOutput = 758,

        /// <summary>
        /// FlexiPos could not be killed
        /// </summary>
        VectronFlexiPosCouldNotBeKilled = 759,

        /// <summary>
        /// Vectron POS Products File Not Found
        /// </summary>
        VectronPosProductsFileNotFound = 760,

        /// <summary>
        /// Vectron POS Categories File Not Found
        /// </summary>
        VectronPosCategoriesFileNotFound = 761,

        #endregion

        #region Aloha 800 - 849

        /// <summary>
        /// Aloha Could Not Get Profiles
        /// </summary>
        AlohaCouldNotGetProfiles = 800,

        /// <summary>
        /// Aloha Could Not Set Profile.
        /// </summary>
        AlohaCouldNotSetProfile = 801,

        /// <summary>
        /// Aloha Could Not Login Employee.
        /// </summary>        
        AlohaCouldNotLoginEmployee = 802,

        /// <summary>
        /// Aloha Could Not Clock In Employee.
        /// </summary>
        AlohaCouldNotClockInEmployee = 803,

        /// <summary>
        /// Aloha Could Not Get Open Tables.
        /// </summary>
        AlohaCouldNotGetOpenTables = 804,

        /// <summary>
        /// Aloha Could Not Get Table Details.
        /// </summary>
        AlohaCouldNotGetTableDetails = 805,

        /// <summary>
        /// Aloha Could Not Open Table.
        /// </summary>
        AlohaCouldNotOpenTable = 806,

        /// <summary>
        /// Aloha Could Not Add Items To Check.
        /// </summary>
        AlohaCouldNotAddItemsToCheck = 807,

        /// <summary>
        /// Aloha Could Not End Check.
        /// </summary>
        AlohaCouldNotEndCheck = 808,

        /// <summary>
        /// Aloha Could Not End Table.
        /// </summary>
        AlohaCouldNotEndTable = 809,

        /// <summary>
        /// Aloha Could Not Log Out Employee.
        /// </summary>
        AlohaCouldNotLogOutEmployee = 810,

        /// <summary>
        /// Aloha could not open check
        /// </summary>
        AlohaCouldNotOpenCheck = 811,

        /// <summary>
        /// Aloha Could Not get check details
        /// </summary>
        AlohaCouldNotGetCheckDetails = 812,

        #endregion

        #region Unitouch 850 - 899

        /// <summary>
        /// Could not open table
        /// </summary>
        UnitouchCouldNotOpenTable = 850,

        /// <summary>
        /// Table was not yet opened
        /// </summary>        
        UnitouchTableWasNotOpenedYet = 851,

        /// <summary>
        /// Could not set user
        /// </summary>
        UnitouchCouldNotSetUser = 852,

        /// <summary>
        /// Could not get bill
        /// </summary>        
        UnitouchCouldNotGetBill = 853,

        /// <summary>
        /// Could not put order
        /// </summary>        
        UnitouchCouldNotPutOrder = 854,

        /// <summary>
        /// Could not put order items
        /// </summary>        
        UnitouchCouldNotPutOrderitems = 855,

        #endregion

        #region MicrosMcp 900 - 949

        /// <summary>
        /// Could not get session.
        /// </summary>
        MicrosMcpCouldntGetSession = 900,

        /// <summary>
        /// Could not get store.
        /// </summary>
        MicrosMcpCouldntGetStore = 901,

        /// <summary>
        /// Could not clear order.
        /// </summary>
        MicrosMcpCouldntClearOrder = 902,

        /// <summary>
        /// Session was not initialized.
        /// </summary>
        MicrosMcpSessionWasntInitialized = 903,

        /// <summary>
        /// Store was not initialized.
        /// </summary>
        MicrosMcpStoreWasntInitialized = 904,

        /// <summary>
        /// Menu was not initialized.
        /// </summary>
        MicrosMcpMenuWasntInitialized = 905,

        #endregion

        #endregion

        #region HotSOS 1500 - 1549

        /// <summary>
        /// No issue connected to product.
        /// </summary>
        HotSOSNoIssueConnectedToProduct = 1500,

        /// <summary>
        /// No room connected to delivery point.
        /// </summary>
        HotSOSNoRoomConnectedToDeliverypoint = 1501,

        #endregion

        #region Failures

        /// <summary>
        /// The order was cancelled by the customer
        /// </summary>
        UnspecifiedSendingMailError = 9994,

        /// <summary>
        /// The order was cancelled by the customer
        /// </summary>
        SmtpServerError = 9995,

        /// <summary>
        /// The order has timed out.
        /// </summary>
        TimedOut = 9996,

        /// <summary>
        /// The order was cancelled by the customer
        /// </summary>
        Unroutable = 9997,

        /// <summary>
        /// The order was cancelled by the customer
        /// </summary>
        CancelledByCustomer = 9998,

        #endregion
    }
}
