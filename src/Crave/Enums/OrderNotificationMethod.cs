﻿using System;

namespace Crave.Enums
{
    [Flags]
    public enum OrderNotificationMethod
    {
        None = 0,
        Email = 1,
        SMS = 2,
        EmailSMS = Email | SMS
    }
}