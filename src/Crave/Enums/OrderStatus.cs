﻿using System;
using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// This should only indicate the status of the order, not the cause of a certain (failed) status.
    /// (In the past this enumerator combined status and cause, this is now moved to order route step handlers 
    /// and .ErrorCode on the Order which is populated based on OrderProcessingError enumerator.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum OrderStatus : int
    {
        /// <summary>
        /// Order status is unknown
        /// </summary>
        [Stylesheet("warning")]
        NotRouted = 0,

        /// <summary>
        /// Waiting for the order to be paid by the customer
        /// </summary>
        [Stylesheet("info")]
        WaitingForPayment = 50,

        /// <summary>
        /// Customer has paid the order, it can now be further processed
        /// </summary>
        [Stylesheet("default")]
        Paid = 60,

        /// <summary>
        /// The order has been routed, waiting for the RoutingHelper to Start it.
        /// </summary>
        [Stylesheet("default")]
        Routed = 150,

        /// <summary>
        /// The order has been routed, and the steps have started being processed
        /// </summary>
        [Stylesheet("primary")]
        InRoute = 175,

        /// <summary>
        /// Wait for the order to be paid for by the customer
        /// </summary>
        [Stylesheet("info")]
        [Obsolete("Use WaitingForPayment instead")]
        WaitingForPaymentCompleted = 301,

        /// <summary>
        /// The order is processed = done
        /// </summary>
        [Stylesheet("success")]
        Processed = 1000,

        /// <summary>
        /// The order cannot be processed
        /// </summary>
        [Stylesheet("danger")]
        Unprocessable = 9999
    }
}
