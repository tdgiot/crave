﻿using Crave.Attributes;
using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Defines the type of order it is, which 
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum OrderType : int
    {
        /// <summary>
        /// Standard order, print receipt and serve
        /// </summary>        
        Standard = 200,

        /// <summary>
        /// Request for service
        /// </summary>
        [DoesNotRequireOrderitems]
        RequestForService = 500,

        /// <summary>
        /// Request for checkout
        /// </summary>
        [DoesNotRequireOrderitems]
        RequestForCheckout = 600,

        /// <summary>
        /// Request for checkout
        /// </summary>
        UnlockDeliverypointRequest = 700,

        /// <summary>
        /// Request for battery charge
        /// </summary>
        [DoesNotRequireOrderitems]
        RequestForBatteryCharge = 800,

        /// <summary>
        /// Request for printing
        /// </summary>
        [DoesNotRequireOrderitems]
        RequestForPrint = 900,

        /// <summary>
        /// New document
        /// </summary>
        [NoPrinting]
        Document = 1000,

        /// <summary>
        /// Wake up
        /// </summary>
        [DoesNotRequireOrderitems]
        [NoPrinting]
        RequestForWakeUp = 1100,
    }
}
