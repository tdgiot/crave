﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the result of saving an order item.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum OrderitemSaveResult
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        Success = 100,

        /// <summary>
        /// QuantityLessThanOrZero
        /// </summary>
        QuantityLessThanOrZero = 201,

        /// <summary>
        /// PriceExLessThanZero
        /// </summary>
        PriceExLessThanZero = 202,

        /// <summary>
        /// VatPercentageLessThanZero
        /// </summary>
        VatPercentageLessThanZero = 203,

        /// <summary>
        /// PriceExSubtotalInCents larger or smaller than int
        /// </summary>
        PriceExSubtotalInCentsLargerOrSmallerThanInt = 204,

        /// <summary>
        /// Change not allowed to an existing orderitem
        /// </summary>
        ChangeNotAllowedToExistingOrderitem = 205,

        /// <summary>
        /// Orderitems of paid orders can't be deleted
        /// </summary>
        OrderitemIsNotValidatedByOrderProcessingHelper = 207,
    }
}
