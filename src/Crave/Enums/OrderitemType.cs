﻿using Crave.Attributes;
using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Defines the type of order item it is
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum OrderitemType
    {
        /// <summary>
        /// Product
        /// </summary>        
        Product = 1,

        /// <summary>
        /// Tip
        /// </summary>
        Tip = 2,

        /// <summary>
        /// Service charge
        /// </summary>
        ServiceCharge = 3,

        /// <summary>
        /// Delivery charge
        /// </summary>
        DeliveryCharge = 4,

        /// <summary>
        /// Discount
        /// </summary>
        Discount = 5,

        /// <summary>
        /// Eat out to help out discount
        /// </summary>
        EatOutToHelpOutDiscount = 999
    }
}
