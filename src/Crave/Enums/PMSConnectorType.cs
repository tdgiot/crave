﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of PMS connector.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum PMSConnectorType : int
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Comtrol PMS.
        /// </summary>
        Comtrol = 1,

        /// <summary>
        /// Tiger TMS.
        /// </summary>
        Tigertms = 2,
    }
}
