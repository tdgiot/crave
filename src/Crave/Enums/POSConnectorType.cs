﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which defines the POS system to connect with
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum POSConnectorType : int
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /*
        /// <summary>
        /// Tonit POS.
        /// </summary>
        Tonit = 1,

        /// <summary>
        /// Vectron POS.
        /// </summary>
        Vectron = 3,

        /// <summary>
        /// unTill POS.
        /// </summary>
        Untill = 4,

        /// <summary>
        /// HoreCat POS.
        /// </summary>
        HoreCat = 5,

        /// <summary>
        /// Relax software POS.
        /// </summary>
        Relax = 6,

        /// <summary>
        /// Unitouch POS.
        /// </summary>
        Unitouch = 7,

        /// <summary>
        /// Aloha POS.
        /// </summary>
        Aloha = 8,

        /// <summary>
        /// ICR touch POS.
        /// </summary>
        Icrtouch = 9,

        /// <summary>
        /// unTill 2 POS.
        /// </summary>
        Untill2 = 10,

        /// <summary>
        /// Future POS.
        /// </summary>
        FuturePos = 11,

        /// <summary>
        /// Cenium POS.
        /// </summary>
        Cenium = 12,

        /// <summary>
        /// Micros MCP POS.
        /// </summary>
        MicrosMcp = 13,

        /// <summary>
        /// Torex Alpha POS.
        /// </summary>
        TorexAlpha = 1000,
        */
    }
}
