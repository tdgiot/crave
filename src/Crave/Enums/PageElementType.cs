﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a page element.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum PageElementType : int
    {
        /// <summary>
        /// Single line text.
        /// </summary>
        SingleLineText = 100,

        /// <summary>
        /// Multi line text.
        /// </summary>
        MultiLineText = 200,

        /// <summary>
        /// An image.
        /// </summary>
        Image = 300,

        /// <summary>
        /// A web view.
        /// </summary>
        WebView = 400,

        /// <summary>
        /// YouTube element.
        /// </summary>
        YouTube = 500,

        /// <summary>
        /// A map.
        /// </summary>
        Map = 700,

        /// <summary>
        /// A venue.
        /// </summary>
        Venue = 800,

        /// <summary>
        /// A page link.
        /// </summary>
        PageLink = 900
    }
}
