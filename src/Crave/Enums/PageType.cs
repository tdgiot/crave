﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a page.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum PageType
    {
        /// <summary>
        /// Text with image left.
        /// </summary>
        TextWithImageLeft = 1,

        /// <summary>
        /// Text with image right.
        /// </summary>
        TextWithImageRight = 2,

        /// <summary>
        /// Text with image left transparent.
        /// </summary>
        TextWithImageLeftTransparent = 3,

        /// <summary>
        /// Text with image right transparent.
        /// </summary>
        TextWithImageRightTransparent = 4,

        /// <summary>
        /// Text with images bottom.
        /// </summary>
        TextWithImagesBottom = 5,

        /// <summary>
        /// Single image.
        /// </summary>
        SingleImage = 6,

        /// <summary>
        /// YouTube video.
        /// </summary>
        YouTube = 7,

        /// <summary>
        /// Google Maps.
        /// </summary>
        Map = 8,

        /// <summary>
        /// Web view.
        /// </summary>
        WebView = 9,

        /// <summary>
        /// A venue.
        /// </summary>
        Venue = 10,

        /// <summary>
        /// Web link.
        /// </summary>
        WebLink = 11,

        /// <summary>
        /// Page link.
        /// </summary>
        PageLink = 12
    }
}
