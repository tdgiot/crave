﻿namespace Crave.Enums
{
    public enum PaymentProviderEnvironment
    {
        Live = 1,
        Test = 2,
    }
}