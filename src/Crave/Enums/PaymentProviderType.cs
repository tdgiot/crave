﻿namespace Crave.Enums
{
    public enum PaymentProviderType
    {
        Adyen = 1,
        Mollie = 2,
        Omise = 3,
        AdyenHpp = 4,
        AdyenForPlatforms = 5
    }
}