﻿namespace Crave.Enums
{
    public enum PaymentSplitType
    {
        BalanceAccount = 1,
        Commission = 2,
        Default = 3,
        MarketPlace = 4,
        PaymentFee = 5,
        VAT = 6,
        Verification = 7
    }
}