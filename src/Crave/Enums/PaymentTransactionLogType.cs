﻿namespace Crave.Enums
{
    public enum PaymentTransactionLogType
    {
        Generic = 0,
        Payment = 1,
        Refund = 2,
        Chargeback = 3,
        Cancelled = 4,
        Failed = 10
    }
}