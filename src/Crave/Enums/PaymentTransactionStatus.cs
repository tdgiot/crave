﻿namespace Crave.Enums
{
    public enum PaymentTransactionStatus
    {
        Pending = 0, // Default state
        Initiated = 1,
        Paid = 2,
        Failed = 3,
        Cancelled = 4,
        Error = 100
    }
}