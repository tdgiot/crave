﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// PMS action type
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum PmsActionType
    {
        /// <summary>
        /// Add to messagegroup
        /// </summary>
        AddToMessagegroup = 0,
        /// <summary>
        /// Send message
        /// </summary>
        SendMessage = 1,
        /// <summary>
        /// Change client configuration
        /// </summary>
        ChangeClientConfiguration = 2,
    }
}