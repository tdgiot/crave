﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// PMS report type
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum PmsReportType
    {
        /// <summary>
        /// Plain text
        /// </summary>
        PlainText = 0,
        /// <summary>
        /// Excel
        /// </summary>
        Excel = 1        
    }
}