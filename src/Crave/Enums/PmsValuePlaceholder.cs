﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Pms value placeholder
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum PmsValuePlaceholder
    {
        /// <summary>
        /// Today
        /// </summary>        
        Today = 0,
        /// <summary>
        /// Tomorrow
        /// </summary>        
        Tomorrow = 1
    }
}