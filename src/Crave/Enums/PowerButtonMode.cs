﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumerator which represents the mode when the power button is pressed.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum PowerButtonMode
    {
        /// <summary>
        /// Power dialog.
        /// </summary>
        Dialog = 0,

        /// <summary>
        /// Screen Off.
        /// </summary>
        ScreenOff = 1,
    }
}
