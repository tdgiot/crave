﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the way prices are formatted. 
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum PriceFormatType
    {
        /// <summary>
        /// No decimal (e.g. $5)
        /// </summary>        
        NoDecimal = 0,

        /// <summary>
        /// One decimal (e.g. $5.0)
        /// </summary>
        OneDecimal = 1,

        /// <summary>
        /// Two decimals (e.g. $5.00)
        /// </summary>
        TwoDecimals = 2
    }
}
