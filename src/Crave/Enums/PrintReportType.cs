﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration type which represents the type of printed report.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum PrintReportType : int
    {
        /// <summary>
        /// Standard report.
        /// </summary>
        Standaard = 100
    }
}