﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the way a product is displayed. 
    /// </summary>
    /// <remarks>
    /// When changing anything in this enumerator you also have to change it in Product.aspx.cs (CMS)
    /// </remarks>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ProductSubType
    {
        /// <summary>
        /// Inherit from parent category. 
        /// </summary>
        Inherit = 0,

        /// <summary>
        /// Product will be added to order basket.
        /// </summary>
        Normal = 1,

        /// <summary>
        /// Product is displayed as service item and will directly being sent when ordered.
        /// </summary>
        ServiceItems = 2,

        /// <summary>
        /// Product is displayed as directory item (non-orderable).
        /// </summary>
        Directory = 3,

        /// <summary>
        /// Product is displayed as a web link.
        /// </summary>
        WebLink = 4,

        /// <summary>
        /// Product is an internal system product (eg, service/delivery charge, tip)
        /// </summary>
        SystemProduct = 5
    }
}
