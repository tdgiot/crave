﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumerator which represents the type of a product.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ProductType : int
    {
        /// <summary>
        /// A product that is added to the order basket when it is ordered.
        /// </summary>
        Product = 200,

        /// <summary>
        /// Service item that is directly sent as an order when it is ordered.
        /// </summary>
        Service = 300,

        /// <summary>
        /// Payment method that is used on legacy systems.
        /// </summary>
        Paymentmethod = 400,

        /// <summary>
        /// Delivery point. 
        /// </summary>
        Deliverypoint = 500,

        /// <summary>
        /// Tablet that is used on legacy systems.
        /// </summary>
        Tablet = 600,

        /// <summary>
        /// Category TO DO
        /// </summary>
        Category = 700,

        /// <summary>
        /// POS service message.
        /// </summary>
        PosServiceMessage = 800,

        /// <summary>
        /// Email document.
        /// </summary>
        Document = 900,

        /// <summary>
        /// PMS checkout.
        /// </summary>
        PmsCheckout = 1000,
    }
}
