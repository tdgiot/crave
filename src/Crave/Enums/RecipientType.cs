﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the RecipientType of a message.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RecipientType
    {
        /// <summary>
        /// Unknown.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Customer.
        /// </summary>
        Customer = 1,

        /// <summary>
        /// Client.
        /// </summary>
        Client = 2,

        /// <summary>
        /// Deliverypoint.
        /// </summary>
        Deliverypoint = 3
    }
}
