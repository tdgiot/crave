﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// RecordType enum for search results.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RecordType
    {
        /// <summary>
        /// Product Entity
        /// </summary>
        [Stylesheet("default")]
        Product = 0,

        /// <summary>
        /// Delivery point Entity
        /// </summary>
        [Stylesheet("primary")]
        Deliverypoint = 1,

        /// <summary>
        /// Delivery point group Entity
        /// </summary>
        [Stylesheet("dark-blue")]
        Deliverypointgroup = 2,

        /// <summary>
        /// Order Entity
        /// </summary>
        [Stylesheet("green")]
        Order = 4,

        /// <summary>
        /// Client Entity
        /// </summary>
        [Stylesheet("success")]
        Client = 8,

        /// <summary>
        /// Terminal Entity
        /// </summary>
        [Stylesheet("info")]
        Terminal = 16,

        /// <summary>
        /// Alteration Entity
        /// </summary>
        [Stylesheet("danger")]
        Alteration = 32,

        /// <summary>
        /// Alterationoption Entity
        /// </summary>
        [Stylesheet("warning")]
        Alterationoption = 64,

        /// <summary>
        /// Error message if no results are found.
        /// </summary>
        [Stylesheet("danger")]
        ErrorMessage = 99999
    }
}
