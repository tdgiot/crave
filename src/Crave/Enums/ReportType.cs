﻿namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the different reporting types
    /// </summary>
    public enum ReportType
    {
        /// <summary>
        /// Analytics - Order transactions
        /// </summary>
        Transactions = 1,

        /// <summary>
        /// Analytics - Google pageviews
        /// </summary>
        PageViews = 2,

        /// <summary>
        /// Analytics - Combination report for marketing
        /// </summary>
        Marketing = 3,

        /// <summary>
        /// Content - Check broken urls
        /// </summary>
        BrokenLinks = 4,

        /// <summary>
        /// Wake-up calls
        /// </summary>
        WakeUpCalls = 5,

        /// <summary>
        /// No-one knows...
        /// </summary>
        Batch = 6,

        /// <summary>
        /// Product overview
        /// </summary>
        ProductOverview = 7,

        /// <summary>
        /// Key performance indicators
        /// </summary>
        Kpi = 8,
    }
}