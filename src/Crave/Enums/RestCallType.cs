﻿namespace Crave.Enums
{
    public enum RestCallType
    {
        Post,
        Get,
        Put,
        Patch,
        Delete
    }
}