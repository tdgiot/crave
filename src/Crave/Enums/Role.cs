﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the role of a user.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum Role
    {
        /// <summary>
        /// God mode role
        /// </summary>
        GodMode = 9999,

        /// <summary>
        /// Crave role
        /// </summary>
        Crave = 5000,

        /// <summary>
        /// Administrator role
        /// </summary>
        Administrator = 1000,

        /// <summary>
        /// Reseller role
        /// </summary>
        Reseller = 500,

        /// <summary>
        /// Supervisor role
        /// </summary>
        Supervisor = 250,

        /// <summary>
        /// Manager role
        /// </summary>
        Manager = 200,

        /// <summary>
        /// Console role
        /// </summary>
        Console = 150,

        /// <summary>
        /// Staff/Customer role
        /// </summary>
        Staff = 100
    }
}
