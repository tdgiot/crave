﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of room control area.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RoomControlAreaType : int
    {
        /// <summary>
        /// Normal area (room controls for one area).
        /// </summary>
        Normal = 1,

        /// <summary>
        /// Show all area (room controls for all areas).
        /// </summary>
        ShowAll = 2,
    }
}
