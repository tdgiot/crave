﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a room control component.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RoomControlComponentType : int
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Light component.
        /// </summary>
        Light = 1,

        /// <summary>
        /// Lighting scene.
        /// </summary>
        LightingScene = 2,

        /// <summary>
        /// Thermostat component.
        /// </summary>
        Thermostat = 3,

        /// <summary>
        /// Blind component.
        /// </summary>
        Blind = 4,

        /// <summary>
        /// Service component.
        /// </summary>
        Service = 5,
    }
}
