﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of room control section item.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RoomControlSectionItemType : int
    {
        /// <summary>
        /// Lighting scene.
        /// </summary>
        Scene = 1,

        /// <summary>
        /// Station Listing.
        /// </summary>
        StationListing = 2,

        /// <summary>
        /// Wake up timer.
        /// </summary>
        WakeUpTimer = 3,

        /// <summary>
        /// Sleep timer.
        /// </summary>
        SleepTimer = 4,

        /// <summary>
        /// Remote control.
        /// </summary>
        RemoteControl = 5,
    }
}
