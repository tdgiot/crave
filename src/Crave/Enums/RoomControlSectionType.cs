﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a room control section.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RoomControlSectionType : int
    {
        /// <summary>
        /// Lighting section containing lights and lighting scenes.
        /// </summary>
        Lighting = 1,

        /// <summary>
        /// Comfort section containing thermostats.
        /// </summary>
        Comfort = 2,

        /// <summary>
        /// Drapes section containing blinds, drapes and shapes.
        /// </summary>
        Blinds = 3,

        /// <summary>
        /// Service section containing Do-Not-Disturb and Clean Room.
        /// </summary>
        Service = 4,

        /// <summary>
        /// Screen sleep section, which turns the screen to sleep.
        /// </summary>
        ScreenSleep = 5,

        /// <summary>
        /// Dashboard section containing the room control dashboard.
        /// </summary>
        Dashboard = 6,

        /// <summary>
        /// Timers section containing the wake-up and sleep timers.
        /// </summary>
        Timers = 7,

        /// <summary>
        /// Custom section containing custom items.
        /// </summary>
        Custom = 99
    }
}
