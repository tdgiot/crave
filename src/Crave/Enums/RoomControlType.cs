﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of the room control integration.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RoomControlType
    {
        /// <summary>
        /// None. Default.
        /// </summary>
        None = -1,

        /// <summary>
        /// Demo room controls.
        /// </summary>
        Demo = 0,

        /// <summary>
        /// Control4 integration using the SimpleAPI driver.
        /// </summary>
        Control4 = 1,

        /// <summary>
        /// Control4 integration using the Crave driver.
        /// </summary>
        C4CraveApi = 2,

        /// <summary>
        /// Modbus integration.
        /// </summary>
        Modbus = 3,

        /// <summary>
        /// Inncom integration
        /// </summary>
        Inncom = 4,

        /// <summary>
        /// Interel integration
        /// </summary>
        Interel = 5
    }
}