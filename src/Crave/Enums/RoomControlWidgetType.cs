﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a room control widget.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RoomControlWidgetType : int
    {
        /// <summary>
        /// Light (2x1).
        /// </summary>
        Light2x1 = 1,

        /// <summary>
        /// Scene (1x1).
        /// </summary>
        Scene1x1 = 2,

        /// <summary>
        /// Blind (1x2).
        /// </summary>
        Blind1x2 = 3,

        /// <summary>
        /// Thermostat (4x2).
        /// </summary>
        Thermostat4x2 = 4,

        /// <summary>
        /// Volume (1x2).
        /// </summary>
        Volume1x2 = 5,

        /// <summary>
        /// Light (1x1).
        /// </summary>
        Light1x1 = 6,

        /// <summary>
        /// Channel (1x2).
        /// </summary>
        Channel1x2 = 7,

        /// <summary>
        /// Navigation (1x2).
        /// </summary>
        Navigation1x2 = 8,

        /// <summary>
        /// Volume (1x4).
        /// </summary>
        Volume1x4 = 9,

        /// <summary>
        /// Channel (1x4).
        /// </summary>
        Channel1x4 = 10,

        /// <summary>
        /// Navigation (2x3).
        /// </summary>
        Navigation2x3 = 11,

        /// <summary>
        /// Numpad (2x3).
        /// </summary>
        Numpad2x3 = 12,

        /// <summary>
        /// Power button (1x1).
        /// </summary>
        PowerButton1x1 = 13,

        /// <summary>
        /// Blind (1x1).
        /// </summary>
        Blind1x1 = 14,

        /// <summary>
        /// Channel (1x1).
        /// </summary>
        Channel1x1 = 15,

        /// <summary>
        /// Volume (1x1).
        /// </summary>
        Volume1x1 = 16,

        /// <summary>
        /// Area Blinds (1x1).
        /// </summary>
        AreaBlinds1x2 = 17,

        /// <summary>
        /// Placeholder (1x1).
        /// </summary>
        Placeholder1x1 = 101,

        /// <summary>
        /// Placeholder (1x2).
        /// </summary>
        Placeholder1x2 = 102,
    }
}