﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the variant of the alteration dialog.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RoomControllerType : int
    {
        /// <summary>
        /// No room controls.
        /// </summary>
        None = -1,

        /// <summary>
        /// Demo room controls.
        /// </summary>
        Demo = 0,

        /// <summary>
        /// Control 4 room controls using SimpleAPI driver.
        /// </summary>
        Control4 = 1,

        /// <summary>
        /// Control 4 room controls using Crave driver.
        /// </summary>
        C4CraveApi = 2,

        /// <summary>
        /// Modbus room controls.
        /// </summary>
        Modbus = 3,

        /// <summary>
        /// Inncom room controls.
        /// </summary>
        Inncom = 4
    }
}
