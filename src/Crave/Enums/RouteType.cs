﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a route (normal, system messages, email documents, etc).
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RouteType
    {
        /// <summary>
        /// Normal route.
        /// </summary>
        Normal = 0,

        /// <summary>
        /// System message route.
        /// </summary>
        SystemMessage = 1,

        /// <summary>
        /// Order notes route.
        /// </summary>
        OrderNotes = 2,

        /// <summary>
        /// Email document route.
        /// </summary>
        EmailDocument = 3
    }
}
