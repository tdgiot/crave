﻿using System;
using Crave.Attributes;
using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a route step handler.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RoutestephandlerType : int
    {
        /// <summary>
        /// The route step is handled by a Console.
        /// </summary>
        [TerminalType(TerminalType.Console)]
        Console = 100,

        /// <summary>
        /// The route step is handled by a printer.
        /// </summary>
        [TerminalType(TerminalType.Console, TerminalType.OnSiteServer)]
        [AutomaticallyHandled]
        Printer = 200,

        /// <summary>
        /// The route step is handled by a POS.
        /// </summary>
        [TerminalType(TerminalType.OnSiteServer)]
        [AutomaticallyHandled]
        POS = 300,

        /// <summary>
        /// The route step is handled by sending an email of the order.
        /// </summary>
        [AutomaticallyHandled]
        [NoTerminal]
        EmailOrder = 400,

        /// <summary>
        /// The route step is handled by sending a SMS of the order.
        /// </summary>
        [AutomaticallyHandled]
        [NoTerminal]
        SMS = 500,

        /// <summary>
        /// The route step is handled by a HotSOS integration.
        /// </summary>
        [AutomaticallyHandled]
        [NoTerminal]
        HotSOS = 600,

        /// <summary>
        /// The route step is handled by a Quore integration.
        /// </summary>
        [AutomaticallyHandled]
        [NoTerminal]
        Quore = 601,

        /// <summary>
        /// The route step is handled by a Hyatt integration.
        /// </summary>
        [AutomaticallyHandled]
        [NoTerminal]
        Hyatt = 602,

        /// <summary>
        /// The route step is handled by an Alice integration.
        /// </summary>
        [AutomaticallyHandled]
        [NoTerminal]
        Alice = 603,

        /// <summary>
        /// The route step is handled by an external system.
        /// </summary>
        [AutomaticallyHandled]
        [NoTerminal]
        ExternalSystem = 604,

        /// <summary>
        /// The route step us handled by sending an email with a document.
        /// </summary>
        [TerminalType(TerminalType.Console)]
        [AutomaticallyHandled]
        [NoTerminal]
        EmailDocument = 700
    }
}
