﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the result of routing an order.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RoutingResult
    {
        /// <summary>
        ///     Route is ambigious cause there are multiple categories with different routes
        /// </summary>
        AmbigiousRouteDueToMultipleCategoriesWithDifferentRoutes = 200,

        /// <summary>
        ///     Multiple active transactions are used
        /// </summary>
        MultipleActiveTransactions = 201,

        /// <summary>
        ///     Required parameters are missing
        /// </summary>
        RequiredParametersMissing = 202,

        /// <summary>
        ///     The order doesn't have a route
        /// </summary>
        NoRouteForOrder = 203,

        /// <summary>
        ///     No routesteps for this route
        /// </summary>
        RouteHasZeroSteps = 204,

        /// <summary>
        ///     The route doesn't start with number one
        /// </summary>
        RouteDoesNotStartWithNumberOne = 205,

        /// <summary>
        ///     The orderRoutestephandlerId is defined twice
        /// </summary>
        OrderroutestephandlerIdDefinedTwiceForUpdate = 206,

        /// <summary>
        ///     The orderRoutestephandlerId and the statusList aren't equal
        /// </summary>
        OrderroutestephandlerIdAndStatusListsAreUnequal = 207,

        /// <summary>
        ///     The orderRoutestep update is missing
        /// </summary>
        OrderroutestepUpdateMissing = 208,

        /// <summary>
        ///     The data is inconsistent
        /// </summary>
        InconsistentData = 209,

        /// <summary>
        ///     Paremeters are missing
        /// </summary>
        ParametersMissing = 210,

        /// <summary>
        ///     There are multiple orderRoutesteps active for the same terminal
        /// </summary>
        MultipleOrderRouteStepsForSameTerminalActive = 211,

        /// <summary>
        ///     There aren't any handlers for the routeStep
        /// </summary>
        RoutestepHasZeroHandlers = 212,

        /// <summary>
        ///     The orderRoutestep doesn't have any terminal
        /// </summary>
        NoTerminalSpecifiedForOrderRoutestep = 213,

        /// <summary>
        ///     The terminal used in the route is offline
        /// </summary>
        TerminalOfflineInRoute = 214,

        /// <summary>
        ///     Potential Stack overflow has been prevented
        /// </summary>
        PotentialStackOverflowPrevented = 215,

        /// <summary>
        ///     An attempt was made to change the Timeout on the orderRoutestephandler
        /// </summary>
        AttemptedToChangeTimedoutOrderRoutestephandler = 216,

        /// <summary>
        ///     An attempt was made to change a failed orderRoutestephandler
        /// </summary>
        AttemptedToChangeFailedOrderRoutestephandler = 217,

        /// <summary>
        ///     Attempt was made to update an orderRoutestephandler while terminal was not the owner of this step or has been set
        ///     as master
        /// </summary>
        AttemptToChangeOrderRoutestephandlerWhileNotOwnerOrMaster = 218
    }
}
