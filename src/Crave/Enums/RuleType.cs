﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Rule type
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum RuleType
    {
        /// <summary>
        /// Equals
        /// </summary>
        Equals = 0,
        /// <summary>
        /// Does not equal
        /// </summary>
        DoesNotEqual = 1,
        /// <summary>
        /// Is greater than
        /// </summary>
        IsGreaterThan = 2,
        /// <summary>
        /// Is greater than or equal to
        /// </summary>
        IsGreaterThanOrEqualTo = 3,
        /// <summary>
        /// Is less than
        /// </summary>
        IsLessThan = 4,
        /// <summary>
        /// Is greater than or equal to
        /// </summary>
        IsLessThanOrEqualTo = 5,
        /// <summary>
        /// Contains
        /// </summary>
        Contains = 6,
        /// <summary>
        /// Does not contain
        /// </summary>
        DoesNotContain = 7,
        /// <summary>
        /// Begins with
        /// </summary>
        BeginsWith = 8,
        /// <summary>
        /// Ends with
        /// </summary>
        EndsWith = 9,
        /// <summary>
        /// Is blank
        /// </summary>
        IsBlank = 10,
        /// <summary>
        /// Is not blank
        /// </summary>
        IsNotBlank = 11        
    }
}