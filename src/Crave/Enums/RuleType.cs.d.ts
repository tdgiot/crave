declare module server {
	/** Rule type */
	const enum ruleType {
		/** Equals */
		equals = 0,
		/** Does not equal */
		doesNotEqual = 1,
		/** Is greater than */
		isGreaterThan = 2,
		/** Is greater than or equal to */
		isGreaterThanOrEqualTo = 3,
		/** Is less than */
		isLessThan = 4,
		/** Is greater than or equal to */
		isLessThanOrEqualTo = 5,
		/** Contains */
		contains = 6,
		/** Does not contain */
		doesNotContain = 7,
		/** Begins with */
		beginsWith = 8,
		/** Ends with */
		endsWith = 9,
		/** Is blank */
		isBlank = 10,
		/** Is not blank */
		isNotBlank = 11,
	}
}
