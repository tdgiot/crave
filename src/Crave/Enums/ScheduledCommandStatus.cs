﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the status of a scheduled command.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ScheduledCommandStatus : int
    {
        /// <summary>
        /// Command is inactive.
        /// </summary>
        Inactive = 0,

        /// <summary>
        /// Command is waiting to be started.
        /// </summary>
        Pending = 100,

        /// <summary>
        /// Command is in progress.
        /// </summary>
        Started = 200,

        /// <summary>
        /// Command has succeeded.
        /// </summary>
        Succeeded = 300,

        /// <summary>
        /// Command has expired.
        /// </summary>
        Expired = 400,

        /// <summary>
        /// Command has been skipped.
        /// </summary>
        Skipped = 500,

        /// <summary>
        /// Command has failed.
        /// </summary>
        Failed = 999
    }
}
