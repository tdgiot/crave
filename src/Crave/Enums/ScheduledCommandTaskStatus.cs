﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the status of a scheduled command task.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ScheduledCommandTaskStatus : int
    {
        /// <summary>
        /// The task is inactive.
        /// </summary>
        Inactive = 0,

        /// <summary>
        /// The task is waiting to be started.
        /// </summary>
        Pending = 100,

        /// <summary>
        /// The task is in progress.
        /// </summary>
        Started = 200,

        /// <summary>
        /// The task has been completed.
        /// </summary>
        Completed = 300,

        /// <summary>
        /// The task is expired.
        /// </summary>
        Expired = 400,

        /// <summary>
        /// The task is completed with errors, meaning there were one of more failed or expired commands.
        /// </summary>
        CompletedWithErrors = 999
    }
}
