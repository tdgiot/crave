﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumerator which represents the mode when the screen is turned off.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ScreenOffMode
    {
        /// <summary>
        /// Responsive (Brightness 0%).
        /// </summary>
        Responsive = 0,

        /// <summary>
        /// Non-Responsive (Screen Off)
        /// </summary>
        NonResponsive = 1,
    }
}
