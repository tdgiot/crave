﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumerator which represents the mode of displaying the screensaver.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ScreensaverMode : int
    {
        /// <summary>
        /// Disabled i.e. no screensaver displayed.
        /// </summary>
        Disabled = 0,

        /// <summary>
        /// Screensaver is always disabled (day and night mode).
        /// </summary>
        Always = 1,

        /// <summary>
        /// Screensaver is only displayed in night mode.
        /// </summary>
        NightModeOnly = 2,
    }
}
