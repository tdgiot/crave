﻿namespace Crave.Enums
{
    /// <summary>
    /// Enumeration of service types.
    /// </summary>
    public enum ServiceMethodType
    {
        /// <summary>
        /// An order delivered to the guest room.
        /// </summary>
        RoomService,

        /// <summary>
        /// An order delivered to a table.
        /// </summary>
        TableService,

        /// <summary>
        /// An order which has to be picked up by the guest.
        /// </summary>
        PickUp,

        /// <summary>
        /// An order which has to be deliverd to the customer.
        /// </summary>
        Delivery
    }
}
