﻿namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the state of the mvc session.
    /// </summary>
    public enum SessionState
    {
        /// <summary>
        /// No active session has been found.
        /// </summary>
        None = 0,

        /// <summary>
        /// Active session has an application user.
        /// </summary>
        UserSession = 1,

        /// <summary>
        /// Active session has an application user with a selected company.
        /// </summary>
        CompanySession = 2
    }
}