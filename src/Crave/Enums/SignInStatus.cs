﻿namespace Crave.Enums
{
    public enum SignInStatus
    {
        /// <summary>
        /// Sign in was successful
        /// </summary>
        Success = 0,

        /// <summary>
        /// User is locked out
        /// </summary>
        LockedOut = 1,

        /// <summary>
        /// Sign in failed
        /// </summary>
        Failure = 2,

        /// <summary>
        /// User is not linked to an account
        /// </summary>
        UserNotLinkedToAccount = 3,

        /// <summary>
        /// No companies are linked to the account of the user
        /// </summary>
        NoCompaniesLinkedToAccount = 4
    }
}
