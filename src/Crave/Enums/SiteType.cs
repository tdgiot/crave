﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a site.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum SiteType : int
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Micro site.
        /// </summary>
        Microsite = 1,

        /// <summary>
        /// Directory site.
        /// </summary>
        Directory = 2,
    }
}
