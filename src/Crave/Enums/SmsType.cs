﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of inbound SMS.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum SmsType : int
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Request for download url.
        /// </summary>
        RequestForDownloadUrl = 1,
    }
}
