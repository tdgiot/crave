﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Types of support notification
    /// </summary>
    /// <remarks>
    /// VALUES NEED TO BE POWERS OF TWO (0,1,2,4,8,16 etc..)
    /// </remarks>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum SupportNotificationType
    {
        /// <summary>
        /// Inherit from category
        /// </summary>
        Inherit = 0,

        /// <summary>
        /// No notifications
        /// </summary>
        None = 1,

        /// <summary>
        /// 'Retrieval' timeout
        /// </summary>
        RetrievalTimeout = 2,

        /// <summary>
        /// 'On the case' timeout
        /// </summary>
        OnTheCaseTimeout = 4,

        /// <summary>
        /// 'Retrieval' and 'On the case' timeouts
        /// </summary>
        All = (RetrievalTimeout | OnTheCaseTimeout),
    }
}
