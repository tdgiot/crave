﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// System type
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum SystemType
    {
        /// <summary>
        /// Crave
        /// </summary>
        Crave = 1,
        /// <summary>
        /// Otoucho
        /// </summary>
        Otoucho = 2
    }
}