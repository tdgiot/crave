﻿using System.ComponentModel;

namespace Crave.Enums
{
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum TaxBreakdown
    {
        None = 0,

        Receipt = 1,

        ReceiptAndCheckout = 2
    }
}
