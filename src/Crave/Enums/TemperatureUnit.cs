﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the temperature scale.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum TemperatureUnit
    {
        /// <summary>
        /// Celsius temperature scale.
        /// </summary>
        Celsius = 0,

        /// <summary>
        /// Fahrenheit temperature scale.
        /// </summary>
        Fahrenheit = 1
    }
}
