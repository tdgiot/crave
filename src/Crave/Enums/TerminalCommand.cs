﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the command to send to terminals.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum TerminalCommand
    {
        /// <summary>
        /// Do not use. Does nothing.
        /// </summary>
        DoNothing = 0,

        /// <summary>
        /// Restarts the application.
        /// </summary>
        RestartApplication = 1,

        /// <summary>
        /// Restarts the tablet device.
        /// </summary>
        RestartDevice = 2,

        /// <summary>
        /// Restarts the tablet device in recovery mode.
        /// </summary>
        RestartInRecovery = 3,

        /// <summary>
        /// Sends the log to Crave.
        /// </summary>
        SendLogToWebservice = 4,

        /// <summary>
        /// Executes a non-POS data synchronization.
        /// </summary>
        NonPosDataSynchronisation = 5,

        /// <summary>
        /// Executes a POS data synchronization.
        /// </summary>
        PosDataSynchronisation = 6,

        /// <summary>
        /// Executes a menu consistency check.
        /// </summary>
        MenuConsistencyCheck = 7,

        /// <summary>
        /// Downloads an update for the Console application.
        /// </summary>
        DownloadConsoleUpdate = 8,

        /// <summary>
        /// Downloads an update for the Agent application.
        /// </summary>
        DownloadAgentUpdate = 9,

        /// <summary>
        /// Downloads an update for the SupportTools application.
        /// </summary>
        DownloadSupportToolsUpdate = 10,

        /// <summary>
        /// Installs an update for the Console application.
        /// </summary>
        InstallConsoleUpdate = 11,

        /// <summary>
        /// Installs an update for the Agent application.
        /// </summary>
        InstallAgentUpdate = 12,

        /// <summary>
        /// Installs an update for the SupportTools application.
        /// </summary>
        InstallSupportToolsUpdate = 13,

        /// <summary>
        /// Clears the browser cache.
        /// </summary>
        ClearBrowserCache = 14,

        /// <summary>
        /// Downloads an update for CraveOS.
        /// </summary>
        DownloadOSUpdate = 15,

        /// <summary>
        /// Installs an update for CraveOS.
        /// </summary>
        InstallOSUpdate = 16,
    }
}
