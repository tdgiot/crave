﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a terminal log.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum TerminalLogType : int
    {
        /// <summary>
        /// Device is being rebooted
        /// </summary>
        DeviceReboot = 50,

        /// <summary>
        /// Device is being shutdown
        /// </summary>
        DeviceShutdown = 55,

        /// <summary>
        /// The terminal was started.
        /// </summary>
        Start = 100,

        /// <summary>
        /// The terminal was stopped.
        /// </summary>
        Stop = 200,

        /// <summary>
        /// A message from the terminal.
        /// </summary>
        Message = 300,

        /// <summary>
        /// TO DO.
        /// </summary>
        ShippedLog = 301,

        /// <summary>
        /// TO DO.
        /// </summary>
        DeviceChanged = 305,

        /// <summary>
        /// TO DO.
        /// </summary>
        PosSynchronisationReport = 310,

        /// <summary>
        /// TO DO.
        /// </summary>
        NonQualifiedException = 400,

        /// <summary>
        /// TO DO.
        /// </summary>
        OrderTypeNotSupported = 401,

        /// <summary>
        /// Non qualified POS exception.
        /// </summary>
        NonQualifiedPosException = 500,

        /// <summary>
        /// Delivery point locked.
        /// </summary>
        DeliverypointLockedPosException = 501,

        /// <summary>
        /// Invalid products or alterations.
        /// </summary>
        InvalidProductsOrAlterationsPosException = 502,

        /// <summary>
        /// Unlock tables request failed.
        /// </summary>
        UnlockTablesRequestFailedPosException = 503,

        /// <summary>
        /// Connectivity problem
        /// </summary>
        ConnectivityException = 504,

        /// <summary>
        /// Unspecified non fatal POS problem.
        /// </summary>
        UnspecifiedNonFatalPosProblem = 505,

        /// <summary>
        /// Download started
        /// </summary>
        DownloadStarted = 600,

        /// <summary>
        /// Download completed
        /// </summary>
        DownloadCompleted = 601,

        /// <summary>
        /// Download failed
        /// </summary>
        DownloadFailed = 602,

        /// <summary>
        /// Installation started
        /// </summary>
        InstallationStarted = 603,

        /// <summary>
        /// Installation completed
        /// </summary>
        InstallationCompleted = 604,

        /// <summary>
        /// Installation failed
        /// </summary>
        InstallationFailed = 605,

        /// <summary>
        /// Installation failed
        /// </summary>
        ConfigurationBackupRestored = 606,

        /// <summary>
        /// Installation failed
        /// </summary>
        OsUpdated = 607,

        /// <summary>
        /// Device froze
        /// </summary>
        FreezeLog = 608,

        /// <summary>
        /// Last terminal communication method changed
        /// </summary>
        CommunicationMethodChanged = 700,
    }
}
