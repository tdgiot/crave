﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the operation mode of a terminal.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum TerminalOperationMode : int
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Terminal used in normal operation mode
        /// </summary>
        Normal = 1,
    }
}
