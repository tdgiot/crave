﻿using System;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the state of a terminal.
    /// </summary>
    /// <remarks>
    /// Values need to be powers of two (0,1,2,4,8,16,etc..)
    /// </remarks>
    [Flags]
    public enum TerminalState
    {
        /// <summary>
        /// Online i.e. the last request was less than three minutes ago.
        /// </summary>
        Online = 0,

        /// <summary>
        /// Offline i.e. the last request was more than three minutes ago.
        /// </summary>
        Offline = 1,

        /// <summary>
        /// The battery level is low.
        /// </summary>
        BatteryLow = 2,

        /// <summary>
        /// The terminal is out of battery.
        /// </summary>
        OutOfBattery = 4,

        /// <summary>
        /// Software on the terminal is outdated.
        /// </summary>
        Outdated = 8,

        /// <summary>
        /// The terminal is not loaded.
        /// </summary>
        NotLoaded = 16
    }
}
