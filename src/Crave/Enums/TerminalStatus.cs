﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the status of a terminal.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum TerminalStatus : int
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        [Stylesheet("default")]
        Unknown = 0,

        /// <summary>
        /// The terminal is OK.
        /// </summary>
        [Stylesheet("success")]
        OK = 100,

        /// <summary>
        /// The terminal has a printer error.
        /// </summary>
        [Stylesheet("warning")]
        PrinterError = 200,

        /// <summary>
        /// The terminal has a configuration error.
        /// </summary>
        [Stylesheet("warning")]
        ConfigurationError = 201,

        /// <summary>
        /// The terminal has an internet error.
        /// </summary>
        [Stylesheet("danger")]
        InternetError = 202,

        /// <summary>
        /// The terminal has a web service error.
        /// </summary>
        [Stylesheet("danger")]
        WebserviceError = 203,

        /// <summary>
        /// The terminal has a database error.
        /// </summary>
        [Stylesheet("danger")]
        DatabaseError = 204,

        /// <summary>
        /// The terminal has a POS error.
        /// </summary>
        [Stylesheet("danger")]
        POSError = 205,

        /// <summary>
        /// The application stopped on the terminal.
        /// </summary>
        [Stylesheet("danger")]
        ApplicationStopped = 206,
    }
}
