﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a terminal.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum TerminalType
    {
        /// <summary>
        /// On-site server terminal.
        /// </summary>
        OnSiteServer = 1,

        /// <summary>
        /// Console terminal.
        /// </summary>
        Console = 2,

        /// <summary>
        /// Demo terminal. Obsolete?
        /// </summary>
        Demo = 100
    }
}
