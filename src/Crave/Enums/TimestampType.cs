﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a timestamp.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum TimestampType : int
    {
        /// <summary>
        /// Last publish of client configuration.
        /// </summary>
        ClientConfiguration = 1,

        /// <summary>
        /// Last publish of company information.
        /// </summary>
        Company = 2,

        /// <summary>
        /// Last publish of a menu.
        /// </summary>
        Menu = 3,

        /// <summary>
        /// Last publish of a schedule.
        /// </summary>
        Schedules = 4,

        /// <summary>
        /// Last publish of an UI mode.
        /// </summary>
        UIMode = 5,

        /// <summary>
        /// Last publish of an UI schedule. 
        /// </summary>
        UISchedule = 6,

        /// <summary>
        /// Last publish of a UI theme.
        /// </summary>
        UITheme = 7,

        /// <summary>
        /// Last publish of a room control configuration.
        /// </summary>
        RoomControlConfiguration = 8,

        /// <summary>
        /// Last publish of a set of entertainment items.
        /// </summary>
        Entertainment = 9,
    }
}
