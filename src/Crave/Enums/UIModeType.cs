﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a UI mode.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum UIModeType : int
    {
        /// <summary>
        /// UI mode for in-room tablets.
        /// </summary>
        CraveTablet = 1,

        /// <summary>
        /// UI mode for consoles.
        /// </summary>
        CraveConsole = 2,

        /// <summary>
        /// UI model for smart phones.
        /// </summary>
        GuestMobile = 3,

        /// <summary>
        /// UI mode for tablets.
        /// </summary>
        GuestTablet = 4
    }
}
