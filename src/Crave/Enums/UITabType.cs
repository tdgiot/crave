﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of a UI tab.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum UITabType : int
    {
        /// <summary>
        /// Do not use. Default.
        /// </summary>
        Unknown = 0,

        #region Emenu 1-100

        /// <summary>
        /// Home tab. 
        /// </summary>
        Home = 1,

        /// <summary>
        /// Basket tab.
        /// </summary>
        Basket = 2,

        /// <summary>
        /// More tab.
        /// </summary>
        More = 3,

        /// <summary>
        /// Category tab.
        /// </summary>
        Category = 4,

        /// <summary>
        /// Entertainment tab.
        /// </summary>
        Entertainment = 5,

        /// <summary>
        /// Web tab.
        /// </summary>
        Web = 6,

        /// <summary>
        /// Placeholder (empty) tab.
        /// </summary>
        Placeholder = 7,

        /// <summary>
        /// Site tab.
        /// </summary>
        Site = 8,

        /// <summary>
        /// Room control tab.
        /// </summary>
        RoomControl = 9,

        /// <summary>
        /// Used to show the 'Crave Explorer' in our Apps within a PoI/Company
        /// centered around the location of the PoI/Company
        /// </summary>
        ExploreMap = 10,

        #endregion

        #region Console 101-200

        /// <summary>
        /// Orders tab.
        /// </summary>
        Orders = 101,

        /// <summary>
        /// Message tab.
        /// </summary>
        Message = 102,

        /// <summary>
        /// Management tab.
        /// </summary>
        Management = 103,

        /// <summary>
        /// Social media tab.
        /// </summary>
        Socialmedia = 104,

        /// <summary>
        /// Master orders tab.
        /// </summary>
        MasterOrders = 105,

        /// <summary>
        /// Terminal status tab.
        /// </summary>
        TerminalStatus = 106,

        /// <summary>
        /// Order count tab.
        /// </summary>
        OrderCount = 107,

        /// <summary>
        /// CMS tab.
        /// </summary>
        Cms = 108,

        /// <summary>
        /// Delivery times tab.
        /// </summary>
        DeliveryTimes = 109,

        #endregion
    }
}
