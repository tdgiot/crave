﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of the UI text size
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum UITextSizeType : int
    {
        // Controls
        Button = 101,
        Textbox = 102,
        Spinner = 103,
        Tab = 104,
        BulletPager = 105,
        CheckBox = 106,
        RadioButton = 107,
        TextView = 108,

        // Widgets
        Widget = 201,
        WidgetCaption = 202,
        WidgetDescription = 203,

        // Dialogs
        DialogTitle = 301,
        DialogSubtitle = 302,
        DialogText = 303,

        // Lists
        ListCategory = 401,
        ListItem = 402,
        ListItemPrice = 403,

        // Pages
        PageTitle = 501,
        PageDescription = 502,
        PageError = 503,
        PagePrice = 504,
        PageButton = 505,

        // Entertainment
        EntertainmentName = 601,
        EntertainmentDescription = 602,

        // Messages
        MessageTitle = 701,

        // Browser
        BrowserCloseButton = 801,
        BrowserAddressHeader = 802,

        // PDF
        PdfPanel = 901,

        // Room controls
        RoomControl = 1001,
        RoomControlSpinner = 1002,
        RoomControlStationName = 1003,
        RoomControlStationDescription = 1004,
        RoomControlStationNumber = 1005,
        RoomControlWidgetCaption = 1006,
        RoomControlWidgetValue = 1007,
        RoomControlThermostatFanCaption = 1008,
        RoomControlSleepViewTextView = 1009,
        RoomControlArea = 1010,
        RoomControlTwoStateButtonCenterText = 1011,
        RoomControlTwoStateButtonTopBottomText = 1012,
        RoomControlTemperatureSeekArc = 1013,
        RoomControlTemperatureSeekArcWidget = 1014,
        RoomControlTemperatureToggleButton = 1015,
        RoomControlCurtainName = 1016,
        RoomControlCurtainButton = 1017,
        RoomControlLightName = 1018,
        RoomControlLightPercentage = 1019,
        RoomControlSceneName = 1020,
        RoomControlSceneButton = 1021,
        RoomControlBlindName = 1022,
        RoomControlBlindButton = 1023,
        RoomControlWidgetName = 1024,
        RoomControlWidgetButton = 1025,
        RoomControlRemoteControlButton = 1026,

        // Alterations
        AlterationDialogTitle = 1101,
        AlterationDialogInput = 1102,
        AlterationDialogDescription = 1103,
        AlterationDialogPrice = 1104,
        AlterationDialogDate = 1105,
        AlterationDialogSummaryProduct = 1106,
        AlterationDialogSummaryPrice = 1107,
        AlterationDialogSummaryNoItems = 1108,
        AlterationDialogSummaryTotal = 1109,

        // Product dialog
        ProductDialogTitle = 1201,
        ProductDialogSubtitle = 1202,
        ProductDialogButton = 1203,
        ProductDialogPrice = 1204,
        ProductDialogDescription = 1205,
        ProductDialogColumnInstructions = 1206,
        ProductDialogColumnOptionSecondaryText = 1207,
        ProductDialogColumnOptionSuggestionText = 1208,

        // Other
        AlarmClockAlertDialog = 1301,
        BluetoothControlDialogSubtext = 1302,
        BluetoothControlDialogName = 1303,
        BluetoothControlDialogStatus = 1304,
        BluetoothDevicesDialog = 1305,
        CurrencyDialogRadioButton = 1306,
        LanguageDialogRadioButton = 1307,
        LogDialogText = 1308,
        ManagementDialogText = 1309,
        ManagementDialogTableNumberCaption = 1310,
        ManagementDialogTableNumberValue = 1311,
        ManagementDialogStatusCaption = 1312,
        ManagementDialogStatusText = 1313,
        OrderitemAddedDialogButtonAdd = 1314,
        OrderitemAddedDialogQuantity = 1315,
        ScreenSaverDialog = 1316,
        ScreenSaverDialogTime = 1317,
        ScreenSaverDialogColon = 1318,
        ServiceItemAddedDialogButtonAdd = 1319,
        ServiceItemAddedDialogQuantity = 1320,
        VenueDialog = 1321,
        WebAttachmentDialog = 1322,
        OrderViewHeader = 1323,
        OrderDeliverypointSmall = 1324,
        OrderDeliverypointMedium = 1325,
        OrderDeliverypointLarge = 1326,
        ConnectBtDeviceWidget1x1 = 1327,
        ConnectBtDeviceWidget = 1328,
        RoomControlNavigationWidget1x2 = 1329,
        VenueHeaderViewTab = 1330,
        OrderHistoryButton = 1331,
        OrderHistoryQuantity = 1332,
        VenueSummaryCategory = 1333,
        ConsoleDialogTab = 1334,
        ConsoleUIModeName = 1335,
        ScreenSaverDialogRadio = 1336
    }
}
