﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the types of UI widgets
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum UIWidgetType : int
    {
        /// <summary>
        /// Weather clock version 1.
        /// </summary>
        WeatherClockV1 = 1,

        /// <summary>
        /// Weather clock version 2.
        /// </summary>
        WeatherClockV2 = 2,

        /// <summary>
        /// Language picker.
        /// </summary>
        LanguagePicker = 3,

        /// <summary>
        /// Small advertisement.
        /// </summary>
        AdvertisementSmall = 4,

        /// <summary>
        /// Medium advertisement.
        /// </summary>
        AdvertisementMedium = 5,

        /// <summary>
        /// Connect BlueTooth speaker (1x1).
        /// </summary>
        ConnectBtSpeaker1x1 = 6,

        /// <summary>
        /// Connect BlueTooth keyboard (1x1).
        /// </summary>
        ConnectBtKeyboard1x1 = 7,

        /// <summary>
        /// PMS voicemail (2x1).
        /// </summary>
        PmsVoicemail2x1 = 8,

        /// <summary>
        /// Connect BlueTooth speaker (2x1).
        /// </summary>
        ConnectBtSpeaker2x1 = 9,

        /// <summary>
        /// Connect BlueTooth keyboard (2x1).
        /// </summary>
        ConnectBtKeyboard2x1 = 10,

        /// <summary>
        /// Placeholder (empty widget).
        /// </summary>
        Placeholder = 11,

        /// <summary>
        /// PMS bill (2x1).
        /// </summary>
        PmsBill2x1 = 12,

        /// <summary>
        /// Service request.
        /// </summary>
        ServiceRequest2x1 = 13,

        /// <summary>
        /// PMS voicemail (1x1).
        /// </summary>
        PmsVoicemail1x1 = 14,

        /// <summary>
        /// PMS bill (1x1).
        /// </summary>
        PmsBill1x1 = 15,

        /// <summary>
        /// Advertisement (1x1).
        /// </summary>
        Advertisement1x1 = 16,

        /// <summary>
        /// Link widget (1x1).
        /// </summary>
        LinkWidget1x1 = 17,

        /// <summary>
        /// Link widget (2x1).
        /// </summary>
        LinkWidget2x1 = 18,

        /// <summary>
        /// Link widget (2x2).
        /// </summary>
        LinkWidget2x2 = 19,

        /// <summary>
        /// Scene widget (1x1).
        /// </summary>
        SceneWidget1x1 = 20,

        /// <summary>
        /// Scene widget (2x1).
        /// </summary>
        SceneWidget2x1 = 21,

        /// <summary>
        /// Clock widget (2x2).
        /// </summary>
        Clock2x2 = 22,

        /// <summary>
        /// Weather widget (2x3).
        /// </summary>
        Weather2x3 = 23,

        /// <summary>
        /// Delivery time widget (2x1).
        /// </summary>
        Deliverytime2x1 = 24,

        /// <summary>
        /// Alarm clock widget (1x1).
        /// </summary>
        AlarmClock1x1 = 25,

        /// <summary>
        /// Alarm clock widget (2x1).
        /// </summary>
        AlarmClock2x1 = 26,

        /// <summary>
        /// Menu widget fill
        /// </summary>
        MenuFill = 27,

        /// <summary>
        /// Order history widget
        /// </summary>
        OrderHistory = 28,

        /// <summary>
        /// Count Down
        /// </summary>
        CountDown2x1 = 29,

        /// <summary>
        /// Count Down
        /// </summary>
        CountDown2x2 = 30,

        /// <summary>
        /// Availability
        /// </summary>
        Availability2x1 = 31,

        /// <summary>
        /// Availability
        /// </summary>
        Availability2x2 = 32,

        /// <summary>
        /// Text 2x2
        /// </summary>
        Text2x2 = 33,

        /// <summary>
        /// Text 2x4
        /// </summary>
        Text2x4 = 34,

        /// <summary>
        /// Currency 2x1
        /// </summary>
        Currency2x1 = 35,

        /// <summary>
        /// Currency 2x2
        /// </summary>
        Currency2x2 = 36,

        /// <summary>
        /// Currency 1x1
        /// </summary>
        Currency1x1 = 37,

        /// <summary>
        /// Deliverytime 1x1
        /// </summary>
        Deliverytime1x1 = 38,

        /// <summary>
        /// Connect Bt Device Instructions Widget 1x1
        /// </summary>
        ConnectBtDeviceInstructions1x1 = 39,

        /// <summary>
        /// Ad Widget 2x1
        /// </summary>
        AdWidget2x1 = 40,

        /// <summary>
        /// Ad Widget 2x2
        /// </summary>
        AdWidget2x2 = 41,

        /// <summary>
        /// Ad Widget 2x5
        /// </summary>
        AdWidget2x5 = 42,

        /// <summary>
        /// Ad Widget 2x8
        /// </summary>
        AdWidget2x8 = 43,

        /// <summary>
        /// Ad Widget 1x10
        /// </summary>
        AdWidget1x10 = 44,

        /// <summary>
        /// Connect Bt Device Instructions Widget 2x1
        /// </summary>
        ConnectBtDeviceInstructions2x1 = 45,

        /// <summary>
        /// Connect Bt Device Instructions Widget 2x2
        /// </summary>
        ConnectBtDeviceInstructions2x2 = 46,

        /// <summary>
        /// Connect bluetooth speaker 2x2
        /// </summary>
        ConnectBtSpeaker2x2 = 47,

        /// <summary>
        /// Ad Widget 2x10
        /// </summary>
        AdWidget2x10 = 48,

        /// <summary>
        /// Map menu fill
        /// </summary>
        MapMenuFill = 49,

        /// <summary>
        /// Map page
        /// </summary>
        MapPage = 50,

        /// <summary>
        /// Service requests 1x1
        /// </summary>
        ServiceRequest1x1 = 51,
    }
}
