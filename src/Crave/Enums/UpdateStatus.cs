﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the variant of the alteration dialog.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum UpdateStatus
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Idle = 0,

        /// <summary>
        /// Download started
        /// </summary>
        DownloadStarted = 100,

        /// <summary>
        /// Download completed
        /// </summary>
        DownloadCompleted = 200,

        /// <summary>
        /// Download failed
        /// </summary>
        DownloadFailed = 300,

        /// <summary>
        /// Installation started
        /// </summary>
        InstallationStarted = 400,

        /// <summary>
        /// Installation completed
        /// </summary>
        InstallationCompleted = 500,

        /// <summary>
        /// Installation failed
        /// </summary>
        InstallationFailed = 600
    }
}