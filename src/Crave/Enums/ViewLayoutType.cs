﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the layout of the view.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum ViewLayoutType
    {
        /// <summary>
        /// Inherit from parent category.
        /// </summary>
        Inherit = 0,

        /// <summary>
        /// Horizontal image.
        /// </summary>
        HorizontalImage = 1,

        /// <summary>
        /// Vertical wide image.
        /// </summary>
        VerticalWideImage = 2,

        /// <summary>
        /// Vertical narrow image.
        /// </summary>
        VerticalNarrowImage = 3
    }
}
