﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the visibility of an item.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum VisibilityType
    {
        /// <summary>
        /// Always visible.
        /// </summary>
        Always = 0,

        /// <summary>
        /// Never visible.
        /// </summary>
        Never = 1,

        /// <summary>
        /// When one or more linked items (e.g. widget or slide) are visible.
        /// </summary>
        WhenLinkedItemVisible = 2,

        /// <summary>
        /// When orderable according to schedule.
        /// </summary>
        WhenAvailableOnSchedule = 3
    }
}
