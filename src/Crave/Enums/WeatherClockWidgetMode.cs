﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the type of weather clock widget.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum WeatherClockWidgetMode
    {
        /// <summary>
        /// Version 1 (HTC like clock)
        /// </summary>
        v1 = 1,

        /// <summary>
        /// Version 2 (digital clock)
        /// </summary>
        v2 = 2,
    }
}