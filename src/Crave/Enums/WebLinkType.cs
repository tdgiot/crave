﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crave.Enums
{
    public enum WebLinkType
    {
        Button,

        QrCode,

        Combination
    }
}
