﻿using System.ComponentModel;

namespace Crave.Enums
{
    /// <summary>
    /// Enumeration which represents the strength of a wi-fi signal.
    /// </summary>
    [LocalizedEnumDisplayName(typeof(Resources.Enums))]
    public enum WifiStrength
    {
        /// <summary>
        /// The wi-fi signal is excellent (greater than -55dB).
        /// </summary>
        Excellent = 0,

        /// <summary>
        /// The wi-fi signal is good (between -55dB and -66dB).
        /// </summary>
        Good = 1,

        /// <summary>
        /// The wi-fi signal is good (between -66dB and -77dB).
        /// </summary>
        Average = 2,

        /// <summary>
        /// The wi-fi signal is poor (between -77dB and -88dB).
        /// </summary>
        Poor = 3,

        /// <summary>
        /// The wi-fi signal is bad (below -88dB).
        /// </summary>
        Bad = 4
    }
}
