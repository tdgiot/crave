﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Extensions
{
    public static class StreamExtensions
    {
        public static async Task<string> ReadAsStringAsync(this Stream stream)
        {
            if (!stream.CanRead || !stream.CanSeek)
            {
                throw new NotSupportedException("The stream does not allow read or seek.");
            }

            stream.Seek(0, SeekOrigin.Begin);

            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8, true, 1024, true))
            {
                string body = await reader.ReadToEndAsync();

                // Reset so the stream can be read again
                stream.Seek(0, SeekOrigin.Begin);

                return body;
            }

        }
    }
}
