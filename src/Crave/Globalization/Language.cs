﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Crave.Resources;

namespace Crave.Globalization
{
    /// <summary>
    /// Class which represents a language and holds mappings of all languages.
    /// </summary>
    /// <remarks>StyleCop and ReSharper naming rules have been disabled for better readability purposes.</remarks>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "One time allowance of underscores.")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "One time ignoring of spelling.")]
    // ReSharper disable InconsistentNaming
    public class Language
    {
        #region Fields

        /// <summary>The dictionary containing the languages, mapped by language code.</summary>
        public static readonly Dictionary<string, Language> Mappings = new Dictionary<string, Language>();

        #region Language list

        /// <summary>Afrikaans (afr)</summary>
        public static readonly Language Afrikaans = new Language("af", "afr", Strings.Language_Afrikaans);

        /// <summary>Albanian (sqi)</summary>
        public static readonly Language Albanian = new Language("sq", "sqi", Strings.Language_Albanian);

        /// <summary>Alsatian (gsw)</summary>
        public static readonly Language Alsatian = new Language("gsw", "gsw", Strings.Language_Alsatian);

        /// <summary>Amharic (amh)</summary>
        public static readonly Language Amharic = new Language("am", "amh", Strings.Language_Amharic);

        /// <summary>Arabic (ara)</summary>
        public static readonly Language Arabic = new Language("ar", "ara", Strings.Language_Arabic);

        /// <summary>Armenian (hye)</summary>
        public static readonly Language Armenian = new Language("hy", "hye", Strings.Language_Armenian);

        /// <summary>Assamese (asm)</summary>
        public static readonly Language Assamese = new Language(" as", "asm", Strings.Language_Assamese);

        /// <summary>Azeri (aze)</summary>
        public static readonly Language Azeri = new Language("az", "aze", Strings.Language_Azeri);

        /// <summary>Bashkir (bak)</summary>
        public static readonly Language Bashkir = new Language("ba", "bak", Strings.Language_Bashkir);

        /// <summary>Basque (eus)</summary>
        public static readonly Language Basque = new Language("eu", "eus", Strings.Language_Basque);

        /// <summary>Belarusian (bel)</summary>
        public static readonly Language Belarusian = new Language("be", "bel", Strings.Language_Belarusian);

        /// <summary>Bengali (bng)</summary>
        public static readonly Language Bengali = new Language("bn", "bng", Strings.Language_Bengali);

        /// <summary>Bosnian (bsb)</summary>
        public static readonly Language Bosnian = new Language("bs", "bsb", Strings.Language_Bosnian);

        /// <summary>Breton (bre)</summary>
        public static readonly Language Breton = new Language("br", "bre", Strings.Language_Breton);

        /// <summary>Bulgarian (bul)</summary>
        public static readonly Language Bulgarian = new Language("bg", "bul", Strings.Language_Bulgarian);

        /// <summary>Catalan (cat)</summary>
        public static readonly Language Catalan = new Language("ca", "cat", Strings.Language_Catalan);

        /// <summary>Chinese (zho)</summary>
        public static readonly Language Chinese = new Language("zh", "zho", Strings.Language_Chinese);

        /// <summary>Corsican (cos)</summary>
        public static readonly Language Corsican = new Language("co", "cos", Strings.Language_Corsican);

        /// <summary>Croatian (hrb)</summary>
        public static readonly Language Croatian = new Language("hr", "hrb", Strings.Language_Croatian);

        /// <summary>Czech (ces)</summary>
        public static readonly Language Czech = new Language("cs", "ces", Strings.Language_Czech);

        /// <summary>Danish (dan)</summary>
        public static readonly Language Danish = new Language("da", "dan", Strings.Language_Danish);

        /// <summary>Dari (prs)</summary>
        public static readonly Language Dari = new Language("prs", "prs", Strings.Language_Dari);

        /// <summary>Divehi (div)</summary>
        public static readonly Language Divehi = new Language("dv", "div", Strings.Language_Divehi);

        /// <summary>Dutch (nld)</summary>
        public static readonly Language Dutch = new Language("nl", "nld", Strings.Language_Dutch);

        /// <summary>English (eng)</summary>
        public static readonly Language English = new Language("en", "eng", Strings.Language_English);

        /// <summary>Estonian (est)</summary>
        public static readonly Language Estonian = new Language("et", "est", Strings.Language_Estonian);

        /// <summary>Faroese (fao)</summary>
        public static readonly Language Faroese = new Language("fo", "fao", Strings.Language_Faroese);

        /// <summary>Filipino (fil)</summary>
        public static readonly Language Filipino = new Language("fil", "fil", Strings.Language_Filipino);

        /// <summary>Finnish (fin)</summary>
        public static readonly Language Finnish = new Language("fi", "fin", Strings.Language_Finnish);

        /// <summary>French (fra)</summary>
        public static readonly Language French = new Language("fr", "fra", Strings.Language_French);

        /// <summary>Frisian (fry)</summary>
        public static readonly Language Frisian = new Language("fy", "fry", Strings.Language_Frisian);

        /// <summary>Galician (glg)</summary>
        public static readonly Language Galician = new Language("gl", "glg", Strings.Language_Galician);

        /// <summary>Georgian (kat)</summary>
        public static readonly Language Georgian = new Language("ka", "kat", Strings.Language_Georgian);

        /// <summary>German (deu)</summary>
        public static readonly Language German = new Language("de", "deu", Strings.Language_German);

        /// <summary>Greek (ell)</summary>
        public static readonly Language Greek = new Language("el", "ell", Strings.Language_Greek);

        /// <summary>Greenlandic (kal)</summary>
        public static readonly Language Greenlandic = new Language("kl", "kal", Strings.Language_Greenlandic);

        /// <summary>Gujarati (guj)</summary>
        public static readonly Language Gujarati = new Language("gu", "guj", Strings.Language_Gujarati);

        /// <summary>Hausa (hau)</summary>
        public static readonly Language Hausa = new Language("ha", "hau", Strings.Language_Hausa);

        /// <summary>Hebrew (heb)</summary>
        public static readonly Language Hebrew = new Language("he", "heb", Strings.Language_Hebrew);

        /// <summary>Hindi (hin)</summary>
        public static readonly Language Hindi = new Language("hi", "hin", Strings.Language_Hindi);

        /// <summary>Hungarian (hun)</summary>
        public static readonly Language Hungarian = new Language("hu", "hun", Strings.Language_Hungarian);

        /// <summary>Icelandic (isl)</summary>
        public static readonly Language Icelandic = new Language(" is", "isl", Strings.Language_Icelandic);

        /// <summary>Igbo (ibo)</summary>
        public static readonly Language Igbo = new Language("ig", "ibo", Strings.Language_Igbo);

        /// <summary>Indonesian (ind)</summary>
        public static readonly Language Indonesian = new Language("id", "ind", Strings.Language_Indonesian);

        /// <summary>Inuktitut (iku)</summary>
        public static readonly Language Inuktitut = new Language("iu", "iku", Strings.Language_Inuktitut);

        /// <summary>Irish (gle)</summary>
        public static readonly Language Irish = new Language("ga", "gle", Strings.Language_Irish);

        /// <summary>isiXhosa (xho)</summary>
        public static readonly Language IsiXhosa = new Language("xh", "xho", Strings.Language_IsiXhosa);

        /// <summary>isiZulu (zul)</summary>
        public static readonly Language IsiZulu = new Language("zu", "zul", Strings.Language_IsiZulu);

        /// <summary>Italian (ita)</summary>
        public static readonly Language Italian = new Language("it", "ita", Strings.Language_Italian);

        /// <summary>Japanese (jpn)</summary>
        public static readonly Language Japanese = new Language("ja", "jpn", Strings.Language_Japanese);

        /// <summary>Kannada (kan)</summary>
        public static readonly Language Kannada = new Language("kn", "kan", Strings.Language_Kannada);

        /// <summary>Kazakh (kaz)</summary>
        public static readonly Language Kazakh = new Language("kk", "kaz", Strings.Language_Kazakh);

        /// <summary>Khmer (khm)</summary>
        public static readonly Language Khmer = new Language("km", "khm", Strings.Language_Khmer);

        /// <summary>K_iche (qut)</summary>
        public static readonly Language K_iche = new Language("qut", "qut", Strings.Language_K_iche);

        /// <summary>Kinyarwanda (kin)</summary>
        public static readonly Language Kinyarwanda = new Language("rw", "kin", Strings.Language_Kinyarwanda);

        /// <summary>Kiswahili (swa)</summary>
        public static readonly Language Kiswahili = new Language("sw", "swa", Strings.Language_Kiswahili);

        /// <summary>Konkani (kok)</summary>
        public static readonly Language Konkani = new Language("kok", "kok", Strings.Language_Konkani);

        /// <summary>Korean (kor)</summary>
        public static readonly Language Korean = new Language("ko", "kor", Strings.Language_Korean);

        /// <summary>Kyrgyz (kir)</summary>
        public static readonly Language Kyrgyz = new Language("ky", "kir", Strings.Language_Kyrgyz);

        /// <summary>Lao (lao)</summary>
        public static readonly Language Lao = new Language("lo", "lao", Strings.Language_Lao);

        /// <summary>Latvian (lav)</summary>
        public static readonly Language Latvian = new Language("lv", "lav", Strings.Language_Latvian);

        /// <summary>Lithuanian (lit)</summary>
        public static readonly Language Lithuanian = new Language("lt", "lit", Strings.Language_Lithuanian);

        /// <summary>Lower Sorbian (dsb)</summary>
        public static readonly Language Lower_Sorbian = new Language("dsb", "dsb", Strings.Language_Lower_Sorbian);

        /// <summary>Luxembourgish (ltz)</summary>
        public static readonly Language Luxembourgish = new Language("lb", "ltz", Strings.Language_Luxembourgish);

        /// <summary>Macedonian (mkd)</summary>
        public static readonly Language Macedonian = new Language("mk", "mkd", Strings.Language_Macedonian);

        /// <summary>Malay (msa)</summary>
        public static readonly Language Malay = new Language("ms", "msa", Strings.Language_Malay);

        /// <summary>Malayalam (mym)</summary>
        public static readonly Language Malayalam = new Language("ml", "mym", Strings.Language_Malayalam);

        /// <summary>Maltese (mlt)</summary>
        public static readonly Language Maltese = new Language("mt", "mlt", Strings.Language_Maltese);

        /// <summary>Maori (mri)</summary>
        public static readonly Language Maori = new Language("mi", "mri", Strings.Language_Maori);

        /// <summary>Mapudungun (arn)</summary>
        public static readonly Language Mapudungun = new Language("arn", "arn", Strings.Language_Mapudungun);

        /// <summary>Marathi (mar)</summary>
        public static readonly Language Marathi = new Language("mr", "mar", Strings.Language_Marathi);

        /// <summary>Mohawk (moh)</summary>
        public static readonly Language Mohawk = new Language("moh", "moh", Strings.Language_Mohawk);

        /// <summary>Mongolian (mon)</summary>
        public static readonly Language Mongolian = new Language("mn", "mon", Strings.Language_Mongolian);

        /// <summary>Nepali (nep)</summary>
        public static readonly Language Nepali = new Language("ne", "nep", Strings.Language_Nepali);

        /// <summary>Norwegian, Bokmål (nob)</summary>
        public static readonly Language Norwegian_Bokmal = new Language("nb", "nob", Strings.Language_Norwegian_Bokmal);

        /// <summary>Norwegian, Nynorsk (nno)</summary>
        public static readonly Language Norwegian_Nynorsk = new Language("nn", "nno", Strings.Language_Norwegian_Nynorsk);

        /// <summary>Occitan (oci)</summary>
        public static readonly Language Occitan = new Language("oc", "oci", Strings.Language_Occitan);

        /// <summary>Oriya (ori)</summary>
        public static readonly Language Oriya = new Language("or", "ori", Strings.Language_Oriya);

        /// <summary>Pashto (pus)</summary>
        public static readonly Language Pashto = new Language("ps", "pus", Strings.Language_Pashto);

        /// <summary>Persian (fas)</summary>
        public static readonly Language Persian = new Language("fa", "fas", Strings.Language_Persian);

        /// <summary>Polish (pol)</summary>
        public static readonly Language Polish = new Language("pl", "pol", Strings.Language_Polish);

        /// <summary>Portuguese (por)</summary>
        public static readonly Language Portuguese = new Language("pt", "por", Strings.Language_Portuguese);

        /// <summary>Punjabi (pan)</summary>
        public static readonly Language Punjabi = new Language("pa", "pan", Strings.Language_Punjabi);

        /// <summary>Quechua (qup)</summary>
        public static readonly Language Quechua = new Language("quz", "qup", Strings.Language_Quechua);

        /// <summary>Romanian (ron)</summary>
        public static readonly Language Romanian = new Language("ro", "ron", Strings.Language_Romanian);

        /// <summary>Romansh (roh)</summary>
        public static readonly Language Romansh = new Language("rm", "roh", Strings.Language_Romansh);

        /// <summary>Russian (rus)</summary>
        public static readonly Language Russian = new Language("ru", "rus", Strings.Language_Russian);

        /// <summary>Sami_Inari (smn)</summary>
        public static readonly Language Sami_Inari = new Language("smn", "smn", Strings.Language_Sami_Inari);

        /// <summary>Sami_Lule (smj)</summary>
        public static readonly Language Sami_Lule = new Language("smj", "smj", Strings.Language_Sami_Lule);

        /// <summary>Sami_Northern (smf)</summary>
        public static readonly Language Sami_Northern = new Language("se", "smf", Strings.Language_Sami_Northern);

        /// <summary>Sami_Skolt (sms)</summary>
        public static readonly Language Sami_Skolt = new Language("sms", "sms", Strings.Language_Sami_Skolt);

        /// <summary>Sami_Southern (sma)</summary>
        public static readonly Language Sami_Southern = new Language("sma", "sma", Strings.Language_Sami_Southern);

        /// <summary>Sanskrit (san)</summary>
        public static readonly Language Sanskrit = new Language("sa", "san", Strings.Language_Sanskrit);

        /// <summary>Scottish Gaelic (gla)</summary>
        public static readonly Language Scottish_Gaelic = new Language("gd", "gla", Strings.Language_Scottish_Gaelic);

        /// <summary>Serbian (srp)</summary>
        public static readonly Language Serbian = new Language("sr", "srp", Strings.Language_Serbian);

        /// <summary>Sesotho sa Leboa (nso)</summary>
        public static readonly Language Sesotho_sa_Leboa = new Language("nso", "nso", Strings.Language_Sesotho_sa_Leboa);

        /// <summary>Setswana (tsn)</summary>
        public static readonly Language Setswana = new Language("tn", "tsn", Strings.Language_Setswana);

        /// <summary>Sinhala (sin)</summary>
        public static readonly Language Sinhala = new Language("si", "sin", Strings.Language_Sinhala);

        /// <summary>Slovak (slk)</summary>
        public static readonly Language Slovak = new Language("sk", "slk", Strings.Language_Slovak);

        /// <summary>Slovenian (slv)</summary>
        public static readonly Language Slovenian = new Language("sl", "slv", Strings.Language_Slovenian);

        /// <summary>Spanish (spa)</summary>
        public static readonly Language Spanish = new Language("es", "spa", Strings.Language_Spanish);

        /// <summary>Swedish (swe)</summary>
        public static readonly Language Swedish = new Language("sv", "swe", Strings.Language_Swedish);

        /// <summary>Syriac (syr)</summary>
        public static readonly Language Syriac = new Language("syr", "syr", Strings.Language_Syriac);

        /// <summary>Tajik (tgk)</summary>
        public static readonly Language Tajik = new Language("tg", "tgk", Strings.Language_Tajik);

        /// <summary>Tamazight (tzm)</summary>
        public static readonly Language Tamazight = new Language("tzm", "tzm", Strings.Language_Tamazight);

        /// <summary>Tamil (tam)</summary>
        public static readonly Language Tamil = new Language("ta", "tam", Strings.Language_Tamil);

        /// <summary>Tatar (tat)</summary>
        public static readonly Language Tatar = new Language("tt", "tat", Strings.Language_Tatar);

        /// <summary>Telugu (tel)</summary>
        public static readonly Language Telugu = new Language("te", "tel", Strings.Language_Telugu);

        /// <summary>Thai (tha)</summary>
        public static readonly Language Thai = new Language("th", "tha", Strings.Language_Thai);

        /// <summary>Tibetan (bod)</summary>
        public static readonly Language Tibetan = new Language("bo", "bod", Strings.Language_Tibetan);

        /// <summary>Turkish (tur)</summary>
        public static readonly Language Turkish = new Language("tr", "tur", Strings.Language_Turkish);

        /// <summary>Turkmen (tuk)</summary>
        public static readonly Language Turkmen = new Language("tk", "tuk", Strings.Language_Turkmen);

        /// <summary>Ukrainian (ukr)</summary>
        public static readonly Language Ukrainian = new Language("uk", "ukr", Strings.Language_Ukrainian);

        /// <summary>Upper Sorbian (hsb)</summary>
        public static readonly Language Upper_Sorbian = new Language("hsb", "hsb", Strings.Language_Upper_Sorbian);

        /// <summary>Urdu (urd)</summary>
        public static readonly Language Urdu = new Language("ur", "urd", Strings.Language_Urdu);

        /// <summary>Uyghur (uig)</summary>
        public static readonly Language Uyghur = new Language("ug", "uig", Strings.Language_Uyghur);

        /// <summary>Uzbek (uzb)</summary>
        public static readonly Language Uzbek = new Language("uz", "uzb", Strings.Language_Uzbek);

        /// <summary>Vietnamese (vie)</summary>
        public static readonly Language Vietnamese = new Language("vi", "vie", Strings.Language_Vietnamese);

        /// <summary>Welsh (cym)</summary>
        public static readonly Language Welsh = new Language("cy", "cym", Strings.Language_Welsh);

        /// <summary>Wolof (wol)</summary>
        public static readonly Language Wolof = new Language("wo", "wol", Strings.Language_Wolof);

        /// <summary>Yakut (sah)</summary>
        public static readonly Language Yakut = new Language("sah", "sah", Strings.Language_Yakut);

        /// <summary>Yi (iii)</summary>
        public static readonly Language Yi = new Language("ii", "iii", Strings.Language_Yi);

        /// <summary>Yoruba (yor)</summary>
        public static readonly Language Yoruba = new Language("yo", "yor", Strings.Language_Yoruba);

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Language" /> class.
        /// </summary>
        /// <param name="codeAlpha2">The Alpha-2 code of the language.</param>
        /// <param name="codeAlpha3">The Alpha-3 code of the language.</param>
        /// <param name="name">The name of the language.</param>
        private Language(string codeAlpha2, string codeAlpha3, string name)
        {
            this.CodeAlpha2 = codeAlpha2;
            this.CodeAlpha3 = codeAlpha3;
            this.Name = name;

            Mappings[codeAlpha3] = this;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Alpha-2 code of the language.
        /// </summary>
        public string CodeAlpha2 { get; set; }

        /// <summary>
        /// Gets or sets the Alpha-3 code of the language.
        /// </summary>
        public string CodeAlpha3 { get; set; }

        /// <summary>
        /// Gets or sets the name of the language.
        /// </summary>
        public string Name { get; set; }

        #endregion

        #region Operators

        /// <summary>
        /// Converts the specified string to the corresponding <see cref="Language" /> instance. 
        /// </summary>
        /// <param name="code">The code of the language.</param>
        public static explicit operator Language(string code)
        {
            Language result;
            if (Mappings.TryGetValue(code.ToLower(), out result))
            {
                return result;
            }

            throw new InvalidCastException();
        }

        #endregion
    }
}
