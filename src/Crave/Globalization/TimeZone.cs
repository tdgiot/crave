﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace Crave.Globalization
{
    /// <summary>
    /// Class which represents a time zone and holds mappings of all time zones.
    /// Windows timezones taken from: http://unicode.org/repos/cldr-tmp/trunk/diff/supplemental/zone_tzid.html
    /// </summary>
    /// <remarks>StyleCop and ReSharper naming rules have been disabled for better readability purposes.</remarks>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "One time allowance of underscores.")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "One time ignoring of spelling.")]
    // ReSharper disable InconsistentNaming
    public class TimeZone
    {
        #region Fields

        /// <summary>The dictionary containing the time zones, mapped by Olson time zone ID.</summary>
        public static readonly Dictionary<string, TimeZone> Mappings = new Dictionary<string, TimeZone>();

        #region TimeZone list

        /// <summary>Pacific/Apia (UTC-11:00)</summary>
        public static readonly TimeZone Pacific_Apia = new TimeZone("Pacific/Apia", "Samoa Standard Time", "(UTC-11:00) Samoa");

        /// <summary>Pacific/Midway (UTC-11:00)</summary>
        public static readonly TimeZone Pacific_Midway = new TimeZone("Pacific/Midway", "UTC-11", "(UTC-11:00)");

        /// <summary>Pacific/Niue (UTC-11:00)</summary>
        public static readonly TimeZone Pacific_Niue = new TimeZone("Pacific/Niue", "Samoa Standard Time", "(UTC-11:00) Samoa");

        /// <summary>Pacific/Pago_Pago (UTC-11:00)</summary>
        public static readonly TimeZone Pacific_Pago_Pago = new TimeZone("Pacific/Pago_Pago", "UTC-11", "(UTC-11:00)");

        /// <summary>America/Adak (UTC-10:00)</summary>
        public static readonly TimeZone America_Adak = new TimeZone("America/Adak", "Hawaiian Standard Time", "(UTC-10:00) Hawaii");

        /// <summary>Pacific/Fakaofo (UTC-10:00)</summary>
        public static readonly TimeZone Pacific_Fakaofo = new TimeZone("Pacific/Fakaofo", "Hawaiian Standard Time", "(UTC-10:00) Hawaii");

        /// <summary>Pacific/Honolulu (UTC-10:00)</summary>
        public static readonly TimeZone Pacific_Honolulu = new TimeZone("Pacific/Honolulu", "Hawaiian Standard Time", "(UTC-10:00) Hawaii");

        /// <summary>Pacific/Johnston (UTC-10:00)</summary>
        public static readonly TimeZone Pacific_Johnston = new TimeZone("Pacific/Johnston", "Hawaiian Standard Time", "(UTC-10:00) Hawaii");

        /// <summary>Pacific/Rarotonga (UTC-10:00)</summary>
        public static readonly TimeZone Pacific_Rarotonga = new TimeZone("Pacific/Rarotonga", "Hawaiian Standard Time", "(UTC-10:00) Hawaii");

        /// <summary>Pacific/Tahiti (UTC-10:00)</summary>
        public static readonly TimeZone Pacific_Tahiti = new TimeZone("Pacific/Tahiti", "Hawaiian Standard Time", "(UTC-10:00) Hawaii");

        /// <summary>Pacific/Marquesas (UTC-09:30)</summary>
        public static readonly TimeZone Pacific_Marquesas = new TimeZone("Pacific/Marquesas", "Hawaiian Standard Time", "(UTC-10:00) Hawaii");

        /// <summary>America/Anchorage (UTC-09:00)</summary>
        public static readonly TimeZone America_Anchorage = new TimeZone("America/Anchorage", "Alaskan Standard Time", "(UTC-09:00) Alaska");

        /// <summary>America/Juneau (UTC-09:00)</summary>
        public static readonly TimeZone America_Juneau = new TimeZone("America/Juneau", "Alaskan Standard Time", "(UTC-09:00) Alaska");

        /// <summary>America/Nome (UTC-09:00)</summary>
        public static readonly TimeZone America_Nome = new TimeZone("America/Nome", "Alaskan Standard Time", "(UTC-09:00) Alaska");

        /// <summary>America/Yakutat (UTC-09:00)</summary>
        public static readonly TimeZone America_Yakutat = new TimeZone("America/Yakutat", "Alaskan Standard Time", "(UTC-09:00) Alaska");

        /// <summary>Pacific/Gambier (UTC-09:00)</summary>
        public static readonly TimeZone Pacific_Gambier = new TimeZone("Pacific/Gambier", "Alaskan Standard Time", "(UTC-09:00) Alaska");

        /// <summary>America/Dawson (UTC-08:00)</summary>
        public static readonly TimeZone America_Dawson = new TimeZone("America/Dawson", "Pacific Standard Time", "(UTC-08:00) Pacific Time(US & Canada)");

        /// <summary>America/Los_Angeles (UTC-08:00)</summary>
        public static readonly TimeZone America_Los_Angeles = new TimeZone("America/Los_Angeles", "Pacific Standard Time", "(UTC-08:00) Pacific Time(US & Canada)");

        /// <summary>America/Santa_Isabel (UTC-08:00)</summary>
        public static readonly TimeZone America_Santa_Isabel = new TimeZone("America/Santa_Isabel", "Pacific Standard Time (Mexico)", "(UTC-08:00) Baja California");

        /// <summary>America/Tijuana (UTC-08:00)</summary>
        public static readonly TimeZone America_Tijuana = new TimeZone("America/Tijuana", "Pacific Standard Time", "(UTC-08:00) Baja California");

        /// <summary>America/Vancouver (UTC-08:00)</summary>
        public static readonly TimeZone America_Vancouver = new TimeZone("America/Vancouver", "Pacific Standard Time", "(UTC-08:00) Pacific Time(US & Canada)");

        /// <summary>America/Whitehorse (UTC-08:00)</summary>
        public static readonly TimeZone America_Whitehorse = new TimeZone("America/Whitehorse", "Pacific Standard Time", "(UTC-08:00) Pacific Time(US & Canada)");

        /// <summary>Pacific/Pitcairn (UTC-08:00)</summary>
        public static readonly TimeZone Pacific_Pitcairn = new TimeZone("Pacific/Pitcairn", "Pacific Standard Time", "(UTC-08:00) Pacific Time(US & Canada)");

        /// <summary>America/Boise (UTC-07:00)</summary>
        public static readonly TimeZone America_Boise = new TimeZone("America/Boise", "Mountain Standard Time", "(UTC-07:00) Mountain Time(US & Canada)");

        /// <summary>America/Cambridge_Bay (UTC-07:00)</summary>
        public static readonly TimeZone America_Cambridge_Bay = new TimeZone("America/Cambridge_Bay", "Mountain Standard Time", "(UTC-07:00) Mountain Time(US & Canada)");

        /// <summary>America/Chihuahua (UTC-07:00)</summary>
        public static readonly TimeZone America_Chihuahua = new TimeZone("America/Chihuahua", "Mountain Standard Time (Mexico)", "(UTC-07:00) Chihuahua, La Paz, Mazatlan");

        /// <summary>America/Dawson_Creek (UTC-07:00)</summary>
        public static readonly TimeZone America_Dawson_Creek = new TimeZone("America/Dawson_Creek", "US Mountain Standard Time", "(UTC-07:00) Arizona");

        /// <summary>America/Denver (UTC-07:00)</summary>
        public static readonly TimeZone America_Denver = new TimeZone("America/Denver", "Mountain Standard Time", "(UTC-07:00) Mountain Time(US & Canada)");

        /// <summary>America/Edmonton (UTC-07:00)</summary>
        public static readonly TimeZone America_Edmonton = new TimeZone("America/Edmonton", "Mountain Standard Time", "(UTC-07:00) Mountain Time(US & Canada)");

        /// <summary>America/Hermosillo (UTC-07:00)</summary>
        public static readonly TimeZone America_Hermosillo = new TimeZone("America/Hermosillo", "US Mountain Standard Time", "(UTC-07:00) Arizona");

        /// <summary>America/Inuvik (UTC-07:00)</summary>
        public static readonly TimeZone America_Inuvik = new TimeZone("America/Inuvik", "Mountain Standard Time", "(UTC-07:00) Mountain Time(US & Canada)");

        /// <summary>America/Mazatlan (UTC-07:00)</summary>
        public static readonly TimeZone America_Mazatlan = new TimeZone("America/Mazatlan", "Mountain Standard Time (Mexico)", "(UTC-07:00) Chihuahua, La Paz, Mazatlan");

        /// <summary>America/Ojinaga (UTC-07:00)</summary>
        public static readonly TimeZone America_Ojinaga = new TimeZone("America/Ojinaga", "Mountain Standard Time", "(UTC-07:00) Mountain Time (US & Canada)");

        /// <summary>America/Phoenix (UTC-07:00)</summary>
        public static readonly TimeZone America_Phoenix = new TimeZone("America/Phoenix", "US Mountain Standard Time", "(UTC-07:00) Arizona");

        /// <summary>America/Shiprock (UTC-07:00)</summary>
        public static readonly TimeZone America_Shiprock = new TimeZone("America/Shiprock", "Mountain Standard Time", "(UTC-07:00) Mountain Time(US & Canada)");

        /// <summary>America/Yellowknife (UTC-07:00)</summary>
        public static readonly TimeZone America_Yellowknife = new TimeZone("America/Yellowknife", "Mountain Standard Time", "(UTC-07:00) Mountain Time(US & Canada)");

        /// <summary>America/Belize (UTC-06:00)</summary>
        public static readonly TimeZone America_Belize = new TimeZone("America/Belize", "Central America Standard Time", "(UTC-06:00) Central America");       

        /// <summary>America/Chicago (UTC-06:00)</summary>
        public static readonly TimeZone America_Chicago = new TimeZone("America/Chicago", "Central Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>America/Costa_Rica (UTC-06:00)</summary>
        public static readonly TimeZone America_Costa_Rica = new TimeZone("America/Costa_Rica", "Central America Standard Time", "(UTC-06:00) Central America");

        /// <summary>America/El_Salvador (UTC-06:00)</summary>
        public static readonly TimeZone America_El_Salvador = new TimeZone("America/El_Salvador", "Central America Standard Time", "(UTC-06:00) Central America");

        /// <summary>America/Guatemala (UTC-06:00)</summary>
        public static readonly TimeZone America_Guatemala = new TimeZone("America/Guatemala", "Central America Standard Time", "(UTC-06:00) Central America");

        /// <summary>America/Indiana/Knox (UTC-06:00)</summary>
        public static readonly TimeZone America_Indiana_Knox = new TimeZone("America/Indiana/Knox", "Central Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>America/Indiana/Tell_City (UTC-06:00)</summary>
        public static readonly TimeZone America_Indiana_Tell_City = new TimeZone("America/Indiana/Tell_City", "Central Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>America/Managua (UTC-06:00)</summary>
        public static readonly TimeZone America_Managua = new TimeZone("America/Managua", "Central America Standard Time", "(UTC-06:00) Central America");

        /// <summary>America/Matamoros (UTC-06:00)</summary>
        public static readonly TimeZone America_Matamoros = new TimeZone("America/Matamoros", "Central Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>America/Menominee (UTC-06:00)</summary>
        public static readonly TimeZone America_Menominee = new TimeZone("America/Menominee", "Central Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>America/Merida (UTC-06:00)</summary>
        public static readonly TimeZone America_Merida = new TimeZone("America/Merida", "Central Standard Time (Mexico)", "(UTC-06:00) Guadalajara, Mexico City, Monterrey");

        /// <summary>America/Mexico_City (UTC-06:00)</summary>
        public static readonly TimeZone America_Mexico_City = new TimeZone("America/Mexico_City", "Central Standard Time (Mexico)", "(UTC-06:00) Guadalajara, Mexico City, Monterrey");

        /// <summary>America/Monterrey (UTC-06:00)</summary>
        public static readonly TimeZone America_Monterrey = new TimeZone("America/Monterrey", "Central Standard Time (Mexico)", "(UTC-06:00) Guadalajara, Mexico City, Monterrey");

        /// <summary>America/North_Dakota/Center (UTC-06:00)</summary>
        public static readonly TimeZone America_North_Dakota_Center = new TimeZone("America/North_Dakota/Center", "Central Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>America/North_Dakota/New_Salem (UTC-06:00)</summary>
        public static readonly TimeZone America_North_Dakota_New_Salem = new TimeZone("America/North_Dakota/New_Salem", "Central Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>America/Rainy_River (UTC-06:00)</summary>
        public static readonly TimeZone America_Rainy_River = new TimeZone("America/Rainy_River", "Central Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>America/Rankin_Inlet (UTC-06:00)</summary>
        public static readonly TimeZone America_Rankin_Inlet = new TimeZone("America/Rankin_Inlet", "Central Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>America/Regina (UTC-06:00)</summary>
        public static readonly TimeZone America_Regina = new TimeZone("America/Regina", "Canada Central Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>America/Swift_Current (UTC-06:00)</summary>
        public static readonly TimeZone America_Swift_Current = new TimeZone("America/Swift_Current", "Canada Central Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>America/Tegucigalpa (UTC-06:00)</summary>
        public static readonly TimeZone America_Tegucigalpa = new TimeZone("America/Tegucigalpa", "Central America Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>America/Winnipeg (UTC-06:00)</summary>
        public static readonly TimeZone America_Winnipeg = new TimeZone("America/Winnipeg", "Central Standard Time", "(UTC-06:00) Central Time(US & Canada)");

        /// <summary>Pacific/Easter (UTC-06:00)</summary>
        public static readonly TimeZone Pacific_Easter = new TimeZone("Pacific/Easter", "Central America Standard Time", "(UTC-06:00) Central America");

        /// <summary>Pacific/Galapagos (UTC-06:00)</summary>
        public static readonly TimeZone Pacific_Galapagos = new TimeZone("Pacific/Galapagos", "Central America Standard Time", "(UTC-06:00) Central America");

        /// <summary>America/Atikokan (UTC-05:00)</summary>
        public static readonly TimeZone America_Atikokan = new TimeZone("America/Atikokan", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Cancun (UTC-05:00)</summary>
        public static readonly TimeZone America_Cancun = new TimeZone("America/Cancun", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Bogota (UTC-05:00)</summary>
        public static readonly TimeZone America_Bogota = new TimeZone("America/Bogota", "SA Pacific Standard Time", "(UTC-05:00) Bogota, Lima, Quito");

        /// <summary>America/Cayman (UTC-05:00)</summary>
        public static readonly TimeZone America_Cayman = new TimeZone("America/Cayman", "SA Pacific Standard Time", "(UTC-05:00) Bogota, Lima, Quito");

        /// <summary>America/Detroit (UTC-05:00)</summary>
        public static readonly TimeZone America_Detroit = new TimeZone("America/Detroit", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Grand_Turk (UTC-05:00)</summary>
        public static readonly TimeZone America_Grand_Turk = new TimeZone("America/Grand_Turk", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Guayaquil (UTC-05:00)</summary>
        public static readonly TimeZone America_Guayaquil = new TimeZone("America/Guayaquil", "SA Pacific Standard Time", "(UTC-05:00) Bogota, Lima, Quito");

        /// <summary>America/Havana (UTC-05:00)</summary>
        public static readonly TimeZone America_Havana = new TimeZone("America/Havana", "SA Pacific Standard Time", "(UTC-05:00) Bogota, Lima, Quito");

        /// <summary>America/Indiana/Indianapolis (UTC-05:00)</summary>
        public static readonly TimeZone America_Indiana_Indianapolis = new TimeZone("America/Indiana/Indianapolis", "US Eastern Standard Time", "(UTC-05:00) Indiana(East)");

        /// <summary>America/Indiana/Marengo (UTC-05:00)</summary>
        public static readonly TimeZone America_Indiana_Marengo = new TimeZone("America/Indiana/Marengo", "US Eastern Standard Time", "(UTC-05:00) Indiana(East)");

        /// <summary>America/Indiana/Petersburg (UTC-05:00)</summary>
        public static readonly TimeZone America_Indiana_Petersburg = new TimeZone("America/Indiana/Petersburg", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Indiana/Vevay (UTC-05:00)</summary>
        public static readonly TimeZone America_Indiana_Vevay = new TimeZone("America/Indiana/Vevay", "US Eastern Standard Time", "(UTC-05:00) Indiana(East)");

        /// <summary>America/Indiana/Vincennes (UTC-05:00)</summary>
        public static readonly TimeZone America_Indiana_Vincennes = new TimeZone("America/Indiana/Vincennes", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Indiana/Winamac (UTC-05:00)</summary>
        public static readonly TimeZone America_Indiana_Winamac = new TimeZone("America/Indiana/Winamac", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Iqaluit (UTC-05:00)</summary>
        public static readonly TimeZone America_Iqaluit = new TimeZone("America/Iqaluit", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Jamaica (UTC-05:00)</summary>
        public static readonly TimeZone America_Jamaica = new TimeZone("America/Jamaica", "SA Pacific Standard Time", "(UTC-05:00) Bogota, Lima, Quito");

        /// <summary>America/Kentucky/Louisville (UTC-05:00)</summary>
        public static readonly TimeZone America_Kentucky_Louisville = new TimeZone("America/Kentucky/Louisville", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Kentucky/Monticello (UTC-05:00)</summary>
        public static readonly TimeZone America_Kentucky_Monticello = new TimeZone("America/Kentucky/Monticello", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Lima (UTC-05:00)</summary>
        public static readonly TimeZone America_Lima = new TimeZone("America/Lima", "SA Pacific Standard Time", "(UTC-05:00) Bogota, Lima, Quito");

        /// <summary>America/Montreal (UTC-05:00)</summary>
        public static readonly TimeZone America_Montreal = new TimeZone("America/Montreal", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Nassau (UTC-05:00)</summary>
        public static readonly TimeZone America_Nassau = new TimeZone("America/Nassau", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/New_York (UTC-05:00)</summary>
        public static readonly TimeZone America_New_York = new TimeZone("America/New_York", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Nipigon (UTC-05:00)</summary>
        public static readonly TimeZone America_Nipigon = new TimeZone("America/Nipigon", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Panama (UTC-05:00)</summary>
        public static readonly TimeZone America_Panama = new TimeZone("America/Panama", "SA Pacific Standard Time", "(UTC-05:00) Bogota, Lima, Quito");

        /// <summary>America/Pangnirtung (UTC-05:00)</summary>
        public static readonly TimeZone America_Pangnirtung = new TimeZone("America/Pangnirtung", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Port-au-Prince (UTC-05:00)</summary>
        public static readonly TimeZone America_Port_au_Prince = new TimeZone("America/Port-au-Prince", "SA Pacific Standard Time", "(UTC-05:00) Bogota, Lima, Quito");

        /// <summary>America/Resolute (UTC-05:00)</summary>
        public static readonly TimeZone America_Resolute = new TimeZone("America/Resolute", "Central Standard Time", "(UTC-05:00) Central Time(US & Canada)");

        /// <summary>America/Thunder_Bay (UTC-05:00)</summary>
        public static readonly TimeZone America_Thunder_Bay = new TimeZone("America/Thunder_Bay", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Toronto (UTC-05:00)</summary>
        public static readonly TimeZone America_Toronto = new TimeZone("America/Toronto", "Eastern Standard Time", "(UTC-05:00) Eastern Time(US & Canada)");

        /// <summary>America/Caracas (UTC-04:30)</summary>
        public static readonly TimeZone America_Caracas = new TimeZone("America/Caracas", "Venezuela Standard Time", "(UTC-04:30) Caracas");

        /// <summary>America/Anguilla (UTC-04:00)</summary>
        public static readonly TimeZone America_Anguilla = new TimeZone("America/Anguilla", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Antigua (UTC-04:00)</summary>
        public static readonly TimeZone America_Antigua = new TimeZone("America/Antigua", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Aruba (UTC-04:00)</summary>
        public static readonly TimeZone America_Aruba = new TimeZone("America/Aruba", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleze");

        /// <summary>America/Asuncion (UTC-04:00)</summary>
        public static readonly TimeZone America_Asuncion = new TimeZone("America/Asuncion", "Paraguay Standard Time", "(UTC-04:00) Asuncion");

        /// <summary>America/Barbados (UTC-04:00)</summary>
        public static readonly TimeZone America_Barbados = new TimeZone("America/Barbados", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Blanc-Sablon (UTC-04:00)</summary>
        public static readonly TimeZone America_Blanc_Sablon = new TimeZone("America/Blanc-Sablon", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Boa_Vista (UTC-04:00)</summary>
        public static readonly TimeZone America_Boa_Vista = new TimeZone("America/Boa_Vista", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Campo_Grande (UTC-04:00)</summary>
        public static readonly TimeZone America_Campo_Grande = new TimeZone("America/Campo_Grande", "Central Brazilian Standard Time", "(UTC-04:00) Cuiaba");

        /// <summary>America/Cuiaba (UTC-04:00)</summary>
        public static readonly TimeZone America_Cuiaba = new TimeZone("America/Cuiaba", "Central Brazilian Standard Time", "(UTC-04:00) Cuiaba");

        /// <summary>America/Curacao (UTC-04:00)</summary>
        public static readonly TimeZone America_Curacao = new TimeZone("America/Curacao", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Dominica (UTC-04:00)</summary>
        public static readonly TimeZone America_Dominica = new TimeZone("America/Dominica", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Eirunepe (UTC-04:00)</summary>
        public static readonly TimeZone America_Eirunepe = new TimeZone("America/Eirunepe", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Glace_Bay (UTC-04:00)</summary>
        public static readonly TimeZone America_Glace_Bay = new TimeZone("America/Glace_Bay", "Atlantic Standard Time", "(UTC-04:00) Atlantic Time(Canada)");

        /// <summary>America/Goose_Bay (UTC-04:00)</summary>
        public static readonly TimeZone America_Goose_Bay = new TimeZone("America/Goose_Bay", "Atlantic Standard Time", "(UTC-04:00) Atlantic Time(Canada)");

        /// <summary>America/Grenada (UTC-04:00)</summary>
        public static readonly TimeZone America_Grenada = new TimeZone("America/Grenada", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Guadeloupe (UTC-04:00)</summary>
        public static readonly TimeZone America_Guadeloupe = new TimeZone("America/Guadeloupe", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Guyana (UTC-04:00)</summary>
        public static readonly TimeZone America_Guyana = new TimeZone("America/Guyana", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Halifax (UTC-04:00)</summary>
        public static readonly TimeZone America_Halifax = new TimeZone("America/Halifax", "Atlantic Standard Time", "(UTC-04:00) Atlantic Time(Canada)");

        /// <summary>America/La_Paz (UTC-04:00)</summary>
        public static readonly TimeZone America_La_Paz = new TimeZone("America/La_Paz", "SA Western Standard Time", "(UTC-04:00) Georgetown, La Paz, Manaus, San Juan");

        /// <summary>America/Manaus (UTC-04:00)</summary>
        public static readonly TimeZone America_Manaus = new TimeZone("America/Manaus", "SA Western Standard Time", "(UTC-04:00) Georgetown, La Paz, Manaus, San Juan");

        /// <summary>America/Marigot (UTC-04:00)</summary>
        public static readonly TimeZone America_Marigot = new TimeZone("America/Marigot", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Martinique (UTC-04:00)</summary>
        public static readonly TimeZone America_Martinique = new TimeZone("America/Martinique", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Moncton (UTC-04:00)</summary>
        public static readonly TimeZone America_Moncton = new TimeZone("America/Moncton", "Atlantic Standard Time", "(UTC-04:00) Atlantic Time(Canada)");

        /// <summary>America/Montserrat (UTC-04:00)</summary>
        public static readonly TimeZone America_Montserrat = new TimeZone("America/Montserrat", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Port_of_Spain (UTC-04:00)</summary>
        public static readonly TimeZone America_Port_of_Spain = new TimeZone("America/Port_of_Spain", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Porto_Velho (UTC-04:00)</summary>
        public static readonly TimeZone America_Porto_Velho = new TimeZone("America/Porto_Velho", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Puerto_Rico (UTC-04:00)</summary>
        public static readonly TimeZone America_Puerto_Rico = new TimeZone("America/Puerto_Rico", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Rio_Branco (UTC-04:00)</summary>
        public static readonly TimeZone America_Rio_Branco = new TimeZone("America/Rio_Branco", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Santiago (UTC-04:00)</summary>
        public static readonly TimeZone America_Santiago = new TimeZone("America/Santiago", "Pacific SA Standard Time", "(UTC-04:00) Santiago");

        /// <summary>America/Santo_Domingo (UTC-04:00)</summary>
        public static readonly TimeZone America_Santo_Domingo = new TimeZone("America/Santo_Domingo", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/St_Barthelemy (UTC-04:00)</summary>
        public static readonly TimeZone America_St_Barthelemy = new TimeZone("America/St_Barthelemy", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/St_Kitts (UTC-04:00)</summary>
        public static readonly TimeZone America_St_Kitts = new TimeZone("America/St_Kitts", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/St_Lucia (UTC-04:00)</summary>
        public static readonly TimeZone America_St_Lucia = new TimeZone("America/St_Lucia", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/St_Thomas (UTC-04:00)</summary>
        public static readonly TimeZone America_St_Thomas = new TimeZone("America/St_Thomas", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/St_Vincent (UTC-04:00)</summary>
        public static readonly TimeZone America_St_Vincent = new TimeZone("America/St_Vincent", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>America/Thule (UTC-04:00)</summary>
        public static readonly TimeZone America_Thule = new TimeZone("America/Thule", "Atlantic Standard Time", "(UTC-04:00) Atlantic Time(Canada)");

        /// <summary>America/Tortola (UTC-04:00)</summary>
        public static readonly TimeZone America_Tortola = new TimeZone("America/Tortola", "SA Western Standard Time", "(UTC-04:00) Cayenne, Fortaleza");

        /// <summary>Antarctica/Palmer (UTC-04:00)</summary>
        public static readonly TimeZone Antarctica_Palmer = new TimeZone("Antarctica/Palmer", "Pacific SA Standard Time", "(UTC-04:00) Santiago");

        /// <summary>Atlantic/Bermuda (UTC-04:00)</summary>
        public static readonly TimeZone Atlantic_Bermuda = new TimeZone("Atlantic/Bermuda", "Atlantic Standard Time", "(UTC-04:00) Atlantic Time(Canada)");

        /// <summary>America/St_Johns (UTC-03:30)</summary>
        public static readonly TimeZone America_St_Johns = new TimeZone("America/St_Johns", "Newfoundland Standard Time", "(UTC-03:30) Newfoundland");

        /// <summary>America/Araguaina (UTC-03:00)</summary>
        public static readonly TimeZone America_Araguaina = new TimeZone("America/Araguaina", "SA Eastern Standard Time", "(UTC-03:00) Cayenne, Fortaleza");

        /// <summary>America/Argentina/Buenos_Aires (UTC-03:00)</summary>
        public static readonly TimeZone America_Argentina_Buenos_Aires = new TimeZone("America/Argentina/Buenos_Aires", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Argentina/Catamarca (UTC-03:00)</summary>
        public static readonly TimeZone America_Argentina_Catamarca = new TimeZone("America/Argentina/Catamarca", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Argentina/Cordoba (UTC-03:00)</summary>
        public static readonly TimeZone America_Argentina_Cordoba = new TimeZone("America/Argentina/Cordoba", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Argentina/Jujuy (UTC-03:00)</summary>
        public static readonly TimeZone America_Argentina_Jujuy = new TimeZone("America/Argentina/Jujuy", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Argentina/La_Rioja (UTC-03:00)</summary>
        public static readonly TimeZone America_Argentina_La_Rioja = new TimeZone("America/Argentina/La_Rioja", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Argentina/Mendoza (UTC-03:00)</summary>
        public static readonly TimeZone America_Argentina_Mendoza = new TimeZone("America/Argentina/Mendoza", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Argentina/Rio_Gallegos (UTC-03:00)</summary>
        public static readonly TimeZone America_Argentina_Rio_Gallegos = new TimeZone("America/Argentina/Rio_Gallegos", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Argentina/Salta (UTC-03:00)</summary>
        public static readonly TimeZone America_Argentina_Salta = new TimeZone("America/Argentina/Salta", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Argentina/San_Juan (UTC-03:00)</summary>
        public static readonly TimeZone America_Argentina_San_Juan = new TimeZone("America/Argentina/San_Juan", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Argentina/San_Luis (UTC-03:00)</summary>
        public static readonly TimeZone America_Argentina_San_Luis = new TimeZone("America/Argentina/San_Luis", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Argentina/Tucuman (UTC-03:00)</summary>
        public static readonly TimeZone America_Argentina_Tucuman = new TimeZone("America/Argentina/Tucuman", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Argentina/Ushuaia (UTC-03:00)</summary>
        public static readonly TimeZone America_Argentina_Ushuaia = new TimeZone("America/Argentina/Ushuaia", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Bahia (UTC-03:00)</summary>
        public static readonly TimeZone America_Bahia = new TimeZone("America/Bahia", "Bahia Standard Time", "(UTC-03:00) Bahia");

        /// <summary>America/Belem (UTC-03:00)</summary>
        public static readonly TimeZone America_Belem = new TimeZone("America/Belem", "SA Eastern Standard Time", "(UTC-03:00) Cayenne, Fortaleza");

        /// <summary>America/Cayenne (UTC-03:00)</summary>
        public static readonly TimeZone America_Cayenne = new TimeZone("America/Cayenne", "SA Eastern Standard Time", "(UTC-03:00) Cayenne, Fortaleza");

        /// <summary>America/Fortaleza (UTC-03:00)</summary>
        public static readonly TimeZone America_Fortaleza = new TimeZone("America/Fortaleza", "SA Eastern Standard Time", "(UTC-03:00) Cayenne, Fortaleza");

        /// <summary>America/Godthab (UTC-03:00)</summary>
        public static readonly TimeZone America_Godthab = new TimeZone("America/Godthab", "Greenland Standard Time", "(UTC-03:00) Greenland");

        /// <summary>America/Maceio (UTC-03:00)</summary>
        public static readonly TimeZone America_Maceio = new TimeZone("America/Maceio", "SA Eastern Standard Time", "(UTC-03:00) Cayenne, Fortaleza");

        /// <summary>America/Miquelon (UTC-03:00)</summary>
        public static readonly TimeZone America_Miquelon = new TimeZone("America/Miquelon", "SA Eastern Standard Time", "(UTC-03:00) Cayenne, Fortaleza");

        /// <summary>America/Montevideo (UTC-03:00)</summary>
        public static readonly TimeZone America_Montevideo = new TimeZone("America/Montevideo", "Montevideo Standard Time", "(UTC-03:00) Montevideo");

        /// <summary>America/Paramaribo (UTC-03:00)</summary>
        public static readonly TimeZone America_Paramaribo = new TimeZone("America/Paramaribo", "SA Eastern Standard Time", "(UTC-03:00) Cayenne, Fortaleza");

        /// <summary>America/Recife (UTC-03:00)</summary>
        public static readonly TimeZone America_Recife = new TimeZone("America/Recife", "SA Eastern Standard Time", "(UTC-03:00) Cayenne, Fortaleza");

        /// <summary>America/Santarem (UTC-03:00)</summary>
        public static readonly TimeZone America_Santarem = new TimeZone("America/Santarem", "SA Eastern Standard Time", "(UTC-03:00) Cayenne, Fortaleza");

        /// <summary>America/Sao_Paulo (UTC-03:00)</summary>
        public static readonly TimeZone America_Sao_Paulo = new TimeZone("America/Sao_Paulo", "E. South America Standard Time", "(UTC-03:00) Sao Paulo");

        /// <summary>Antarctica/Rothera (UTC-03:00)</summary>
        public static readonly TimeZone Antarctica_Rothera = new TimeZone("Antarctica/Rothera", "SA Eastern Standard Time", "(UTC-03:00) Cayenne, Fortaleza");

        /// <summary>Atlantic/Stanley (UTC-03:00)</summary>
        public static readonly TimeZone Atlantic_Stanley = new TimeZone("Atlantic/Stanley", "Argentina Standard Time", "(UTC-03:00) Buenos Aires");

        /// <summary>America/Noronha (UTC-02:00)</summary>
        public static readonly TimeZone America_Noronha = new TimeZone("America/Noronha", "UTC-02", "(UTC-02:00)");

        /// <summary>Atlantic/South_Georgia (UTC-02:00)</summary>
        public static readonly TimeZone Atlantic_South_Georgia = new TimeZone("Atlantic/South_Georgia", "UTC-02", "(UTC-02:00)");

        /// <summary>America/Scoresbysund (UTC-01:00)</summary>
        public static readonly TimeZone America_Scoresbysund = new TimeZone("America/Scoresbysund", "Azores Standard Time", "(UTC-01:00) Azores");

        /// <summary>Atlantic/Azores (UTC-01:00)</summary>
        public static readonly TimeZone Atlantic_Azores = new TimeZone("Atlantic/Azores", "Azores Standard Time", "(UTC-01:00) Azores");

        /// <summary>Atlantic/Cape_Verde (UTC-01:00)</summary>
        public static readonly TimeZone Atlantic_Cape_Verde = new TimeZone("Atlantic/Cape_Verde", "Cape Verde Standard Time", "(UTC-01:00) Cape Verde Is.");

        /// <summary>Africa/Abidjan (UTC)</summary>
        public static readonly TimeZone Africa_Abidjan = new TimeZone("Africa/Abidjan", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/Accra (UTC)</summary>
        public static readonly TimeZone Africa_Accra = new TimeZone("Africa/Accra", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/Bamako (UTC)</summary>
        public static readonly TimeZone Africa_Bamako = new TimeZone("Africa/Bamako", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/Banjul (UTC)</summary>
        public static readonly TimeZone Africa_Banjul = new TimeZone("Africa/Banjul", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/Bissau (UTC)</summary>
        public static readonly TimeZone Africa_Bissau = new TimeZone("Africa/Bissau", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/Casablanca (UTC)</summary>
        public static readonly TimeZone Africa_Casablanca = new TimeZone("Africa/Casablanca", "Morocco Standard Time", "(UTC) Casablanca");

        /// <summary>Africa/Conakry (UTC)</summary>
        public static readonly TimeZone Africa_Conakry = new TimeZone("Africa/Conakry", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/Dakar (UTC)</summary>
        public static readonly TimeZone Africa_Dakar = new TimeZone("Africa/Dakar", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/El_Aaiun (UTC)</summary>
        public static readonly TimeZone Africa_El_Aaiun = new TimeZone("Africa/El_Aaiun", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/Freetown (UTC)</summary>
        public static readonly TimeZone Africa_Freetown = new TimeZone("Africa/Freetown", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/Lome (UTC)</summary>
        public static readonly TimeZone Africa_Lome = new TimeZone("Africa/Lome", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/Monrovia (UTC)</summary>
        public static readonly TimeZone Africa_Monrovia = new TimeZone("Africa/Monrovia", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/Nouakchott (UTC)</summary>
        public static readonly TimeZone Africa_Nouakchott = new TimeZone("Africa/Nouakchott", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/Ouagadougou (UTC)</summary>
        public static readonly TimeZone Africa_Ouagadougou = new TimeZone("Africa/Ouagadougou", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Africa/Sao_Tome (UTC)</summary>
        public static readonly TimeZone Africa_Sao_Tome = new TimeZone("Africa/Sao_Tome", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>America/Danmarkshavn (UTC)</summary>
        public static readonly TimeZone America_Danmarkshavn = new TimeZone("America/Danmarkshavn", "UTC", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Antarctica/Vostok (UTC)</summary>
        public static readonly TimeZone Antarctica_Vostok = new TimeZone("Antarctica/Vostok", "Central Asia Standard Time", "(UTC) Astana");

        /// <summary>Atlantic/Canary (UTC)</summary>
        public static readonly TimeZone Atlantic_Canary = new TimeZone("Atlantic/Canary", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Atlantic/Faroe (UTC)</summary>
        public static readonly TimeZone Atlantic_Faroe = new TimeZone("Atlantic/Faroe", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Atlantic/Madeira (UTC)</summary>
        public static readonly TimeZone Atlantic_Madeira = new TimeZone("Atlantic/Madeira", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Atlantic/Reykjavik (UTC)</summary>
        public static readonly TimeZone Atlantic_Reykjavik = new TimeZone("Atlantic/Reykjavik", "Greenwich Standard Time", "(UTC) Monrovia, Reykjavik");

        /// <summary>Atlantic/St_Helena (UTC)</summary>
        public static readonly TimeZone Atlantic_St_Helena = new TimeZone("Atlantic/St_Helena", "Greenwich Standard Time", "(UTC) Monrovia, Reykjavik");

        /// <summary>Europe/Dublin (UTC)</summary>
        public static readonly TimeZone Europe_Dublin = new TimeZone("Europe/Dublin", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Europe/Guernsey (UTC)</summary>
        public static readonly TimeZone Europe_Guernsey = new TimeZone("Europe/Guernsey", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Europe/Isle_of_Man (UTC)</summary>
        public static readonly TimeZone Europe_Isle_of_Man = new TimeZone("Europe/Isle_of_Man", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Europe/Jersey (UTC)</summary>
        public static readonly TimeZone Europe_Jersey = new TimeZone("Europe/Jersey", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Europe/Lisbon (UTC)</summary>
        public static readonly TimeZone Europe_Lisbon = new TimeZone("Europe/Lisbon", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>Europe/London (UTC)</summary>
        public static readonly TimeZone Europe_London = new TimeZone("Europe/London", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>GMT (UTC)</summary>
        public static readonly TimeZone GMT = new TimeZone("GMT", "GMT Standard Time", "(UTC) Dublin, Edinburgh, Lisbon, London");

        /// <summary>UTC (UTC)</summary>
        public static readonly TimeZone UTC = new TimeZone("UTC", "UTC", "(UTC) Coordinated Universal Time");

        /// <summary>Africa/Algiers (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Algiers = new TimeZone("Africa/Algiers", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Bangui (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Bangui = new TimeZone("Africa/Bangui", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Brazzaville (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Brazzaville = new TimeZone("Africa/Brazzaville", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Ceuta (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Ceuta = new TimeZone("Africa/Ceuta", "Romance Standard Time", "(UTC+01:00) Brussels, Copenhagen, Madrid, Paris");

        /// <summary>Africa/Douala (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Douala = new TimeZone("Africa/Douala", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Kinshasa (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Kinshasa = new TimeZone("Africa/Kinshasa", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Lagos (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Lagos = new TimeZone("Africa/Lagos", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Libreville (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Libreville = new TimeZone("Africa/Libreville", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Luanda (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Luanda = new TimeZone("Africa/Luanda", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Malabo (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Malabo = new TimeZone("Africa/Malabo", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Ndjamena (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Ndjamena = new TimeZone("Africa/Ndjamena", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Niamey (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Niamey = new TimeZone("Africa/Niamey", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Porto-Novo (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Porto_Novo = new TimeZone("Africa/Porto-Novo", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Tunis (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Tunis = new TimeZone("Africa/Tunis", "W. Central Africa Standard Time", "(UTC+01:00) West Central Africa");

        /// <summary>Africa/Windhoek (UTC+01:00)</summary>
        public static readonly TimeZone Africa_Windhoek = new TimeZone("Africa/Windhoek", "Namibia Standard Time", "(UTC+01:00) Windhoek");

        /// <summary>Arctic/Longyearbyen (UTC+01:00)</summary>
        public static readonly TimeZone Arctic_Longyearbyen = new TimeZone("Arctic/Longyearbyen", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Amsterdam (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Amsterdam = new TimeZone("Europe/Amsterdam", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Andorra (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Andorra = new TimeZone("Europe/Andorra", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Belgrade (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Belgrade = new TimeZone("Europe/Belgrade", "Central Europe Standard Time", "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague");

        /// <summary>Europe/Berlin (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Berlin = new TimeZone("Europe/Berlin", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Bratislava (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Bratislava = new TimeZone("Europe/Bratislava", "Central Europe Standard Time", "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague");

        /// <summary>Europe/Brussels (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Brussels = new TimeZone("Europe/Brussels", "Romance Standard Time", "(UTC+01:00) Brussels, Copenhagen, Madrid, Paris");

        /// <summary>Europe/Budapest (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Budapest = new TimeZone("Europe/Budapest", "Central Europe Standard Time", "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague");

        /// <summary>Europe/Copenhagen (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Copenhagen = new TimeZone("Europe/Copenhagen", "Romance Standard Time", "(UTC+01:00) Brussels, Copenhagen, Madrid, Paris");

        /// <summary>Europe/Gibraltar (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Gibraltar = new TimeZone("Europe/Gibraltar", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Ljubljana (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Ljubljana = new TimeZone("Europe/Ljubljana", "Central Europe Standard Time", "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague");

        /// <summary>Europe/Luxembourg (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Luxembourg = new TimeZone("Europe/Luxembourg", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Madrid (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Madrid = new TimeZone("Europe/Madrid", "Romance Standard Time", "(UTC+01:00) Brussels, Copenhagen, Madrid, Paris");

        /// <summary>Europe/Malta (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Malta = new TimeZone("Europe/Malta", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Monaco (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Monaco = new TimeZone("Europe/Monaco", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Oslo (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Oslo = new TimeZone("Europe/Oslo", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Paris (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Paris = new TimeZone("Europe/Paris", "Romance Standard Time", "(UTC+01:00) Brussels, Copenhagen, Madrid, Paris");

        /// <summary>Europe/Podgorica (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Podgorica = new TimeZone("Europe/Podgorica", "Central Europe Standard Time", "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague");

        /// <summary>Europe/Prague (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Prague = new TimeZone("Europe/Prague", "Central Europe Standard Time", "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague");

        /// <summary>Europe/Rome (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Rome = new TimeZone("Europe/Rome", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/San_Marino (UTC+01:00)</summary>
        public static readonly TimeZone Europe_San_Marino = new TimeZone("Europe/San_Marino", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Sarajevo (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Sarajevo = new TimeZone("Europe/Sarajevo", "Central Europe Standard Time", "(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb");

        /// <summary>Europe/Skopje (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Skopje = new TimeZone("Europe/Skopje", "Central Europe Standard Time", "(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb");

        /// <summary>Europe/Stockholm (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Stockholm = new TimeZone("Europe/Stockholm", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Tirane (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Tirane = new TimeZone("Europe/Tirane", "Central Europe Standard Time", "(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb");

        /// <summary>Europe/Vaduz (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Vaduz = new TimeZone("Europe/Vaduz", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Vatican (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Vatican = new TimeZone("Europe/Vatican", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Vienna (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Vienna = new TimeZone("Europe/Vienna", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Europe/Warsaw (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Warsaw = new TimeZone("Europe/Warsaw", "Central Europe Standard Time", "(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb");

        /// <summary>Europe/Zagreb (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Zagreb = new TimeZone("Europe/Zagreb", "Central Europe Standard Time", "(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb");

        /// <summary>Europe/Zurich (UTC+01:00)</summary>
        public static readonly TimeZone Europe_Zurich = new TimeZone("Europe/Zurich", "W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");

        /// <summary>Africa/Blantyre (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Blantyre = new TimeZone("Africa/Blantyre", "South Africa Standard Time", "(UTC+02:00) Harare, Pretoria");

        /// <summary>Africa/Bujumbura (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Bujumbura = new TimeZone("Africa/Bujumbura", "South Africa Standard Time", "(UTC+02:00) Harare, Pretoria");

        /// <summary>Africa/Cairo (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Cairo = new TimeZone("Africa/Cairo", "Egypt Standard Time", "(UTC+02:00) Cairo");

        /// <summary>Africa/Gaborone (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Gaborone = new TimeZone("Africa/Gaborone", "South Africa Standard Time", "(UTC+02:00) Harare, Pretoria");

        /// <summary>Africa/Harare (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Harare = new TimeZone("Africa/Harare", "South Africa Standard Time", "(UTC+02:00) Harare, Pretoria");

        /// <summary>Africa/Johannesburg (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Johannesburg = new TimeZone("Africa/Johannesburg", "South Africa Standard Time", "(UTC+02:00) Harare, Pretoria");

        /// <summary>Africa/Kigali (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Kigali = new TimeZone("Africa/Kigali", "South Africa Standard Time", "(UTC+02:00) Harare, Pretoria");

        /// <summary>Africa/Lubumbashi (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Lubumbashi = new TimeZone("Africa/Lubumbashi", "South Africa Standard Time", "(UTC+02:00) Harare, Pretoria");

        /// <summary>Africa/Lusaka (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Lusaka = new TimeZone("Africa/Lusaka", "South Africa Standard Time", "(UTC+02:00) Harare, Pretoria");

        /// <summary>Africa/Maputo (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Maputo = new TimeZone("Africa/Maputo", "South Africa Standard Time", "(UTC+02:00) Harare, Pretoria");

        /// <summary>Africa/Maseru (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Maseru = new TimeZone("Africa/Maseru", "South Africa Standard Time", "(UTC+02:00) Harare, Pretoria");

        /// <summary>Africa/Mbabane (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Mbabane = new TimeZone("Africa/Mbabane", "South Africa Standard Time", "(UTC+02:00) Harare, Pretoria");

        /// <summary>Africa/Tripoli (UTC+02:00)</summary>
        public static readonly TimeZone Africa_Tripoli = new TimeZone("Africa/Tripoli", "South Africa Standard Time", "(UTC+02:00) Harare, Pretoria");

        /// <summary>Asia/Amman (UTC+02:00)</summary>
        public static readonly TimeZone Asia_Amman = new TimeZone("Asia/Amman", "Jordan Standard Time", "(UTC+02:00) Amman");

        /// <summary>Asia/Beirut (UTC+02:00)</summary>
        public static readonly TimeZone Asia_Beirut = new TimeZone("Asia/Beirut", "Middle East Standard Time", "(UTC+02:00) Beirut");

        /// <summary>Asia/Damascus (UTC+02:00)</summary>
        public static readonly TimeZone Asia_Damascus = new TimeZone("Asia/Damascus", "Syria Standard Time", "(UTC+02:00) Damascus");

        /// <summary>Asia/Gaza (UTC+02:00)</summary>
        public static readonly TimeZone Asia_Gaza = new TimeZone("Asia/Gaza", "Egypt Standard Time", "(UTC+02:00) Cairo");

        /// <summary>Asia/Jerusalem (UTC+02:00)</summary>
        public static readonly TimeZone Asia_Jerusalem = new TimeZone("Asia/Jerusalem", "Israel Standard Time", "(UTC+02:00) Jerusalem");

        /// <summary>Asia/Nicosia (UTC+02:00)</summary>
        public static readonly TimeZone Asia_Nicosia = new TimeZone("Asia/Nicosia", "E. Europe Standard Time", "(UTC+02:00) Nicosia");

        /// <summary>Europe/Athens (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Athens = new TimeZone("Europe/Athens", "GTB Standard Time", "(UTC+02:00) Athens, Bucharest");

        /// <summary>Europe/Bucharest (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Bucharest = new TimeZone("Europe/Bucharest", "GTB Standard Time", "(UTC+02:00) Athens, Bucharest");

        /// <summary>Europe/Chisinau (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Chisinau = new TimeZone("Europe/Chisinau", "GTB Standard Time", "(UTC+02:00) Athens, Bucharest");

        /// <summary>Europe/Helsinki (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Helsinki = new TimeZone("Europe/Helsinki", "FLE Standard Time", "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius");

        /// <summary>Europe/Istanbul (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Istanbul = new TimeZone("Europe/Istanbul", "Turkey Standard Time", "(UTC+02:00) Istanbul");

        /// <summary>Europe/Kiev (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Kiev = new TimeZone("Europe/Kiev", "FLE Standard Time", "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius");

        /// <summary>Europe/Mariehamn (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Mariehamn = new TimeZone("Europe/Mariehamn", "FLE Standard Time", "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius");

        /// <summary>Europe/Riga (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Riga = new TimeZone("Europe/Riga", "FLE Standard Time", "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius");

        /// <summary>Europe/Simferopol (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Simferopol = new TimeZone("Europe/Simferopol", "FLE Standard Time", "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius");

        /// <summary>Europe/Sofia (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Sofia = new TimeZone("Europe/Sofia", "FLE Standard Time", "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius");

        /// <summary>Europe/Tallinn (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Tallinn = new TimeZone("Europe/Tallinn", "FLE Standard Time", "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius");

        /// <summary>Europe/Uzhgorod (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Uzhgorod = new TimeZone("Europe/Uzhgorod", "FLE Standard Time", "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius");

        /// <summary>Europe/Vilnius (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Vilnius = new TimeZone("Europe/Vilnius", "FLE Standard Time", "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius");

        /// <summary>Europe/Zaporozhye (UTC+02:00)</summary>
        public static readonly TimeZone Europe_Zaporozhye = new TimeZone("Europe/Zaporozhye", "FLE Standard Time", "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius");

        /// <summary>Africa/Addis_Ababa (UTC+03:00)</summary>
        public static readonly TimeZone Africa_Addis_Ababa = new TimeZone("Africa/Addis_Ababa", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Africa/Asmara (UTC+03:00)</summary>
        public static readonly TimeZone Africa_Asmara = new TimeZone("Africa/Asmara", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Africa/Dar_es_Salaam (UTC+03:00)</summary>
        public static readonly TimeZone Africa_Dar_es_Salaam = new TimeZone("Africa/Dar_es_Salaam", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Africa/Djibouti (UTC+03:00)</summary>
        public static readonly TimeZone Africa_Djibouti = new TimeZone("Africa/Djibouti", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Africa/Juba (UTC+03:00)</summary>
        public static readonly TimeZone Africa_Juba = new TimeZone("Africa/Juba", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Africa/Kampala (UTC+03:00)</summary>
        public static readonly TimeZone Africa_Kampala = new TimeZone("Africa/Kampala", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Africa/Khartoum (UTC+03:00)</summary>
        public static readonly TimeZone Africa_Khartoum = new TimeZone("Africa/Khartoum", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Africa/Mogadishu (UTC+03:00)</summary>
        public static readonly TimeZone Africa_Mogadishu = new TimeZone("Africa/Mogadishu", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Africa/Nairobi (UTC+03:00)</summary>
        public static readonly TimeZone Africa_Nairobi = new TimeZone("Africa/Nairobi", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Antarctica/Syowa (UTC+03:00)</summary>
        public static readonly TimeZone Antarctica_Syowa = new TimeZone("Antarctica/Syowa", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Asia/Aden (UTC+03:00)</summary>
        public static readonly TimeZone Asia_Aden = new TimeZone("Asia/Aden", "Arab Standard Time", "(UTC+03:00) Kuwait, Riyadh");

        /// <summary>Asia/Baghdad (UTC+03:00)</summary>
        public static readonly TimeZone Asia_Baghdad = new TimeZone("Asia/Baghdad", "Arabic Standard Time", "(UTC+03:00) Baghdad");

        /// <summary>Asia/Bahrain (UTC+03:00)</summary>
        public static readonly TimeZone Asia_Bahrain = new TimeZone("Asia/Bahrain", "Arabic Standard Time", "(UTC+03:00) Baghdad");

        /// <summary>Asia/Kuwait (UTC+03:00)</summary>
        public static readonly TimeZone Asia_Kuwait = new TimeZone("Asia/Kuwait", "Arab Standard Time", "(UTC+03:00) Kuwait, Riyadh");

        /// <summary>Asia/Qatar (UTC+03:00)</summary>
        public static readonly TimeZone Asia_Qatar = new TimeZone("Asia/Qatar", "Arab Standard Time", "(UTC+03:00) Kuwait, Riyadh");

        /// <summary>Asia/Riyadh (UTC+03:00)</summary>
        public static readonly TimeZone Asia_Riyadh = new TimeZone("Asia/Riyadh", "Arab Standard Time", "(UTC+03:00) Kuwait, Riyadh");

        /// <summary>Europe/Kaliningrad (UTC+03:00)</summary>
        public static readonly TimeZone Europe_Kaliningrad = new TimeZone("Europe/Kaliningrad", "Kaliningrad Standard Time", "(UTC+03:00) Kaliningrad");

        /// <summary>Europe/Minsk (UTC+03:00)</summary>
        public static readonly TimeZone Europe_Minsk = new TimeZone("Europe/Minsk", "Kaliningrad Standard Time", "(UTC+03:00) Kaliningrad");

        /// <summary>Indian/Antananarivo (UTC+03:00)</summary>
        public static readonly TimeZone Indian_Antananarivo = new TimeZone("Indian/Antananarivo", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Indian/Comoro (UTC+03:00)</summary>
        public static readonly TimeZone Indian_Comoro = new TimeZone("Indian/Comoro", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Indian/Mayotte (UTC+03:00)</summary>
        public static readonly TimeZone Indian_Mayotte = new TimeZone("Indian/Mayotte", "E. Africa Standard Time", "(UTC+03:00) Nairobi");

        /// <summary>Asia/Tehran (UTC+03:30)</summary>
        public static readonly TimeZone Asia_Tehran = new TimeZone("Asia/Tehran", "Iran Standard Time", "(UTC+03:30) Tehran");

        /// <summary>Asia/Baku (UTC+04:00)</summary>
        public static readonly TimeZone Asia_Baku = new TimeZone("Asia/Baku", "Azerbaijan Standard Time", "(UTC+04:00) Azerbaijan");

        /// <summary>Asia/Dubai (UTC+04:00)</summary>
        public static readonly TimeZone Asia_Dubai = new TimeZone("Asia/Dubai", "Arabian Standard Time", "(UTC+04:00) Abu Dhabi, Muscat");

        /// <summary>Asia/Muscat (UTC+04:00)</summary>
        public static readonly TimeZone Asia_Muscat = new TimeZone("Asia/Muscat", "Arabian Standard Time", "(UTC+04:00) Abu Dhabi, Muscat");

        /// <summary>Asia/Tbilisi (UTC+04:00)</summary>
        public static readonly TimeZone Asia_Tbilisi = new TimeZone("Asia/Tbilisi", "Georgian Standard Time", "(UTC+04:00) Tbilisi");

        /// <summary>Asia/Yerevan (UTC+04:00)</summary>
        public static readonly TimeZone Asia_Yerevan = new TimeZone("Asia/Yerevan", "Caucasus Standard Time", "(UTC+04:00) Yerevan");

        /// <summary>Europe/Moscow (UTC+04:00)</summary>
        public static readonly TimeZone Europe_Moscow = new TimeZone("Europe/Moscow", "Russian Standard Time", "(UTC+04:00) Moscow, St.Petersburg, Volgograd");

        /// <summary>Europe/Samara (UTC+04:00)</summary>
        public static readonly TimeZone Europe_Samara = new TimeZone("Europe/Samara", "Russian Standard Time", "(UTC+04:00) Moscow, St.Petersburg, Volgograd");

        /// <summary>Europe/Volgograd (UTC+04:00)</summary>
        public static readonly TimeZone Europe_Volgograd = new TimeZone("Europe/Volgograd", "Russian Standard Time", "(UTC+04:00) Moscow, St.Petersburg, Volgograd");

        /// <summary>Indian/Mahe (UTC+04:00)</summary>
        public static readonly TimeZone Indian_Mahe = new TimeZone("Indian/Mahe", "Mauritius Standard Time", "(UTC+04:00) Port Louis");

        /// <summary>Indian/Mauritius (UTC+04:00)</summary>
        public static readonly TimeZone Indian_Mauritius = new TimeZone("Indian/Mauritius", "Mauritius Standard Time", "(UTC+04:00) Port Louis");

        /// <summary>Indian/Reunion (UTC+04:00)</summary>
        public static readonly TimeZone Indian_Reunion = new TimeZone("Indian/Reunion", "Mauritius Standard Time", "(UTC+04:00) Port Louis");

        /// <summary>Asia/Kabul (UTC+04:30)</summary>
        public static readonly TimeZone Asia_Kabul = new TimeZone("Asia/Kabul", "Afghanistan Standard Time", "(UTC+04:30) Kabul");

        /// <summary>Asia/Aqtau (UTC+05:00)</summary>
        public static readonly TimeZone Asia_Aqtau = new TimeZone("Asia/Aqtau", "West Asia Standard Time", "(UTC+05:00) Tashkent");

        /// <summary>Asia/Aqtobe (UTC+05:00)</summary>
        public static readonly TimeZone Asia_Aqtobe = new TimeZone("Asia/Aqtobe", "West Asia Standard Time", "(UTC+05:00) Tashkent");

        /// <summary>Asia/Ashgabat (UTC+05:00)</summary>
        public static readonly TimeZone Asia_Ashgabat = new TimeZone("Asia/Ashgabat", "West Asia Standard Time", "(UTC+05:00) Tashkent");

        /// <summary>Asia/Dushanbe (UTC+05:00)</summary>
        public static readonly TimeZone Asia_Dushanbe = new TimeZone("Asia/Dushanbe", "West Asia Standard Time", "(UTC+05:00) Tashkent");

        /// <summary>Asia/Karachi (UTC+05:00)</summary>
        public static readonly TimeZone Asia_Karachi = new TimeZone("Asia/Karachi", "Pakistan Standard Time", "(UTC+05:00) Islamabad, Karachi");

        /// <summary>Asia/Oral (UTC+05:00)</summary>
        public static readonly TimeZone Asia_Oral = new TimeZone("Asia/Oral", "West Asia Standard Time", "(UTC+05:00) Tashkent");

        /// <summary>Asia/Samarkand (UTC+05:00)</summary>
        public static readonly TimeZone Asia_Samarkand = new TimeZone("Asia/Samarkand", "West Asia Standard Time", "(UTC+05:00) Tashkent");

        /// <summary>Asia/Tashkent (UTC+05:00)</summary>
        public static readonly TimeZone Asia_Tashkent = new TimeZone("Asia/Tashkent", "West Asia Standard Time", "(UTC+05:00) Tashkent");

        /// <summary>Indian/Kerguelen (UTC+05:00)</summary>
        public static readonly TimeZone Indian_Kerguelen = new TimeZone("Indian/Kerguelen", "West Asia Standard Time", "(UTC+05:00) Tashkent");

        /// <summary>Indian/Maldives (UTC+05:00)</summary>
        public static readonly TimeZone Indian_Maldives = new TimeZone("Indian/Maldives", "West Asia Standard Time", "(UTC+05:00) Tashkent");

        /// <summary>Asia/Colombo (UTC+05:30)</summary>
        public static readonly TimeZone Asia_Colombo = new TimeZone("Asia/Colombo", "Sri Lanka Standard Time", "(UTC+05:30) Sri Jayawardenepura");

        /// <summary>Asia/Kolkata (UTC+05:30)</summary>
        public static readonly TimeZone Asia_Kolkata = new TimeZone("Asia/Kolkata", "India Standard Time", "(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi");

        /// <summary>Asia/Kathmandu (UTC+05:45)</summary>
        public static readonly TimeZone Asia_Kathmandu = new TimeZone("Asia/Kathmandu", "Nepal Standard Time", "(UTC+05:45) Kathmandu");

        /// <summary>Antarctica/Mawson (UTC+06:00)</summary>
        public static readonly TimeZone Antarctica_Mawson = new TimeZone("Antarctica/Mawson", "West Asia Standard Time", "(UTC+06:00) Tashkent");

        /// <summary>Asia/Almaty (UTC+06:00)</summary>
        public static readonly TimeZone Asia_Almaty = new TimeZone("Asia/Almaty", "Central Asia Standard Time", "(UTC+06:00) Astana");

        /// <summary>Asia/Bishkek (UTC+06:00)</summary>
        public static readonly TimeZone Asia_Bishkek = new TimeZone("Asia/Bishkek", "Central Asia Standard Time", "(UTC+06:00) Astana");

        /// <summary>Asia/Dhaka (UTC+06:00)</summary>
        public static readonly TimeZone Asia_Dhaka = new TimeZone("Asia/Dhaka", "Bangladesh Standard Time", "(UTC+06:00) Dhaka");

        /// <summary>Asia/Qyzylorda (UTC+06:00)</summary>
        public static readonly TimeZone Asia_Qyzylorda = new TimeZone("Asia/Qyzylorda", "Central Asia Standard Time", "(UTC+06:00) Astana");

        /// <summary>Asia/Thimphu (UTC+06:00)</summary>
        public static readonly TimeZone Asia_Thimphu = new TimeZone("Asia/Thimphu", "Bangladesh Standard Time", "(UTC+06:00) Dhaka");

        /// <summary>Asia/Yekaterinburg (UTC+06:00)</summary>
        public static readonly TimeZone Asia_Yekaterinburg = new TimeZone("Asia/Yekaterinburg", "Ekaterinburg Standard Time", "(UTC+06:00) Ekaterinburg");

        /// <summary>Indian/Chagos (UTC+06:00)</summary>
        public static readonly TimeZone Indian_Chagos = new TimeZone("Indian/Chagos", "Central Asia Standard Time", "(UTC+06:00) Astana");

        /// <summary>Asia/Rangoon (UTC+06:30)</summary>
        public static readonly TimeZone Asia_Rangoon = new TimeZone("Asia/Rangoon", "Myanmar Standard Time", "(UTC+06:30) Yangon(Rangoon)");

        /// <summary>Indian/Cocos (UTC+06:30)</summary>
        public static readonly TimeZone Indian_Cocos = new TimeZone("Indian/Cocos", "Myanmar Standard Time", "(UTC+06:30) Yangon(Rangoon)");

        /// <summary>Antarctica/Davis (UTC+07:00)</summary>
        public static readonly TimeZone Antarctica_Davis = new TimeZone("Antarctica/Davis", "SE Asia Standard Time", "(UTC+07:00) Bangkok, Hanoi, Jakarta");

        /// <summary>Asia/Bangkok (UTC+07:00)</summary>
        public static readonly TimeZone Asia_Bangkok = new TimeZone("Asia/Bangkok", "SE Asia Standard Time", "(UTC+07:00) Bangkok, Hanoi, Jakarta");

        /// <summary>Asia/Ho_Chi_Minh (UTC+07:00)</summary>
        public static readonly TimeZone Asia_Ho_Chi_Minh = new TimeZone("Asia/Ho_Chi_Minh", "SE Asia Standard Time", "(UTC+07:00) Bangkok, Hanoi, Jakarta");

        /// <summary>Asia/Hovd (UTC+07:00)</summary>
        public static readonly TimeZone Asia_Hovd = new TimeZone("Asia/Hovd", "SE Asia Standard Time", "(UTC+07:00) Bangkok, Hanoi, Jakarta");

        /// <summary>Asia/Jakarta (UTC+07:00)</summary>
        public static readonly TimeZone Asia_Jakarta = new TimeZone("Asia/Jakarta", "SE Asia Standard Time", "(UTC+07:00) Bangkok, Hanoi, Jakarta");

        /// <summary>Asia/Novokuznetsk (UTC+07:00)</summary>
        public static readonly TimeZone Asia_Novokuznetsk = new TimeZone("Asia/Novokuznetsk", "N. Central Asia Standard Time", "(UTC+07:00) Novosibirsk");

        /// <summary>Asia/Novosibirsk (UTC+07:00)</summary>
        public static readonly TimeZone Asia_Novosibirsk = new TimeZone("Asia/Novosibirsk", "N. Central Asia Standard Time", "(UTC+07:00) Novosibirsk");

        /// <summary>Asia/Omsk (UTC+07:00)</summary>
        public static readonly TimeZone Asia_Omsk = new TimeZone("Asia/Omsk", "N. Central Asia Standard Time", "(UTC+07:00) Novosibirsk");

        /// <summary>Asia/Phnom_Penh (UTC+07:00)</summary>
        public static readonly TimeZone Asia_Phnom_Penh = new TimeZone("Asia/Phnom_Penh", "SE Asia Standard Time", "(UTC+07:00) Bangkok, Hanoi, Jakarta");

        /// <summary>Asia/Pontianak (UTC+07:00)</summary>
        public static readonly TimeZone Asia_Pontianak = new TimeZone("Asia/Pontianak", "SE Asia Standard Time", "(UTC+07:00) Bangkok, Hanoi, Jakarta");

        /// <summary>Asia/Vientiane (UTC+07:00)</summary>
        public static readonly TimeZone Asia_Vientiane = new TimeZone("Asia/Vientiane", "SE Asia Standard Time", "(UTC+07:00) Bangkok, Hanoi, Jakarta");

        /// <summary>Indian/Christmas (UTC+07:00)</summary>
        public static readonly TimeZone Indian_Christmas = new TimeZone("Indian/Christmas", "SE Asia Standard Time", "(UTC+07:00) Bangkok, Hanoi, Jakarta");

        /// <summary>Antarctica/Casey (UTC+08:00)</summary>
        public static readonly TimeZone Antarctica_Casey = new TimeZone("Antarctica/Casey", "W. Australia Standard Time", "(UTC+08:00) Perth");

        /// <summary>Asia/Brunei (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Brunei = new TimeZone("Asia/Brunei", "Singapore Standard Time", "(UTC+08:00) Kuala Lumpur, Singapore");

        /// <summary>Asia/Choibalsan (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Choibalsan = new TimeZone("Asia/Choibalsan", "Ulaanbaatar Standard Time", "(UTC+08:00) Ulaanbaatar");

        /// <summary>Asia/Chongqing (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Chongqing = new TimeZone("Asia/Chongqing", "China Standard Time", "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi");

        /// <summary>Asia/Harbin (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Harbin = new TimeZone("Asia/Harbin", "China Standard Time", "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi");

        /// <summary>Asia/Hong_Kong (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Hong_Kong = new TimeZone("Asia/Hong_Kong", "China Standard Time", "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi");

        /// <summary>Asia/Kashgar (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Kashgar = new TimeZone("Asia/Kashgar", "China Standard Time", "(UTC+08:00) Beijing, Chongquing, Hong Kong, Urumqi");

        /// <summary>Asia/Krasnoyarsk (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Krasnoyarsk = new TimeZone("Asia/Krasnoyarsk", "North Asia Standard Time", "(UTC+08:00) Krasnoyarsk");

        /// <summary>Asia/Kuala_Lumpur (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Kuala_Lumpur = new TimeZone("Asia/Kuala_Lumpur", "Singapore Standard Time", "(UTC+08:00) Kuala Lumpur, Singapore");

        /// <summary>Asia/Kuching (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Kuching = new TimeZone("Asia/Kuching", "Singapore Standard Time", "(UTC+08:00) Kuala Lumpur, Singapore");

        /// <summary>Asia/Macau (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Macau = new TimeZone("Asia/Macau", "China Standard Time", "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi");

        /// <summary>Asia/Makassar (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Makassar = new TimeZone("Asia/Makassar", "Singapore Standard Time", "(UTC+08:00) Kuala Lumpur, Singapore");

        /// <summary>Asia/Manila (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Manila = new TimeZone("Asia/Manila", "Singapore Standard Time", "(UTC+08:00) Kuala Lumpur, Singapore");

        /// <summary>Asia/Shanghai (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Shanghai = new TimeZone("Asia/Shanghai", "China Standard Time", "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi");

        /// <summary>Asia/Singapore (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Singapore = new TimeZone("Asia/Singapore", "Singapore Standard Time", "(UTC+08:00) Kuala Lumpur, Singapore");

        /// <summary>Asia/Taipei (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Taipei = new TimeZone("Asia/Taipei", "Taipei Standard Time", "(UTC+08:00) Taipei");

        /// <summary>Asia/Ulaanbaatar (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Ulaanbaatar = new TimeZone("Asia/Ulaanbaatar", "Ulaanbaatar Standard Time", "(UTC+08:00) Ulaanbaatar");

        /// <summary>Asia/Urumqi (UTC+08:00)</summary>
        public static readonly TimeZone Asia_Urumqi = new TimeZone("Asia/Urumqi", "China Standard Time", "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi");

        /// <summary>Australia/Perth (UTC+08:00)</summary>
        public static readonly TimeZone Australia_Perth = new TimeZone("Australia/Perth", "W. Australia Standard Time", "(UTC+08:00) Perth");

        /// <summary>Australia/Eucla (UTC8,75:00)</summary>
        public static readonly TimeZone Australia_Eucla = new TimeZone("Australia/Eucla", "W. Australia Standard Time", "(UTC+08:00) Perth");

        /// <summary>Asia/Dili (UTC+09:00)</summary>
        public static readonly TimeZone Asia_Dili = new TimeZone("Asia/Dili", "Tokyo Standard Time", "(UTC+09:00) Osaka, Sapporo, Tokyo");

        /// <summary>Asia/Irkutsk (UTC+09:00)</summary>
        public static readonly TimeZone Asia_Irkutsk = new TimeZone("Asia/Irkutsk", "North Asia East Standard Time", "(UTC+09:00) Irkutsk");

        /// <summary>Asia/Jayapura (UTC+09:00)</summary>
        public static readonly TimeZone Asia_Jayapura = new TimeZone("Asia/Jayapura", "Tokyo Standard Time", "(UTC+09:00) Osaka, Sapporo, Tokyo");

        /// <summary>Asia/Pyongyang (UTC+09:00)</summary>
        public static readonly TimeZone Asia_Pyongyang = new TimeZone("Asia/Pyongyang", "Korea Standard Time", "(UTC+09:00) Seoul");

        /// <summary>Asia/Seoul (UTC+09:00)</summary>
        public static readonly TimeZone Asia_Seoul = new TimeZone("Asia/Seoul", "Korea Standard Time", "(UTC+09:00) Seoul");

        /// <summary>Asia/Tokyo (UTC+09:00)</summary>
        public static readonly TimeZone Asia_Tokyo = new TimeZone("Asia/Tokyo", "Tokyo Standard Time", "(UTC+09:00) Osaka, Sapporo, Tokyo");

        /// <summary>Pacific/Palau (UTC+09:00)</summary>
        public static readonly TimeZone Pacific_Palau = new TimeZone("Pacific/Palau", "Tokyo Standard Time", "(UTC+09:00) Osaka, Sapporo, Tokyo");

        /// <summary>Australia/Adelaide (UTC+09:30)</summary>
        public static readonly TimeZone Australia_Adelaide = new TimeZone("Australia/Adelaide", "Cen. Australia Standard Time", "(UTC+09:30) Adelaide");

        /// <summary>Australia/Broken_Hill (UTC+09:30)</summary>
        public static readonly TimeZone Australia_Broken_Hill = new TimeZone("Australia/Broken_Hill", "Cen. Australia Standard Time", "(UTC+09:30) Adelaide");

        /// <summary>Australia/Darwin (UTC+09:30)</summary>
        public static readonly TimeZone Australia_Darwin = new TimeZone("Australia/Darwin", "AUS Central Standard Time", "(UTC+09:30) Darwin");

        /// <summary>Antarctica/DumontDUrville (UTC+10:00)</summary>
        public static readonly TimeZone Antarctica_DumontDUrville = new TimeZone("Antarctica/DumontDUrville", "West Pacific Standard Time", "(UTC+10:00) BrisbaneGuam, Port Moresby");

        /// <summary>Asia/Yakutsk (UTC+10:00)</summary>
        public static readonly TimeZone Asia_Yakutsk = new TimeZone("Asia/Yakutsk", "Yakutsk Standard Time", "(UTC+10:00) Yakutsk");

        /// <summary>Australia/Brisbane (UTC+10:00)</summary>
        public static readonly TimeZone Australia_Brisbane = new TimeZone("Australia/Brisbane", "E. Australia Standard Time", "(UTC+10:00) Brisbane");

        /// <summary>Australia/Currie (UTC+10:00)</summary>
        public static readonly TimeZone Australia_Currie = new TimeZone("Australia/Currie", "Tasmania Standard Time", "(UTC+10:00) Hobart");

        /// <summary>Australia/Hobart (UTC+10:00)</summary>
        public static readonly TimeZone Australia_Hobart = new TimeZone("Australia/Hobart", "Tasmania Standard Time", "(UTC+10:00) Hobart");

        /// <summary>Australia/Lindeman (UTC+10:00)</summary>
        public static readonly TimeZone Australia_Lindeman = new TimeZone("Australia/Lindeman", "E. Australia Standard Time", "(UTC+10:00) Brisbane");

        /// <summary>Australia/Melbourne (UTC+10:00)</summary>
        public static readonly TimeZone Australia_Melbourne = new TimeZone("Australia/Melbourne", "AUS Eastern Standard Time", "(UTC+10:00) Canberra, Melbourne, Sydney");

        /// <summary>Australia/Sydney (UTC+10:00)</summary>
        public static readonly TimeZone Australia_Sydney = new TimeZone("Australia/Sydney", "AUS Eastern Standard Time", "(UTC+10:00) Canberra, Melbourne, Sydney");

        /// <summary>Pacific/Guam (UTC+10:00)</summary>
        public static readonly TimeZone Pacific_Guam = new TimeZone("Pacific/Guam", "West Pacific Standard Time", "(UTC+10:00) Guam, Port Moresby");

        /// <summary>Pacific/Port_Moresby (UTC+10:00)</summary>
        public static readonly TimeZone Pacific_Port_Moresby = new TimeZone("Pacific/Port_Moresby", "West Pacific Standard Time", "(UTC+10:00) Guam, Port Moresby");

        /// <summary>Pacific/Saipan (UTC+10:00)</summary>
        public static readonly TimeZone Pacific_Saipan = new TimeZone("Pacific/Saipan", "West Pacific Standard Time", "(UTC+10:00) Guam, Port Moresby");

        /// <summary>Pacific/Truk (UTC+10:00)</summary>
        public static readonly TimeZone Pacific_Truk = new TimeZone("Pacific/Truk", "West Pacific Standard Time", "(UTC+10:00) Guam, Port Moresby");

        /// <summary>Australia/Lord_Howe (UTC+10:00)</summary>
        public static readonly TimeZone Australia_Lord_Howe = new TimeZone("Australia/Lord_Howe", "E. Australia Standard Time", "(UTC+10:00) Brisbane");

        /// <summary>Asia/Sakhalin (UTC+11:00)</summary>
        public static readonly TimeZone Asia_Sakhalin = new TimeZone("Asia/Sakhalin", "Vladivostok Standard Time", "(UTC+011:00) Vladivostok");

        /// <summary>Asia/Vladivostok (UTC+11:00)</summary>
        public static readonly TimeZone Asia_Vladivostok = new TimeZone("Asia/Vladivostok", "Vladivostok Standard Time", "(UTC+011:00) Vladivostok");

        /// <summary>Pacific/Efate (UTC+11:00)</summary>
        public static readonly TimeZone Pacific_Efate = new TimeZone("Pacific/Efate", "Central Pacific Standard Time", "(UTC+011:00) Solomon Is., New Caledonia");

        /// <summary>Pacific/Guadalcanal (UTC+11:00)</summary>
        public static readonly TimeZone Pacific_Guadalcanal = new TimeZone("Pacific/Guadalcanal", "Central Pacific Standard Time", "(UTC+011:00) Solomon Is., New Caledonia");

        /// <summary>Pacific/Kosrae (UTC+11:00)</summary>
        public static readonly TimeZone Pacific_Kosrae = new TimeZone("Pacific/Kosrae", "Central Pacific Standard Time", "(UTC+011:00) Solomon Is., New Caledonia");

        /// <summary>Pacific/Noumea (UTC+11:00)</summary>
        public static readonly TimeZone Pacific_Noumea = new TimeZone("Pacific/Noumea", "Central Pacific Standard Time", "(UTC+011:00) Solomon Is., New Caledonia");

        /// <summary>Pacific/Ponape (UTC+11:00)</summary>
        public static readonly TimeZone Pacific_Ponape = new TimeZone("Pacific/Ponape", "Central Pacific Standard Time", "(UTC+011:00) Solomon Is., New Caledonia");

        /// <summary>Pacific/Norfolk (UTC11,5:00)</summary>
        public static readonly TimeZone Pacific_Norfolk = new TimeZone("Pacific/Norfolk", "Central Pacific Standard Time", "(UTC+011:00) Solomon Is., New Caledonia");

        /// <summary>Antarctica/McMurdo (UTC+12:00)</summary>
        public static readonly TimeZone Antarctica_McMurdo = new TimeZone("Antarctica/McMurdo", "New Zealand Standard Time", "(UTC+012:00) Auckland, Wellington");

        /// <summary>Antarctica/South_Pole (UTC+12:00)</summary>
        public static readonly TimeZone Antarctica_South_Pole = new TimeZone("Antarctica/South_Pole", "New Zealand Standard Time", "(UTC+012:00) Auckland, Wellington");

        /// <summary>Asia/Anadyr (UTC+12:00)</summary>
        public static readonly TimeZone Asia_Anadyr = new TimeZone("Asia/Anadyr", "Magadan Standard Time", "(UTC+012:00) Magadan");

        /// <summary>Asia/Kamchatka (UTC+12:00)</summary>
        public static readonly TimeZone Asia_Kamchatka = new TimeZone("Asia/Kamchatka", "Magadan Standard Time", "(UTC+012:00) Magadan");

        /// <summary>Asia/Magadan (UTC+12:00)</summary>
        public static readonly TimeZone Asia_Magadan = new TimeZone("Asia/Magadan", "Magadan Standard Time", "(UTC+012:00) Magadan");

        /// <summary>Pacific/Auckland (UTC+12:00)</summary>
        public static readonly TimeZone Pacific_Auckland = new TimeZone("Pacific/Auckland", "New Zealand Standard Time", "(UTC+012:00) Auckland, Wellington");

        /// <summary>Pacific/Fiji (UTC+12:00)</summary>
        public static readonly TimeZone Pacific_Fiji = new TimeZone("Pacific/Fiji", "Fiji Standard Time", "(UTC+012:00) Fiji");

        /// <summary>Pacific/Funafuti (UTC+12:00)</summary>
        public static readonly TimeZone Pacific_Funafuti = new TimeZone("Pacific/Funafuti", "UTC+12", "(UTC+012:00)");

        /// <summary>Pacific/Kwajalein (UTC+12:00)</summary>
        public static readonly TimeZone Pacific_Kwajalein = new TimeZone("Pacific/Kwajalein", "UTC+12", "(UTC+012:00) Fiji");

        /// <summary>Pacific/Majuro (UTC+12:00)</summary>
        public static readonly TimeZone Pacific_Majuro = new TimeZone("Pacific/Majuro", "UTC+12", "(UTC+012:00) Fiji");

        /// <summary>Pacific/Nauru (UTC+12:00)</summary>
        public static readonly TimeZone Pacific_Nauru = new TimeZone("Pacific/Nauru", "UTC+12", "(UTC+012:00) Fiji");

        /// <summary>Pacific/Tarawa (UTC+12:00)</summary>
        public static readonly TimeZone Pacific_Tarawa = new TimeZone("Pacific/Tarawa", "UTC+12", "(UTC+012:00) Fiji");

        /// <summary>Pacific/Wake (UTC+12:00)</summary>
        public static readonly TimeZone Pacific_Wake = new TimeZone("Pacific/Wake", "UTC+12", "(UTC+012:00) Fiji");

        /// <summary>Pacific/Wallis (UTC+12:00)</summary>
        public static readonly TimeZone Pacific_Wallis = new TimeZone("Pacific/Wallis", "UTC+12", "(UTC+012:00) Fiji");

        /// <summary>Pacific/Chatham (UTC+13:00)</summary>
        public static readonly TimeZone Pacific_Chatham = new TimeZone("Pacific/Chatham", "Tonga Standard Time", "(UTC+013:00) Nuku'alofa");

        /// <summary>Pacific/Enderbury (UTC+13:00)</summary>
        public static readonly TimeZone Pacific_Enderbury = new TimeZone("Pacific/Enderbury", "Tonga Standard Time", "(UTC+013:00) Nuku'alofa");

        /// <summary>Pacific/Tongatapu (UTC+13:00)</summary>
        public static readonly TimeZone Pacific_Tongatapu = new TimeZone("Pacific/Tongatapu", "Tonga Standard Time", "(UTC+013:00) Nuku'alofa");

        /// <summary>Pacific/Kiritimati (UTC+14:00)</summary>
        public static readonly TimeZone Pacific_Kiritimati = new TimeZone("Pacific/Kiritimati", "Tonga Standard Time", "(UTC+013:00) Nuku'alofa");

        #endregion

        /// <summary>The .NET time zone info instance for this time zone.</summary>
        private TimeZoneInfo timeZoneInfo;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeZone" /> class.
        /// </summary>
        /// <param name="olsonTimeZoneId">The Olson time zone ID of the time zone.</param>
        /// <param name="windowsTimeZoneId">The Windows time zone ID of the time zone.</param>
        /// <param name="windowsDisplayName">The Windows display name of the time zone.</param>
        private TimeZone(string olsonTimeZoneId, string windowsTimeZoneId, string windowsDisplayName)
        {
            this.OlsonTimeZoneId = olsonTimeZoneId;
            this.WindowsTimeZoneId = windowsTimeZoneId;
            this.WindowsDisplayName = windowsDisplayName;

            Mappings[olsonTimeZoneId] = this;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Olson time zone ID of the time zone.
        /// </summary>
        public string OlsonTimeZoneId { get; set; }

        /// <summary>
        /// Gets or sets the Windows time zone ID of the time zone.
        /// </summary>
        public string WindowsTimeZoneId { get; set; }

        /// <summary>
        /// Gets or sets the Windows display name of the time zone.
        /// </summary>
        public string WindowsDisplayName { get; set; }

        /// <summary>
        /// Gets or sets the offset compared to UTC of the timezone.
        /// </summary>
        public double UtcOffset
        {
            get
            {
                return this.TimeZoneInfo.GetUtcOffset(DateTime.Now).TotalHours;
            }
        }

        /// <summary>
        /// Gets the .NET time zone info instance for this culture.
        /// </summary>
        public TimeZoneInfo TimeZoneInfo
        {
            get
            {
                if (this.timeZoneInfo == null)
                {
                    try
                    {
                        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                        {
                            this.timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(this.WindowsTimeZoneId);
                        }
                        else
                        {
                            this.timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(this.OlsonTimeZoneId);
                        }
                    }
                    catch(TimeZoneNotFoundException)
                    {
                        this.timeZoneInfo = null;
                    }
                }

                return this.timeZoneInfo;
            }
        }

        /// <summary>
        /// Gets the windows display name formatted with daylight saving.
        /// </summary>
        /// <returns>A <see cref="string"/> containing the windows display name.</returns>
        public string DisplayName
        {
            get
            {
                string displayName = this.TimeZoneInfo.DisplayName;
                string utcOffset = this.UtcOffset.ToString("0.00");

                // Add a leading zero
                if (this.UtcOffset > -10 && this.UtcOffset < 10)
                {
                    utcOffset = "0" + utcOffset;
                }

                utcOffset = Regex.Replace(utcOffset, @"\D", ":");
                displayName = Regex.Replace(displayName, @"\d\d:\d\d", utcOffset);

                return displayName;
            }
        }

        #endregion

        #region Operators

        /// <summary>
        /// Converts the specified string to the corresponding <see cref="TimeZone" /> instance. 
        /// </summary>
        /// <param name="olsonTimeZoneId">The Olson time zone ID of the time zone.</param>
        public static explicit operator TimeZone(string olsonTimeZoneId)
        {
            TimeZone result = Mappings.Values.SingleOrDefault(x => x.OlsonTimeZoneId.ToLower() == olsonTimeZoneId.ToLower());

            if (result == null)
            {
                throw new InvalidCastException();
            }

            return result;
        }

        #endregion
    }
}