﻿namespace Crave
{
    public class MediaConstants
    {
        public const int ImageQuality = 50;

        public const string FilesDirectory = "files";

        public const string GenericFilesSubdirectory = "generic";

        public const string AdvertisementsSubdirectory = "advertisements";
        public const string AlterationsSubdirectory = "alterations";
        public const string AttachmentsSubdirectory = "attachments";
        public const string CategoriesSubdirectory = "categories";
        public const string CompanySubdirectory = "eye-catchers";
        public const string DeliverypointgroupSubdirectory = "deliverypointgroups";
        public const string EntertainmentSubdirectory = "entertainment";
        public const string PageSubdirectory = "page";
        public const string PageElementSubdirectory = "pageelement";
        public const string ProductsSubdirectory = "products";
        public const string PointOfInterestSubdirectory = "pointsofinterest";
        public const string RoomControlSubdirectory = "roomcontrol";
        public const string RoutestephandlerSubdirectory = "routestephandlers";
        public const string SiteSubdirectory = "site";
        public const string FooterSubdirectory = "footerbar";
        public const string UIThemesSubdirectory = "themes";
        public const string UIWidgetsSubdirectory = "widgets";
        public const string ProductgroupSubdirectory = "productgroups";

    }
}
