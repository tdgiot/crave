﻿using Crave.Enums;

namespace Crave
{
    public class MediaRatioType
    {
        #region Constructors        

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaRatioType"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="deviceType">The type of the device.</param>
        /// <param name="mediaTypeGroup">The type of the media group.</param>
        /// <param name="deviceIndependent">Flag which indicates whether the device is independent.</param>
        /// <param name="jpgQuality">The jpg quality.</param>
        public MediaRatioType(string name, MediaType mediaType, string entityType, int width, int height, DeviceType deviceType = Enums.DeviceType.Unknown, MediaTypeGroup mediaTypeGroup = MediaTypeGroup.None,
            bool deviceIndependent = false, int jpgQuality = 50)
        {
            this.Name = name;
            this.MediaType = mediaType;
            this.EntityType = entityType;
            this.Width = width;
            this.Height = height;
            this.DeviceType = deviceType;
            this.MediaTypeGroup = mediaTypeGroup;
            this.JpgQuality = jpgQuality;
            this.DeviceTypeIndependent = deviceIndependent;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the media ratio type
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the media of the media ratio type
        /// </summary>
        public MediaType MediaType { get; set; }

        /// <summary>
        /// Gets or sets the type of the entity of the media ratio type
        /// </summary>
        public string EntityType { get; set; }

        /// <summary>
        /// Gets or sets the width of the media ratio type
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Gets or sets the height of the media ratio type
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets the type of the device
        /// </summary>
        public DeviceType DeviceType { get; set; }

        /// <summary>
        /// Gets or sets the group to which the MediaType belong
        /// </summary>
        public MediaTypeGroup MediaTypeGroup { get; set; }

        /// <summary>
        /// Gets or sets the JPG Quality
        /// </summary>
        public int JpgQuality { get; set; }

        /// <summary>
        /// Hint used by the webservices to not strip this Media out, but add it independent of the device type.
        /// </summary>
        public bool DeviceTypeIndependent { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is portrait.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is portrait; otherwise, <c>false</c>.
        /// </value>
        public bool IsPortrait => this.Height > this.Width;

        /// <summary>
        /// Gets the device category.
        /// </summary>
        public DeviceCategory DeviceCategory
        {
            get
            {
                string mediaRatioTypeName = this.Name.ToLower();

                if (mediaRatioTypeName.Contains("800x480"))
                {
                    return DeviceCategory.Archos;
                }
                else if (mediaRatioTypeName.Contains("1280x800"))
                {
                    return DeviceCategory.InRoomTablet;
                }
                else if (mediaRatioTypeName.Contains("1280x720"))
                {
                    return DeviceCategory.TvBox;
                }
                else if (mediaRatioTypeName.Contains("otoucho"))
                {
                    return DeviceCategory.Otoucho;
                }
                else if (mediaRatioTypeName.Contains("iphone"))
                {
                    return DeviceCategory.MobileIphone;
                }
                else if (mediaRatioTypeName.Contains("ipad"))
                {
                    return DeviceCategory.Ipad;
                }
                else if (mediaRatioTypeName.Contains("(phone)") ||
                         mediaRatioTypeName.Contains("(tablet)"))
                {
                    return DeviceCategory.MobileAndroid;
                }
                else
                {
                    return DeviceCategory.Misc;
                }
            }
        }

        #endregion
    }
}
