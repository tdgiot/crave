﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Crave.Enums;

namespace Crave
{
    public class MediaRatioTypes
    {
        #region Fields

        private static MediaRatioTypes instance = null;
        private static Dictionary<MediaType, MediaType> normalToGenericMediaTypes = null;
        private static Dictionary<DeviceType, List<MediaType>> mediaTypesForDeviceType = new Dictionary<DeviceType, List<MediaType>>();
        private static Dictionary<Tuple<MediaTypeGroup, DeviceType>, MediaType> mediaTypeGroupDeviceCache = new Dictionary<Tuple<MediaTypeGroup, DeviceType>, MediaType>();

        private Dictionary<MediaType, MediaRatioType> mediaRatioTypes = null;
        private Dictionary<MediaType, string> mediaTypeMediaTypeGroupRatioLookup = new Dictionary<MediaType, string>();

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes the <see cref="MediaRatioTypes"/> class.
        /// </summary>
        static MediaRatioTypes()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new MediaRatioTypes();

                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        public static bool TryGetGetMediaTypeGroupRatioKey(MediaType mediaType, out string mediaTypeRatioKey)
        {
            return instance.mediaTypeMediaTypeGroupRatioLookup.TryGetValue(mediaType, out mediaTypeRatioKey);
        }

        public static List<int> GetActiveMediaRatioTypes()
        {
            List<int> types = new List<int>();
            foreach (MediaType key in instance.mediaRatioTypes.Keys)
            {
                types.Add((int)key);
            }
            return types;
        }

        /// <summary>
        /// Gets the media ratio type instance for the specified media type
        /// </summary>
        /// <param name="mediaType">Type of the media to retrieve the media ratio type object for</param>
        /// <returns>A <see cref="MediaRatioType"/> instance</returns>
        public static MediaRatioType GetMediaRatioType(MediaType mediaType)
        {
            MediaRatioType mediaRatioType = null;

            if (instance.mediaRatioTypes.ContainsKey(mediaType))
            {
                mediaRatioType = instance.mediaRatioTypes[mediaType];
            }

            return mediaRatioType;
        }

        /// <summary>
        /// Gets the media ratio types for the specified entity type
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="systemName">The name of the system.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns>A <see cref="List{MediaRatioType}"/> of <see cref="MediaRatioType"/> instances</returns>
        public static List<MediaRatioType> GetMediaRatioTypes(string entityType, string systemName = "", int width = 0, int height = 0)
        {
            List<MediaRatioType> mediaRatioTypes = new List<MediaRatioType>();

            foreach (MediaRatioType mediaRatioType in instance.mediaRatioTypes.Values)
            {
                if (mediaRatioType.EntityType.Equals(entityType, StringComparison.InvariantCultureIgnoreCase))
                {
                    bool add = !(!systemName.IsNullOrWhiteSpace() && !mediaRatioType.MediaType.ToString().Equals(systemName, StringComparison.InvariantCultureIgnoreCase));

                    if (width > 0 && height > 0 && (width != mediaRatioType.Width || height != mediaRatioType.Height))
                    {
                        add = false;
                    }

                    if (add)
                    {
                        mediaRatioTypes.Add(mediaRatioType);
                    }
                }
            }

            return mediaRatioTypes;
        }

        /// <summary>
        /// Gets the media ratio types for the specified entity type
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="excludeCategories">The device categories to exclude from the result.</param>
        /// <param name="systemName">The name of the system.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns>A <see cref="List{MediaRatioType}"/> of <see cref="MediaRatioType"/> instances</returns>
        public static List<MediaRatioType> GetMediaRatioTypes(string entityType, ICollection<DeviceCategory> excludeCategories, string systemName = "", int width = 0, int height = 0)
        {
            List<MediaRatioType> mediaRatioTypes = new List<MediaRatioType>();

            entityType = entityType.ToLower();

            foreach (MediaRatioType mediaRatioType in instance.mediaRatioTypes.Values)
            {
                string mediaRatioTypeEntityType = mediaRatioType.EntityType.ToLower();

                if (entityType != mediaRatioTypeEntityType ||
                    excludeCategories.Contains(mediaRatioType.DeviceCategory))
                {
                    continue;
                }

                bool add = !(!systemName.IsNullOrWhiteSpace() && !mediaRatioType.MediaType.ToString().Equals(systemName, StringComparison.InvariantCultureIgnoreCase));

                if (width > 0 && height > 0 && (width != mediaRatioType.Width || height != mediaRatioType.Height))
                {
                    add = false;
                }

                if (add)
                {
                    mediaRatioTypes.Add(mediaRatioType);
                }
            }

            return mediaRatioTypes;
        }

        /// <summary>
        /// Gets a list of all media ratio types for a specific device type
        /// </summary>
        /// <param name="deviceTypes">Device types for which to get media ratio types</param>
        /// <returns>A <see cref="List{MediaRatioType}"/> instance.</returns>
        public static List<MediaRatioType> GetMediaRatioTypesForDevice(params DeviceType[] deviceTypes)
        {
            List<MediaRatioType> mediaRatioTypes = new List<MediaRatioType>();

            foreach (MediaRatioType mediaRatioType in instance.mediaRatioTypes.Values)
            {
                if (deviceTypes.Contains(mediaRatioType.DeviceType))
                {
                    mediaRatioTypes.Add(mediaRatioType);
                }
            }

            return mediaRatioTypes;
        }

        /// <summary>
        /// Gets a list of all media types for the specified device type(s)
        /// </summary>
        /// <param name="deviceTypes">Device types for which to get media ratio types</param>
        /// <returns>A <see cref="List{MediaType}"/> instance.</returns>
        public static List<MediaType> GetMediaTypeListForDeviceTypes(params DeviceType[] deviceTypes)
        {
            List<MediaType> mediaTypes = new List<MediaType>();

            foreach (DeviceType deviceType in deviceTypes)
            {
                if (!mediaTypesForDeviceType.ContainsKey(deviceType))
                {
                    List<MediaType> typesForDeviceType = new List<MediaType>();

                    // Sometimes you just have to break the rules and feel free.. :) One-liner.
                    foreach (MediaRatioType mediaRatioType in instance.mediaRatioTypes.Values)
                    {
                        if (mediaRatioType.DeviceType == deviceType || mediaRatioType.DeviceType == DeviceType.Unknown)
                        {
                            typesForDeviceType.Add(mediaRatioType.MediaType);
                        }
                    }

                    if (!MediaRatioTypes.mediaTypesForDeviceType.ContainsKey(deviceType))
                    {
                        MediaRatioTypes.mediaTypesForDeviceType.Add(deviceType, typesForDeviceType);
                    }
                }

                if (mediaTypesForDeviceType.ContainsKey(deviceType) && mediaTypesForDeviceType[deviceType] != null)
                {
                    mediaTypes.AddRange(mediaTypesForDeviceType[deviceType]);
                }
            }

            return mediaTypes;
        }

        /// <summary>
        /// Gets an array of all media types for the specified device type(s)
        /// </summary>
        /// <param name="deviceTypes">The device types to get the media types for.</param>
        /// <returns>An array of <see cref="MediaType"/>instances.</returns>
        public static MediaType[] GetMediaTypeArrayForDeviceTypes(params DeviceType[] deviceTypes)
        {
            return GetMediaTypeListForDeviceTypes(deviceTypes).ToArray();
        }

        public static List<int> GetMediaTypeArrayForDeviceTypeInts(params DeviceType[] deviceTypes)
        {
            List<int> mediaTypeInts = new List<int>();
            List<MediaType> mediaTypes = GetMediaTypeListForDeviceTypes(deviceTypes);
            foreach (MediaType mediaType in mediaTypes)
            {
                int mediaTypeInt = (int)mediaType;

                if (mediaTypeInts.IndexOf(mediaTypeInt) == -1)
                {
                    mediaTypeInts.Add(mediaTypeInt);
                }
            }
            return mediaTypeInts;
        }

        public static MediaType GetMediaType(MediaTypeGroup mediaTypeGroup, DeviceType deviceType)
        {
            Tuple<MediaTypeGroup, DeviceType> key = new Tuple<MediaTypeGroup, DeviceType>(mediaTypeGroup, deviceType);

            MediaType mediaType;
            if (!mediaTypeGroupDeviceCache.TryGetValue(key, out mediaType))
            {
                // Look up
                // Done is such a stupid way because KeyValuePair and FirstOrDefault don't really work (no null since KVP is a struct)
                // http://stackoverflow.com/questions/793897/check-if-keyvaluepair-exists-with-linqs-firstordefault
                List<KeyValuePair<MediaType, MediaRatioType>> found = instance.mediaRatioTypes.Where(x => x.Value.MediaTypeGroup == mediaTypeGroup && x.Value.DeviceType == deviceType).Take(1).ToList();
                if (found.Count == 0)
                {
                    mediaType = MediaType.MediaTypeNotSpecifiedForMediaTypeGroupDeviceCombination;
                }
                else
                {
                    mediaType = found[0].Value.MediaType;
                }

                // Add to cache for quick lookups later
                lock (mediaTypeGroupDeviceCache)
                {
                    if (!mediaTypeGroupDeviceCache.ContainsKey(key))
                    {
                        mediaTypeGroupDeviceCache.Add(key, mediaType);
                    }
                }
            }

            return mediaType;
        }

        public static List<MediaRatioType> GetMediaRatioTypes(MediaTypeGroup mediaTypeGroup)
        {
            return MediaRatioTypes.GetMediaRatioTypes(mediaTypeGroup, null);
        }

        public static List<MediaRatioType> GetMediaRatioTypes(MediaTypeGroup mediaTypeGroup, string entityType)
        {
            if (entityType.IsNullOrWhiteSpace() && MediaRatioTypes.instance.mediaRatioTypes.Any(x => x.Value.MediaTypeGroup == mediaTypeGroup))
            {
                return MediaRatioTypes.instance.mediaRatioTypes.Where(x => x.Value.MediaTypeGroup == mediaTypeGroup).Select(x => x.Value).ToList();
            }
            else if (!entityType.IsNullOrWhiteSpace() && MediaRatioTypes.instance.mediaRatioTypes.Any(x => x.Value.MediaTypeGroup == mediaTypeGroup && x.Value.EntityType.Equals(entityType, StringComparison.InvariantCultureIgnoreCase)))
            {
                return MediaRatioTypes.instance.mediaRatioTypes.Where(x => x.Value.MediaTypeGroup == mediaTypeGroup && x.Value.EntityType.Equals(entityType, StringComparison.InvariantCultureIgnoreCase)).Select(x => x.Value).ToList();
            }
            else
            {
                return new List<MediaRatioType>();
            }
        }

        public static void InitNormalToGenericMediaTypes()
        {
            if (normalToGenericMediaTypes == null)
            {
                normalToGenericMediaTypes = new Dictionary<MediaType, MediaType>();

                // Archos
                normalToGenericMediaTypes.Add(MediaType.ProductButton800x480, MediaType.GenericProductButton800x480);
                normalToGenericMediaTypes.Add(MediaType.ProductBranding800x480, MediaType.GenericProductBranding800x480);
                normalToGenericMediaTypes.Add(MediaType.CraveProductBrandingMobile, MediaType.CraveGenericProductBrandingMobile);

                // Samsung
                normalToGenericMediaTypes.Add(MediaType.ProductBranding1280x800, MediaType.GenericProductBranding1280x800);
                normalToGenericMediaTypes.Add(MediaType.ProductPagePhotoHorizontal1280x800, MediaType.GenericProductPagePhotoHorizontal1280x800);
                normalToGenericMediaTypes.Add(MediaType.ProductPagePhotoVertical1280x800, MediaType.GenericProductPagePhotoVertical1280x800);
                normalToGenericMediaTypes.Add(MediaType.ProductButton1280x800, MediaType.GenericProductButton1280x800);
                normalToGenericMediaTypes.Add(MediaType.ProductIcon1280x800, MediaType.GenericProductIcon1280x800);

                // Android TV Box
                normalToGenericMediaTypes.Add(MediaType.ProductBranding1280x720, MediaType.GenericProductBranding1280x720);
                normalToGenericMediaTypes.Add(MediaType.ProductButton1280x720, MediaType.GenericProductButton1280x720);

                // Mobile
                normalToGenericMediaTypes.Add(MediaType.ProductButtonPhoneSmall, MediaType.GenericProductButtonPhoneSmall);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingPhoneSmall, MediaType.GenericProductBrandingPhoneSmall);
                normalToGenericMediaTypes.Add(MediaType.ProductButtonPhoneNormal, MediaType.GenericProductButtonPhoneNormal);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingPhoneNormal, MediaType.GenericProductBrandingPhoneNormal);
                normalToGenericMediaTypes.Add(MediaType.ProductButtonPhoneLarge, MediaType.GenericProductButtonPhoneLarge);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingPhoneLarge, MediaType.GenericProductBrandingPhoneLarge);
                normalToGenericMediaTypes.Add(MediaType.ProductButtonPhoneXLarge, MediaType.GenericProductButtonPhoneXLarge);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingPhoneXLarge, MediaType.GenericProductBrandingPhoneXLarge);
                normalToGenericMediaTypes.Add(MediaType.ProductButtonTabletSmall, MediaType.GenericProductButtonTabletSmall);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingTabletSmall, MediaType.GenericProductBrandingTabletSmall);
                normalToGenericMediaTypes.Add(MediaType.ProductButtonTabletNormal, MediaType.GenericProductButtonTabletNormal);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingTabletNormal, MediaType.GenericProductBrandingTabletNormal);
                normalToGenericMediaTypes.Add(MediaType.ProductButtonTabletLarge, MediaType.GenericProductButtonTabletLarge);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingTabletLarge, MediaType.GenericProductBrandingTabletLarge);
                normalToGenericMediaTypes.Add(MediaType.ProductButtonTabletXLarge, MediaType.GenericProductButtonTabletXLarge);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingTabletXLarge, MediaType.GenericProductBrandingTabletXLarge);

                // Misc
                normalToGenericMediaTypes.Add(MediaType.ProductButtoniPhone, MediaType.GenericProductButtoniPhone);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingiPhone, MediaType.GenericProductBrandingiPhone);
                normalToGenericMediaTypes.Add(MediaType.ProductButtoniPhoneRetina, MediaType.GenericProductButtoniPhoneRetina);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingiPhoneRetina, MediaType.GenericProductBrandingiPhoneRetina);
                normalToGenericMediaTypes.Add(MediaType.ProductButtoniPad, MediaType.GenericProductButtoniPad);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingiPad, MediaType.GenericProductBrandingiPad);
                normalToGenericMediaTypes.Add(MediaType.ProductButtoniPadRetina, MediaType.GenericProductButtoniPadRetina);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingiPadRetina, MediaType.GenericProductBrandingiPadRetina);
                normalToGenericMediaTypes.Add(MediaType.ProductButtoniPhoneRetinaHD, MediaType.GenericProductButtoniPhoneRetinaHD); // BBFIX 2014.11.09 added missing HD and HDPlus ones
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingiPhoneRetinaHD, MediaType.GenericProductBrandingiPhoneRetinaHD);
                normalToGenericMediaTypes.Add(MediaType.ProductButtoniPhoneRetinaHDPlus, MediaType.GenericProductButtoniPhoneRetinaHDPlus);
                normalToGenericMediaTypes.Add(MediaType.ProductBrandingiPhoneRetinaHDPlus, MediaType.GenericProductBrandingiPhoneRetinaHDPlus);
            }
        }

        /// <summary>
        /// Retrieves the generic media type matching the normal media type
        /// </summary>
        /// <param name="normalMediaType">The media type to get the generic media type for.</param>
        /// <returns>A <see cref="MediaType"/> instance.</returns>
        public static MediaType GetGenericMediaTypeByNormalMediaType(MediaType normalMediaType)
        {
            MediaType genericMediaType = MediaType.NoMediaTypeSpecified;

            if (normalToGenericMediaTypes == null)
            {
                InitNormalToGenericMediaTypes();
            }

            if (normalToGenericMediaTypes.ContainsKey(normalMediaType))
            {
                genericMediaType = normalToGenericMediaTypes[normalMediaType];
            }

            return genericMediaType;
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        private void Init()
        {
            // Initialize the static members
            this.mediaRatioTypes = new Dictionary<MediaType, MediaRatioType>();
            try
            {
                // Otoucho - 1024x600
                this.mediaRatioTypes.Add(MediaType.ProductPhoto, new MediaRatioType("Product groot (Otoucho)", MediaType.ProductPhoto, "ProductEntity", 828, 513, DeviceType.EM200));
                this.mediaRatioTypes.Add(MediaType.ProductBranding, new MediaRatioType("Product branding (Otoucho)", MediaType.ProductBranding, "ProductEntity", 268, 513, DeviceType.EM200));
                this.mediaRatioTypes.Add(MediaType.ProductButton, new MediaRatioType("Product knop (Otoucho)", MediaType.ProductButton, "ProductEntity", 136, 93, DeviceType.EM200));
                this.mediaRatioTypes.Add(MediaType.CategoryButton, new MediaRatioType("Categorie knop (Otoucho)", MediaType.CategoryButton, "CategoryEntity", 136, 93, DeviceType.EM200));
                this.mediaRatioTypes.Add(MediaType.Advertisement, new MediaRatioType("Advertentie (Otoucho)", MediaType.Advertisement, "AdvertisementEntity", 400, 600, DeviceType.EM200));
                this.mediaRatioTypes.Add(MediaType.AdvertisementPhoto, new MediaRatioType("Advertentie detail (Otoucho)", MediaType.AdvertisementPhoto, "AdvertisementEntity", 840, 525, DeviceType.EM200));
                this.mediaRatioTypes.Add(MediaType.EntertainmentButton, new MediaRatioType("Entertainment knop", MediaType.EntertainmentButton, "EntertainmentEntity", 136, 93, DeviceType.EM200));
                this.mediaRatioTypes.Add(MediaType.GenericproductButton, new MediaRatioType("Basisproduct knop", MediaType.GenericproductButton, "GenericproductEntity", 136, 93, DeviceType.EM200));
                this.mediaRatioTypes.Add(MediaType.GenericproductBranding, new MediaRatioType("Basisproduct branding (Otoucho)", MediaType.GenericproductBranding, "GenericproductEntity", 268, 513, DeviceType.EM200));
                this.mediaRatioTypes.Add(MediaType.AgeCheck, new MediaRatioType("Leeftijdcontrole (Otoucho)", MediaType.AgeCheck, "CompanyEntity", 1260, 675, DeviceType.EM200));
                this.mediaRatioTypes.Add(MediaType.Homepage, new MediaRatioType("Homepage", MediaType.Homepage, "CompanyEntity", 840, 315, DeviceType.EM200));
                this.mediaRatioTypes.Add(MediaType.GenericcategoryButton, new MediaRatioType("Basiscategorie", MediaType.GenericcategoryButton, "GenericcategoryEntity", 136, 93, DeviceType.EM200));

                // Archos - 800x480
                this.mediaRatioTypes.Add(MediaType.NotificationDialog800x480, new MediaRatioType("Notification dialog (800x480)", MediaType.NotificationDialog800x480, "CompanyEntity", 386, 276, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.ProductButton800x480, new MediaRatioType("Product button (800x480)", MediaType.ProductButton800x480, "ProductEntity", 87, 58, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholder800x480, new MediaRatioType("Product button placeholder (800x480)", MediaType.ProductButtonPlaceholder800x480, "CompanyEntity", 87, 58, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.ProductBranding800x480, new MediaRatioType("Product branding (800x480)", MediaType.ProductBranding800x480, "ProductEntity", 188, 246, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholder800x480, new MediaRatioType("Product branding placeholder (800x480)", MediaType.ProductBrandingPlaceholder800x480, "CompanyEntity", 188, 246, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.GenericProductButton800x480, new MediaRatioType("Generic product button (800x480)", MediaType.GenericProductButton800x480, "GenericproductEntity", 87, 58, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.GenericProductBranding800x480, new MediaRatioType("Generic product branding (800x480)", MediaType.GenericProductBranding800x480, "GenericproductEntity", 188, 246, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.Homepage800x480, new MediaRatioType("Homepage (800x480)", MediaType.Homepage800x480, "DeliverypointgroupEntity", 530, 350, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.EntertainmentButton800x480, new MediaRatioType("Entertainment button (800x480)", MediaType.EntertainmentButton800x480, "EntertainmentEntity", 130, 100, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.EntertainmentButtonPlaceholder800x480, new MediaRatioType("Entertainment button placeholder (800x480)", MediaType.EntertainmentButtonPlaceholder800x480, "CompanyEntity", 130, 100, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.SocialmediaBranding800x480, new MediaRatioType("Social media (800x480)", MediaType.SocialmediaBranding800x480, "CompanyEntity", 258, 352, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.CustomerDataCaptureBranding800x480, new MediaRatioType("Customer data capture (800x480)", MediaType.CustomerDataCaptureBranding800x480, "CompanyEntity", 258, 352, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.CategoryPhoto800x480, new MediaRatioType("Category photo (800x480)", MediaType.CategoryPhoto800x480, "CategoryEntity", 800, 315, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.CategoryPhotoPlaceholder800x480, new MediaRatioType("Category photo placeholder (800x480)", MediaType.CategoryPhotoPlaceholder800x480, "CompanyEntity", 800, 315, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.CategoryBranding800x480, new MediaRatioType("Category branding (800x480)", MediaType.CategoryBranding800x480, "CategoryEntity", 250, 315, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.HomepageFullscreen800x480, new MediaRatioType("Homepage fullscreen (800x480)", MediaType.HomepageFullscreen800x480, "CompanyEntity", 800, 350, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.OutOfCharge800x480, new MediaRatioType("Out of charge (800x480)", MediaType.OutOfCharge800x480, "DeliverypointgroupEntity", 386, 276, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.OrderProcessed800x480, new MediaRatioType("Order processed (800x480)", MediaType.OrderProcessed800x480, "DeliverypointgroupEntity", 386, 276, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.ServiceProcessed800x480, new MediaRatioType("Service processed (800x480)", MediaType.ServiceProcessed800x480, "DeliverypointgroupEntity", 386, 276, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.RatingProcessed800x480, new MediaRatioType("Rating processed (800x480)", MediaType.RatingProcessed800x480, "DeliverypointgroupEntity", 386, 276, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.SocialmediaProcessed800x480, new MediaRatioType("Socialmedia processed (800x480)", MediaType.SocialmediaProcessed800x480, "CompanySocialmediaEntity", 386, 276, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.SurveyProcessed800x480, new MediaRatioType("Survey processed (800x480)", MediaType.SurveyProcessed800x480, "SurveyEntity", 386, 276, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.FormProcessed800x480, new MediaRatioType("Form processed (800x480)", MediaType.FormProcessed800x480, "FormEntity", 386, 276, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.Basket800x480, new MediaRatioType("Basket (800x480)", MediaType.Basket800x480, "DeliverypointgroupEntity", 258, 308, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.Advertisement800x480, new MediaRatioType("Advertisement (800x480)", MediaType.Advertisement800x480, "AdvertisementEntity", 246, 314, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.ProductAdded800x480, new MediaRatioType("Product added (800x480)", MediaType.ProductAdded800x480, "CompanyEntity", 626, 170, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.AlterationImage800x480, new MediaRatioType("Alteration image (800x480)", MediaType.AlterationImage800x480, "AlterationEntity", 322, 284, DeviceType.Archos70));
                this.mediaRatioTypes.Add(MediaType.AdvertisementHalf800x480, new MediaRatioType("Advertisement half (800x480)", MediaType.AdvertisementHalf800x480, "AdvertisementEntity", 246, 152, DeviceType.Archos70));

                // Samsung - 1280x800
                this.mediaRatioTypes.Add(MediaType.ProductBranding1280x800, new MediaRatioType("Product branding (1280x800)", MediaType.ProductBranding1280x800, "ProductEntity", 356, 524, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholder1280x800, new MediaRatioType("Product branding placeholder (1280x800)", MediaType.ProductBrandingPlaceholder1280x800, "CompanyEntity", 356, 524, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductButton1280x800, new MediaRatioType("Product button (1280x800)", MediaType.ProductButton1280x800, "ProductEntity", 112, 73, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholder1280x800, new MediaRatioType("Product button placeholder (1280x800)", MediaType.ProductButtonPlaceholder1280x800, "CompanyEntity", 112, 73, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.GenericProductButton1280x800, new MediaRatioType("Generic product button (1280x800)", MediaType.GenericProductButton1280x800, "GenericproductEntity", 112, 73, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.GenericProductBranding1280x800, new MediaRatioType("Generic product branding (1280x800)", MediaType.GenericProductBranding1280x800, "GenericproductEntity", 356, 524, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.Homepage1280x800, new MediaRatioType("Homepage (1280x800)", MediaType.Homepage1280x800, "DeliverypointgroupEntity", 928, 626, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.EntertainmentButton1280x800, new MediaRatioType("Entertainment button (1280x800)", MediaType.EntertainmentButton1280x800, "EntertainmentEntity", 167, 125, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.EntertainmentButtonPlaceholder1280x800, new MediaRatioType("Entertainment button placeholder (1280x800)", MediaType.EntertainmentButtonPlaceholder1280x800, "CompanyEntity", 167, 125, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SocialmediaBranding1280x800, new MediaRatioType("Social media (1280x800)", MediaType.SocialmediaBranding1280x800, "CompanyEntity", 400, 640, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.CustomerDataCaptureBranding1280x800, new MediaRatioType("Customer data capture (1280x800)", MediaType.CustomerDataCaptureBranding1280x800, "CompanyEntity", 400, 640, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.CategoryPhoto1280x800, new MediaRatioType("Category photo (1280x800)", MediaType.CategoryPhoto1280x800, "CategoryEntity", 1280, 600, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.CategoryPhotoPlaceholder1280x800, new MediaRatioType("Category photo placeholder (1280x800)", MediaType.CategoryPhotoPlaceholder1280x800, "CompanyEntity", 1280, 600, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.CategoryBranding1280x800, new MediaRatioType("Category branding (1280x800)", MediaType.CategoryBranding1280x800, "CategoryEntity", 400, 590, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.HomepageFullscreen1280x800, new MediaRatioType("Homepage fullscreen (1280x800)", MediaType.HomepageFullscreen1280x800, "DeliverypointgroupEntity", 1208, 594, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.OutOfCharge1280x800, new MediaRatioType("Out of charge (1280x800)", MediaType.OutOfCharge1280x800, "DeliverypointgroupEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.OrderProcessed1280x800, new MediaRatioType("Order processed (1280x800)", MediaType.OrderProcessed1280x800, "DeliverypointgroupEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ServiceProcessed1280x800, new MediaRatioType("Service processed (1280x800)", MediaType.ServiceProcessed1280x800, "DeliverypointgroupEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.RatingProcessed1280x800, new MediaRatioType("Rating processed (1280x800)", MediaType.RatingProcessed1280x800, "DeliverypointgroupEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SocialmediaProcessed1280x800, new MediaRatioType("Socialmedia processed (1280x800)", MediaType.SocialmediaProcessed1280x800, "CompanySocialmediaEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SurveyProcessed1280x800, new MediaRatioType("Survey processed (1280x800)", MediaType.SurveyProcessed1280x800, "SurveyEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.FormProcessed1280x800, new MediaRatioType("Form processed (1280x800)", MediaType.FormProcessed1280x800, "FormEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.Basket1280x800, new MediaRatioType("Basket (1280x800)", MediaType.Basket1280x800, "DeliverypointgroupEntity", 400, 590, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.Advertisement1280x800, new MediaRatioType("Advertisement (1280x800)", MediaType.Advertisement1280x800, "AdvertisementEntity", 394, 590, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductAddedSmall1280x800, new MediaRatioType("Product added small (1280x800)", MediaType.ProductAddedSmall1280x800, "CompanyEntity", 656, 250, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductAdded1280x800, new MediaRatioType("Product added large (1280x800)", MediaType.ProductAdded1280x800, "CompanyEntity", 876, 250, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.NotificationDialog1280x800, new MediaRatioType("Notification dialog (1280x800)", MediaType.NotificationDialog1280x800, "CompanyEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.AlterationImage1280x800, new MediaRatioType("Alteration image (1280x800)", MediaType.AlterationImage1280x800, "AlterationEntity", 413, 355, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.AdvertisementSmall1280x800, new MediaRatioType("Advertisement small (1280x800)", MediaType.AdvertisementSmall1280x800, "AdvertisementEntity", 70, 84, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.AdvertisementHalf1280x800, new MediaRatioType("Advertisement half (1280x800)", MediaType.AdvertisementHalf1280x800, "AdvertisementEntity", 394, 290, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SurveyPageBottom1280x800, new MediaRatioType("Survey page bottom (1280x800)", MediaType.SurveyPageBottom1280x800, "SurveyPageEntity", 820, 200, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SurveyPageRight1280x800, new MediaRatioType("Survey page right (1280x800)", MediaType.SurveyPageRight1280x800, "SurveyPageEntity", 380, 370, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SurveyPageLogo1280x800, new MediaRatioType("Survey page logo (1280x800)", MediaType.SurveyPageLogo1280x800, "SurveyPageEntity", 150, 150, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SurveyPageRightHalf1280x800, new MediaRatioType("Survey page right half (1280x800)", MediaType.SurveyPageRightHalf1280x800, "SurveyPageEntity", 180, 370, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ChargerRemovedDialog1280x800, new MediaRatioType("Charger removed dialog (1280x800)", MediaType.ChargerRemovedDialog1280x800, "DeliverypointgroupEntity", 400, 292, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ChargerRemovedReminderDialog1280x800, new MediaRatioType("Charger removed reminder dialog (1280x800)", MediaType.ChargerRemovedReminderDialog1280x800, "DeliverypointgroupEntity", 400, 292, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SurveyAnswerRequired1280x800, new MediaRatioType("Survey answer required (1280x800)", MediaType.SurveyAnswerRequired1280x800, "SurveyEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.PmsMessage1280x800, new MediaRatioType("PMS Message dialog (1280x800)", MediaType.PmsMessage1280x800, "DeliverypointgroupEntity", 200, 280, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.PmsBill1280x800, new MediaRatioType("PMS Bill logo (1280x800)", MediaType.PmsBill1280x800, "DeliverypointgroupEntity", 230, 85, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.PmsCheckoutApproval1280x800, new MediaRatioType("PMS Checkout approval (1280x800)", MediaType.PmsCheckoutApproval1280x800, "DeliverypointgroupEntity", 380, 200, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.AlarmSetWhileNotCharging1280x800, new MediaRatioType("Alarm set while not charging (1280x800)", MediaType.AlarmSetWhileNotCharging1280x800, "DeliverypointgroupEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ClearBasketDialog1280x800, new MediaRatioType("Clear basket dialog (1280x800)", MediaType.ClearBasketDialog1280x800, "DeliverypointgroupEntity", 495, 345, DeviceType.Tablet1280x800));

                this.mediaRatioTypes.Add(MediaType.ProductPagePhotoHorizontal1280x800, new MediaRatioType("Product page horizontal photo (1280x800)", MediaType.ProductPagePhotoHorizontal1280x800, "ProductEntity", 930, 386, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductPagePhotoHorizontalPlaceholder1280x800, new MediaRatioType("Product page horizontal photo placeholder (1280x800)", MediaType.ProductPagePhotoHorizontalPlaceholder1280x800, "CompanyEntity", 930, 386, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductPagePhotoVertical1280x800, new MediaRatioType("Product page vertical photo (1280x800)", MediaType.ProductPagePhotoVertical1280x800, "ProductEntity", 630, 646, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductPagePhotoVerticalPlaceholder1280x800, new MediaRatioType("Product page vertical photo placeholder (1280x800)", MediaType.ProductPagePhotoVerticalPlaceholder1280x800, "CompanyEntity", 630, 646, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductIcon1280x800, new MediaRatioType("Product icon (1280x800)", MediaType.ProductIcon1280x800, "ProductEntity", 69, 69, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductIconPlaceholder1280x800, new MediaRatioType("Product icon placeholder (1280x800)", MediaType.ProductIconPlaceholder1280x800, "CompanyEntity", 69, 69, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.GenericProductIcon1280x800, new MediaRatioType("Generic product icon (1280x800)", MediaType.GenericProductIcon1280x800, "GenericproductEntity", 69, 69, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.GenericProductPagePhotoHorizontal1280x800, new MediaRatioType("Generic product page horizontal photo (1280x800)", MediaType.GenericProductPagePhotoHorizontal1280x800, "GenericproductEntity", 930, 386, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.GenericProductPagePhotoVertical1280x800, new MediaRatioType("Generic product page vertical photo (1280x800)", MediaType.GenericProductPagePhotoVertical1280x800, "GenericproductEntity", 630, 646, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.CategoryPagePhotoHorizontal1280x800, new MediaRatioType("Category page horizontal photo (1280x800)", MediaType.CategoryPagePhotoHorizontal1280x800, "CategoryEntity", 930, 386, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.CategoryPagePhotoHorizontalPlaceholder1280x800, new MediaRatioType("Category page horizontal photo placeholder (1280x800)", MediaType.CategoryPagePhotoHorizontalPlaceholder1280x800, "CompanyEntity", 930, 386, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.CategoryPagePhotoVertical1280x800, new MediaRatioType("Category page vertical photo (1280x800)", MediaType.CategoryPagePhotoVertical1280x800, "CategoryEntity", 630, 646, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.CategoryPagePhotoVerticalPlaceholder1280x800, new MediaRatioType("Category page vertical photo placeholder (1280x800)", MediaType.CategoryPagePhotoVerticalPlaceholder1280x800, "CompanyEntity", 630, 646, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.CategoryPagePhotoFullPage1280x800, new MediaRatioType("Full page image (1280x800)", MediaType.CategoryPagePhotoFullPage1280x800, "CategoryEntity", 928, 644, DeviceType.Tablet1280x800));

                this.mediaRatioTypes.Add(MediaType.AdvertisementWidgetSmall1280x800, new MediaRatioType("Advertisement Widget Small (1280x800)", MediaType.AdvertisementWidgetSmall1280x800, "AdvertisementEntity", 52, 52, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.AdvertisementWidgetMedium1280x800, new MediaRatioType("Advertisement Widget Medium (1280x800)", MediaType.AdvertisementWidgetMedium1280x800, "AdvertisementEntity", 116, 116, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.WidgetIconSmall1280x800, new MediaRatioType("Widget Icon Small (1280x800)", MediaType.WidgetIconSmall1280x800, "UIWidgetEntity", 52, 52, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.WidgetIconMedium1280x800, new MediaRatioType("Widget Icon Medium (1280x800)", MediaType.WidgetIconMedium1280x800, "UIWidgetEntity", 116, 116, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.Widget1x1Background1280x800, new MediaRatioType("Widget 1x1 Background (1280x800)", MediaType.Widget1x1Background1280x800, "UIWidgetEntity", 153, 52, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.Widget1x2Background1280x800, new MediaRatioType("Widget 1x2 Background (1280x800)", MediaType.Widget1x2Background1280x800, "UIWidgetEntity", 153, 116, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.Widget2x1Background1280x800, new MediaRatioType("Widget 2x1 Background (1280x800)", MediaType.Widget2x1Background1280x800, "UIWidgetEntity", 318, 52, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.Widget2x2Background1280x800, new MediaRatioType("Widget 2x2 Background (1280x800)", MediaType.Widget2x2Background1280x800, "UIWidgetEntity", 318, 116, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.AdvertisementWidget1x1Full1280x800, new MediaRatioType("Advertisement Widget 1x1 Full (1280x800)", MediaType.AdvertisementWidget1x1Full1280x800, "AdvertisementEntity", 153, 52, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.AdvertisementWidgetSmallFull1280x800, new MediaRatioType("Advertisement Widget Small Full (1280x800)", MediaType.AdvertisementWidgetSmallFull1280x800, "AdvertisementEntity", 318, 52, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.AdvertisementWidgetMediumFull1280x800, new MediaRatioType("Advertisement Widget Medium Full (1280x800)", MediaType.AdvertisementWidgetMediumFull1280x800, "AdvertisementEntity", 318, 116, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.WidgetPdf1280x800, new MediaRatioType("Widget Pdf", MediaType.WidgetPdf1280x800, "UIWidgetEntity", 500, 500, DeviceType.Tablet1280x800, MediaTypeGroup.None, true, 70));
                this.mediaRatioTypes.Add(MediaType.WidgetAvailability1280x800, new MediaRatioType("Widget Availability", MediaType.WidgetAvailability1280x800, "UIWidgetEntity", 320, 422, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.Widget2x4Background1280x800, new MediaRatioType("Widget 2x4 Background (1280x800)", MediaType.Widget2x4Background1280x800, "UIWidgetEntity", 318, 244, DeviceType.Tablet1280x800));

                this.mediaRatioTypes.Add(MediaType.ScreenBackground1280x800, new MediaRatioType("Screen Background (1280x800)", MediaType.ScreenBackground1280x800, "UIThemeEntity", 1280, 800, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.FooterLogo1280x800, new MediaRatioType("Footer Logo (1280x800)", MediaType.FooterLogo1280x800, "UIThemeEntity", 126, 50, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.FooterBrightnessIcon1280x800, new MediaRatioType("Brightness Icon (1280x800)", MediaType.FooterBrightnessIcon1280x800, "UIThemeEntity", 64, 64, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.FooterWarningIcon1280x800, new MediaRatioType("Warning Icon (1280x800)", MediaType.FooterWarningIcon1280x800, "UIThemeEntity", 64, 64, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.FooterPendingOrdersIcon1280x800, new MediaRatioType("Pending Orders Icon (1280x800)", MediaType.FooterPendingOrdersIcon1280x800, "UIThemeEntity", 64, 64, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.FooterGuestInformationIcon1280x800, new MediaRatioType("Guest Information Icon (1280x800)", MediaType.FooterGuestInformationIcon1280x800, "UIThemeEntity", 64, 64, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ScreensaverBackground1280x800, new MediaRatioType("Screensaver Background (1280x800)", MediaType.ScreensaverBackground1280x800, "UIThemeEntity", 1280, 800, DeviceType.Tablet1280x800));

                this.mediaRatioTypes.Add(MediaType.FooterCustomIcon1280x800, new MediaRatioType("Custom Icon (1280x800)", MediaType.FooterCustomIcon1280x800, "UIFooterItemEntity", 50, 50, DeviceType.Tablet1280x800));

                this.mediaRatioTypes.Add(MediaType.RoomControlSectionIcon1280x800, new MediaRatioType("Room Control Section Icon (1280x800)", MediaType.RoomControlSectionIcon1280x800, "RoomControlSectionEntity", 60, 60, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.RoomControlSectionItemIcon1280x800, new MediaRatioType("Room Control Section Item Icon (1280x800)", MediaType.RoomControlSectionItemIcon1280x800, "RoomControlSectionItemEntity", 69, 69, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.RoomControlStation1280x800, new MediaRatioType("Room Control Station (1280x800)", MediaType.RoomControlStation1280x800, "StationEntity", 132, 80, DeviceType.Tablet1280x800));

                this.mediaRatioTypes.Add(MediaType.AlterationDialogPage1280x800, new MediaRatioType("Alteration Dialog Page (1280x800)", MediaType.AlterationDialogPage1280x800, "AlterationEntity", 210, 120, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.AlterationDialogColumn1280x800, new MediaRatioType("Alteration Dialog Column (1280x800)", MediaType.AlterationDialogColumn1280x800, "AlterationEntity", 395, 530, DeviceType.Tablet1280x800));

                this.mediaRatioTypes.Add(MediaType.ProductDialogAlterationColumn1280x800, new MediaRatioType("Alteration Dialog Alteration Column (1280x800)", MediaType.ProductDialogAlterationColumn1280x800, "AlterationEntity", 324, 491, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductDialogProductgroupColumn1280x800, new MediaRatioType("Alteration Dialog Productgroup column (1280x800)", MediaType.ProductDialogProductgroupColumn1280x800, "ProductgroupEntity", 324, 491, DeviceType.Tablet1280x800));

                this.mediaRatioTypes.Add(MediaType.BrowserAgeVerificationImageSmall1280x800, new MediaRatioType("Browser Age Verification Image Small (1280x800)", MediaType.BrowserAgeVerificationImageSmall1280x800, "DeliverypointgroupEntity", 400, 200, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.BrowserAgeVerificationImageBig1280x800, new MediaRatioType("Browser Age Verification Image Big (1280x800)", MediaType.BrowserAgeVerificationImageBig1280x800, "DeliverypointgroupEntity", 400, 400, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.BrowserAgeVerificationBackground1280x800, new MediaRatioType("Browser Age Verification Background (1280x800)", MediaType.BrowserAgeVerificationBackground1280x800, "DeliverypointgroupEntity", 1258, 709, DeviceType.Tablet1280x800));

                this.mediaRatioTypes.Add(MediaType.RestartApplicationDialog1280x800, new MediaRatioType("Restart Application Dialog (1280x800)", MediaType.RestartApplicationDialog1280x800, "DeliverypointgroupEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.RebootDeviceDialog1280x800, new MediaRatioType("Reboot Device Dialog (1280x800)", MediaType.RebootDeviceDialog1280x800, "DeliverypointgroupEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.CategoryConfirmation1280x800, new MediaRatioType("Category Confirmation (1280x800)", MediaType.CategoryConfirmation1280x800, "CategoryEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductConfirmation1280x800, new MediaRatioType("Product Confirmation (1280x800)", MediaType.ProductConfirmation1280x800, "ProductEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.CategoryProcessed1280x800, new MediaRatioType("Category Processed (1280x800)", MediaType.CategoryProcessed1280x800, "CategoryEntity", 495, 345, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.ProductProcessed1280x800, new MediaRatioType("Product Processed (1280x800)", MediaType.ProductProcessed1280x800, "ProductEntity", 495, 345, DeviceType.Tablet1280x800));

                this.mediaRatioTypes.Add(MediaType.CraveReceiptLogo, new MediaRatioType("Receipt logo", MediaType.CraveReceiptLogo, "CompanyEntity", 300, 250, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.CraveConsoleLogo, new MediaRatioType("Console Logo", MediaType.CraveConsoleLogo, "DeliverypointgroupEntity", 150, 78, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.Gallery, new MediaRatioType("Gallery", MediaType.Gallery, "CompanyEntity", 300, 250, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.GallerySmall, new MediaRatioType("GallerySmall", MediaType.GallerySmall, "CompanyEntity", 320, 250, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.GalleryLarge, new MediaRatioType("GalleryLarge", MediaType.GalleryLarge, "CompanyEntity", 320, 422, DeviceType.Tablet1280x800));

                // Mobile
                this.mediaRatioTypes.Add(MediaType.CraveProductBrandingMobile, new MediaRatioType("Product branding mobile (800x480)", MediaType.CraveProductBrandingMobile, "ProductEntity", 720, 360, DeviceType.Unknown));
                this.mediaRatioTypes.Add(MediaType.CraveGenericProductBrandingMobile, new MediaRatioType("Generic product branding mobile (800x480)", MediaType.CraveGenericProductBrandingMobile, "GenericproductEntity", 720, 360, DeviceType.Unknown));

                // Android TV Box - 1280x720
                this.mediaRatioTypes.Add(MediaType.ProductBranding1280x720, new MediaRatioType("Product branding (1280x720)", MediaType.ProductBranding1280x720, "ProductEntity", 356, 442, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholder1280x720, new MediaRatioType("Product branding placeholder (1280x720)", MediaType.ProductBrandingPlaceholder1280x720, "CompanyEntity", 356, 442, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.ProductButton1280x720, new MediaRatioType("Product button (1280x720)", MediaType.ProductButton1280x720, "ProductEntity", 112, 71, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholder1280x720, new MediaRatioType("Product button placeholder (1280x720)", MediaType.ProductButtonPlaceholder1280x720, "CompanyEntity", 112, 71, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.GenericProductButton1280x720, new MediaRatioType("Generic product button (1280x720)", MediaType.GenericProductButton1280x720, "GenericproductEntity", 112, 71, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.GenericProductBranding1280x720, new MediaRatioType("Generic product branding (1280x720)", MediaType.GenericProductBranding1280x720, "GenericproductEntity", 356, 442, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.Homepage1280x720, new MediaRatioType("Homepage (1280x720)", MediaType.Homepage1280x720, "DeliverypointgroupEntity", 923, 539, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.EntertainmentButton1280x720, new MediaRatioType("Entertainment button (1280x720)", MediaType.EntertainmentButton1280x720, "EntertainmentEntity", 167, 125, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.EntertainmentButtonPlaceholder1280x720, new MediaRatioType("Entertainment button placeholder (1280x720)", MediaType.EntertainmentButtonPlaceholder1280x720, "CompanyEntity", 167, 125, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.SocialmediaBranding1280x720, new MediaRatioType("Social media (1280x720)", MediaType.SocialmediaBranding1280x720, "CompanyEntity", 388, 558, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.CategoryPhoto1280x720, new MediaRatioType("Category photo (1280x720)", MediaType.CategoryPhoto1280x720, "CategoryEntity", 1280, 519, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.CategoryPhotoPlaceholder1280x720, new MediaRatioType("Category photo placeholder (1280x720)", MediaType.CategoryPhotoPlaceholder1280x720, "CompanyEntity", 1280, 519, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.CategoryBranding1280x720, new MediaRatioType("Category branding (1280x720)", MediaType.CategoryBranding1280x720, "CategoryEntity", 394, 515, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.OutOfCharge1280x720, new MediaRatioType("Out of charge (1280x720)", MediaType.OutOfCharge1280x720, "DeliverypointgroupEntity", 495, 345, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.OrderProcessed1280x720, new MediaRatioType("Order processed (1280x720)", MediaType.OrderProcessed1280x720, "DeliverypointgroupEntity", 495, 345, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.ServiceProcessed1280x720, new MediaRatioType("Service processed (1280x720)", MediaType.ServiceProcessed1280x720, "DeliverypointgroupEntity", 495, 345, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.RatingProcessed1280x720, new MediaRatioType("Rating processed (1280x720)", MediaType.RatingProcessed1280x720, "DeliverypointgroupEntity", 495, 345, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.SocialmediaProcessed1280x720, new MediaRatioType("Socialmedia processed (1280x720)", MediaType.SocialmediaProcessed1280x720, "CompanySocialmediaEntity", 495, 345, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.SurveyProcessed1280x720, new MediaRatioType("Survey processed (1280x720)", MediaType.SurveyProcessed1280x720, "SurveyEntity", 495, 345, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.FormProcessed1280x720, new MediaRatioType("Form processed (1280x720)", MediaType.FormProcessed1280x720, "FormEntity", 495, 345, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.Basket1280x720, new MediaRatioType("Basket (1280x720)", MediaType.Basket1280x720, "DeliverypointgroupEntity", 400, 511, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.Advertisement1280x720, new MediaRatioType("Advertisement (1280x720)", MediaType.Advertisement1280x720, "AdvertisementEntity", 394, 515, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.ProductAdded1280x720, new MediaRatioType("Product added (1280x720)", MediaType.ProductAdded1280x720, "CompanyEntity", 876, 250, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.NotificationDialog1280x720, new MediaRatioType("Notification dialog (1280x720)", MediaType.NotificationDialog1280x720, "CompanyEntity", 495, 345, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.AlterationImage1280x720, new MediaRatioType("Alteration image (1280x720)", MediaType.AlterationImage1280x720, "AlterationEntity", 413, 355, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.AdvertisementSmall1280x720, new MediaRatioType("Advertisement small (1280x720)", MediaType.AdvertisementSmall1280x720, "AdvertisementEntity", 70, 80, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.AdvertisementHalf1280x720, new MediaRatioType("Advertisement half (1280x720)", MediaType.AdvertisementHalf1280x720, "AdvertisementEntity", 394, 250, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.SurveyPageBottom1280x720, new MediaRatioType("Survey page bottom (1280x720)", MediaType.SurveyPageBottom1280x720, "SurveyPageEntity", 820, 200, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.SurveyPageRight1280x720, new MediaRatioType("Survey page right (1280x720)", MediaType.SurveyPageRight1280x720, "SurveyPageEntity", 380, 370, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.SurveyPageLogo1280x720, new MediaRatioType("Survey page logo (1280x720)", MediaType.SurveyPageLogo1280x720, "SurveyPageEntity", 150, 150, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.SurveyPageRightHalf1280x720, new MediaRatioType("Survey page right half (1280x720)", MediaType.SurveyPageRightHalf1280x720, "SurveyPageEntity", 180, 370, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.SurveyAnswerRequired1280x720, new MediaRatioType("Survey answer required (1280x720)", MediaType.SurveyAnswerRequired1280x720, "SurveyEntity", 495, 345, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.PmsMessage1280x720, new MediaRatioType("PMS Message dialog (1280x720)", MediaType.PmsMessage1280x720, "DeliverypointgroupEntity", 200, 280, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.PmsBill1280x720, new MediaRatioType("PMS Bill logo (1280x720)", MediaType.PmsBill1280x720, "DeliverypointgroupEntity", 230, 85, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.PmsCheckoutApproval1280x720, new MediaRatioType("PMS Checkout approval (1280x720)", MediaType.PmsCheckoutApproval1280x720, "DeliverypointgroupEntity", 380, 200, DeviceType.AndroidTvBox));
                this.mediaRatioTypes.Add(MediaType.HomepageFullscreen1280x720, new MediaRatioType("Homepage Fullscreen (1280x720)", MediaType.HomepageFullscreen1280x720, "DeliverypointgroupEntity", 1280, 539, DeviceType.AndroidTvBox));

                // Phone small
                this.mediaRatioTypes.Add(MediaType.ProductButtonPhoneSmall, new MediaRatioType("Product button small (phone)", MediaType.ProductButtonPhoneSmall, "ProductEntity", 56, 56, DeviceType.PhoneSmall, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtonPhoneSmall, new MediaRatioType("Generic product button small (phone)", MediaType.GenericProductButtonPhoneSmall, "GenericproductEntity", 56, 56, DeviceType.PhoneSmall, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPhoneSmall, new MediaRatioType("Product branding small (phone)", MediaType.ProductBrandingPhoneSmall, "ProductEntity", 300, 169, DeviceType.PhoneSmall, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingPhoneSmall, new MediaRatioType("Generic product branding small (phone)", MediaType.GenericProductBrandingPhoneSmall, "GenericproductEntity", 300, 169, DeviceType.PhoneSmall, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPhoneSmall, new MediaRatioType("Category button small (phone)", MediaType.CategoryButtonPhoneSmall, "CategoryEntity", 40, 40, DeviceType.PhoneSmall, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtonPhoneSmall, new MediaRatioType("Generic category button small (phone)", MediaType.GenericCategoryButtonPhoneSmall, "GenericcategoryEntity", 40, 40, DeviceType.PhoneSmall, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPhoneSmall, new MediaRatioType("Category branding small (phone)", MediaType.CategoryBrandingPhoneSmall, "CategoryEntity", 320, 150, DeviceType.PhoneSmall, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingPhoneSmall, new MediaRatioType("Generic category branding small (phone)", MediaType.GenericCategoryBrandingPhoneSmall, "GenericcategoryEntity", 320, 150, DeviceType.PhoneSmall, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListPhoneSmall, new MediaRatioType("Company list small (phone)", MediaType.CompanyListPhoneSmall, "CompanyEntity", 94, 197, DeviceType.PhoneSmall, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.HomepagePhoneSmall, new MediaRatioType("Homepage small (phone)", MediaType.HomepagePhoneSmall, "DeliverypointgroupEntity", 320, 150, DeviceType.PhoneSmall, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListPhoneSmall, new MediaRatioType("PointOfInterest list small (phone)", MediaType.PointOfInterestListPhoneSmall, "PointOfInterestEntity", 94, 197, DeviceType.PhoneSmall, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepagePhoneSmall, new MediaRatioType("PointOfInterest homepage small (phone)", MediaType.PointOfInterestHomepagePhoneSmall, "PointOfInterestEntity", 320, 150, DeviceType.PhoneSmall, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconPhoneSmall, new MediaRatioType("Map icon small (phone)", MediaType.MapIconPhoneSmall, "CompanyEntity", 56, 56, DeviceType.PhoneSmall, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconPhoneSmall, new MediaRatioType("PointOfInterest map icon small (phone)", MediaType.PointOfInterestMapIconPhoneSmall, "PointOfInterestEntity", 56, 56, DeviceType.PhoneSmall, MediaTypeGroup.ByodPointOfInterestMapIcon));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderPhoneSmall, new MediaRatioType("Product button placeholder small (phone)", MediaType.ProductButtonPlaceholderPhoneSmall, "CompanyEntity", 56, 56, DeviceType.PhoneSmall, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderPhoneSmall, new MediaRatioType("Product branding placeholder small (phone)", MediaType.ProductBrandingPlaceholderPhoneSmall, "CompanyEntity", 300, 169, DeviceType.PhoneSmall, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderPhoneSmall, new MediaRatioType("Category button placeholder small (phone)", MediaType.CategoryButtonPlaceholderPhoneSmall, "CompanyEntity", 40, 40, DeviceType.PhoneSmall, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderPhoneSmall, new MediaRatioType("Category branding placeholder small (phone)", MediaType.CategoryBrandingPlaceholderPhoneSmall, "CompanyEntity", 320, 150, DeviceType.PhoneSmall, MediaTypeGroup.ByodCategoryBrandingPlaceholder));

                // Phone normal
                this.mediaRatioTypes.Add(MediaType.ProductButtonPhoneNormal, new MediaRatioType("Product button normal (phone)", MediaType.ProductButtonPhoneNormal, "ProductEntity", 75, 75, DeviceType.PhoneNormal, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtonPhoneNormal, new MediaRatioType("Generic product button normal (phone)", MediaType.GenericProductButtonPhoneNormal, "GenericproductEntity", 75, 75, DeviceType.PhoneNormal, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPhoneNormal, new MediaRatioType("Product branding normal (phone)", MediaType.ProductBrandingPhoneNormal, "ProductEntity", 834, 470, DeviceType.PhoneNormal, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingPhoneNormal, new MediaRatioType("Generic product branding normal (phone)", MediaType.GenericProductBrandingPhoneNormal, "GenericproductEntity", 834, 470, DeviceType.PhoneNormal, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPhoneNormal, new MediaRatioType("Category button normal (phone)", MediaType.CategoryButtonPhoneNormal, "CategoryEntity", 53, 53, DeviceType.PhoneNormal, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtonPhoneNormal, new MediaRatioType("Generic category button normal (phone)", MediaType.GenericCategoryButtonPhoneNormal, "GenericcategoryEntity", 53, 53, DeviceType.PhoneNormal, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPhoneNormal, new MediaRatioType("Category branding normal (phone)", MediaType.CategoryBrandingPhoneNormal, "CategoryEntity", 854, 400, DeviceType.PhoneNormal, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingPhoneNormal, new MediaRatioType("Generic category branding normal (phone)", MediaType.GenericCategoryBrandingPhoneNormal, "GenericcategoryEntity", 854, 400, DeviceType.PhoneNormal, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListPhoneNormal, new MediaRatioType("Company list normal (phone)", MediaType.CompanyListPhoneNormal, "CompanyEntity", 251, 527, DeviceType.PhoneNormal, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.HomepagePhoneNormal, new MediaRatioType("Homepage normal (phone)", MediaType.HomepagePhoneNormal, "DeliverypointgroupEntity", 854, 400, DeviceType.PhoneNormal, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListPhoneNormal, new MediaRatioType("PointOfInterest list normal (phone)", MediaType.PointOfInterestListPhoneNormal, "PointOfInterestEntity", 251, 527, DeviceType.PhoneNormal, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepagePhoneNormal, new MediaRatioType("PointOfInterest homepage normal (phone)", MediaType.PointOfInterestHomepagePhoneNormal, "PointOfInterestEntity", 854, 400, DeviceType.PhoneNormal, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconPhoneNormal, new MediaRatioType("Map icon normal (phone)", MediaType.MapIconPhoneNormal, "CompanyEntity", 75, 75, DeviceType.PhoneNormal, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconPhoneNormal, new MediaRatioType("PointOfInterest map icon normal (phone)", MediaType.PointOfInterestMapIconPhoneNormal, "PointOfInterestEntity", 75, 75, DeviceType.PhoneNormal, MediaTypeGroup.ByodPointOfInterestMapIcon));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderPhoneNormal, new MediaRatioType("Product button placeholder normal (phone)", MediaType.ProductButtonPlaceholderPhoneNormal, "CompanyEntity", 75, 75, DeviceType.PhoneNormal, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderPhoneNormal, new MediaRatioType("Product branding placeholder normal (phone)", MediaType.ProductBrandingPlaceholderPhoneNormal, "CompanyEntity", 834, 470, DeviceType.PhoneNormal, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderPhoneNormal, new MediaRatioType("Category button placeholder normal (phone)", MediaType.CategoryButtonPlaceholderPhoneNormal, "CompanyEntity", 53, 53, DeviceType.PhoneNormal, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderPhoneNormal, new MediaRatioType("Category branding placeholder normal (phone)", MediaType.CategoryBrandingPlaceholderPhoneNormal, "CompanyEntity", 854, 400, DeviceType.PhoneNormal, MediaTypeGroup.ByodCategoryBrandingPlaceholder));

                // Phone large
                this.mediaRatioTypes.Add(MediaType.ProductButtonPhoneLarge, new MediaRatioType("Product button large (phone)", MediaType.ProductButtonPhoneLarge, "ProductEntity", 112, 112, DeviceType.PhoneLarge, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtonPhoneLarge, new MediaRatioType("Generic product button large (phone)", MediaType.GenericProductButtonPhoneLarge, "GenericproductEntity", 112, 112, DeviceType.PhoneLarge, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPhoneLarge, new MediaRatioType("Product branding large (phone)", MediaType.ProductBrandingPhoneLarge, "ProductEntity", 1240, 697, DeviceType.PhoneLarge, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingPhoneLarge, new MediaRatioType("Generic product branding large (phone)", MediaType.GenericProductBrandingPhoneLarge, "GenericproductEntity", 1240, 697, DeviceType.PhoneLarge, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPhoneLarge, new MediaRatioType("Category button large (phone)", MediaType.CategoryButtonPhoneLarge, "CategoryEntity", 80, 80, DeviceType.PhoneLarge, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtonPhoneLarge, new MediaRatioType("Generic category button large (phone)", MediaType.GenericCategoryButtonPhoneLarge, "GenericcategoryEntity", 80, 80, DeviceType.PhoneLarge, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPhoneLarge, new MediaRatioType("Category branding large (phone)", MediaType.CategoryBrandingPhoneLarge, "CategoryEntity", 1280, 600, DeviceType.PhoneLarge, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingPhoneLarge, new MediaRatioType("Generic category branding large (phone)", MediaType.GenericCategoryBrandingPhoneLarge, "GenericcategoryEntity", 1280, 600, DeviceType.PhoneLarge, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListPhoneLarge, new MediaRatioType("Company list large (phone)", MediaType.CompanyListPhoneLarge, "CompanyEntity", 375, 789, DeviceType.PhoneLarge, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.HomepagePhoneLarge, new MediaRatioType("Homepage large (phone)", MediaType.HomepagePhoneLarge, "DeliverypointgroupEntity", 1280, 600, DeviceType.PhoneLarge, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListPhoneLarge, new MediaRatioType("PointOfInterest list large (phone)", MediaType.PointOfInterestListPhoneLarge, "PointOfInterestEntity", 375, 789, DeviceType.PhoneLarge, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepagePhoneLarge, new MediaRatioType("PointOfInterest homepage large (phone)", MediaType.PointOfInterestHomepagePhoneLarge, "PointOfInterestEntity", 1280, 600, DeviceType.PhoneLarge, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconPhoneLarge, new MediaRatioType("Map icon large (phone)", MediaType.MapIconPhoneLarge, "CompanyEntity", 112, 112, DeviceType.PhoneLarge, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconPhoneLarge, new MediaRatioType("PointOfInterest map icon large (phone)", MediaType.PointOfInterestMapIconPhoneLarge, "PointOfInterestEntity", 112, 112, DeviceType.PhoneLarge, MediaTypeGroup.ByodPointOfInterestMapIcon));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderPhoneLarge, new MediaRatioType("Product button placeholder large (phone)", MediaType.ProductButtonPlaceholderPhoneLarge, "CompanyEntity", 112, 112, DeviceType.PhoneLarge, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderPhoneLarge, new MediaRatioType("Product branding placeholder large (phone)", MediaType.ProductBrandingPlaceholderPhoneLarge, "CompanyEntity", 1240, 697, DeviceType.PhoneLarge, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderPhoneLarge, new MediaRatioType("Category button placeholder large (phone)", MediaType.CategoryButtonPlaceholderPhoneLarge, "CompanyEntity", 80, 80, DeviceType.PhoneLarge, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderPhoneLarge, new MediaRatioType("Category branding placeholder large (phone)", MediaType.CategoryBrandingPlaceholderPhoneLarge, "CompanyEntity", 1280, 600, DeviceType.PhoneLarge, MediaTypeGroup.ByodCategoryBrandingPlaceholder));

                // Phone xlarge
                this.mediaRatioTypes.Add(MediaType.ProductButtonPhoneXLarge, new MediaRatioType("Product button xlarge (phone)", MediaType.ProductButtonPhoneXLarge, "ProductEntity", 150, 150, DeviceType.PhoneXLarge, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtonPhoneXLarge, new MediaRatioType("Generic product button xlarge (phone)", MediaType.GenericProductButtonPhoneXLarge, "GenericproductEntity", 150, 150, DeviceType.PhoneXLarge, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPhoneXLarge, new MediaRatioType("Product branding xlarge (phone)", MediaType.ProductBrandingPhoneXLarge, "ProductEntity", 1880, 1060, DeviceType.PhoneXLarge, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingPhoneXLarge, new MediaRatioType("Generic product branding xlarge (phone)", MediaType.GenericProductBrandingPhoneXLarge, "GenericproductEntity", 1880, 1060, DeviceType.PhoneXLarge, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPhoneXLarge, new MediaRatioType("Category button xlarge (phone)", MediaType.CategoryButtonPhoneXLarge, "CategoryEntity", 107, 107, DeviceType.PhoneXLarge, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtonPhoneXLarge, new MediaRatioType("Generic category button xlarge (phone)", MediaType.GenericCategoryButtonPhoneXLarge, "GenericcategoryEntity", 107, 107, DeviceType.PhoneXLarge, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPhoneXLarge, new MediaRatioType("Category branding xlarge (phone)", MediaType.CategoryBrandingPhoneXLarge, "CategoryEntity", 1920, 900, DeviceType.PhoneXLarge, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingPhoneXLarge, new MediaRatioType("Generic category branding xlarge (phone)", MediaType.GenericCategoryBrandingPhoneXLarge, "GenericcategoryEntity", 1920, 900, DeviceType.PhoneXLarge, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListPhoneXLarge, new MediaRatioType("Company list xlarge (phone)", MediaType.CompanyListPhoneXLarge, "CompanyEntity", 564, 1184, DeviceType.PhoneXLarge, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.HomepagePhoneXLarge, new MediaRatioType("Homepage xlarge (phone)", MediaType.HomepagePhoneXLarge, "DeliverypointgroupEntity", 1920, 900, DeviceType.PhoneXLarge, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListPhoneXLarge, new MediaRatioType("PointOfInterest list xlarge (phone)", MediaType.PointOfInterestListPhoneXLarge, "PointOfInterestEntity", 564, 1184, DeviceType.PhoneXLarge, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepagePhoneXLarge, new MediaRatioType("PointOfInterest homepage xlarge (phone)", MediaType.PointOfInterestHomepagePhoneXLarge, "PointOfInterestEntity", 1920, 900, DeviceType.PhoneXLarge, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconPhoneXLarge, new MediaRatioType("Map icon xlarge (phone)", MediaType.MapIconPhoneXLarge, "CompanyEntity", 150, 150, DeviceType.PhoneXLarge, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconPhoneXLarge, new MediaRatioType("PointOfInterest map icon xlarge (phone)", MediaType.PointOfInterestMapIconPhoneXLarge, "PointOfInterestEntity", 150, 150, DeviceType.PhoneXLarge, MediaTypeGroup.ByodPointOfInterestMapIcon));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderPhoneXLarge, new MediaRatioType("Product button placeholder xlarge (phone)", MediaType.ProductButtonPlaceholderPhoneXLarge, "CompanyEntity", 150, 150, DeviceType.PhoneXLarge, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderPhoneXLarge, new MediaRatioType("Product branding placeholder xlarge (phone)", MediaType.ProductBrandingPlaceholderPhoneXLarge, "CompanyEntity", 1880, 1060, DeviceType.PhoneXLarge, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderPhoneXLarge, new MediaRatioType("Category button placeholder xlarge (phone)", MediaType.CategoryButtonPlaceholderPhoneXLarge, "CompanyEntity", 107, 107, DeviceType.PhoneXLarge, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderPhoneXLarge, new MediaRatioType("Category branding placeholder xlarge (phone)", MediaType.CategoryBrandingPlaceholderPhoneXLarge, "CompanyEntity", 1920, 900, DeviceType.PhoneXLarge, MediaTypeGroup.ByodCategoryBrandingPlaceholder));

                // Tablet small
                this.mediaRatioTypes.Add(MediaType.ProductButtonTabletSmall, new MediaRatioType("Product button small (tablet)", MediaType.ProductButtonTabletSmall, "ProductEntity", 56, 56, DeviceType.TabletSmall, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtonTabletSmall, new MediaRatioType("Generic product button small (tablet)", MediaType.GenericProductButtonTabletSmall, "GenericproductEntity", 56, 56, DeviceType.TabletSmall, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingTabletSmall, new MediaRatioType("Product branding small (tablet)", MediaType.ProductBrandingTabletSmall, "ProductEntity", 780, 439, DeviceType.TabletSmall, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingTabletSmall, new MediaRatioType("Generic product branding small (tablet)", MediaType.GenericProductBrandingTabletSmall, "GenericproductEntity", 780, 439, DeviceType.TabletSmall, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonTabletSmall, new MediaRatioType("Category button small (tablet)", MediaType.CategoryButtonTabletSmall, "CategoryEntity", 40, 40, DeviceType.TabletSmall, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtonTabletSmall, new MediaRatioType("Generic category button small (tablet)", MediaType.GenericCategoryButtonTabletSmall, "GenericcategoryEntity", 40, 40, DeviceType.TabletSmall, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingTabletSmall, new MediaRatioType("Category branding small (tablet)", MediaType.CategoryBrandingTabletSmall, "CategoryEntity", 803, 452, DeviceType.TabletSmall, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingTabletSmall, new MediaRatioType("Generic category branding small (tablet)", MediaType.GenericCategoryBrandingTabletSmall, "GenericcategoryEntity", 803, 452, DeviceType.TabletSmall, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListTabletSmall, new MediaRatioType("Company list small (tablet)", MediaType.CompanyListTabletSmall, "CompanyEntity", 441, 925, DeviceType.TabletSmall, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.HomepageTabletSmall, new MediaRatioType("Homepage small (tablet)", MediaType.HomepageTabletSmall, "DeliverypointgroupEntity", 1280, 600, DeviceType.TabletSmall, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListTabletSmall, new MediaRatioType("PointOfInterest list small (tablet)", MediaType.PointOfInterestListTabletSmall, "PointOfInterestEntity", 441, 925, DeviceType.TabletSmall, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepageTabletSmall, new MediaRatioType("PointOfInterest homepage small (tablet)", MediaType.PointOfInterestHomepageTabletSmall, "PointOfInterestEntity", 1280, 600, DeviceType.TabletSmall, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconTabletSmall, new MediaRatioType("Map icon small (tablet)", MediaType.MapIconTabletSmall, "CompanyEntity", 75, 75, DeviceType.TabletSmall, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconTabletSmall, new MediaRatioType("PointOfInterest map icon small (tablet)", MediaType.PointOfInterestMapIconTabletSmall, "PointOfInterestEntity", 75, 75, DeviceType.TabletSmall, MediaTypeGroup.ByodPointOfInterestMapIcon));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderTabletSmall, new MediaRatioType("Product button placeholder small (tablet)", MediaType.ProductButtonPlaceholderTabletSmall, "CompanyEntity", 56, 56, DeviceType.TabletSmall, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderTabletSmall, new MediaRatioType("Product branding placeholder small (tablet)", MediaType.ProductBrandingPlaceholderTabletSmall, "CompanyEntity", 780, 439, DeviceType.TabletSmall, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderTabletSmall, new MediaRatioType("Category button placeholder small (tablet)", MediaType.CategoryButtonPlaceholderTabletSmall, "CompanyEntity", 40, 40, DeviceType.TabletSmall, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderTabletSmall, new MediaRatioType("Category branding placeholder small (tablet)", MediaType.CategoryBrandingPlaceholderTabletSmall, "CompanyEntity", 803, 452, DeviceType.TabletSmall, MediaTypeGroup.ByodCategoryBrandingPlaceholder));

                // Tablet normal
                this.mediaRatioTypes.Add(MediaType.ProductButtonTabletNormal, new MediaRatioType("Product button normal (tablet)", MediaType.ProductButtonTabletNormal, "ProductEntity", 75, 75, DeviceType.TabletNormal, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtonTabletNormal, new MediaRatioType("Generic product button normal (tablet)", MediaType.GenericProductButtonTabletNormal, "GenericproductEntity", 75, 75, DeviceType.TabletNormal, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingTabletNormal, new MediaRatioType("Product branding normal (tablet)", MediaType.ProductBrandingTabletNormal, "ProductEntity", 1204, 677, DeviceType.TabletNormal, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingTabletNormal, new MediaRatioType("Generic product branding normal (tablet)", MediaType.GenericProductBrandingTabletNormal, "GenericproductEntity", 1204, 677, DeviceType.TabletNormal, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonTabletNormal, new MediaRatioType("Category button normal (tablet)", MediaType.CategoryButtonTabletNormal, "CategoryEntity", 53, 53, DeviceType.TabletNormal, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtonTabletNormal, new MediaRatioType("Generic category button normal (tablet)", MediaType.GenericCategoryButtonTabletNormal, "GenericcategoryEntity", 53, 53, DeviceType.TabletNormal, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingTabletNormal, new MediaRatioType("Category branding normal (tablet)", MediaType.CategoryBrandingTabletNormal, "CategoryEntity", 1204, 677, DeviceType.TabletNormal, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingTabletNormal, new MediaRatioType("Generic category branding normal (tablet)", MediaType.GenericCategoryBrandingTabletNormal, "GenericcategoryEntity", 1204, 677, DeviceType.TabletNormal, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListTabletNormal, new MediaRatioType("Company list normal (tablet)", MediaType.CompanyListTabletNormal, "CompanyEntity", 662, 1388, DeviceType.TabletNormal, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.HomepageTabletNormal, new MediaRatioType("Homepage normal (tablet)", MediaType.HomepageTabletNormal, "DeliverypointgroupEntity", 1920, 900, DeviceType.TabletNormal, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListTabletNormal, new MediaRatioType("PointOfInterest list normal (tablet)", MediaType.PointOfInterestListTabletNormal, "PointOfInterestEntity", 662, 1388, DeviceType.TabletNormal, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepageTabletNormal, new MediaRatioType("PointOfInterest homepage normal (tablet)", MediaType.PointOfInterestHomepageTabletNormal, "PointOfInterestEntity", 1920, 900, DeviceType.TabletNormal, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconTabletNormal, new MediaRatioType("Map icon normal (tablet)", MediaType.MapIconTabletNormal, "CompanyEntity", 75, 75, DeviceType.TabletNormal, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconTabletNormal, new MediaRatioType("PointOfInterest map icon normal (tablet)", MediaType.PointOfInterestMapIconTabletNormal, "PointOfInterestEntity", 75, 75, DeviceType.TabletNormal, MediaTypeGroup.ByodPointOfInterestMapIcon));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderTabletNormal, new MediaRatioType("Product button placeholder normal (tablet)", MediaType.ProductButtonPlaceholderTabletNormal, "CompanyEntity", 75, 75, DeviceType.TabletNormal, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderTabletNormal, new MediaRatioType("Product branding placeholder normal (tablet)", MediaType.ProductBrandingPlaceholderTabletNormal, "CompanyEntity", 1204, 677, DeviceType.TabletNormal, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderTabletNormal, new MediaRatioType("Category button placeholder normal (tablet)", MediaType.CategoryButtonPlaceholderTabletNormal, "CompanyEntity", 53, 53, DeviceType.TabletNormal, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderTabletNormal, new MediaRatioType("Category branding placeholder normal (tablet)", MediaType.CategoryBrandingPlaceholderTabletNormal, "CompanyEntity", 1204, 677, DeviceType.TabletNormal, MediaTypeGroup.ByodCategoryBrandingPlaceholder));

                // Tablet large
                this.mediaRatioTypes.Add(MediaType.ProductButtonTabletLarge, new MediaRatioType("Product button large (tablet)", MediaType.ProductButtonTabletLarge, "ProductEntity", 112, 112, DeviceType.TabletLarge, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtonTabletLarge, new MediaRatioType("Generic product button large (tablet)", MediaType.GenericProductButtonTabletLarge, "GenericproductEntity", 112, 112, DeviceType.TabletLarge, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingTabletLarge, new MediaRatioType("Product branding large (tablet)", MediaType.ProductBrandingTabletLarge, "ProductEntity", 1605, 903, DeviceType.TabletLarge, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingTabletLarge, new MediaRatioType("Generic product branding large (tablet)", MediaType.GenericProductBrandingTabletLarge, "GenericproductEntity", 1605, 903, DeviceType.TabletLarge, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonTabletLarge, new MediaRatioType("Category button large (tablet)", MediaType.CategoryButtonTabletLarge, "CategoryEntity", 80, 80, DeviceType.TabletLarge, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtonTabletLarge, new MediaRatioType("Generic category button large (tablet)", MediaType.GenericCategoryButtonTabletLarge, "GenericcategoryEntity", 80, 80, DeviceType.TabletLarge, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingTabletLarge, new MediaRatioType("Category branding large (tablet)", MediaType.CategoryBrandingTabletLarge, "CategoryEntity", 1605, 903, DeviceType.TabletLarge, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingTabletLarge, new MediaRatioType("Generic category branding large (tablet)", MediaType.GenericCategoryBrandingTabletLarge, "GenericcategoryEntity", 1605, 903, DeviceType.TabletLarge, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListTabletLarge, new MediaRatioType("Company list large (tablet)", MediaType.CompanyListTabletLarge, "CompanyEntity", 882, 1850, DeviceType.TabletLarge, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.HomepageTabletLarge, new MediaRatioType("Homepage large (tablet)", MediaType.HomepageTabletLarge, "DeliverypointgroupEntity", 2560, 1200, DeviceType.TabletLarge, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListTabletLarge, new MediaRatioType("PointOfInterest list large (tablet)", MediaType.PointOfInterestListTabletLarge, "PointOfInterestEntity", 882, 1850, DeviceType.TabletLarge, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepageTabletLarge, new MediaRatioType("PointOfInterest homepage large (tablet)", MediaType.PointOfInterestHomepageTabletLarge, "PointOfInterestEntity", 2560, 1200, DeviceType.TabletLarge, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconTabletLarge, new MediaRatioType("Map icon large (tablet)", MediaType.MapIconTabletLarge, "CompanyEntity", 112, 112, DeviceType.TabletLarge, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconTabletLarge, new MediaRatioType("PointOfInterest map icon large (tablet)", MediaType.PointOfInterestMapIconTabletLarge, "PointOfInterestEntity", 112, 112, DeviceType.TabletLarge, MediaTypeGroup.ByodPointOfInterestMapIcon));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderTabletLarge, new MediaRatioType("Product button placeholder large (tablet)", MediaType.ProductButtonPlaceholderTabletLarge, "CompanyEntity", 112, 112, DeviceType.TabletLarge, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderTabletLarge, new MediaRatioType("Product branding placeholder large (tablet)", MediaType.ProductBrandingPlaceholderTabletLarge, "CompanyEntity", 1605, 903, DeviceType.TabletLarge, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderTabletLarge, new MediaRatioType("Category button placeholder large (tablet)", MediaType.CategoryButtonPlaceholderTabletLarge, "CompanyEntity", 80, 80, DeviceType.TabletLarge, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderTabletLarge, new MediaRatioType("Category branding placeholder large (tablet)", MediaType.CategoryBrandingPlaceholderTabletLarge, "CompanyEntity", 1605, 903, DeviceType.TabletLarge, MediaTypeGroup.ByodCategoryBrandingPlaceholder));

                // Tablet xlarge
                this.mediaRatioTypes.Add(MediaType.ProductButtonTabletXLarge, new MediaRatioType("Product button xlarge (tablet)", MediaType.ProductButtonTabletXLarge, "ProductEntity", 150, 150, DeviceType.TabletXLarge, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtonTabletXLarge, new MediaRatioType("Generic product button xlarge (tablet)", MediaType.GenericProductButtonTabletXLarge, "GenericproductEntity", 150, 150, DeviceType.TabletXLarge, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingTabletXLarge, new MediaRatioType("Product branding xlarge (tablet)", MediaType.ProductBrandingTabletXLarge, "ProductEntity", 2006, 1128, DeviceType.TabletXLarge, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingTabletXLarge, new MediaRatioType("Generic product branding xlarge (tablet)", MediaType.GenericProductBrandingTabletXLarge, "GenericproductEntity", 2006, 1128, DeviceType.TabletXLarge, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonTabletXLarge, new MediaRatioType("Category button xlarge (tablet)", MediaType.CategoryButtonTabletXLarge, "CategoryEntity", 107, 107, DeviceType.TabletXLarge, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtonTabletXLarge, new MediaRatioType("Generic category button xlarge (tablet)", MediaType.GenericCategoryButtonTabletXLarge, "GenericcategoryEntity", 107, 107, DeviceType.TabletXLarge, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingTabletXLarge, new MediaRatioType("Category branding xlarge (tablet)", MediaType.CategoryBrandingTabletXLarge, "CategoryEntity", 2006, 1129, DeviceType.TabletXLarge, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingTabletXLarge, new MediaRatioType("Generic category branding xlarge (tablet)", MediaType.GenericCategoryBrandingTabletXLarge, "GenericcategoryEntity", 2006, 1129, DeviceType.TabletXLarge, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListTabletXLarge, new MediaRatioType("Company list xlarge (tablet)", MediaType.CompanyListTabletXLarge, "CompanyEntity", 1103, 2312, DeviceType.TabletXLarge, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.HomepageTabletXLarge, new MediaRatioType("Homepage xlarge (tablet)", MediaType.HomepageTabletXLarge, "DeliverypointgroupEntity", 3200, 1500, DeviceType.TabletXLarge, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListTabletXLarge, new MediaRatioType("PointOfInterest list xlarge (tablet)", MediaType.PointOfInterestListTabletXLarge, "PointOfInterestEntity", 1103, 2312, DeviceType.TabletXLarge, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepageTabletXLarge, new MediaRatioType("PointOfInterest homepage xlarge (tablet)", MediaType.PointOfInterestHomepageTabletXLarge, "PointOfInterestEntity", 3200, 1500, DeviceType.TabletXLarge, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconTabletXLarge, new MediaRatioType("Map icon xlarge (tablet)", MediaType.MapIconTabletXLarge, "CompanyEntity", 150, 150, DeviceType.TabletXLarge, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconTabletXLarge, new MediaRatioType("PointOfInterest map icon xlarge (tablet)", MediaType.PointOfInterestMapIconTabletXLarge, "PointOfInterestEntity", 150, 150, DeviceType.TabletXLarge, MediaTypeGroup.ByodPointOfInterestMapIcon));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderTabletXLarge, new MediaRatioType("Product button placeholder xlarge (tablet)", MediaType.ProductButtonPlaceholderTabletXLarge, "CompanyEntity", 150, 150, DeviceType.TabletXLarge, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderTabletXLarge, new MediaRatioType("Product branding placeholder xlarge (tablet)", MediaType.ProductBrandingPlaceholderTabletXLarge, "CompanyEntity", 2006, 1128, DeviceType.TabletXLarge, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderTabletXLarge, new MediaRatioType("Category button placeholder xlarge (tablet)", MediaType.CategoryButtonPlaceholderTabletXLarge, "CompanyEntity", 107, 107, DeviceType.TabletXLarge, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderTabletXLarge, new MediaRatioType("Category branding placeholder xlarge (tablet)", MediaType.CategoryBrandingPlaceholderTabletXLarge, "CompanyEntity", 2006, 1129, DeviceType.TabletXLarge, MediaTypeGroup.ByodCategoryBrandingPlaceholder));

                // iPhone
                this.mediaRatioTypes.Add(MediaType.ProductButtoniPhone, new MediaRatioType("Product button iPhone", MediaType.ProductButtoniPhone, "ProductEntity", 56, 56, DeviceType.iPhone, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtoniPhone, new MediaRatioType("Generic product button iPhone", MediaType.GenericProductButtoniPhone, "GenericproductEntity", 56, 56, DeviceType.iPhone, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingiPhone, new MediaRatioType("Product branding iPhone", MediaType.ProductBrandingiPhone, "ProductEntity", 460, 259, DeviceType.iPhone, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingiPhone, new MediaRatioType("Generic product branding iPhone", MediaType.GenericProductBrandingiPhone, "GenericproductEntity", 460, 259, DeviceType.iPhone, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtoniPhone, new MediaRatioType("Category button iPhone", MediaType.CategoryButtoniPhone, "CategoryEntity", 40, 40, DeviceType.iPhone, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtoniPhone, new MediaRatioType("Generic category button iPhone", MediaType.GenericCategoryButtoniPhone, "GenericcategoryEntity", 40, 40, DeviceType.iPhone, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingiPhone, new MediaRatioType("Category branding iPhone", MediaType.CategoryBrandingiPhone, "CategoryEntity", 320, 167, DeviceType.iPhone, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingiPhone, new MediaRatioType("Generic category branding iPhone", MediaType.GenericCategoryBrandingiPhone, "GenericcategoryEntity", 320, 167, DeviceType.iPhone, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListiPhone, new MediaRatioType("Company list iPhone", MediaType.CompanyListiPhone, "CompanyEntity", 141, 296, DeviceType.iPhone, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.DeliverypointgroupListiPhone, new MediaRatioType("Deliverypointgroup list iPhone", MediaType.DeliverypointgroupListiPhone, "DeliverypointgroupEntity", 138, 78, DeviceType.iPhone, MediaTypeGroup.ByodDeliverypointgroupList));
                this.mediaRatioTypes.Add(MediaType.HomepageiPhone, new MediaRatioType("Homepage iPhone", MediaType.HomepageiPhone, "DeliverypointgroupEntity", 320, 167, DeviceType.iPhone, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListiPhone, new MediaRatioType("PointOfInterest list iPhone", MediaType.PointOfInterestListiPhone, "PointOfInterestEntity", 141, 296, DeviceType.iPhone, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepageiPhone, new MediaRatioType("PointOfInterest homepage iPhone", MediaType.PointOfInterestHomepageiPhone, "PointOfInterestEntity", 320, 167, DeviceType.iPhone, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconiPhone, new MediaRatioType("Map icon iPhone", MediaType.MapIconiPhone, "CompanyEntity", 56, 56, DeviceType.iPhone, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconiPhone, new MediaRatioType("PointOfInterest map icon iPhone", MediaType.PointOfInterestMapIconiPhone, "PointOfInterestEntity", 56, 56, DeviceType.iPhone, MediaTypeGroup.ByodPointOfInterestMapIcon));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderiPhone, new MediaRatioType("Product button placeholder iPhone", MediaType.ProductButtonPlaceholderiPhone, "CompanyEntity", 56, 56, DeviceType.iPhone, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderiPhone, new MediaRatioType("Product branding placeholder iPhone", MediaType.ProductBrandingPlaceholderiPhone, "CompanyEntity", 460, 259, DeviceType.iPhone, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderiPhone, new MediaRatioType("Category button placeholder iPhone", MediaType.CategoryButtonPlaceholderiPhone, "CompanyEntity", 40, 40, DeviceType.iPhone, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderiPhone, new MediaRatioType("Category branding placeholder iPhone", MediaType.CategoryBrandingPlaceholderiPhone, "CompanyEntity", 320, 167, DeviceType.iPhone, MediaTypeGroup.ByodCategoryBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.AlterationDialogPageiPhone, new MediaRatioType("Alteration dialog page iPhone", MediaType.AlterationDialogPageiPhone, "AlterationEntity", 128, 91, DeviceType.iPhone, MediaTypeGroup.ByodAlterationDialogPage));

                this.mediaRatioTypes.Add(MediaType.AttachmentThumbnailiPhone, new MediaRatioType("Attachment thumbnail iPhone", MediaType.AttachmentThumbnailiPhone, "AttachmentEntity", 56, 56, DeviceType.iPhone, MediaTypeGroup.ByodAttachmentThumbnail));

                // iPhone Retina
                this.mediaRatioTypes.Add(MediaType.ProductButtoniPhoneRetina, new MediaRatioType("Product button iPhone Retina", MediaType.ProductButtoniPhoneRetina, "ProductEntity", 112, 112, DeviceType.iPhoneRetina, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtoniPhoneRetina, new MediaRatioType("Generic product button iPhone Retina", MediaType.GenericProductButtoniPhoneRetina, "GenericproductEntity", 112, 112, DeviceType.iPhoneRetina, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingiPhoneRetina, new MediaRatioType("Product branding iPhone Retina", MediaType.ProductBrandingiPhoneRetina, "ProductEntity", 920, 518, DeviceType.iPhoneRetina, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingiPhoneRetina, new MediaRatioType("Generic product branding iPhone Retina", MediaType.GenericProductBrandingiPhoneRetina, "GenericproductEntity", 920, 518, DeviceType.iPhoneRetina, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtoniPhoneRetina, new MediaRatioType("Category button iPhone Retina", MediaType.CategoryButtoniPhoneRetina, "CategoryEntity", 80, 80, DeviceType.iPhoneRetina, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtoniPhoneRetina, new MediaRatioType("Generic category button iPhone Retina", MediaType.GenericCategoryButtoniPhoneRetina, "GenericcategoryEntity", 80, 80, DeviceType.iPhoneRetina, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingiPhoneRetina, new MediaRatioType("Category branding iPhone Retina", MediaType.CategoryBrandingiPhoneRetina, "CategoryEntity", 640, 334, DeviceType.iPhoneRetina, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingiPhoneRetina, new MediaRatioType("Generic category branding iPhone Retina", MediaType.GenericCategoryBrandingiPhoneRetina, "GenericcategoryEntity", 640, 334, DeviceType.iPhoneRetina, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListiPhoneRetina, new MediaRatioType("Company list iPhone Retina", MediaType.CompanyListiPhoneRetina, "CompanyEntity", 282, 592, DeviceType.iPhoneRetina, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.DeliverypointgroupListiPhoneRetina, new MediaRatioType("Deliverypointgroup list iPhone Retina", MediaType.DeliverypointgroupListiPhoneRetina, "DeliverypointgroupEntity", 275, 155, DeviceType.iPhoneRetina, MediaTypeGroup.ByodDeliverypointgroupList));
                this.mediaRatioTypes.Add(MediaType.HomepageiPhoneRetina, new MediaRatioType("Homepage iPhone Retina", MediaType.HomepageiPhoneRetina, "DeliverypointgroupEntity", 640, 334, DeviceType.iPhoneRetina, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListiPhoneRetina, new MediaRatioType("PointOfInterest list iPhone Retina", MediaType.PointOfInterestListiPhoneRetina, "PointOfInterestEntity", 282, 592, DeviceType.iPhoneRetina, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepageiPhoneRetina, new MediaRatioType("PointOfInterest homepage iPhone Retina", MediaType.PointOfInterestHomepageiPhoneRetina, "PointOfInterestEntity", 640, 334, DeviceType.iPhoneRetina, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconiPhoneRetina, new MediaRatioType("Map icon iPhone Retina", MediaType.MapIconiPhoneRetina, "CompanyEntity", 112, 112, DeviceType.iPhoneRetina, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconiPhoneRetina, new MediaRatioType("PointOfInterest map icon iPhone Retina", MediaType.PointOfInterestMapIconiPhoneRetina, "PointOfInterestEntity", 112, 112, DeviceType.iPhoneRetina, MediaTypeGroup.ByodPointOfInterestMapIcon));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderiPhoneRetina, new MediaRatioType("Product button placeholder iPhone Retina", MediaType.ProductButtonPlaceholderiPhoneRetina, "CompanyEntity", 112, 112, DeviceType.iPhoneRetina, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderiPhoneRetina, new MediaRatioType("Product branding placeholder iPhone Retina", MediaType.ProductBrandingPlaceholderiPhoneRetina, "CompanyEntity", 920, 518, DeviceType.iPhoneRetina, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderiPhoneRetina, new MediaRatioType("Category button placeholder iPhone Retina", MediaType.CategoryButtonPlaceholderiPhoneRetina, "CompanyEntity", 80, 80, DeviceType.iPhoneRetina, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderiPhoneRetina, new MediaRatioType("Category branding placeholder iPhone Retina", MediaType.CategoryBrandingPlaceholderiPhoneRetina, "CompanyEntity", 640, 334, DeviceType.iPhoneRetina, MediaTypeGroup.ByodCategoryBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.AlterationDialogPageiPhoneRetina, new MediaRatioType("Alteration dialog page iPhone Retina", MediaType.AlterationDialogPageiPhoneRetina, "AlterationEntity", 256, 182, DeviceType.iPhoneRetina, MediaTypeGroup.ByodAlterationDialogPage));

                this.mediaRatioTypes.Add(MediaType.AttachmentThumbnailiPhoneRetina, new MediaRatioType("Attachment thumbnail iPhone Retina", MediaType.AttachmentThumbnailiPhoneRetina, "AttachmentEntity", 112, 112, DeviceType.iPhoneRetina, MediaTypeGroup.ByodAttachmentThumbnail));

                // iPhone Retina HD
                this.mediaRatioTypes.Add(MediaType.ProductButtoniPhoneRetinaHD, new MediaRatioType("Product button iPhone Retina HD", MediaType.ProductButtoniPhoneRetinaHD, "ProductEntity", 112, 112, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtoniPhoneRetinaHD, new MediaRatioType("Generic product button iPhone Retina HD", MediaType.GenericProductButtoniPhoneRetinaHD, "GenericproductEntity", 112, 112, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingiPhoneRetinaHD, new MediaRatioType("Product branding iPhone Retina HD", MediaType.ProductBrandingiPhoneRetinaHD, "ProductEntity", 1094, 615, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingiPhoneRetinaHD, new MediaRatioType("Generic product branding iPhone Retina HD", MediaType.GenericProductBrandingiPhoneRetinaHD, "GenericproductEntity", 1094, 615, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtoniPhoneRetinaHD, new MediaRatioType("Category button iPhone Retina HD", MediaType.CategoryButtoniPhoneRetinaHD, "CategoryEntity", 80, 80, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtoniPhoneRetinaHD, new MediaRatioType("Generic category button iPhone Retina HD", MediaType.GenericCategoryButtoniPhoneRetinaHD, "GenericcategoryEntity", 80, 80, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingiPhoneRetinaHD, new MediaRatioType("Category branding iPhone Retina HD", MediaType.CategoryBrandingiPhoneRetinaHD, "CategoryEntity", 1134, 592, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingiPhoneRetinaHD, new MediaRatioType("Generic category branding iPhone Retina HD", MediaType.GenericCategoryBrandingiPhoneRetinaHD, "GenericcategoryEntity", 1134, 592, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListiPhoneRetinaHD, new MediaRatioType("Company list iPhone Retina HD", MediaType.CompanyListiPhoneRetinaHD, "CompanyEntity", 443, 930, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.DeliverypointgroupListiPhoneRetinaHD, new MediaRatioType("Deliverypointgroup list iPhone Retina HD", MediaType.DeliverypointgroupListiPhoneRetinaHD, "DeliverypointgroupEntity", 330, 186, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodDeliverypointgroupList));
                this.mediaRatioTypes.Add(MediaType.HomepageiPhoneRetinaHD, new MediaRatioType("Homepage iPhone Retina HD", MediaType.HomepageiPhoneRetinaHD, "DeliverypointgroupEntity", 1134, 592, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListiPhoneRetinaHD, new MediaRatioType("PointOfInterest list iPhone Retina HD", MediaType.PointOfInterestListiPhoneRetinaHD, "PointOfInterestEntity", 443, 930, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepageiPhoneRetinaHD, new MediaRatioType("PointOfInterest homepage iPhone Retina HD", MediaType.PointOfInterestHomepageiPhoneRetinaHD, "PointOfInterestEntity", 1134, 592, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconiPhoneRetinaHD, new MediaRatioType("Map icon iPhone Retina HD", MediaType.MapIconiPhoneRetinaHD, "CompanyEntity", 112, 112, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconiPhoneRetinaHD, new MediaRatioType("PointOfInterest map icon iPhone Retina HD", MediaType.PointOfInterestMapIconiPhoneRetinaHD, "PointOfInterestEntity", 112, 112, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodPointOfInterestMapIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconiPhoneRetinaHD, new MediaRatioType("Page icon iPhone Retina HD", MediaType.SitePageIconiPhoneRetinaHD, "PageEntity", 112, 112, DeviceType.iPhoneRetinaHD, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconiPhoneRetinaHD, new MediaRatioType("Page icon iPhone Retina HD", MediaType.TemplateSitePageIconiPhoneRetinaHD, "PageTemplateEntity", 112, 112, DeviceType.iPhoneRetinaHD, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SiteMobileHeaderImageiPhoneRetinaHD, new MediaRatioType("Mobile header image iPhone Retina HD", MediaType.SiteMobileHeaderImageiPhoneRetinaHD, "PageElementEntity", 1134, 592, DeviceType.iPhoneRetinaHD, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteMobileHeaderImageiPhoneRetinaHD, new MediaRatioType("Mobile header image iPhone Retina HD", MediaType.TemplateSiteMobileHeaderImageiPhoneRetinaHD, "PageTemplateElementEntity", 1134, 592, DeviceType.iPhoneRetinaHD, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPageiPhoneRetinaHD, new MediaRatioType("Point of Interest Summary Page image iPhone Retina HD", MediaType.SitePointOfInterestSummaryPageiPhoneRetinaHD, "PointOfInterestEntity", 1134, 592, DeviceType.iPhoneRetinaHD, MediaTypeGroup.SiteMobilePointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPageiPhoneRetinaHD, new MediaRatioType("Company Summary Page image iPhone Retina HD", MediaType.SiteCompanySummaryPageiPhoneRetinaHD, "CompanyEntity", 1134, 592, DeviceType.iPhoneRetinaHD, MediaTypeGroup.SiteMobileCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingiPhoneRetinaHD, new MediaRatioType("Site Branding iPhone Retina HD", MediaType.SiteBrandingiPhoneRetinaHD, "SiteEntity", 1134, 592, DeviceType.iPhoneRetinaHD, MediaTypeGroup.SiteBranding));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderiPhoneRetinaHD, new MediaRatioType("Product button placeholder iPhone Retina HD", MediaType.ProductButtonPlaceholderiPhoneRetinaHD, "CompanyEntity", 112, 112, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderiPhoneRetinaHD, new MediaRatioType("Product branding placeholder iPhone Retina HD", MediaType.ProductBrandingPlaceholderiPhoneRetinaHD, "CompanyEntity", 1094, 615, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderiPhoneRetinaHD, new MediaRatioType("Category button placeholder iPhone Retina HD", MediaType.CategoryButtonPlaceholderiPhoneRetinaHD, "CompanyEntity", 80, 80, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderiPhoneRetinaHD, new MediaRatioType("Category branding placeholder iPhone Retina HD", MediaType.CategoryBrandingPlaceholderiPhoneRetinaHD, "CompanyEntity", 1134, 592, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodCategoryBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.AlterationDialogPageiPhoneRetinaHD, new MediaRatioType("Alteration dialog page iPhone Retina HD", MediaType.AlterationDialogPageiPhoneRetinaHD, "AlterationEntity", 300, 214, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodAlterationDialogPage));

                this.mediaRatioTypes.Add(MediaType.AttachmentThumbnailiPhoneRetinaHD, new MediaRatioType("Attachment thumbnail iPhone Retina HD", MediaType.AttachmentThumbnailiPhoneRetinaHD, "AttachmentEntity", 112, 112, DeviceType.iPhoneRetinaHD, MediaTypeGroup.ByodAttachmentThumbnail));

                // iPhone Retina HD Plus
                this.mediaRatioTypes.Add(MediaType.ProductButtoniPhoneRetinaHDPlus, new MediaRatioType("Product button iPhone Retina HD Plus", MediaType.ProductButtoniPhoneRetinaHDPlus, "ProductEntity", 168, 168, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtoniPhoneRetinaHDPlus, new MediaRatioType("Generic product button iPhone Retina HD Plus", MediaType.GenericProductButtoniPhoneRetinaHDPlus, "GenericproductEntity", 168, 168, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingiPhoneRetinaHDPlus, new MediaRatioType("Product branding iPhone Retina HD Plus", MediaType.ProductBrandingiPhoneRetinaHDPlus, "ProductEntity", 1182, 665, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingiPhoneRetinaHDPlus, new MediaRatioType("Generic product branding iPhone Retina HD Plus", MediaType.GenericProductBrandingiPhoneRetinaHDPlus, "GenericproductEntity", 1182, 665, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtoniPhoneRetinaHDPlus, new MediaRatioType("Category button iPhone Retina HD Plus", MediaType.CategoryButtoniPhoneRetinaHDPlus, "CategoryEntity", 120, 120, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtoniPhoneRetinaHDPlus, new MediaRatioType("Generic category button iPhone Retina HD Plus", MediaType.GenericCategoryButtoniPhoneRetinaHDPlus, "GenericcategoryEntity", 120, 120, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingiPhoneRetinaHDPlus, new MediaRatioType("Category branding iPhone Retina HD Plus", MediaType.CategoryBrandingiPhoneRetinaHDPlus, "CategoryEntity", 1242, 648, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingiPhoneRetinaHDPlus, new MediaRatioType("Generic category branding iPhone Retina HD Plus", MediaType.GenericCategoryBrandingiPhoneRetinaHDPlus, "GenericcategoryEntity", 1242, 648, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListiPhoneRetinaHDPlus, new MediaRatioType("Company list iPhone Retina HD Plus", MediaType.CompanyListiPhoneRetinaHDPlus, "CompanyEntity", 443, 930, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.DeliverypointgroupListiPhoneRetinaHDPlus, new MediaRatioType("Deliverypointgroup list iPhone Retina HD Plus", MediaType.DeliverypointgroupListiPhoneRetinaHDPlus, "DeliverypointgroupEntity", 554, 312, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodDeliverypointgroupList));
                this.mediaRatioTypes.Add(MediaType.HomepageiPhoneRetinaHDPlus, new MediaRatioType("Homepage iPhone Retina HD Plus", MediaType.HomepageiPhoneRetinaHDPlus, "DeliverypointgroupEntity", 1242, 648, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListiPhoneRetinaHDPlus, new MediaRatioType("PointOfInterest list iPhone Retina HD Plus", MediaType.PointOfInterestListiPhoneRetinaHDPlus, "PointOfInterestEntity", 443, 930, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepageiPhoneRetinaHDPlus, new MediaRatioType("PointOfInterest homepage iPhone Retina HD Plus", MediaType.PointOfInterestHomepageiPhoneRetinaHDPlus, "PointOfInterestEntity", 1242, 648, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconiPhoneRetinaHDPlus, new MediaRatioType("Map icon iPhone Retina HD Plus", MediaType.MapIconiPhoneRetinaHDPlus, "CompanyEntity", 168, 168, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconiPhoneRetinaHDPlus, new MediaRatioType("PointOfInterest map icon iPhone Retina HD Plus", MediaType.PointOfInterestMapIconiPhoneRetinaHDPlus, "PointOfInterestEntity", 168, 168, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodPointOfInterestMapIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconiPhoneRetinaHDPlus, new MediaRatioType("Page icon iPhone Retina HD Plus", MediaType.SitePageIconiPhoneRetinaHDPlus, "PageEntity", 168, 168, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconiPhoneRetinaHDPlus, new MediaRatioType("Page icon iPhone Retina HD Plus", MediaType.TemplateSitePageIconiPhoneRetinaHDPlus, "PageTemplateEntity", 168, 168, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SiteMobileHeaderImageiPhoneRetinaHDPlus, new MediaRatioType("Mobile header image iPhone Retina HD Plus", MediaType.SiteMobileHeaderImageiPhoneRetinaHDPlus, "PageElementEntity", 1242, 648, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteMobileHeaderImageiPhoneRetinaHDPlus, new MediaRatioType("Mobile header image iPhone Retina HD Plus", MediaType.TemplateSiteMobileHeaderImageiPhoneRetinaHDPlus, "PageTemplateElementEntity", 1242, 648, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPageiPhoneRetinaHDPlus, new MediaRatioType("Point of Interest Summary Page image iPhone Retina HD Plus", MediaType.SitePointOfInterestSummaryPageiPhoneRetinaHDPlus, "PointOfInterestEntity", 1242, 648, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.SiteMobilePointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPageiPhoneRetinaHDPlus, new MediaRatioType("Company Summary Page image iPhone Retina HD Plus", MediaType.SiteCompanySummaryPageiPhoneRetinaHDPlus, "CompanyEntity", 1242, 648, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.SiteMobileCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingiPhoneRetinaHDPlus, new MediaRatioType("Site Branding iPhone Retina HD Plus", MediaType.SiteBrandingiPhoneRetinaHDPlus, "SiteEntity", 1242, 648, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.SiteBranding));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderiPhoneRetinaHDPlus, new MediaRatioType("Product button placeholder iPhone Retina HD Plus", MediaType.ProductButtonPlaceholderiPhoneRetinaHDPlus, "CompanyEntity", 168, 168, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderiPhoneRetinaHDPlus, new MediaRatioType("Product branding placeholder iPhone Retina HD Plus", MediaType.ProductBrandingPlaceholderiPhoneRetinaHDPlus, "CompanyEntity", 1182, 665, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderiPhoneRetinaHDPlus, new MediaRatioType("Category button placeholder iPhone Retina HD Plus", MediaType.CategoryButtonPlaceholderiPhoneRetinaHDPlus, "CompanyEntity", 120, 120, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderiPhoneRetinaHDPlus, new MediaRatioType("Category branding placeholder iPhone Retina HD Plus", MediaType.CategoryBrandingPlaceholderiPhoneRetinaHDPlus, "CompanyEntity", 1242, 648, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodCategoryBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.AlterationDialogPageiPhoneRetinaHDPlus, new MediaRatioType("Alteration dialog page iPhone Retina HD Plus", MediaType.AlterationDialogPageiPhoneRetinaHDPlus, "AlterationEntity", 498, 357, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodAlterationDialogPage));

                this.mediaRatioTypes.Add(MediaType.AttachmentThumbnailiPhoneRetinaHDPlus, new MediaRatioType("Attachment thumbnail iPhone Retina HD Plus", MediaType.AttachmentThumbnailiPhoneRetinaHDPlus, "AttachmentEntity", 168, 168, DeviceType.iPhoneRetinaHDPlus, MediaTypeGroup.ByodAttachmentThumbnail));

                // iPad
                this.mediaRatioTypes.Add(MediaType.ProductButtoniPad, new MediaRatioType("Product button iPad", MediaType.ProductButtoniPad, "ProductEntity", 56, 56, DeviceType.iPad, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtoniPad, new MediaRatioType("Generic product button iPad", MediaType.GenericProductButtoniPad, "GenericproductEntity", 56, 56, DeviceType.iPad, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingiPad, new MediaRatioType("Product branding iPad", MediaType.ProductBrandingiPad, "ProductEntity", 674, 379, DeviceType.iPad, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingiPad, new MediaRatioType("Generic product branding iPad", MediaType.GenericProductBrandingiPad, "GenericproductEntity", 674, 379, DeviceType.iPad, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtoniPad, new MediaRatioType("Category button iPad", MediaType.CategoryButtoniPad, "CategoryEntity", 40, 40, DeviceType.iPad, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtoniPad, new MediaRatioType("Generic category button iPad", MediaType.GenericCategoryButtoniPad, "GenericcategoryEntity", 40, 40, DeviceType.iPad, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingiPad, new MediaRatioType("Category branding iPad", MediaType.CategoryBrandingiPad, "CategoryEntity", 674, 379, DeviceType.iPad, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingiPad, new MediaRatioType("Generic category branding iPad", MediaType.GenericCategoryBrandingiPad, "GenericcategoryEntity", 674, 379, DeviceType.iPad, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListiPad, new MediaRatioType("Company list iPad", MediaType.CompanyListiPad, "CompanyEntity", 353, 740, DeviceType.iPad, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.DeliverypointgroupListiPad, new MediaRatioType("Deliverypointgroup list iPad", MediaType.DeliverypointgroupListiPad, "DeliverypointgroupEntity", 307, 173, DeviceType.iPad, MediaTypeGroup.ByodDeliverypointgroupList));
                this.mediaRatioTypes.Add(MediaType.HomepageiPad, new MediaRatioType("Homepage iPad", MediaType.HomepageiPad, "DeliverypointgroupEntity", 1004, 565, DeviceType.iPad, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListiPad, new MediaRatioType("PointOfInterest list iPad", MediaType.PointOfInterestListiPad, "PointOfInterestEntity", 353, 740, DeviceType.iPad, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepageiPad, new MediaRatioType("PointOfInterest homepage iPad", MediaType.PointOfInterestHomepageiPad, "PointOfInterestEntity", 1004, 565, DeviceType.iPad, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconiPad, new MediaRatioType("Map icon iPad", MediaType.MapIconiPad, "CompanyEntity", 56, 56, DeviceType.iPad, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconiPad, new MediaRatioType("PointOfInterest map icon iPad", MediaType.PointOfInterestMapIconiPad, "PointOfInterestEntity", 56, 56, DeviceType.iPad, MediaTypeGroup.ByodPointOfInterestMapIcon));
                this.mediaRatioTypes.Add(MediaType.Homepage2iPad, new MediaRatioType("Homepage2 iPad", MediaType.Homepage2iPad, "DeliverypointgroupEntity", 674, 556, DeviceType.iPad, MediaTypeGroup.ByodHomepage2));
                this.mediaRatioTypes.Add(MediaType.HomepageAdvertiPad, new MediaRatioType("Homepage advert iPad", MediaType.HomepageAdvertiPad, "DeliverypointgroupEntity", 192, 108, DeviceType.iPad, MediaTypeGroup.ByodHomepageAdvert));
                this.mediaRatioTypes.Add(MediaType.Homepage3iPad, new MediaRatioType("Homepage3 iPad", MediaType.Homepage3iPad, "DeliverypointgroupEntity", 674, 684, DeviceType.iPad, MediaTypeGroup.ByodHomepage3));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderiPad, new MediaRatioType("Product button placeholder iPad", MediaType.ProductButtonPlaceholderiPad, "CompanyEntity", 56, 56, DeviceType.iPad, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderiPad, new MediaRatioType("Product branding placeholder iPad", MediaType.ProductBrandingPlaceholderiPad, "CompanyEntity", 674, 379, DeviceType.iPad, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderiPad, new MediaRatioType("Category button placeholder iPad", MediaType.CategoryButtonPlaceholderiPad, "CompanyEntity", 40, 40, DeviceType.iPad, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderiPad, new MediaRatioType("Category branding placeholder iPad", MediaType.CategoryBrandingPlaceholderiPad, "CompanyEntity", 674, 379, DeviceType.iPad, MediaTypeGroup.ByodCategoryBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.AlterationDialogPageiPad, new MediaRatioType("Alteration dialog page iPad", MediaType.AlterationDialogPageiPad, "AlterationEntity", 150, 107, DeviceType.iPad, MediaTypeGroup.ByodAlterationDialogPage));

                this.mediaRatioTypes.Add(MediaType.AttachmentThumbnailiPad, new MediaRatioType("Attachment thumbnail iPad", MediaType.AttachmentThumbnailiPad, "AttachmentEntity", 56, 56, DeviceType.iPad, MediaTypeGroup.ByodAttachmentThumbnail));

                // iPad Retina
                this.mediaRatioTypes.Add(MediaType.ProductButtoniPadRetina, new MediaRatioType("Product button iPad Retina", MediaType.ProductButtoniPadRetina, "ProductEntity", 112, 112, DeviceType.iPadRetina, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.GenericProductButtoniPadRetina, new MediaRatioType("Generic product button iPad Retina", MediaType.GenericProductButtoniPadRetina, "GenericproductEntity", 112, 112, DeviceType.iPadRetina, MediaTypeGroup.ByodProductButton));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingiPadRetina, new MediaRatioType("Product branding iPad Retina", MediaType.ProductBrandingiPadRetina, "ProductEntity", 1348, 758, DeviceType.iPadRetina, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.GenericProductBrandingiPadRetina, new MediaRatioType("Generic product branding iPad Retina", MediaType.GenericProductBrandingiPadRetina, "GenericproductEntity", 1348, 758, DeviceType.iPadRetina, MediaTypeGroup.ByodProductBranding));
                this.mediaRatioTypes.Add(MediaType.CategoryButtoniPadRetina, new MediaRatioType("Category button iPad Retina", MediaType.CategoryButtoniPadRetina, "CategoryEntity", 80, 80, DeviceType.iPadRetina, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryButtoniPadRetina, new MediaRatioType("Generic category button iPad Retina", MediaType.GenericCategoryButtoniPadRetina, "GenericcategoryEntity", 80, 80, DeviceType.iPadRetina, MediaTypeGroup.ByodCategoryButton));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingiPadRetina, new MediaRatioType("Category branding iPad Retina", MediaType.CategoryBrandingiPadRetina, "CategoryEntity", 1348, 758, DeviceType.iPadRetina, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.GenericCategoryBrandingiPadRetina, new MediaRatioType("Generic category branding iPad Retina", MediaType.GenericCategoryBrandingiPadRetina, "GenericcategoryEntity", 1348, 758, DeviceType.iPadRetina, MediaTypeGroup.ByodCategoryBranding));
                this.mediaRatioTypes.Add(MediaType.CompanyListiPadRetina, new MediaRatioType("Company list iPad Retina", MediaType.CompanyListiPadRetina, "CompanyEntity", 706, 1480, DeviceType.iPadRetina, MediaTypeGroup.ByodCompanyList));
                this.mediaRatioTypes.Add(MediaType.DeliverypointgroupListiPadRetina, new MediaRatioType("Deliverypointgroup list iPad Retina", MediaType.DeliverypointgroupListiPadRetina, "DeliverypointgroupEntity", 614, 345, DeviceType.iPadRetina, MediaTypeGroup.ByodDeliverypointgroupList));
                this.mediaRatioTypes.Add(MediaType.HomepageiPadRetina, new MediaRatioType("Homepage iPad Retina", MediaType.HomepageiPadRetina, "DeliverypointgroupEntity", 2008, 1130, DeviceType.iPadRetina, MediaTypeGroup.ByodHomepage));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestListiPadRetina, new MediaRatioType("PointOfInterest list iPad Retina", MediaType.PointOfInterestListiPadRetina, "PointOfInterestEntity", 706, 1480, DeviceType.iPadRetina, MediaTypeGroup.ByodPointOfInterestList));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestHomepageiPadRetina, new MediaRatioType("PointOfInterest homepage iPad Retina", MediaType.PointOfInterestHomepageiPadRetina, "PointOfInterestEntity", 2008, 1130, DeviceType.iPadRetina, MediaTypeGroup.ByodPointOfInterestHomepage));
                this.mediaRatioTypes.Add(MediaType.MapIconiPadRetina, new MediaRatioType("Map icon iPad Retina", MediaType.MapIconiPadRetina, "CompanyEntity", 112, 112, DeviceType.iPadRetina, MediaTypeGroup.ByodMapIcon));
                this.mediaRatioTypes.Add(MediaType.PointOfInterestMapIconiPadRetina, new MediaRatioType("PointOfInterest map icon iPad Retina", MediaType.PointOfInterestMapIconiPadRetina, "PointOfInterestEntity", 112, 112, DeviceType.iPadRetina, MediaTypeGroup.ByodPointOfInterestMapIcon));
                this.mediaRatioTypes.Add(MediaType.Homepage2iPadRetina, new MediaRatioType("Homepage2 iPad Retina", MediaType.Homepage2iPadRetina, "DeliverypointgroupEntity", 1348, 1112, DeviceType.iPadRetina, MediaTypeGroup.ByodHomepage2));
                this.mediaRatioTypes.Add(MediaType.HomepageAdvertiPadRetina, new MediaRatioType("Homepage advert iPad Retina", MediaType.HomepageAdvertiPadRetina, "DeliverypointgroupEntity", 384, 216, DeviceType.iPadRetina, MediaTypeGroup.ByodHomepageAdvert));
                this.mediaRatioTypes.Add(MediaType.Homepage3iPadRetina, new MediaRatioType("Homepage3 iPad Retina", MediaType.Homepage3iPadRetina, "DeliverypointgroupEntity", 1348, 1368, DeviceType.iPadRetina, MediaTypeGroup.ByodHomepage3));

                this.mediaRatioTypes.Add(MediaType.ProductButtonPlaceholderiPadRetina, new MediaRatioType("Product button placeholder iPad Retina", MediaType.ProductButtonPlaceholderiPadRetina, "CompanyEntity", 112, 112, DeviceType.iPadRetina, MediaTypeGroup.ByodProductButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.ProductBrandingPlaceholderiPadRetina, new MediaRatioType("Product branding placeholder iPad Retina", MediaType.ProductBrandingPlaceholderiPadRetina, "CompanyEntity", 1348, 758, DeviceType.iPadRetina, MediaTypeGroup.ByodProductBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryButtonPlaceholderiPadRetina, new MediaRatioType("Category button placeholder iPad Retina", MediaType.CategoryButtonPlaceholderiPadRetina, "CompanyEntity", 80, 80, DeviceType.iPadRetina, MediaTypeGroup.ByodCategoryButtonPlaceholder));
                this.mediaRatioTypes.Add(MediaType.CategoryBrandingPlaceholderiPadRetina, new MediaRatioType("Category branding placeholder iPad Retina", MediaType.CategoryBrandingPlaceholderiPadRetina, "CompanyEntity", 1348, 758, DeviceType.iPadRetina, MediaTypeGroup.ByodCategoryBrandingPlaceholder));
                this.mediaRatioTypes.Add(MediaType.AlterationDialogPageiPadRetina, new MediaRatioType("Alteration dialog page iPad Retina", MediaType.AlterationDialogPageiPadRetina, "AlterationEntity", 300, 214, DeviceType.iPadRetina, MediaTypeGroup.ByodAlterationDialogPage));

                this.mediaRatioTypes.Add(MediaType.AttachmentThumbnailiPadRetina, new MediaRatioType("Attachment thumbnail iPad Retina", MediaType.AttachmentThumbnailiPadRetina, "AttachmentEntity", 112, 112, DeviceType.iPadRetina, MediaTypeGroup.ByodAttachmentThumbnail));

                // Android Notification Icon (fixed size)
                this.mediaRatioTypes.Add(MediaType.NotificationIconNormal, new MediaRatioType("Notification Icon (Android)", MediaType.NotificationIconNormal, "CompanyEntity", 192, 192));

                // Non device related
                this.mediaRatioTypes.Add(MediaType.RouteStepHandlerEmailResource, new MediaRatioType("Route Step Handler Email Resource", MediaType.RouteStepHandlerEmailResource, "RoutestephandlerEntity", 200, 200));

                this.mediaRatioTypes.Add(MediaType.AttachmentImage, new MediaRatioType("Product Attachment Picture", MediaType.AttachmentImage, "AttachmentEntity", 500, 500, DeviceType.Unknown, MediaTypeGroup.None, true, 70));
                this.mediaRatioTypes.Add(MediaType.AttachmentPdf, new MediaRatioType("Product Attachment Pdf", MediaType.AttachmentPdf, "AttachmentEntity", 500, 500, DeviceType.Unknown, MediaTypeGroup.None, true, 70));
                this.mediaRatioTypes.Add(MediaType.AttachmentThumbnail, new MediaRatioType("Product Attachment Thumbnail", MediaType.AttachmentThumbnail, "AttachmentEntity", 60, 60, DeviceType.Unknown, MediaTypeGroup.None, true, 70));

                // App Download Page Branding
                this.mediaRatioTypes.Add(MediaType.DownloadPageBranding, new MediaRatioType("Download Page Branding", MediaType.DownloadPageBranding, "CompanyEntity", 312, 128, DeviceType.Unknown, MediaTypeGroup.None, true, 100));

                // AppLess small
                this.mediaRatioTypes.Add(MediaType.AppLessHeaderLogoSmall, new MediaRatioType("AppLess header logo (small)", MediaType.AppLessHeaderLogoSmall, "LandingPageEntity", 200, 50, DeviceType.Unknown, MediaTypeGroup.AppLessHeaderLogo));
                this.mediaRatioTypes.Add(MediaType.AppLessHeroSmall, new MediaRatioType("AppLess hero (small)", MediaType.AppLessHeroSmall, "WidgetHeroEntity", 414, 176, DeviceType.Unknown, MediaTypeGroup.AppLessHero));
                this.mediaRatioTypes.Add(MediaType.AppLessCarouselSmall, new MediaRatioType("AppLess carousel (small)", MediaType.AppLessCarouselSmall, "CarouselItemEntity", 280, 280, DeviceType.Unknown, MediaTypeGroup.AppLessCarousel));
                this.mediaRatioTypes.Add(MediaType.AppLessActionBannerSmall, new MediaRatioType("AppLess action banner (small)", MediaType.AppLessActionBannerSmall, "WidgetActionBannerEntity", 374, 120, DeviceType.Unknown, MediaTypeGroup.AppLessActionBanner));
                this.mediaRatioTypes.Add(MediaType.AppLessApplicationIconSmall, new MediaRatioType("AppLess application icon (small)", MediaType.AppLessApplicationIconSmall, "ApplicationConfigurationEntity", 144, 144, DeviceType.Unknown, MediaTypeGroup.AppLessApplicationIcon));
                this.mediaRatioTypes.Add(MediaType.AppLessProductBannerSmall, new MediaRatioType("AppLess product banner (small)", MediaType.AppLessProductBannerSmall, "ProductEntity", 450, 150, DeviceType.Unknown, MediaTypeGroup.AppLessProductBanner));
                this.mediaRatioTypes.Add(MediaType.AppLessCategoryBannerSmall, new MediaRatioType("AppLess category banner (small)", MediaType.AppLessCategoryBannerSmall, "CategoryEntity", 450, 150, DeviceType.Unknown, MediaTypeGroup.AppLessCategoryBanner));

                // AppLess medium
                this.mediaRatioTypes.Add(MediaType.AppLessHeaderLogoMedium, new MediaRatioType("AppLess header logo (medium)", MediaType.AppLessHeaderLogoMedium, "LandingPageEntity", 400, 100, DeviceType.Unknown, MediaTypeGroup.AppLessHeaderLogo));
                this.mediaRatioTypes.Add(MediaType.AppLessHeroMedium, new MediaRatioType("AppLess hero (medium)", MediaType.AppLessHeroMedium, "WidgetHeroEntity", 828, 352, DeviceType.Unknown, MediaTypeGroup.AppLessHero));
                this.mediaRatioTypes.Add(MediaType.AppLessCarouselMedium, new MediaRatioType("AppLess carousel (medium)", MediaType.AppLessCarouselMedium, "CarouselItemEntity", 560, 560, DeviceType.Unknown, MediaTypeGroup.AppLessCarousel));
                this.mediaRatioTypes.Add(MediaType.AppLessActionBannerMedium, new MediaRatioType("AppLess action banner (medium)", MediaType.AppLessActionBannerMedium, "WidgetActionBannerEntity", 748, 240, DeviceType.Unknown, MediaTypeGroup.AppLessActionBanner));
                this.mediaRatioTypes.Add(MediaType.AppLessApplicationIconMedium, new MediaRatioType("AppLess application icon (medium)", MediaType.AppLessApplicationIconMedium, "ApplicationConfigurationEntity", 192, 192, DeviceType.Unknown, MediaTypeGroup.AppLessApplicationIcon));
                this.mediaRatioTypes.Add(MediaType.AppLessProductBannerMedium, new MediaRatioType("AppLess product banner (medium)", MediaType.AppLessProductBannerMedium, "ProductEntity", 750, 250, DeviceType.Unknown, MediaTypeGroup.AppLessProductBanner));
                this.mediaRatioTypes.Add(MediaType.AppLessCategoryBannerMedium, new MediaRatioType("AppLess category banner (medium)", MediaType.AppLessCategoryBannerMedium, "CategoryEntity", 750, 250, DeviceType.Unknown, MediaTypeGroup.AppLessCategoryBanner));

                // AppLess large
                this.mediaRatioTypes.Add(MediaType.AppLessHeaderLogoLarge, new MediaRatioType("AppLess header logo (large)", MediaType.AppLessHeaderLogoLarge, "LandingPageEntity", 600, 150, DeviceType.Unknown, MediaTypeGroup.AppLessHeaderLogo));
                this.mediaRatioTypes.Add(MediaType.AppLessHeroLarge, new MediaRatioType("AppLess hero (large)", MediaType.AppLessHeroLarge, "WidgetHeroEntity", 1242, 528, DeviceType.Unknown, MediaTypeGroup.AppLessHero));
                this.mediaRatioTypes.Add(MediaType.AppLessCarouselLarge, new MediaRatioType("AppLess carousel (large)", MediaType.AppLessCarouselLarge, "CarouselItemEntity", 840, 840, DeviceType.Unknown, MediaTypeGroup.AppLessCarousel));
                this.mediaRatioTypes.Add(MediaType.AppLessActionBannerLarge, new MediaRatioType("AppLess action banner (large)", MediaType.AppLessActionBannerLarge, "WidgetActionBannerEntity", 1122, 360, DeviceType.Unknown, MediaTypeGroup.AppLessActionBanner));
                this.mediaRatioTypes.Add(MediaType.AppLessApplicationIconLarge, new MediaRatioType("AppLess application icon (large)", MediaType.AppLessApplicationIconLarge, "ApplicationConfigurationEntity", 512, 512, DeviceType.Unknown, MediaTypeGroup.AppLessApplicationIcon));
                this.mediaRatioTypes.Add(MediaType.AppLessProductBannerLarge, new MediaRatioType("AppLess product banner (large)", MediaType.AppLessProductBannerLarge, "ProductEntity", 1050, 350, DeviceType.Unknown, MediaTypeGroup.AppLessProductBanner));
                this.mediaRatioTypes.Add(MediaType.AppLessCategoryBannerLarge, new MediaRatioType("AppLess category banner (large)", MediaType.AppLessCategoryBannerLarge, "CategoryEntity", 1050, 350, DeviceType.Unknown, MediaTypeGroup.AppLessCategoryBanner));

                // Page Icons
                this.mediaRatioTypes.Add(MediaType.SitePageIcon1280x720, new MediaRatioType("Page icon (1280x720)", MediaType.SitePageIcon1280x720, "PageEntity", 75, 75, DeviceType.AndroidTvBox, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIcon1280x800, new MediaRatioType("Page icon (1280x800)", MediaType.SitePageIcon1280x800, "PageEntity", 75, 75, DeviceType.Tablet1280x800, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconPhoneSmall, new MediaRatioType("Page icon small (phone)", MediaType.SitePageIconPhoneSmall, "PageEntity", 56, 56, DeviceType.PhoneSmall, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconPhoneNormal, new MediaRatioType("Page icon normal (phone)", MediaType.SitePageIconPhoneNormal, "PageEntity", 75, 75, DeviceType.PhoneNormal, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconPhoneLarge, new MediaRatioType("Page icon large (phone)", MediaType.SitePageIconPhoneLarge, "PageEntity", 112, 112, DeviceType.PhoneLarge, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconPhoneXLarge, new MediaRatioType("Page icon xlarge (phone)", MediaType.SitePageIconPhoneXLarge, "PageEntity", 150, 150, DeviceType.PhoneXLarge, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconTabletSmall, new MediaRatioType("Page icon small (tablet)", MediaType.SitePageIconTabletSmall, "PageEntity", 56, 56, DeviceType.TabletSmall, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconTabletNormal, new MediaRatioType("Page icon normal (tablet)", MediaType.SitePageIconTabletNormal, "PageEntity", 75, 75, DeviceType.TabletNormal, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconTabletLarge, new MediaRatioType("Page icon large (tablet)", MediaType.SitePageIconTabletLarge, "PageEntity", 112, 112, DeviceType.TabletLarge, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconTabletXLarge, new MediaRatioType("Page icon xlarge (tablet)", MediaType.SitePageIconTabletXLarge, "PageEntity", 150, 150, DeviceType.TabletXLarge, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconiPhone, new MediaRatioType("Page icon iPhone", MediaType.SitePageIconiPhone, "PageEntity", 56, 56, DeviceType.iPhone, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconiPhoneRetina, new MediaRatioType("Page icon iPhone Retina", MediaType.SitePageIconiPhoneRetina, "PageEntity", 112, 112, DeviceType.iPhoneRetina, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconiPad, new MediaRatioType("Page icon iPad", MediaType.SitePageIconiPad, "PageEntity", 56, 56, DeviceType.iPad, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.SitePageIconiPadRetina, new MediaRatioType("Page icon iPad Retina", MediaType.SitePageIconiPadRetina, "PageEntity", 112, 112, DeviceType.iPadRetina, MediaTypeGroup.SitePageIcon));

                // Page Template Icons
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIcon1280x720, new MediaRatioType("Page icon (1280x720)", MediaType.TemplateSitePageIcon1280x720, "PageTemplateEntity", 75, 75, DeviceType.AndroidTvBox, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIcon1280x800, new MediaRatioType("Page icon (1280x800)", MediaType.TemplateSitePageIcon1280x800, "PageTemplateEntity", 75, 75, DeviceType.Tablet1280x800, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconPhoneSmall, new MediaRatioType("Page icon small (phone)", MediaType.TemplateSitePageIconPhoneSmall, "PageTemplateEntity", 56, 56, DeviceType.PhoneSmall, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconPhoneNormal, new MediaRatioType("Page icon normal (phone)", MediaType.TemplateSitePageIconPhoneNormal, "PageTemplateEntity", 75, 75, DeviceType.PhoneNormal, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconPhoneLarge, new MediaRatioType("Page icon large (phone)", MediaType.TemplateSitePageIconPhoneLarge, "PageTemplateEntity", 112, 112, DeviceType.PhoneLarge, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconPhoneXLarge, new MediaRatioType("Page icon xlarge (phone)", MediaType.TemplateSitePageIconPhoneXLarge, "PageTemplateEntity", 150, 150, DeviceType.PhoneXLarge, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconTabletSmall, new MediaRatioType("Page icon small (tablet)", MediaType.TemplateSitePageIconTabletSmall, "PageTemplateEntity", 56, 56, DeviceType.TabletSmall, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconTabletNormal, new MediaRatioType("Page icon normal (tablet)", MediaType.TemplateSitePageIconTabletNormal, "PageTemplateEntity", 75, 75, DeviceType.TabletNormal, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconTabletLarge, new MediaRatioType("Page icon large (tablet)", MediaType.TemplateSitePageIconTabletLarge, "PageTemplateEntity", 112, 112, DeviceType.TabletLarge, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconTabletXLarge, new MediaRatioType("Page icon xlarge (tablet)", MediaType.TemplateSitePageIconTabletXLarge, "PageTemplateEntity", 150, 150, DeviceType.TabletXLarge, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconiPhone, new MediaRatioType("Page icon iPhone", MediaType.TemplateSitePageIconiPhone, "PageTemplateEntity", 56, 56, DeviceType.iPhone, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconiPhoneRetina, new MediaRatioType("Page icon iPhone Retina", MediaType.TemplateSitePageIconiPhoneRetina, "PageTemplateEntity", 112, 112, DeviceType.iPhoneRetina, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconiPad, new MediaRatioType("Page icon iPad", MediaType.TemplateSitePageIconiPad, "PageTemplateEntity", 56, 56, DeviceType.iPad, MediaTypeGroup.SitePageIcon));
                this.mediaRatioTypes.Add(MediaType.TemplateSitePageIconiPadRetina, new MediaRatioType("Page icon iPad Retina", MediaType.TemplateSitePageIconiPadRetina, "PageTemplateEntity", 112, 112, DeviceType.iPadRetina, MediaTypeGroup.SitePageIcon));

                // Full Size Image (Site Type - Microsite)
                this.mediaRatioTypes.Add(MediaType.SiteFullPageImage1280x720, new MediaRatioType("Full page image (1280x720)", MediaType.SiteFullPageImage1280x720, "PageElementEntity", 958, 564, DeviceType.AndroidTvBox, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteFullPageImage1280x720, new MediaRatioType("Full page image (1280x720)", MediaType.TemplateSiteFullPageImage1280x720, "PageTemplateElementEntity", 958, 564, DeviceType.AndroidTvBox, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteFullPageImage1280x800, new MediaRatioType("Full page image (1280x800)", MediaType.SiteFullPageImage1280x800, "PageElementEntity", 958, 644, DeviceType.Tablet1280x800, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteFullPageImage1280x800, new MediaRatioType("Full page image (1280x800)", MediaType.TemplateSiteFullPageImage1280x800, "PageTemplateElementEntity", 958, 644, DeviceType.Tablet1280x800, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteFullPageImageTabletSmall, new MediaRatioType("Full page image small (tablet)", MediaType.SiteFullPageImageTabletSmall, "PageElementEntity", 56, 56, DeviceType.TabletSmall, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteFullPageImageTabletNormal, new MediaRatioType("Full page image normal (tablet)", MediaType.SiteFullPageImageTabletNormal, "PageElementEntity", 75, 75, DeviceType.TabletNormal, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteFullPageImageTabletLarge, new MediaRatioType("Full page image large (tablet)", MediaType.SiteFullPageImageTabletLarge, "PageElementEntity", 112, 112, DeviceType.TabletLarge, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteFullPageImageTabletXLarge, new MediaRatioType("Full page image xlarge (tablet)", MediaType.SiteFullPageImageTabletXLarge, "PageElementEntity", 150, 150, DeviceType.TabletXLarge, MediaTypeGroup.SiteFullPageImage));

                // GK These have other dimensions because Bill is only implementing a WideScreenImage
                this.mediaRatioTypes.Add(MediaType.SiteFullPageImageiPad, new MediaRatioType("Full page image iPad", MediaType.SiteFullPageImageiPad, "PageElementEntity", 674, 379, DeviceType.iPad, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteFullPageImageiPad, new MediaRatioType("Full page image iPad", MediaType.TemplateSiteFullPageImageiPad, "PageTemplateElementEntity", 674, 379, DeviceType.iPad, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteFullPageImageiPadRetina, new MediaRatioType("Full page image iPad Retina", MediaType.SiteFullPageImageiPadRetina, "PageElementEntity", 1348, 758, DeviceType.iPadRetina, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteFullPageImageiPadRetina, new MediaRatioType("Full page image iPad Retina", MediaType.TemplateSiteFullPageImageiPadRetina, "PageTemplateElementEntity", 1348, 758, DeviceType.iPadRetina, MediaTypeGroup.SiteFullPageImage));

                // Full Size Image (Site Type - Directory)
                this.mediaRatioTypes.Add(MediaType.SiteFullPageImageDirectory1280x720, new MediaRatioType("Full page image - directory (1280x720)", MediaType.SiteFullPageImageDirectory1280x720, "PageElementEntity", 958, 564, DeviceType.AndroidTvBox, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteFullPageImageDirectory1280x720, new MediaRatioType("Full page image - directory (1280x720)", MediaType.TemplateSiteFullPageImageDirectory1280x720, "PageTemplateElementEntity", 958, 564, DeviceType.AndroidTvBox, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteFullPageImageDirectory1280x800, new MediaRatioType("Full page image - directory (1280x800)", MediaType.SiteFullPageImageDirectory1280x800, "PageElementEntity", 958, 644, DeviceType.Tablet1280x800, MediaTypeGroup.SiteFullPageImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteFullPageImageDirectory1280x800, new MediaRatioType("Full page image - directory (1280x800)", MediaType.TemplateSiteFullPageImageDirectory1280x800, "PageTemplateElementEntity", 958, 644, DeviceType.Tablet1280x800, MediaTypeGroup.SiteFullPageImage));

                // Supporting - Two thirds image (Site Type - Microsite)
                this.mediaRatioTypes.Add(MediaType.SiteTwoThirdsPageImage1280x720, new MediaRatioType("Two thirds image (1280x720)", MediaType.SiteTwoThirdsPageImage1280x720, "PageElementEntity", 629, 564, DeviceType.AndroidTvBox, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteTwoThirdsPageImage1280x720, new MediaRatioType("Two thirds image (1280x720)", MediaType.TemplateSiteTwoThirdsPageImage1280x720, "PageTemplateElementEntity", 629, 564, DeviceType.AndroidTvBox, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteTwoThirdsPageImage1280x800, new MediaRatioType("Two thirds image (1280x800)", MediaType.SiteTwoThirdsPageImage1280x800, "PageElementEntity", 629, 644, DeviceType.Tablet1280x800, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteTwoThirdsPageImage1280x800, new MediaRatioType("Two thirds image (1280x800)", MediaType.TemplateSiteTwoThirdsPageImage1280x800, "PageTemplateElementEntity", 629, 644, DeviceType.Tablet1280x800, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteTwoThirdsPageImageTabletSmall, new MediaRatioType("Two thirds image small (tablet)", MediaType.SiteTwoThirdsPageImageTabletSmall, "PageElementEntity", 780, 439, DeviceType.TabletSmall, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteTwoThirdsPageImageTabletNormal, new MediaRatioType("Two thirds image normal (tablet)", MediaType.SiteTwoThirdsPageImageTabletNormal, "PageElementEntity", 1204, 677, DeviceType.TabletNormal, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteTwoThirdsPageImageTabletLarge, new MediaRatioType("Two thirds image large (tablet)", MediaType.SiteTwoThirdsPageImageTabletLarge, "PageElementEntity", 1605, 903, DeviceType.TabletLarge, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteTwoThirdsPageImageTabletXLarge, new MediaRatioType("Two thirds image xlarge (tablet)", MediaType.SiteTwoThirdsPageImageTabletXLarge, "PageElementEntity", 2006, 1128, DeviceType.TabletXLarge, MediaTypeGroup.SiteTwoThirdsPageImage));

                // GK These have other dimensions because Bill is only implementing a WideScreenImage
                this.mediaRatioTypes.Add(MediaType.SiteTwoThirdsPageImageiPad, new MediaRatioType("Two thirds image iPad", MediaType.SiteTwoThirdsPageImageiPad, "PageElementEntity", 674, 379, DeviceType.iPad, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteTwoThirdsPageImageiPad, new MediaRatioType("Two thirds image iPad", MediaType.TemplateSiteTwoThirdsPageImageiPad, "PageTemplateElementEntity", 674, 379, DeviceType.iPad, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteTwoThirdsPageImageiPadRetina, new MediaRatioType("Two thirds image iPad Retina", MediaType.SiteTwoThirdsPageImageiPadRetina, "PageElementEntity", 1348, 758, DeviceType.iPadRetina, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteTwoThirdsPageImageiPadRetina, new MediaRatioType("Two thirds image iPad Retina", MediaType.TemplateSiteTwoThirdsPageImageiPadRetina, "PageTemplateElementEntity", 1348, 758, DeviceType.iPadRetina, MediaTypeGroup.SiteTwoThirdsPageImage));

                // Supporting - Two thirds image (Site Type - Directory)
                this.mediaRatioTypes.Add(MediaType.SiteTwoThirdsPageImageDirectory1280x720, new MediaRatioType("Two thirds image - directory (1280x720)", MediaType.SiteTwoThirdsPageImageDirectory1280x720, "PageElementEntity", 629, 564, DeviceType.AndroidTvBox, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteTwoThirdsPageImageDirectory1280x720, new MediaRatioType("Two thirds image - directory (1280x720)", MediaType.TemplateSiteTwoThirdsPageImageDirectory1280x720, "PageTemplateElementEntity", 629, 564, DeviceType.AndroidTvBox, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.SiteTwoThirdsPageImageDirectory1280x800, new MediaRatioType("Two thirds image - directory (1280x800)", MediaType.SiteTwoThirdsPageImageDirectory1280x800, "PageElementEntity", 629, 644, DeviceType.Tablet1280x800, MediaTypeGroup.SiteTwoThirdsPageImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteTwoThirdsPageImageDirectory1280x800, new MediaRatioType("Two thirds image - directory (1280x800)", MediaType.TemplateSiteTwoThirdsPageImageDirectory1280x800, "PageTemplateElementEntity", 629, 644, DeviceType.Tablet1280x800, MediaTypeGroup.SiteTwoThirdsPageImage));

                // Supporting - Hd Wide Screen Image
                /*
                //this.mediaRatioTypes.Add(MediaType.SiteHdWideScreen1280x720, new MediaRatioType("Hd wide screen image (1280x720)", MediaType.SiteHdWideScreen1280x720, "PageElementEntity", 112, 71, DeviceType.AndroidTvBox, MediaTypeGroup.SiteHdWideScreen));
                //this.mediaRatioTypes.Add(MediaType.SiteHdWideScreen1280x800, new MediaRatioType("Hd wide screen image (1280x800)", MediaType.SiteHdWideScreen1280x800, "PageElementEntity", 112, 73, DeviceType.Tablet1280x800, MediaTypeGroup.SiteHdWideScreen));
                //this.mediaRatioTypes.Add(MediaType.SiteHdWideScreenTabletSmall, new MediaRatioType("Hd wide screen image small (tablet)", MediaType.SiteHdWideScreenTabletSmall, "PageElementEntity", 56, 56, DeviceType.TabletSmall, MediaTypeGroup.SiteHdWideScreen));
                //this.mediaRatioTypes.Add(MediaType.SiteHdWideScreenTabletNormal, new MediaRatioType("Hd wide screen image normal (tablet)", MediaType.SiteHdWideScreenTabletNormal, "PageElementEntity", 75, 75, DeviceType.TabletNormal, MediaTypeGroup.SiteHdWideScreen));
                //this.mediaRatioTypes.Add(MediaType.SiteHdWideScreenTabletLarge, new MediaRatioType("Hd wide screen image large (tablet)", MediaType.SiteHdWideScreenTabletLarge, "PageElementEntity", 112, 112, DeviceType.TabletLarge, MediaTypeGroup.SiteHdWideScreen));
                //this.mediaRatioTypes.Add(MediaType.SiteHdWideScreenTabletXLarge, new MediaRatioType("Hd wide screen image xlarge (tablet)", MediaType.SiteHdWideScreenTabletXLarge, "PageElementEntity", 150, 150, DeviceType.TabletXLarge, MediaTypeGroup.SiteHdWideScreen));
                //this.mediaRatioTypes.Add(MediaType.SiteHdWideScreeniPad, new MediaRatioType("Hd wide screen image iPad", MediaType.SiteHdWideScreeniPad, "PageElementEntity", 56, 56, DeviceType.iPad, MediaTypeGroup.SiteHdWideScreen));
                //this.mediaRatioTypes.Add(MediaType.SiteHdWideScreeniPadRetina, new MediaRatioType("Hd wide screen image iPad Retina", MediaType.SiteHdWideScreeniPadRetina, "PageElementEntity", 112, 112, DeviceType.iPadRetina, MediaTypeGroup.SiteHdWideScreen));

                // Supporting - Cinema Wide Screen Image
                //this.mediaRatioTypes.Add(MediaType.SiteCinemaWideScreen1280x720, new MediaRatioType("Cinema wide screen (1280x720)", MediaType.SiteCinemaWideScreen1280x720, "PageElementEntity", 112, 71, DeviceType.AndroidTvBox, MediaTypeGroup.SiteCinemaWideScreen));
                //this.mediaRatioTypes.Add(MediaType.SiteCinemaWideScreen1280x800, new MediaRatioType("Cinema wide screen (1280x800)", MediaType.SiteCinemaWideScreen1280x800, "PageElementEntity", 112, 73, DeviceType.Tablet1280x800, MediaTypeGroup.SiteCinemaWideScreen));
                //this.mediaRatioTypes.Add(MediaType.SiteCinemaWideScreenTabletSmall, new MediaRatioType("Cinema wide screen small (tablet)", MediaType.SiteCinemaWideScreenTabletSmall, "PageElementEntity", 56, 56, DeviceType.TabletSmall, MediaTypeGroup.SiteCinemaWideScreen));
                //this.mediaRatioTypes.Add(MediaType.SiteCinemaWideScreenTabletNormal, new MediaRatioType("Cinema wide screen normal (tablet)", MediaType.SiteCinemaWideScreenTabletNormal, "PageElementEntity", 75, 75, DeviceType.TabletNormal, MediaTypeGroup.SiteCinemaWideScreen));
                //this.mediaRatioTypes.Add(MediaType.SiteCinemaWideScreenTabletLarge, new MediaRatioType("Cinema wide screen large (tablet)", MediaType.SiteCinemaWideScreenTabletLarge, "PageElementEntity", 112, 112, DeviceType.TabletLarge, MediaTypeGroup.SiteCinemaWideScreen));
                //this.mediaRatioTypes.Add(MediaType.SiteCinemaWideScreenTabletXLarge, new MediaRatioType("Cinema wide screen xlarge (tablet)", MediaType.SiteCinemaWideScreenTabletXLarge, "PageElementEntity", 150, 150, DeviceType.TabletXLarge, MediaTypeGroup.SiteCinemaWideScreen));
                */

                this.mediaRatioTypes.Add(MediaType.SiteCinemaWideScreeniPad, new MediaRatioType("Cinema wide screen iPad", MediaType.SiteCinemaWideScreeniPad, "PageElementEntity", 674, 282, DeviceType.iPad, MediaTypeGroup.SiteCinemaWideScreen));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteCinemaWideScreeniPad, new MediaRatioType("Cinema wide screen iPad", MediaType.TemplateSiteCinemaWideScreeniPad, "PageTemplateElementEntity", 674, 282, DeviceType.iPad, MediaTypeGroup.SiteCinemaWideScreen));
                this.mediaRatioTypes.Add(MediaType.SiteCinemaWideScreeniPadRetina, new MediaRatioType("Cinema wide screen iPad Retina", MediaType.SiteCinemaWideScreeniPadRetina, "PageElementEntity", 1348, 564, DeviceType.iPadRetina, MediaTypeGroup.SiteCinemaWideScreen));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteCinemaWideScreeniPadRetina, new MediaRatioType("Cinema wide screen iPad Retina", MediaType.TemplateSiteCinemaWideScreeniPadRetina, "PageTemplateElementEntity", 1348, 564, DeviceType.iPadRetina, MediaTypeGroup.SiteCinemaWideScreen));

                // Mobile Heading Image
                this.mediaRatioTypes.Add(MediaType.SiteMobileHeaderImagePhoneSmall, new MediaRatioType("Mobile header image small (phone)", MediaType.SiteMobileHeaderImagePhoneSmall, "PageElementEntity", 320, 150, DeviceType.PhoneSmall, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteMobileHeaderImagePhoneSmall, new MediaRatioType("Mobile header image small (phone)", MediaType.TemplateSiteMobileHeaderImagePhoneSmall, "PageTemplateElementEntity", 320, 150, DeviceType.PhoneSmall, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.SiteMobileHeaderImagePhoneNormal, new MediaRatioType("Mobile header image normal (phone)", MediaType.SiteMobileHeaderImagePhoneNormal, "PageElementEntity", 854, 400, DeviceType.PhoneNormal, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteMobileHeaderImagePhoneNormal, new MediaRatioType("Mobile header image normal (phone)", MediaType.TemplateSiteMobileHeaderImagePhoneNormal, "PageTemplateElementEntity", 854, 400, DeviceType.PhoneNormal, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.SiteMobileHeaderImagePhoneLarge, new MediaRatioType("Mobile header image large (phone)", MediaType.SiteMobileHeaderImagePhoneLarge, "PageElementEntity", 1280, 600, DeviceType.PhoneLarge, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteMobileHeaderImagePhoneLarge, new MediaRatioType("Mobile header image large (phone)", MediaType.TemplateSiteMobileHeaderImagePhoneLarge, "PageTemplateElementEntity", 1280, 600, DeviceType.PhoneLarge, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.SiteMobileHeaderImagePhoneXLarge, new MediaRatioType("Mobile header image xlarge (phone)", MediaType.SiteMobileHeaderImagePhoneXLarge, "PageElementEntity", 1920, 900, DeviceType.PhoneXLarge, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteMobileHeaderImagePhoneXLarge, new MediaRatioType("Mobile header image xlarge (phone)", MediaType.TemplateSiteMobileHeaderImagePhoneXLarge, "PageTemplateElementEntity", 1920, 900, DeviceType.PhoneXLarge, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.SiteMobileHeaderImageiPhone, new MediaRatioType("Mobile header image iPhone", MediaType.SiteMobileHeaderImageiPhone, "PageElementEntity", 320, 167, DeviceType.iPhone, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteMobileHeaderImageiPhone, new MediaRatioType("Mobile header image iPhone", MediaType.TemplateSiteMobileHeaderImageiPhone, "PageTemplateElementEntity", 320, 167, DeviceType.iPhone, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.SiteMobileHeaderImageiPhoneRetina, new MediaRatioType("Mobile header image iPhone Retina", MediaType.SiteMobileHeaderImageiPhoneRetina, "PageElementEntity", 640, 334, DeviceType.iPhoneRetina, MediaTypeGroup.SiteMobileHeaderImage));
                this.mediaRatioTypes.Add(MediaType.TemplateSiteMobileHeaderImageiPhoneRetina, new MediaRatioType("Mobile header image iPhone Retina", MediaType.TemplateSiteMobileHeaderImageiPhoneRetina, "PageTemplateElementEntity", 640, 334, DeviceType.iPhoneRetina, MediaTypeGroup.SiteMobileHeaderImage));

                // Supporting - Image for Point of Interest (In room tablet)
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPage1280x720, new MediaRatioType("Point of Interest Summary Page (1280x720)", MediaType.SitePointOfInterestSummaryPage1280x720, "PointOfInterestEntity", 629, 486, DeviceType.AndroidTvBox, MediaTypeGroup.SiteIrtPointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPage1280x800, new MediaRatioType("Point of Interest Summary Page (1280x800)", MediaType.SitePointOfInterestSummaryPage1280x800, "PointOfInterestEntity", 629, 566, DeviceType.Tablet1280x800, MediaTypeGroup.SiteIrtPointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPageMicrosite1280x720, new MediaRatioType("Point of Interest Summary Page Microsite (1280x720)", MediaType.SitePointOfInterestSummaryPageMicrosite1280x720, "PointOfInterestEntity", 629, 486, DeviceType.AndroidTvBox, MediaTypeGroup.SiteIrtPointOfInterestSummaryMicrositeImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPageMicrosite1280x800, new MediaRatioType("Point of Interest Summary Page Microsite (1280x800)", MediaType.SitePointOfInterestSummaryPageMicrosite1280x800, "PointOfInterestEntity", 629, 566, DeviceType.Tablet1280x800, MediaTypeGroup.SiteIrtPointOfInterestSummaryMicrositeImage));

                // Supporting - Image for Point of Interest (Mobile)
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPagePhoneSmall, new MediaRatioType("Point of Interest Summary Page small (phone)", MediaType.SitePointOfInterestSummaryPagePhoneSmall, "PointOfInterestEntity", 320, 150, DeviceType.PhoneSmall, MediaTypeGroup.SiteMobilePointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPagePhoneNormal, new MediaRatioType("Point of Interest Summary Page normal (phone)", MediaType.SitePointOfInterestSummaryPagePhoneNormal, "PointOfInterestEntity", 854, 400, DeviceType.PhoneNormal, MediaTypeGroup.SiteMobilePointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPagePhoneLarge, new MediaRatioType("Point of Interest Summary Page large (phone)", MediaType.SitePointOfInterestSummaryPagePhoneLarge, "PointOfInterestEntity", 1280, 600, DeviceType.PhoneLarge, MediaTypeGroup.SiteMobilePointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPagePhoneXLarge, new MediaRatioType("Point of Interest Summary Page xlarge (phone)", MediaType.SitePointOfInterestSummaryPagePhoneXLarge, "PointOfInterestEntity", 1920, 900, DeviceType.PhoneXLarge, MediaTypeGroup.SiteMobilePointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPageiPhone, new MediaRatioType("Point of Interest Summary Page image iPhone", MediaType.SitePointOfInterestSummaryPageiPhone, "PointOfInterestEntity", 320, 167, DeviceType.iPhone, MediaTypeGroup.SiteMobilePointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPageiPhoneRetina, new MediaRatioType("Point of Interest Summary Page image iPhone Retina", MediaType.SitePointOfInterestSummaryPageiPhoneRetina, "PointOfInterestEntity", 640, 334, DeviceType.iPhoneRetina, MediaTypeGroup.SiteMobilePointOfInterestSummaryImage));

                // Supporting - Image for Point of Interest (Tablet)
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPageTabletSmall, new MediaRatioType("Point of Interest Summary Page small (tablet)", MediaType.SitePointOfInterestSummaryPageTabletSmall, "PointOfInterestEntity", 780, 439, DeviceType.TabletSmall, MediaTypeGroup.SiteTabletPointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPageTabletNormal, new MediaRatioType("Point of Interest Summary Page normal (tablet)", MediaType.SitePointOfInterestSummaryPageTabletNormal, "PointOfInterestEntity", 1204, 677, DeviceType.TabletNormal, MediaTypeGroup.SiteTabletPointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPageTabletLarge, new MediaRatioType("Point of Interest Summary Page large (tablet)", MediaType.SitePointOfInterestSummaryPageTabletLarge, "PointOfInterestEntity", 1605, 903, DeviceType.TabletLarge, MediaTypeGroup.SiteTabletPointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPageTabletXLarge, new MediaRatioType("Point of Interest Summary Page xlarge (tablet)", MediaType.SitePointOfInterestSummaryPageTabletXLarge, "PointOfInterestEntity", 2006, 1128, DeviceType.TabletXLarge, MediaTypeGroup.SiteTabletPointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPageiPad, new MediaRatioType("Point of Interest Summary Page iPad", MediaType.SitePointOfInterestSummaryPageiPad, "PointOfInterestEntity", 1004, 565, DeviceType.iPad, MediaTypeGroup.SiteTabletPointOfInterestSummaryImage));
                this.mediaRatioTypes.Add(MediaType.SitePointOfInterestSummaryPageiPadRetina, new MediaRatioType("Point of Interest Summary Page iPad Retina", MediaType.SitePointOfInterestSummaryPageiPadRetina, "PointOfInterestEntity", 2008, 1130, DeviceType.iPadRetina, MediaTypeGroup.SiteTabletPointOfInterestSummaryImage));

                // Supporting - Image for Company (In room tablet)
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPage1280x720, new MediaRatioType("Company Summary Page (1280x720)", MediaType.SiteCompanySummaryPage1280x720, "CompanyEntity", 629, 486, DeviceType.AndroidTvBox, MediaTypeGroup.SiteIrtCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPage1280x800, new MediaRatioType("Company Summary Page (1280x800)", MediaType.SiteCompanySummaryPage1280x800, "CompanyEntity", 629, 566, DeviceType.Tablet1280x800, MediaTypeGroup.SiteIrtCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPageMicrosite1280x720, new MediaRatioType("Company Summary Page Microsite (1280x720)", MediaType.SiteCompanySummaryPageMicrosite1280x720, "CompanyEntity", 629, 486, DeviceType.AndroidTvBox, MediaTypeGroup.SiteIrtCompanySummaryMicrositeImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPageMicrosite1280x800, new MediaRatioType("Company Summary Page Microsite (1280x800)", MediaType.SiteCompanySummaryPageMicrosite1280x800, "CompanyEntity", 629, 566, DeviceType.Tablet1280x800, MediaTypeGroup.SiteIrtCompanySummaryMicrositeImage));

                // Supporting - Image for Company (Mobile)
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPagePhoneSmall, new MediaRatioType("Company Summary Page small (phone)", MediaType.SiteCompanySummaryPagePhoneSmall, "CompanyEntity", 320, 150, DeviceType.PhoneSmall, MediaTypeGroup.SiteMobileCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPagePhoneNormal, new MediaRatioType("Company Summary Page normal (phone)", MediaType.SiteCompanySummaryPagePhoneNormal, "CompanyEntity", 854, 400, DeviceType.PhoneNormal, MediaTypeGroup.SiteMobileCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPagePhoneLarge, new MediaRatioType("Company Summary Page large (phone)", MediaType.SiteCompanySummaryPagePhoneLarge, "CompanyEntity", 1280, 600, DeviceType.PhoneLarge, MediaTypeGroup.SiteMobileCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPagePhoneXLarge, new MediaRatioType("Company Summary Page xlarge (phone)", MediaType.SiteCompanySummaryPagePhoneXLarge, "CompanyEntity", 1920, 900, DeviceType.PhoneXLarge, MediaTypeGroup.SiteMobileCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPageiPhone, new MediaRatioType("Company Summary Page image iPhone", MediaType.SiteCompanySummaryPageiPhone, "CompanyEntity", 320, 167, DeviceType.iPhone, MediaTypeGroup.SiteMobileCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPageiPhoneRetina, new MediaRatioType("Company Summary Page image iPhone Retina", MediaType.SiteCompanySummaryPageiPhoneRetina, "CompanyEntity", 640, 334, DeviceType.iPhoneRetina, MediaTypeGroup.SiteMobileCompanySummaryImage));

                // Supporting - Image for Company (Tablet)
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPageTabletSmall, new MediaRatioType("Company Summary Page small (tablet)", MediaType.SiteCompanySummaryPageTabletSmall, "CompanyEntity", 780, 439, DeviceType.TabletSmall, MediaTypeGroup.SiteTabletCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPageTabletNormal, new MediaRatioType("Company Summary Page normal (tablet)", MediaType.SiteCompanySummaryPageTabletNormal, "CompanyEntity", 1204, 677, DeviceType.TabletNormal, MediaTypeGroup.SiteTabletCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPageTabletLarge, new MediaRatioType("Company Summary Page large (tablet)", MediaType.SiteCompanySummaryPageTabletLarge, "CompanyEntity", 1605, 903, DeviceType.TabletLarge, MediaTypeGroup.SiteTabletCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPageTabletXLarge, new MediaRatioType("Company Summary Page xlarge (tablet)", MediaType.SiteCompanySummaryPageTabletXLarge, "CompanyEntity", 2006, 1128, DeviceType.TabletXLarge, MediaTypeGroup.SiteTabletCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPageiPad, new MediaRatioType("Company Summary Page iPad", MediaType.SiteCompanySummaryPageiPad, "CompanyEntity", 674, 282, DeviceType.iPad, MediaTypeGroup.SiteTabletCompanySummaryImage));
                this.mediaRatioTypes.Add(MediaType.SiteCompanySummaryPageiPadRetina, new MediaRatioType("Company Summary Page iPad Retina", MediaType.SiteCompanySummaryPageiPadRetina, "CompanyEntity", 1348, 564, DeviceType.iPadRetina, MediaTypeGroup.SiteTabletCompanySummaryImage));

                // Site branding (Mobile)
                this.mediaRatioTypes.Add(MediaType.SiteBrandingPhoneSmall, new MediaRatioType("Site Branding small (phone)", MediaType.SiteBrandingPhoneSmall, "SiteEntity", 320, 150, DeviceType.PhoneSmall, MediaTypeGroup.SiteBranding));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingPhoneNormal, new MediaRatioType("Site Branding normal (phone)", MediaType.SiteBrandingPhoneNormal, "SiteEntity", 854, 400, DeviceType.PhoneNormal, MediaTypeGroup.SiteBranding));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingPhoneLarge, new MediaRatioType("Site Branding large (phone)", MediaType.SiteBrandingPhoneLarge, "SiteEntity", 1280, 600, DeviceType.PhoneLarge, MediaTypeGroup.SiteBranding));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingPhoneXLarge, new MediaRatioType("Site Branding xlarge (phone)", MediaType.SiteBrandingPhoneXLarge, "SiteEntity", 1920, 900, DeviceType.PhoneXLarge, MediaTypeGroup.SiteBranding));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingiPhone, new MediaRatioType("Site Branding iPhone", MediaType.SiteBrandingiPhone, "SiteEntity", 320, 167, DeviceType.iPhone, MediaTypeGroup.SiteBranding));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingiPhoneRetina, new MediaRatioType("Site Branding iPhone Retina", MediaType.SiteBrandingiPhoneRetina, "SiteEntity", 640, 334, DeviceType.iPhoneRetina, MediaTypeGroup.SiteBranding));

                // Site branding (Tablet)
                this.mediaRatioTypes.Add(MediaType.SiteBranding1280x800, new MediaRatioType("Site Branding (1280x800)", MediaType.SiteBranding1280x800, "SiteEntity", 290, 133, DeviceType.Tablet1280x800, MediaTypeGroup.SiteBranding));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingTabletSmall, new MediaRatioType("Site Branding small (tablet)", MediaType.SiteBrandingTabletSmall, "SiteEntity", 803, 452, DeviceType.TabletSmall, MediaTypeGroup.SiteBranding));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingTabletNormal, new MediaRatioType("Site Branding normal (tablet)", MediaType.SiteBrandingTabletNormal, "SiteEntity", 1204, 677, DeviceType.TabletNormal, MediaTypeGroup.SiteBranding));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingTabletLarge, new MediaRatioType("Site Branding large (tablet)", MediaType.SiteBrandingTabletLarge, "SiteEntity", 1605, 903, DeviceType.TabletLarge, MediaTypeGroup.SiteBranding));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingTabletXLarge, new MediaRatioType("Site Branding xlarge (tablet)", MediaType.SiteBrandingTabletXLarge, "SiteEntity", 2006, 1129, DeviceType.TabletXLarge, MediaTypeGroup.SiteBranding));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingiPad, new MediaRatioType("Site Branding iPad", MediaType.SiteBrandingiPad, "SiteEntity", 674, 379, DeviceType.iPad, MediaTypeGroup.SiteBranding));
                this.mediaRatioTypes.Add(MediaType.SiteBrandingiPadRetina, new MediaRatioType("Site Branding iPad Retina", MediaType.SiteBrandingiPadRetina, "SiteEntity", 1348, 758, DeviceType.iPadRetina, MediaTypeGroup.SiteBranding));

                // Sites v2 (Emenu)
                this.mediaRatioTypes.Add(MediaType.SiteMenuHeader1280x800, new MediaRatioType("Menu header - v2 (1280x800)", MediaType.SiteMenuHeader1280x800, "SiteEntity", 320, 133, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SiteMenuIcon1280x800, new MediaRatioType("Menu icon - v2 (1280x800)", MediaType.SiteMenuIcon1280x800, "PageEntity", 69, 69, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SitePageFull1280x800, new MediaRatioType("Page full - v2 (1280x800)", MediaType.SitePageFull1280x800, "PageElementEntity", 930, 646, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SitePageTwoThirds1280x800, new MediaRatioType("Page two thirds - v2 (1280x800)", MediaType.SitePageTwoThirds1280x800, "PageElementEntity", 600, 646, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SitePageTwoThirdsPointOfInterest1280x800, new MediaRatioType("Site page two thirds - v2 (1280x800)", MediaType.SitePageTwoThirdsPointOfInterest1280x800, "PointOfInterestEntity", 600, 646, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.SitePageTwoThirdsCompany1280x800, new MediaRatioType("Site page two thirds - v2 (1280x800)", MediaType.SitePageTwoThirdsCompany1280x800, "CompanyEntity", 600, 646, DeviceType.Tablet1280x800));

                // Maps
                this.mediaRatioTypes.Add(MediaType.MapMenuItem1280x800, new MediaRatioType("Point of Interest menu item (1280x800)", MediaType.MapMenuItem1280x800, "PointOfInterestEntity", 69, 69, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.MapMarkerPointOfInterest1280x800, new MediaRatioType("Point of interest map marker (1280x800)", MediaType.MapMarkerPointOfInterest1280x800, "PointOfInterestEntity", 260, 120, DeviceType.Tablet1280x800));
                this.mediaRatioTypes.Add(MediaType.MapMarkerCompany1280x800, new MediaRatioType("Company map marker (1280x800)", MediaType.MapMarkerCompany1280x800, "CompanyEntity", 260, 120, DeviceType.Tablet1280x800));

                this.mediaRatioTypes.Add(MediaType.QrCodeLogo1280x800, new MediaRatioType("QR Code Logo (1280x800)", MediaType.QrCodeLogo1280x800, "DeliverypointgroupEntity", 60, 60, DeviceType.Tablet1280x800));

                // Since (unfortunately) MediaTypeGroups can contain MediaTypes with different aspect ratio's we also need collections for that
                Dictionary<MediaTypeGroup, List<int>> mediaTypeGroupRatios = new Dictionary<MediaTypeGroup, List<int>>();
                foreach (KeyValuePair<MediaType, MediaRatioType> keyValuePair in this.mediaRatioTypes)
                {
                    // The tricky part - the ratio.
                    MediaRatioType mediaRatioType = keyValuePair.Value;
                    List<int> ratios;
                    if (!mediaTypeGroupRatios.TryGetValue(mediaRatioType.MediaTypeGroup, out ratios))
                    {
                        ratios = new List<int>();
                        mediaTypeGroupRatios.Add(mediaRatioType.MediaTypeGroup, ratios);
                    }

                    int ratio = Convert.ToInt32(decimal.Divide(mediaRatioType.Width, mediaRatioType.Height) * 10000);
                    decimal maxRatio = ratio * (decimal)1.02;
                    decimal minRatio = ratio * (decimal)0.98;

                    int key = ratios.FirstOrDefault(x => x >= minRatio && x <= maxRatio);

                    if (key == default(decimal))
                    {
                        // We need to create the key
                        key = ratio;
                        ratios.Add(ratio);
                    }

                    if (this.mediaTypeMediaTypeGroupRatioLookup.ContainsKey(mediaRatioType.MediaType))
                    {
                        throw new Exception("Multiple defined: " + mediaRatioType.MediaType);
                    }

                    this.mediaTypeMediaTypeGroupRatioLookup.Add(mediaRatioType.MediaType, mediaRatioType.MediaTypeGroup.ToString() + key.ToString());
                }
            }
            catch (Exception ex)
            {
                if (true)
                {
                    // TestUtil.IsPcDeveloper
                    Debugger.Launch();
                    Debug.WriteLine("ERROR: Constructor of MediaRatioTypes failed: {0}", ex.Message);
                    Debug.WriteLine("ERROR: Stack: {0}", ex.StackTrace);
                    Debugger.Break();
                }
            }
        }

        #endregion
    }
}