﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Models
{
    /// <summary>
    /// Class which represents the business hours for a company or point of interest.
    /// </summary>
    public class Businesshours
    {
        /// <summary>
        /// The opening day.
        /// </summary>
        public int OpeningDay;

        /// <summary>
        /// The opening time.
        /// </summary>
        public int OpeningTime;

        /// <summary>
        /// The closing day.
        /// </summary>
        public int ClosingDay;
        
        /// <summary>
        /// The closing time.
        /// </summary>
        public int ClosingTime;
    }
}
