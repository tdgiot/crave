﻿using System;

namespace Crave.Models
{
    /// <summary>
    /// DateTime Interval.
    /// </summary>
    public struct DateTimeInterval
    {
        #region Fields

        private readonly DateTime from, to;

        #endregion

        #region Constructors

        /// <summary>
        /// Initialize a new <see cref="DateTimeInterval"/>.
        /// <param name="from">The from datetime.</param>
        /// <param name="to">The to datetime.</param>
        /// </summary>
        public DateTimeInterval(DateTime from, DateTime to)
        {
            this.from = from;
            this.to = to;
        }

        /// <summary>
        /// Initialize a new <see cref="DateTimeInterval"/> with unix timestamps.
        /// </summary>
        /// <param name="from">The from unix timestamp.</param>
        /// <param name="to">The to unix timestamp.</param>
        public DateTimeInterval(long from, long to)
        {
            this.from = DateTimeUtil.FromUnixTimestamp(from);
            this.to = DateTimeUtil.FromUnixTimestamp(to);
        }

        #endregion

        #region Getters

        /// <summary>
        /// Gets the date time from.
        /// </summary>
        public DateTime From => this.from;

        /// <summary>
        /// Gets the date time to.
        /// </summary>
        public DateTime To => this.TimeSpan == TimeSpan.Zero ? this.to.AddDays(1) : this.to;

        /// <summary>
        /// Gets the timespan between the start and end datetime.
        /// </summary>
        public TimeSpan TimeSpan => DateTimeUtil.GetSpanBetweenDateTimes(this.from, this.to);

        /// <summary>
        /// Gets the number of days of the interval.
        /// </summary>
        public int Days => (this.from - this.to).Days;

        #endregion
    }
}