﻿using System;
using System.Globalization;

namespace Crave.Models
{
    /// <summary>
    /// Contains logic to create a datetime range object.
    /// </summary>
    public class DateTimeRange
    {
        private string dateTimeFormat = "yyyyMMddHHmm";

        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeRange"/> class.
        /// </summary>
        public DateTimeRange() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeRange"/> class.
        /// </summary>
        /// <param name="from">The from datetime.</param>
        /// <param name="till">The till datetime.</param>
        public DateTimeRange(DateTime from, DateTime till)
        {
            this.From = from;
            this.Till = till;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeRange"/> class.
        /// </summary>
        /// <param name="from">The from datetime string.</param>
        /// <param name="till">The till datetime string.</param>
        public DateTimeRange(string from, string till)
        {
            this.From = this.ToDateTime(from);
            this.Till = this.ToDateTime(till);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeRange"/> class.
        /// </summary>
        /// <param name="from">The amount of days from the current date to set the starting date to.</param>
        /// <param name="till">The amount of days from the current date to set the till date to.</param>
        public DateTimeRange(int from, int till)
        {
            this.From = DateTime.Today.AddDays(from).MakeBeginOfDay();
            this.Till = DateTime.Today.AddDays(till).MakeEndOfDay();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeRange"/> class.
        /// </summary>
        /// <param name="from">The amount of days from the current date to set the starting date to.</param>
        public DateTimeRange(int from)
        {
            if (from < 0)
            {
                this.From = DateTime.Today.AddDays(from).MakeBeginOfDay();
            }
            else
            {
                this.From = DateTime.Today.AddDays(-from).MakeBeginOfDay();
            }
            
            this.Till = DateTime.Today.MakeEndOfDay();
        }

        /// <summary>
        /// Gets the from datetime.
        /// </summary>
        public DateTime From { get; private set; }

        /// <summary>
        /// Gets the till datetime.
        /// </summary>
        public DateTime Till { get; private set; }

        /// <summary>
        /// Gets the amount of days between the from and till datetime.
        /// </summary>
        public int NumberOfDays
        {
            get { return (this.Till - this.From).Days; }
        }

        /// <summary>
        /// Check whether a datetime falls in the range.
        /// </summary>
        /// <param name="datetime">The datetime to compare to the datetime range.</param>
        /// <returns>A <see cref="bool"/> set to true if the datetime falls in the range.</returns>
        public bool IsBetweenRange(DateTime datetime)
        {
            bool result;

            if (datetime >= this.From && datetime <= this.Till)
            {
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Check whether a datetime falls in the range.
        /// </summary>
        /// <param name="datetime">The datetime to compare to the datetime range.</param>
        /// <returns>A <see cref="bool"/> set to true if the datetime falls in the range.</returns>
        public bool IsBetweenRange(string datetime)
        {
            return this.IsBetweenRange(this.ToDateTime(datetime));
        }

        /// <summary>
        /// Set the from datetime.
        /// </summary>
        /// <param name="from">The from datetime.</param>
        public void SetFrom(DateTime from)
        {
            this.From = from;
        }

        /// <summary>
        /// Set the till datetime.
        /// </summary>
        /// <param name="till">The till datetime.</param>
        public void SetTill(DateTime till)
        {
            this.Till = till;
        }

        /// <summary>
        /// Set the from datetime.
        /// </summary>
        /// <param name="from">The from datetime.</param>
        public void SetFrom(string from)
        {
            this.SetFrom(this.ToDateTime(from));
        }

        /// <summary>
        /// Set the till datetime.
        /// </summary>
        /// <param name="till">The till datetime.</param>
        public void SetTill(string till)
        {
            this.SetTill(this.ToDateTime(till));
        }

        /// <summary>
        /// Set the datetime format to use when parsing a string to datetime (default: yyyyMMddHHmm).
        /// </summary>
        /// <param name="format">The format to use for parsing.</param>
        public void SetFormat(string format)
        {
            this.dateTimeFormat = format;
        }

        private DateTime ToDateTime(string dateTimeString)
        {
            DateTime datetime;
            
            bool success = DateTime.TryParseExact(dateTimeString, this.dateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out datetime);

            if (success == false)
            {
                throw new FormatException("The string you try to parse is in the wrong format. Expected format is: " + this.dateTimeFormat);
            }

            return datetime;
        }
    }
}