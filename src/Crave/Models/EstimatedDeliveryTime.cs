﻿namespace Crave.Models
{
    /// <summary>
    /// Simple model for communicating estimated delivery time
    /// </summary>
    public class EstimatedDeliveryTime
    {
        /// <summary>
        /// Gets or sets the deliverypoint group id
        /// </summary>
        public int DeliverypointgroupId { get; set; }

        /// <summary>
        /// Gets or sets the delivery time in minutes
        /// </summary>
        public int DeliveryTime { get; set; }
    }
}