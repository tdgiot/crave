﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Models
{
    /// <summary>
    /// Class which represents a range between two time spans.
    /// </summary>
    public class TimeSpanRange
    {
        /// <summary>
        /// The start of the range.
        /// </summary>
        public TimeSpan Start { get; set; }

        /// <summary>
        /// The end of the range.
        /// </summary>
        public TimeSpan End { get; set; }

        /// <summary>
        /// Represents a range between two time spans.
        /// </summary>
        /// <param name="start">The start of the range.</param>
        /// <param name="end">The end of the range.</param>
        public TimeSpanRange(TimeSpan start, TimeSpan end)
        {
            this.Start = start;
            this.End = end;
        }
    }
}
