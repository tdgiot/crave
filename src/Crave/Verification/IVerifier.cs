﻿namespace Crave.Verification
{
    /// <summary>
    /// Interface used to implement verifier classes.
    /// </summary>
    public interface IVerifier
    {
        #region Methods

        /// <summary>
        /// Execute the verification logic.
        /// </summary>
        void Verify();

        #endregion
    }
}
