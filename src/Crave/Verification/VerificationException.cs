﻿using System;

namespace Crave.Verification
{
    /// <summary>
    /// Exception class which is used for throwing exception in the verification process at application start.
    /// </summary>
    public class VerificationException : SystemException
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VerificationException"/> class.
        /// </summary>
        /// <param name="format">The string to format.</param>
        /// <param name="args">The arguments to format the string with.</param>
        public VerificationException(string format, params object[] args) : base(string.Format(format, args))
        {
        }

        #endregion
    }
}
