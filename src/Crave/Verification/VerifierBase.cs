﻿using System.Text;

namespace Crave.Verification
{
    /// <summary>
    /// Base class used to implement verifier classes.
    /// </summary>
    public abstract class VerifierBase : IVerifier
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VerifierBase"/> class.
        /// </summary>
        public VerifierBase()
        {
            this.ErrorMessage = new StringBuilder();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the error message(s) which might have occurred when the verification logic was executed.
        /// </summary>
        private StringBuilder ErrorMessage { get; } // = new StringBuilder(); 

        #endregion

        #region Methods

        /// <summary>
        /// Executes the verification logic. This method is abstract and has to be implemented in sub classes.
        /// </summary>
        public abstract void ExecuteVerification();

        /// <summary>
        /// Calls the ExecuteVerification method and throws a <see cref="VerificationException"/> if the verification failed.
        /// </summary>
        public void Verify()
        {
            this.ExecuteVerification();
            
            if (this.ErrorMessage.Length > 0)
            {
                throw new VerificationException(this.ErrorMessage.ToString());
            }
        }

        /// <summary>
        /// Adds an error message to the collection of error messages.
        /// </summary>
        /// <param name="format">The format of the error message.</param>
        /// <param name="args">The arguments to format the error message with.</param>
        public void AddErrorMessage(string format, params object[] args)
        {
            this.ErrorMessage.AppendLine(format, args);
        }

        #endregion
    }
}
