﻿using System;
using System.Collections.ObjectModel;
using System.Text;

namespace Crave.Verification
{
    /// <summary>
    /// Collection class which is used to store verifier classes in.
    /// </summary>
    public class VerifierCollection : Collection<IVerifier>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VerifierCollection"/> class.
        /// </summary>
        public VerifierCollection()
        {
            this.ErrorMessage = new StringBuilder();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the error message(s) which might have occurred when the verification logic was executed.
        /// </summary>
        private StringBuilder ErrorMessage { get; } // = new StringBuilder();

        #endregion

        #region Methods

        /// <summary>
        /// Calls the Verify method on each of the verifier instances in this collection
        /// and throws a <see cref="VerificationException"/> if the verification failed.
        /// </summary>
        public void Verify()
        {
            foreach (IVerifier verifier in this)
            {
                try
                {
                    verifier.Verify();
                }
                catch (VerificationException vex)
                {
                    this.AddErrorMessage(vex.Message);
                } 
            }

            if (this.ErrorMessage.Length > 0)
            {
                throw new VerificationException(this.ErrorMessage.ToString());
            }
        }

        /// <summary>
        /// Adds an error message to the collection of error messages.
        /// </summary>
        /// <param name="format">The format of the error message.</param>
        /// <param name="args">The arguments to format the error message with.</param>
        private void AddErrorMessage(string format, params object[] args)
        {
            this.ErrorMessage.AppendLine(format, args);
        }

        #endregion 
    }
}
