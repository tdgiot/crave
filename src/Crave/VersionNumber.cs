﻿using System;

namespace Crave
{
    public class VersionNumber
    {
        public enum VersionState
        {
            Unknown,
            Older,
            Equal,
            Newer
        }

        #region Fields

        private int versionNumber = -1;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VersionNumber"/> class.
        /// </summary>
        /// <param name="versionNumber">The <see cref="int"/> representing the version number</param>
        public VersionNumber(int versionNumber)
        {
            this.Parse(versionNumber.ToString());
        }

        /// <summary>
        /// /// Initializes a new instance of the <see cref="VersionNumber"/> class.
        /// </summary>
        /// <param name="versionNumber">The <see cref="String"/> instance representing the version number</param>
        public VersionNumber(string versionNumber)
        {
            this.Parse(versionNumber);
        }

        #endregion

        #region Methods

        public void Parse(string versionNumber)
        {
            if (versionNumber == null || versionNumber.Length == 0)
                return;

            if (versionNumber.Length > 1 && versionNumber.StartsWith("v", StringComparison.InvariantCultureIgnoreCase))
                versionNumber = versionNumber.Substring(1);

            // Remove the leading "1."
            if (versionNumber.Length > 2 && versionNumber.StartsWith("1."))
                versionNumber = versionNumber.Substring(2);

            // Check the length of the version
            if (versionNumber.Length == 8)
            {
                if (versionNumber.StartsWith("20"))
                {
                    // CraveOS versioning
                    versionNumber = versionNumber + "00";
                }
                else
                {
                    // Android APK versioning
                    versionNumber = "20" + versionNumber;
                }
            }

            if (!int.TryParse(versionNumber, out this.versionNumber))
                throw new ApplicationException(string.Format("Version string '{0}' could not be parsed into an integer!", versionNumber));
        }

        /// <summary>
        /// Compares this version number with the version number specified. 
        /// </summary>
        /// <param name="versionNumberToCompare">The <see cref="VersionNumber"/> instance to compare to this version instance</param>
        /// <returns>Equal if both versions are equal, Older if this version instance is older, Newer if this version instance is newer</returns>
        public VersionState CompareTo(VersionNumber versionNumberToCompare)
        {
            VersionState versionState;

            if (this.Number == -1 || versionNumberToCompare.Number == -1)
                versionState = VersionState.Unknown;
            else if (this.Number == versionNumberToCompare.Number)
                versionState = VersionState.Equal;
            else if (this.Number < versionNumberToCompare.Number)
                versionState = VersionState.Older;
            else
                versionState = VersionState.Newer;

            return versionState;
        }

        public VersionState CompareTo(int versionNumber)
        {
            return this.CompareTo(new VersionNumber(versionNumber));
        }

        public VersionState CompareTo(string versionNumber)
        {
            return this.CompareTo(new VersionNumber(versionNumber));
        }

        public static VersionState CompareVersions(int versionNumber1, int versionNumber2)
        {
            VersionNumber version1 = new VersionNumber(versionNumber1);
            VersionNumber version2 = new VersionNumber(versionNumber2);

            return version1.CompareTo(version2);
        }

        public static VersionState CompareVersions(string versionNumber1, string versionNumber2)
        {
            VersionNumber version1 = new VersionNumber(versionNumber1);
            VersionNumber version2 = new VersionNumber(versionNumber2);

            return version1.CompareTo(version2);
        }

        public static VersionState CompareVersions(int versionNumber1, string versionNumber2)
        {
            VersionNumber version1 = new VersionNumber(versionNumber1);
            VersionNumber version2 = new VersionNumber(versionNumber2);

            return version1.CompareTo(version2);
        }

        public static VersionState CompareVersions(string versionNumber1, int versionNumber2)
        {
            VersionNumber version1 = new VersionNumber(versionNumber1);
            VersionNumber version2 = new VersionNumber(versionNumber2);

            return version1.CompareTo(version2);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the version number
        /// </summary>
        public int Number
        {
            get
            {
                return this.versionNumber;
            }
        }

        /// <summary>
        /// Gets the string representation of the version number
        /// </summary>
        public string NumberAsString
        {
            get
            {
                return this.Number.ToString();
            }
        }

        /// <summary>
        /// Gets a flag indicating whether a valid version number has been set
        /// </summary>
        public bool HasVersionNumber
        {
            get
            {
                return (this.versionNumber > -1);
            }
        }

        #endregion
    }
}
