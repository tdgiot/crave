﻿using System.Resources;

// ReSharper disable once CheckNamespace
namespace System.ComponentModel
{
    /// <inheritdoc />
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class LocalizedDisplayNameAttribute : DisplayNameAttribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedDisplayNameAttribute"/> class.
        /// </summary>
        /// <param name="resourceKey">Resource key to find the localized name</param>
        /// <param name="resourceType">Resource type in which to find the resource key</param>
        public LocalizedDisplayNameAttribute(string resourceKey, Type resourceType)
        {
            this.ResourceType = resourceType;
            this.ResourceKey = resourceKey;
        }

        #endregion

        #region Properties

        /// <inheritdoc />
        public override string DisplayName
        {
            get
            {
                ResourceManager resourceManager = new ResourceManager(this.ResourceType);
                string displayName = resourceManager.GetString(this.ResourceKey);

                return string.IsNullOrEmpty(displayName) ? $"[[{this.ResourceKey}]]" : displayName;
            }
        }

        /// <summary>
        /// Gets or sets the resource key used for looking up the display name
        /// </summary>
        public string ResourceKey { get; set; }

        /// <summary>
        /// Gets or sets the resouce type in which to find the resource key
        /// </summary>
        public Type ResourceType { get; set; }

        #endregion
    }
}
