﻿ // ReSharper disable once CheckNamespace
namespace System.ComponentModel
{
    /// <summary>
    /// Specifies the display name of an Enum
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum)]
    public class LocalizedEnumDisplayNameAttribute : DisplayNameAttribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedEnumDisplayNameAttribute"/> class.
        /// </summary>
        /// <param name="resourceType">Resource type or use for lookup</param>
        public LocalizedEnumDisplayNameAttribute(Type resourceType)
        {
            this.ResourceType = resourceType;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the resource type for lookup
        /// </summary>
        public Type ResourceType { get; set; }

        #endregion
    }
}
