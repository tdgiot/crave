﻿ // ReSharper disable once CheckNamespace
namespace System.ComponentModel
{
    /// <summary>
    /// Attribute for assigning stylesheet values to properties and classes
    /// </summary>
    public sealed class StylesheetAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StylesheetAttribute"/> class
        /// </summary>
        /// <param name="value">Stylesheet value</param>
        public StylesheetAttribute(string value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets the stylesheet value
        /// </summary>
        public string Value { get; private set; }
    }
}