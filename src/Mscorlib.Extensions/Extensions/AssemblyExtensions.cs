﻿ // ReSharper disable once CheckNamespace
namespace System.Reflection
{
    /// <summary>
    /// Assembly extension methods
    /// </summary>
    public static class AssemblyExtensions
    {
        /// <summary>
        /// Get the type of a property, method or class inside this assembly
        /// </summary>
        /// <param name="assembly">Asesmbly to use</param>
        /// <param name="format">Name of the item to get the type for</param>
        /// <param name="args">Optional name parameters</param>
        /// <returns>Type object</returns>
        public static Type GetType(this Assembly assembly, string format, params object[] args)
        {
            return assembly.GetType(string.Format(format, args));
        }
    }
}
