﻿using System.Linq;
using System.Reflection;

// ReSharper disable once CheckNamespace
namespace System
{
    /// <summary>
    /// Attribute extension methods
    /// </summary>
    public static class AttributeExtensions
    {
        /// <summary>
        /// Resolve an (custom) attribute on a type or from assembly
        /// </summary>
        /// <typeparam name="TAttribute">Type of the attribute to find</typeparam>
        /// <param name="type">Type on which to execute this method</param>
        /// <returns>First found attribute or null when none found</returns>
        public static TAttribute GetAttributeOnTypeOrAssembly<TAttribute>(this Type type) where TAttribute : Attribute
        {
            return type.First<TAttribute>() ?? type.Assembly.First<TAttribute>();
        }

        /// <summary>
        /// Get the first found attribute for a specific type
        /// </summary>
        /// <typeparam name="TAttribute">Type of the attribute to find</typeparam>
        /// <param name="attributeProvider">Attribute provider to use</param>
        /// <returns>First found attribute or null when none found.</returns>
        public static TAttribute First<TAttribute>(this ICustomAttributeProvider attributeProvider) where TAttribute : Attribute
        {
            return attributeProvider.GetCustomAttributes(typeof (TAttribute), true).FirstOrDefault() as TAttribute;
        }
    }
}