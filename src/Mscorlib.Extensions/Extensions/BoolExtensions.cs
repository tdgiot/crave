﻿namespace Mscorlib.Extensions.Extensions
{
    public static class BoolExtensions
    {
        /// <summary>
        /// Converts the value of this instance to its equivalent string in lowercase.
        /// </summary>
        /// <param name="boolean">The instance to convert.</param>
        /// <returns>A <see cref="string"/> representing the <see cref="bool"/> instrance.</returns>
        public static string ToLowerString(this bool boolean)
        {
            return boolean.ToString().ToLower();
        }
    }
}
