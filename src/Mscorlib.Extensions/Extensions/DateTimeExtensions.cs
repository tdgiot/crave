﻿// ReSharper disable once CheckNamespace

using System.Globalization;
using System.Threading;

namespace System
{
    /// <summary>
    /// DateTime extension methods
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Convert a UTC <see cref="DateTime"/> to a specific timezone
        /// </summary>
        /// <param name="dateTimeUTC">Date time object as UTC to use</param>
        /// <param name="timeZoneInfo">Time zone to convert to</param>
        /// <returns>Converted <see cref="DateTime"/> object.</returns>
        public static DateTime ToLocalTime(this DateTime dateTimeUTC, TimeZoneInfo timeZoneInfo)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(dateTimeUTC, timeZoneInfo);
        }

        /// <summary>
        /// Convert a UTC <see cref="DateTime"/> to a UTC timezone
        /// </summary>
        /// <param name="dateTime">Date time object to use</param>
        /// <param name="timeZoneInfo">Time zone to convert to</param>
        /// <returns>Converted <see cref="DateTime"/> object.</returns>
        public static DateTime FromLocalTime(this DateTime dateTime, TimeZoneInfo timeZoneInfo)
        {
            return TimeZoneInfo.ConvertTimeToUtc(dateTime, timeZoneInfo);
        }

        /// TODO: FINISH
        public static DateTime FromUnixTimeStamp(this DateTime dateTime, long unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();

            return dtDateTime;
        }

        /// <summary>
        /// Converts date time into number of seconds since epoch (1-1-1970)
        /// </summary>
        /// <returns>Number of seconds since 1-1-1970</returns>
        public static long ToUnixTime(this DateTime date, DateTimeKind dateTimeKind = DateTimeKind.Utc)
        {
            if (date.Kind != dateTimeKind)
                date = DateTime.SpecifyKind(date, dateTimeKind);

            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date.ToUniversalTime() - epoch).TotalSeconds);
        }

        /// <summary>
        /// Check if date is in between two other dates
        /// </summary>
        public static bool IsBetweenDates(this DateTime date, DateTime begin, DateTime end)
        {
            return (date.Date >= begin.Date && date.Date <= end.Date);
        }
    }
}
