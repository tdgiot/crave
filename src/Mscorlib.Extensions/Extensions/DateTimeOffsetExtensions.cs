﻿using System;

namespace Mscorlib.Extensions.Extensions
{
    /// <summary>
    /// DateTimeOffset extension methods.
    /// </summary>
    public static class DateTimeOffsetExtensions
    {
        /// <summary>
        /// Checks if this DateTimeOffset is equal to a given object.
        /// </summary>
        /// <param name="dateTimeOffset">This <see cref="DateTimeOffset"/></param>
        /// <param name="otherDateTimeOffset"><see cref="DateTimeOffset"/> to compare with</param>
        /// <param name="acceptanceVarianceInSeconds">The allowed difference in seconds</param>
        /// <returns>Returns true if the given object is a boxed DateTimeOffset and its value is equal to the value of this DateTimeOffset. Returns false otherwise.</returns>
        public static bool Equals(this DateTimeOffset dateTimeOffset, DateTimeOffset otherDateTimeOffset, int acceptanceVarianceInSeconds)
        {
            TimeSpan timeSpan = dateTimeOffset - otherDateTimeOffset;

            return Math.Abs(timeSpan.TotalSeconds) <= acceptanceVarianceInSeconds;
        }
    }
}
