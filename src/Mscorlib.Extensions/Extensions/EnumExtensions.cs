﻿using System.ComponentModel;
using System.Reflection;
using System.Resources;

// ReSharper disable once CheckNamespace
namespace System
{
    /// <summary>
    /// Enum extension methods
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Get the display name for this enum by looking for custom attributes <see cref="LocalizedEnumDisplayNameAttribute"/> and <see cref="LocalizedDisplayNameAttribute"/>
        /// </summary>
        /// <param name="value">Enum to use</param>
        /// <returns>Display name of this enum if it has one of the attributes, otherwise the enum value.</returns>
        public static string GetDisplayName(this Enum value)
        {
            Type enumType = value.GetType();

            // Try to get the display value using the attribute on the enum value
            string displayName = EnumExtensions.GetDisplayNameFromEnumValueAttribute(value, enumType);
            
            if (string.IsNullOrWhiteSpace(displayName))
            {
                // Try to get the display value using the attribute on the enum type
                displayName = EnumExtensions.GetDisplayNameFromEnumAttribute(value, enumType);
            }

            if (string.IsNullOrWhiteSpace(displayName))
            {
                // Fallback to enum value
                displayName = value.ToString(); 
            }

            return displayName;
        }
        
        /// <summary>
        /// Find an attribute on the enum
        /// </summary>
        /// <typeparam name="T">Type of attribute to find</typeparam>
        /// <param name="value">Enum to use</param>
        /// <returns>The found attribute as <typeparamref name="T"/></returns>
        public static T GetAttribute<T>(this Enum value) where T : Attribute
        {
            return value.GetType().GetField(value.ToString()).GetCustomAttribute<T>();
        }

        private static string GetDisplayNameFromEnumAttribute(Enum value, Type enumType)
        {
            string displayName = string.Empty;

            // Get the LocalizedEnumDisplayNameAttributes from the Enum type
            LocalizedEnumDisplayNameAttribute[] localizedEnumDisplayNameAttributes = enumType.GetCustomAttributes<LocalizedEnumDisplayNameAttribute>(false) as LocalizedEnumDisplayNameAttribute[];

            if (!localizedEnumDisplayNameAttributes.IsNullOrEmpty())
            {
                string resourceKey = $"{enumType.Name}_{value}";
                displayName = ResourceManagerUtil.GetString(localizedEnumDisplayNameAttributes[0].ResourceType, resourceKey);
            }

            return displayName;
        }

        private static string GetDisplayNameFromEnumValueAttribute(Enum value, Type enumType)
        {
            string displayName = string.Empty;

            // Get FieldInfo for the Type
            FieldInfo fieldInfo = enumType.GetField(value.ToString());

            // Get the LocalizedDisplayNameAttributes from the Enum value
            LocalizedDisplayNameAttribute[] localizedDisplayNameAttributes = fieldInfo.GetCustomAttributes<LocalizedDisplayNameAttribute>(false) as LocalizedDisplayNameAttribute[];

            if (!localizedDisplayNameAttributes.IsNullOrEmpty())
            {
                LocalizedDisplayNameAttribute localizedDisplayNameAttribute = localizedDisplayNameAttributes[0];

                string resourceKey = localizedDisplayNameAttribute.ResourceKey;
                if (string.IsNullOrWhiteSpace(resourceKey))
                {
                    resourceKey = $"{enumType.Name}_{value}";
                }

                displayName = ResourceManagerUtil.GetString(localizedDisplayNameAttribute.ResourceType, resourceKey);
            }

            return displayName;
        }

        /// <summary>
		/// Convert an Enum to its integer value as string
		/// </summary>
		/// <param name="value">The Enum value to get the integer String value from.</param>
		public static string ToIntString(this Enum value)
        {
            return ((int)(object)value).ToString();
        }

        /// <summary>
        /// Convert an Enum to its lowercase string invariant.
        /// </summary>
        /// <param name="value">The Enum value to get the lowercase string value from.</param>
        public static string ToLowercaseString(this Enum value)
        {
            return value.ToString().ToLower();
        }

        /// <summary>
        /// Retrieve a typed Enum from an int value.
        /// Convert integer to enum. Will throw an <exception cref="ArgumentException" /> if the enum type does not exists.
        /// </summary>
        /// <typeparam name="T">The Enum type to parse to.</typeparam>
        /// <param name="value">The int value.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="OverflowException"></exception>
        public static T ToEnum<T>(this int value) where T : struct, IComparable, IFormattable, IConvertible
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new InvalidOperationException("T must be an enumerated type.");
            }

            return (T)Enum.Parse(enumType, value.ToString());
        }

        /// <summary>
        /// Retrieve a typed Enum from a String value.
        /// </summary>
        /// <typeparam name="T">The Enum type to parse to.</typeparam>
        /// <param name="value">The String value.</param>
        /// <returns>Typed Enum instance for the specified String value.</returns>
        public static T ToEnum<T>(this string value)
            where T : struct, IComparable, IFormattable, IConvertible
        {
            return ToEnum<T>(value, false);
        }

        /// <summary>
        /// Retrieve a typed Enum from a String value.
        /// </summary>
        /// <typeparam name="T">The Enum type to parse to.</typeparam>
        /// <param name="value">The String value.</param>
        /// <param name="ignoreCase">If true, ignore case; otherwise, regard case.</param>
        /// <returns>Typed Enum instance for the specified String value.</returns>
        public static T ToEnum<T>(this string value, bool ignoreCase)
            where T : struct, IComparable, IFormattable, IConvertible
        {
            if (String.IsNullOrEmpty(value))
            {
                throw new ArgumentException("Can't parse an empty string", "value");
            }
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new InvalidOperationException("T must be an enumerated type.");
            }

            // Convert value to PascalCase
            value = value.ToPascalCase();

            // Warning, can throw ArgumentExceptions
            return (T)Enum.Parse(enumType, value, ignoreCase);
        }

        /// <summary>
        /// Convert enum from one type to an other based on the string representation of the source enum
        /// </summary>
        /// <typeparam name="T">Target enum type</typeparam>
        /// <param name="sourceEnum">The source enum value to convert</param>
        /// <param name="defaultValue">The default value when the source enum does not exists in the target enum</param>
        /// <returns>The converted enum value</returns>
        /// <exception cref="InvalidOperationException">Thrown when target type is not an Enum</exception>
        public static T ToEnum<T>(this Enum sourceEnum, T defaultValue) where T : struct, Enum
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new InvalidOperationException("T must be an enumerated type.");
            }

            return Enum.TryParse(sourceEnum.ToString(), true, out T targetEnum) ? targetEnum : defaultValue;
        }
    }
}
