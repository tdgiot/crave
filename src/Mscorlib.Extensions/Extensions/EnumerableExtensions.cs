﻿using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace System
{
    /// <summary>
    /// Enumerable extension methods
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Replace the value in an <see cref="IEnumerable{T}"/> with an other.
        /// </summary>
        /// <typeparam name="T">Typed value of list contents</typeparam>
        /// <param name="collection">Collection in which to replace values</param>
        /// <param name="source">Value to repalce</param>
        /// <param name="replacement">Replacement value</param>
        /// <returns><see cref="IEnumerable{T}"/> collection with the value replaced.</returns>
        public static IEnumerable<T> Replace<T>(this IEnumerable<T> collection, T source, T replacement) where T : class
        {
            IEnumerable<T> collectionWithout = collection;
            if (source != null)
            {
                collectionWithout = collectionWithout.Except(new[] { source });
            }
            return collectionWithout.Union(new[] { replacement });
        }

        /// <summary>
        /// Checks whether the collection is null or empty.
        /// </summary>
        /// <typeparam name="T">Typed value of list contents</typeparam>
        /// <param name="collection">Collection to check</param>
        /// <returns>True when collection is null or empty, otherwise false.</returns>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> collection)
        {
            return (collection == null || !collection.Any());
        }

        /// <summary>
        /// Checks whether the collection is null or empty.
        /// </summary>
        /// <typeparam name="T">Typed value of list contents</typeparam>
        /// <param name="collection">Collection to check</param>
        /// <returns>True when collection is null or empty, otherwise false.</returns>
        public static bool IsNullOrEmpty<T>(this ICollection<T> collection)
        {
            return (collection == null || collection.Count == 0);
        }

        /// <summary>
        /// Convert collection to string with a separator between each item.
        /// </summary>
        /// <typeparam name="T">Typed value of list contents</typeparam>
        /// <param name="collection">Collection to use</param>
        /// <param name="separator">Separator to place in between each item</param>
        /// <returns>String representation of the collection</returns>
        public static string ToSeparatedString<T>(this IEnumerable<T> collection, string separator)
        {
            string separatedString = string.Empty;

            foreach (T item in collection)
            {
                if (!separatedString.IsNullOrWhiteSpace())
                {
                    separatedString += separator;
                }

                separatedString += item.ToString();
            }

            return separatedString;
        }

        /// <summary>
        /// Create an ordered list using a custom function
        /// </summary>
        /// <typeparam name="T">Typed value of list contents</typeparam>
        /// <typeparam name="TKey">Typed value of the key on which to sort</typeparam>
        /// <param name="collection">Collection to order</param>
        /// <param name="keySelector">A function to select a key from an element</param>
        /// <returns>An <see cref="IOrderedEnumerable{T}"/> whose elements are sorted according to a key.</returns>
        public static IOrderedEnumerable<T> OrderByWithNullCheck<T, TKey>(this IEnumerable<T> collection, Func<T, TKey> keySelector)
        {
            List<T> collectionAsList = collection?.ToList() ?? new List<T>(0);
            IOrderedEnumerable<T> orderedCollection = collectionAsList.OrderBy(keySelector);

            return orderedCollection;
        } 
    }
}