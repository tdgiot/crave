﻿ // ReSharper disable once CheckNamespace
namespace System.Reflection
{
    /// <summary>
    /// Guid extension methods
    /// </summary>
    public static class GuidExtensions
    {
        /// <summary>
        /// Creates a guid with or without dashes.
        /// </summary>
        /// <param name="guid">Guid to use</param>        
        /// <returns>String object</returns>
        public static string WithoutDashes(this Guid guid)
        {
            return guid.ToString().Replace("-", string.Empty);
        }
    }
}
