﻿// ReSharper disable once CheckNamespace
namespace System
{
    /// <summary>
    /// Long extension methods
    /// </summary>
    public static class LongExtensions
    {
        /// <summary>
        /// Create a <see cref="DateTime"/> instance from Unix timestamp.
        /// </summary>
        /// <param name="unixTime">Unix timestamp in seconds</param>
        /// <returns>Timestamp as <see cref="DateTime"/></returns>
        public static DateTime FromUnixTime(this long unixTime)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        /// <summary>
        /// Create a <see cref="DateTime"/> instance from a Unix timestamp in milliseconds.
        /// </summary>
        /// <param name="unixTime">Unix timestamp in milliseconds</param>
        /// <returns>Timestamp as <see cref="DateTime"/></returns>
        public static DateTime FromUnixTimeMillis(this long unixTime)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddMilliseconds(unixTime);
        }

        /// <summary>
        /// Create a <see cref="DateTime"/> instance from a Unix timestamp in milliseconds.
        /// </summary>
        /// <param name="unixTime">Unix timestamp in milliseconds</param>
        /// <param name="returnNullIfEmpty">Returns null if unixTime is equal or less than 0</param>
        /// <returns>Timestamp as <see cref="DateTime"/></returns>
        public static DateTime? FromUnixTimeMillis(this long unixTime, bool returnNullIfEmpty)
        {
            return unixTime > 0 ? unixTime.FromUnixTimeMillis() : (DateTime?)null;
        }
    }
}
