﻿ // ReSharper disable once CheckNamespace
namespace System
{
    /// <summary>
    /// Object extension methods
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Throws an <see cref="NullReferenceException"/> when specified object is null.
        /// </summary>
        /// <typeparam name="T">Typed value of value parameter.</typeparam>
        /// <param name="value">The value to test.</param>
        /// <param name="message">The message to add to the exception.</param>
        /// <returns>Value parameter when not null.</returns>
        /// <exception cref="NullReferenceException"></exception>
        public static T ThrowIfNull<T>(this T value, string message = null)
        {
            if (value == null)
            {
                if (message.IsNullOrWhiteSpace())
                {
                    message = $"Parameter is null. Name: {nameof(value)}";
                }
                throw new NullReferenceException(message);
            }

            return value;
        }

        /// <summary>
        /// Throws an <see cref="ArgumentNullException"/> when specified object is null
        /// </summary>
        /// <typeparam name="T">Typed value of value parameter</typeparam>
        /// <param name="value">The value to test</param>
        /// <param name="message">The message to add to the exception.</param>
        /// <returns>Value parameter when not null.</returns>
        public static T ThrowIfArgumentNull<T>(this T value, string message = null)
        {
            if (value != null)
                return value;

            if (message.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException(nameof(value));
            }

            throw new ArgumentNullException(nameof(value), message);
        }

        /// <summary>
        /// If true, the object is numeric.
        /// </summary>
        /// <param name="obj">The object to check for a numeric value.</param>
        /// <returns>True when the object is numeric.</returns>
        public static bool IsNumeric(this object obj)
        {
            switch (Type.GetTypeCode(obj.GetType()))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// If true, the object is a boolean.
        /// </summary>
        /// <param name="obj">The object to check for a boolean value.</param>
        /// <returns>True when the object is a boolean.</returns>
        public static bool IsBoolean(this object obj)
        {
            switch (Type.GetTypeCode(obj.GetType()))
            {
                case TypeCode.Boolean:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// If true, the object is a datetime.
        /// </summary>
        /// <param name="obj">The object to check for a datetime.</param>
        /// <returns>True when the object is a datetime.</returns>
        public static bool IsDateTime(this object obj)
        {
            switch (Type.GetTypeCode(obj.GetType()))
            {
                case TypeCode.DateTime:
                    return true;
                default:
                    return false;
            }
        }
    }
}
