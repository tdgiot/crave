﻿using System.Reflection;

// ReSharper disable once CheckNamespace
namespace System
{
    /// <summary>
    /// Reflection extension methods
    /// </summary>
    public static class ReflectionExtensions
    {
        /// <summary>
        /// Check if a property exist on a type
        /// </summary>
        /// <param name="type">Type on which to look</param>
        /// <param name="propertyName">Name of the property to find</param>
        /// <returns>True when property exists on type, otherwise false</returns>
        public static bool PropertyExists(this Type type, string propertyName)
        {
            if (type == null || propertyName == null)
            {
                return false;
            }

            PropertyInfo property = type.GetProperty(propertyName,
                                                     BindingFlags.NonPublic |
                                                     BindingFlags.Public |
                                                     BindingFlags.Static |
                                                     BindingFlags.Instance);
            if (property == null)
            {
                return false;
            }

            MethodInfo getter = property.GetGetMethod(true);

            return getter.IsPublic || getter.IsAssembly || getter.IsFamilyOrAssembly;
        }
    }
}