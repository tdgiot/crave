﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Mscorlib.Extensions.Extensions
{
    /// <summary>
    /// Stream extension methods
    /// </summary>
    public static class StreamExtensions
    {
        /// <summary>
        /// Write string to stream
        /// </summary>
        /// <param name="stream">Stream to write to</param>
        /// <param name="text">The text to write</param>
        public static void Write(this Stream stream, string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            stream.Write(bytes, 0, bytes.Length);
        }

        /// <summary>
        /// Compute MD5 hash from stream.
        /// </summary>
        /// <param name="stream">Stream to compute the MD5 hash from.</param>
        /// <returns>The computed MD5 hash of this stream.</returns>
        public static string ComputeMD5Hash(this Stream stream)
        {
            HashAlgorithm hasher = new MD5CryptoServiceProvider();
            hasher.Initialize();

            byte[] md5HashBytes = hasher.ComputeHash(stream);
            string md5Hash = BitConverter.ToString(md5HashBytes);
            
            return md5Hash.Replace("-", string.Empty);
        }

        /// <summary>
        /// Get the streams size in kilobytes.
        /// </summary>
        /// <param name="stream">Stream to get the size from.</param>
        /// <returns>The streams size in kilobytes.</returns>
        public static int SizeInKb(this Stream stream)
        {
            return Convert.ToInt32(stream.Length / 1024);
        }
    }
}