﻿ // ReSharper disable once CheckNamespace
namespace System.Text
{
    /// <summary>
    /// StringBuilder extension methods
    /// </summary>
    public static class StringBuilderExtensions
    {
        /// <summary>
        /// Appends a copy of the specified string followed by the default line terminator to the end of the current <see cref="StringBuilder" /> object.
        /// </summary>
        /// <param name="stringBuilder"></param>
        /// <param name="format">The string to append.</param>
        /// <param name="args">Optional arguments for string formatting</param>
        /// <returns>A reference to this instance after the append operation has completed.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Enlarging the value of this instance would exceed <see cref="StringBuilder.MaxCapacity" />.</exception>
        public static StringBuilder AppendLine(this StringBuilder stringBuilder, string format, params object[] args)
        {
            return stringBuilder.AppendLine(string.Format(format, args));
        }

        /// <summary>
        /// Appends the formatted line.
        /// </summary>
        /// <param name="sb">The StringBuilder.</param>
        /// <param name="format">The format.</param>
        /// <param name="args">The args.</param>
        /// <returns>The StringBuilder.</returns>
        public static StringBuilder AppendFormatLine(this StringBuilder sb, string format, params object[] args)
        {
            return sb.Append(format.FormatSafe(args)).AppendLine();
        }
    }
}
