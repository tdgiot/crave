﻿using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

// ReSharper disable once CheckNamespace

namespace System
{
    /// <summary>
    /// String extension methods
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Indicates whether a specified string is null, or consists only of white-space characters.
        /// </summary>
        /// <param name="value">The string to test</param>
        /// <returns>true if the value parameter is null or <see cref="String.Empty" />, or if value consists exclusively of white-space characters.</returns>
        public static bool IsNullOrWhiteSpace(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }

        /// <summary>
        /// Throws a <see cref="ArgumentNullException" /> if the specified strign is null, or consists only of white-space characters.
        /// </summary>
        /// <param name="value">The string to test</param>
        /// <exception cref="ArgumentNullException">The specified strign is null, or consists only of white-space characters</exception>
        public static void ThrowIfArgumentNullOrWhiteSpace(this string value)
        {
            if (value.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException(nameof(value));
            }
        }

        /// <summary>
        /// Throws a <see cref="NullReferenceException" /> if the specified strign is null, or consists only of white-space characters.
        /// </summary>
        /// <param name="value">The string to test</param>
        /// <returns>The passed string parameters if not null, <see cref="String.Empty" /> or if value consists exclusively of white-space characters.</returns>
        /// <exception cref="NullReferenceException">The specified strign is null, or consists only of white-space characters</exception>
        public static string ThrowIfNullOrWhiteSpace(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new NullReferenceException(nameof(value));
            }

            return value;
        }

        /// <summary>
        /// Replace all accent characters to normal ones: Åäö to Aao.
        /// </summary>
        /// <param name="input">The string to replace all diacritics in.</param>
        /// <returns>A <see cref="string"/> instance without diacritics.</returns>
        public static string ReplaceDiacritics(this string input)
        {
            string normalizedString = input.Normalize(NormalizationForm.FormD);
            StringBuilder stringBuilder = new StringBuilder(normalizedString.Length);

            foreach (char c in normalizedString)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Adds a whitespace in front of each uppercase character.
        /// </summary>
        /// <param name="input">The text to split on each uppercase.</param>
        /// <returns>The input string seperated with whitespaces on each uppercase character.</returns>
        public static string AddWhitespaceBeforeUpperCase(this string input)
        {
            Regex regex = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            return regex.Replace(input, " ");
        }

        /// <summary>
		/// Replace all linebreaks with a definable string
		/// </summary>
		/// <param name="input">Text containing linebreaks</param>
		/// <param name="replaceWith">String to replace the linebreaks with</param>
		/// <returns>Linkbreaks replaced by br-tags</returns>
		public static string ReplaceLineBreaks(this string input, string replaceWith)
        {
            return Regex.Replace(input, "(\r\n|\r|\n)", replaceWith);
        }

        /// <summary>
		/// Replaces all occurrences of a specified System.String in this instance, with another specified System.String.
		/// </summary>
		/// <param name="original">The original System.String object.</param>
		/// <param name="oldValue">A System.String to be replaced.</param>
		/// <param name="newValue">A System.String to replace all occurrences of oldValue.</param>
		/// <param name="comparisionType">One of the System.StringComparison values.</param>
		/// <returns>A System.String equivalent to this instance but with all instances of oldValue replaced with newValue.</returns>
		public static string Replace(this string original, string oldValue, string newValue, StringComparison comparisionType)
        {
            if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(oldValue))
            {
                int index = -1;
                int lastIndex = 0;

                StringBuilder buffer = new StringBuilder(original.Length);
                while ((index = original.IndexOf(oldValue, index + 1, comparisionType)) >= 0)
                {
                    buffer.Append(original, lastIndex, index - lastIndex);
                    buffer.Append(newValue);
                    lastIndex = index + oldValue.Length;
                }
                buffer.Append(original, lastIndex, original.Length - lastIndex);

                original = buffer.ToString();
            }

            return original;
        }

        /// <summary>
        /// Check if a string contains a certain part of another string.
        /// </summary>
        /// <param name="text">The original string to check.</param>
        /// <param name="compare">The part the original string should contain.</param>
        /// <returns>Returns true if the original string contains the compare string.</returns>
        public static bool ContainsCaseInsensitive(this string text, string compare)
        {
            return text.ToLower().Contains(compare.ToLower());
        }

        /// <summary>
        /// Converts a string to a base64 encoded string.
        /// </summary>
        /// <param name="string"></param>
        /// <returns></returns>
        public static string ConvertStringToBase64(this string @string)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(@string);
            return Convert.ToBase64String(byteArray);
        }

        /// <summary>
        /// Converts a base64 encoded string to a string.
        /// </summary>
        /// <param name="string">The base 64 string.</param>
        /// <returns>
        /// The base64 encoded string converted to a human readable string.
        /// </returns>
        public static string ConvertBase64ToString(this string @string)
        {
            byte[] byteArray = Convert.FromBase64String(@string);
            return Encoding.UTF8.GetString(byteArray);
        }

        /// <summary>
        /// Converts the first character of a string into it's capitalized version.
        /// </summary>
        /// <param name="string">The original string which needs to be capitalized.</param>
        /// <returns>The original string but with the first character capitalized.</returns>
        public static string CapitalizeFirstCharacter(this string @string)
        {
            if (@string == null)
            {
                return null;
            }

            if (@string.Length > 1)
            {
                return char.ToUpper(@string[0]) + @string.Substring(1);
            }

            return @string.ToUpper();
        }

        /// <summary>
        /// Remove all line terminators from the string.
        /// https://en.wikipedia.org/wiki/Newline#Unicode
        /// </summary>
        /// <param name="string">The string to remove the line terminators from.</param>
        /// <returns>
        /// The string cleaned from line terminators.
        /// </returns>
        public static string RemoveLineTerminators(this string @string)
        {
            return Regex.Replace(@string, @"[\u000A\u000B\u000C\u000D\u2028\u2029\u0085]+", string.Empty);
        }

        /// <summary>
        /// Gets the first occurance of an integer from the string.
        /// </summary>
        /// <param name="string">The string to get the integer from.</param>
        /// <returns>
        /// The first integer from the string, null when no integer could be found.
        /// </returns>
        public static int? GetInteger(this string @string)
        {
            string value = Regex.Match(@string, @"\d+").Value;

            if (value.IsNullOrWhiteSpace()) { return null; }

            return Convert.ToInt32(value);
        }

        /// <summary>
		/// Gets all characters before first occurence of of the specified value.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="valueToSearchFor">The value to search for.</param>
		/// <param name="returnEmptyString">If set to <c>true</c> returns an empty string if not found.</param>
		/// <returns>
		/// The characters before the first occurence of the specified value.
		/// </returns>
		public static string GetAllBeforeFirstOccurenceOf(this string input, string valueToSearchFor, bool returnEmptyString)
        {
            int indexOfSearched = input.IndexOf(valueToSearchFor, StringComparison.Ordinal);

            if (indexOfSearched > -1)
            {
                return input.Substring(0, indexOfSearched);
            }
            
            return returnEmptyString ? string.Empty : input;
        }

        /// <summary>
        /// Format a string using string.Format but with a fall back, if the arguments don't fit the format it won't throw an Exception
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="args">Arguments</param>
        /// <returns>
        /// The string.Format result, if it fails it concats the format with the arguments and prefixes it with: Invalid String Format:
        /// </returns>
        public static string FormatSafe(this string format, params object[] args)
        {
            string formattedText;

            try
            {
                formattedText = args.Length > 0 ? string.Format(format, args) : format;
            }
            catch
            {
                formattedText = $"{format} ({string.Join(", ", args)})";
            }

            return formattedText;
        }

        #region Splitting methods

        /// <summary>
        /// Add whitepace to specified string before each upper-case character
        /// </summary>
        /// <param name="source">The string to split</param>
        /// <returns>Source parameter with white-space before each upper-case character</returns>
        public static string SplitUpperCaseToString(this string source)
        {
            return source == null ? null : string.Join(" ", source.SplitUpperCase());
        }

        /// <summary>
        /// Split specified split on each upper-case character
        /// </summary>
        /// <param name="source">The string to split</param>
        /// <returns>String array of each individual piece of string</returns>
        public static string[] SplitUpperCase(this string source)
        {
            if (source == null)
            {
                return new string[] {}; //Return empty array.
            }

            if (source.Length == 0)
            {
                return new[] {""};
            }

            StringCollection words = new StringCollection();
            int wordStartIndex = 0;

            char[] letters = source.ToCharArray();
            char previousChar = char.MinValue;
            // Skip the first letter. we don't care what case it is.
            for (int i = 1; i < letters.Length; i++)
            {
                if (char.IsUpper(letters[i]) && !char.IsWhiteSpace(previousChar))
                {
                    //Grab everything before the current index.
                    words.Add(new string(letters, wordStartIndex, i - wordStartIndex));
                    wordStartIndex = i;
                }
                previousChar = letters[i];
            }
            //We need to have the last word.
            words.Add(new string(letters, wordStartIndex, letters.Length - wordStartIndex));

            //Copy to a string array.
            string[] wordArray = new string[words.Count];
            words.CopyTo(wordArray, 0);
            return wordArray;
        }

        /// <summary>
		/// Split a string in to it's seperate lines
		/// </summary>
		/// <param name="input">Text containing linebreaks</param>
		/// <param name="options">The options.</param>
		/// <returns>String Array with the lines</returns>
		public static string[] SplitLines(this string input, StringSplitOptions options = StringSplitOptions.None)
        {
            return input.Split(new [] { "\r\n", "\r", "\n" }, options);
        }

        #endregion

        #region Casing methods

        /// <summary>
        /// Make whole string lower and make first letter upper
        /// </summary>
        /// <param name="input">String to have the first letter capitalized for</param>
        /// <param name="applyToLower">Turn the rest of the string to lower, default is true</param>
        public static string TurnFirstToUpper(this string input, bool applyToLower = true)
        {
            if (input.Length <= 1)
            {
                return input.ToUpper();
            }

            string temp = input.Substring(0, 1);

            if (applyToLower)
            {
                temp = temp.ToUpper() + input.ToLower().Remove(0, 1);
            }
            else
            {
                temp = temp.ToUpper() + input.Remove(0, 1);
            }

            return temp;
        }

        /// <summary>
        /// Converts a String to PascalCase (eg. "pascal case" and "pascal-case" become "PascalCase").
        /// </summary>
        /// <param name="input">The String to convert.</param>
        /// <returns>The converted String.</returns>
        public static string ToPascalCase(this string input)
        {
            return Regex.Replace(input,
                                 @"(^|\W)(?<Char>\w)",
                                 m => m.Groups["Char"].Value.ToUpperInvariant(),
                                 RegexOptions.ExplicitCapture | RegexOptions.Singleline);
        }

        /// <summary>
        /// Make the first two characters lower
        /// </summary>
        /// <param name="input">String to have the first two letters lower for</param>
        /// <param name="applyToLower">Turn the rest of the string to Upper</param>
        public static string TurnFirstTwoToLower(this string input, bool applyToLower)
        {
            if (input.Length <= 2)
            {
                return input.ToLower();
            }
            else
            {
                string temp = input.Substring(0, 2);

                if (applyToLower)
                {
                    temp = temp.ToLower() + input.ToUpper().Remove(0, 2);
                }
                else
                {
                    temp = temp.ToLower() + input.Remove(0, 2);
                }

                return temp;
            }
        }

        /// <summary>
        /// Make make first character lower
        /// </summary>
        /// <param name="input">String to have the first letter capitalized for</param>
        /// <param name="applyToLower">Turn the rest of the string to Upper</param>
        public static string TurnFirstToLower(this string input, bool applyToLower)
        {
            if (input.Length <= 1)
            {
                return input.ToLower();
            }
            else
            {
                string temp = input.Substring(0, 1);

                if (applyToLower)
                {
                    temp = temp.ToLower() + input.ToUpper().Remove(0, 1);
                }
                else
                {
                    temp = temp.ToLower() + input.Remove(0, 1);
                }

                return temp;
            }
        }

        /// <summary>
        /// Serializes an object to a xml string.
        /// </summary>
        /// <typeparam name="T">The type of object to serialize.</typeparam>
        /// <param name="obj">The object to serialize.</param>
        /// <returns>The serialized object as string.</returns>
        public static string ToXml<T>(this T obj)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StringWriter textWriter = new StringWriter())
            {
                serializer.Serialize(textWriter, obj);
                return textWriter.ToString();
            }
        }

        /// <summary>
        /// Deserializes a string to an object.
        /// </summary>
        /// <typeparam name="T">The type of object to deserialize to.</typeparam>
        /// <param name="xml">The xml to deserialize.</param>
        /// <returns>The deserialized object.</returns>
        public static T FromXml<T>(this string xml)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            using (StringReader textReader = new StringReader(xml))
            {
                return (T)xmlSerializer.Deserialize(textReader);
            }
        }

        #endregion
    }
}