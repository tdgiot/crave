﻿using System;
using System.Collections.Specialized;
using System.Web;

namespace Mscorlib.Extensions.Extensions
{
    /// <summary>
    /// Uri extension methods
    /// </summary>
    public static class UriExtensions
    {
        /// <summary>
        /// Adds the query value to the url.
        /// </summary>
        /// <param name="originalUri">The original URI.</param>
        /// <param name="queryString">The query string.</param>
        /// <returns>A new Uri instance with the query string added.</returns>
        public static Uri AddQueryString(this Uri originalUri, string queryString)
        {
            UriBuilder uriBuilder = new UriBuilder(originalUri) {Query = queryString };
            originalUri = uriBuilder.Uri;
            return originalUri;
        }

        /// <summary>
        /// Adds the query string.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <returns>A new Uri instance with the query string added.</returns>
        public static Uri AddQueryString(this Uri uri, string name, object value)
        {
            return uri.AddQueryString(name, value, false);
        }

        /// <summary>
        /// Adds the query string.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="replace">If set to <c>true</c> replaces existing values.</param>
        /// <returns>A new Uri instance with the query string added.</returns>
        public static Uri AddQueryString(this Uri uri, string name, object value, bool replace)
        {
            return uri.AddQueryString(new NameValueCollection {{name, Convert.ToString(value)}}, replace);
        }

        /// <summary>
        /// Adds the query string.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="queryString">The query string.</param>
        /// <returns>A new Uri instance with the query string added.</returns>
        public static Uri AddQueryString(this Uri uri, NameValueCollection queryString)
        {
            return uri.AddQueryString(queryString, false);
        }

        /// <summary>
        /// Adds the query string.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="queryString">The query string.</param>
        /// <param name="replace">If set to <c>true</c> replaces existing values.</param>
        /// <returns>A new Uri instance with the query string added.</returns>
        public static Uri AddQueryString(this Uri uri, NameValueCollection queryString, bool replace)
        {
            return new UriBuilder(uri).AddQueryString(queryString, replace).Uri;
        }

        /// <summary>
        /// Adds the query string.
        /// </summary>
        /// <param name="uriBuilder">The URI builder.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <returns>A new UriBuilder instance with the query string added.</returns>
        public static UriBuilder AddQueryString(this UriBuilder uriBuilder, string name, string value)
        {
            return uriBuilder.AddQueryString(name, value, false);
        }

        /// <summary>
        /// Adds the query string.
        /// </summary>
        /// <param name="uriBuilder">The URI builder.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="replace">If set to <c>true</c> replaces existing values.</param>
        /// <returns>A new UriBuilder instance with the query string added.</returns>
        public static UriBuilder AddQueryString(this UriBuilder uriBuilder, string name, string value, bool replace)
        {
            return uriBuilder.AddQueryString(new NameValueCollection {{name, value}}, replace);
        }

        /// <summary>
        /// Adds the query string.
        /// </summary>
        /// <param name="uriBuilder">The URI builder.</param>
        /// <param name="queryString">The query string.</param>
        /// <returns>A new UriBuilder instance with the query string added.</returns>
        public static UriBuilder AddQueryString(this UriBuilder uriBuilder, NameValueCollection queryString)
        {
            return uriBuilder.AddQueryString(queryString, false);
        }

        /// <summary>
        /// Adds the query string.
        /// </summary>
        /// <param name="uriBuilder">The URI builder.</param>
        /// <param name="queryString">The query string.</param>
        /// <param name="replace">If set to <c>true</c> replaces existing values.</param>
        /// <returns>A new UriBuilder instance with the query string added.</returns>
        public static UriBuilder AddQueryString(this UriBuilder uriBuilder, NameValueCollection queryString, bool replace)
        {
            UriBuilder newUri = new UriBuilder(uriBuilder.Uri);
            NameValueCollection newQueryString = new NameValueCollection();
            if (!newUri.Query.IsNullOrWhiteSpace() && newUri.Query.Length > 1)
            {
                newQueryString.Add(HttpUtility.ParseQueryString(newUri.Query.Substring(1)));
                newUri.Query = string.Empty;
            }

            // Replace or add values
            if (replace)
            {
                foreach (string key in queryString.AllKeys)
                {
                    newQueryString.Set(key, queryString[key]);
                }
            }
            else
            {
                newQueryString.Add(queryString);
            }

            if (newQueryString.Count <= 0)
            {
                return newUri;
            }

            // Construct new query string
            string newQuery = string.Empty;
            foreach (string key in newQueryString.AllKeys)
            {
                if (!newQuery.IsNullOrWhiteSpace())
                {
                    newQuery += "&";
                }
                newQuery += $"{key}={Uri.EscapeDataString(newQueryString[key])}";
            }
            newUri.Query = newQuery;

            return newUri;
        }

        /// <summary>
        /// Adds the segment value to the url.
        /// </summary>
        /// <param name="originalUri">The original URI.</param>
        /// <param name="segment">The segment .</param>
        /// <returns>Uri</returns>
        public static Uri AddSegment(this Uri originalUri, string segment)
        {
            originalUri = new Uri(originalUri.OriginalString + "/" + segment);
            return originalUri;
        }
    }
}