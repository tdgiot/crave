﻿using System.Reflection;

// ReSharper disable once CheckNamespace
namespace System
{
    /// <summary>
    /// Activator util for creating new instances of objects by name
    /// </summary>
    public static class ActivatorUtil
    {
        /// <summary>
        /// Create an instance of <typeparamref name="T"/> by typename
        /// </summary>
        /// <typeparam name="T">Type value</typeparam>
        /// <param name="typeName">The name of the type</param>
        /// <param name="assembly">The assembly in which the type exists</param>
        /// <returns>New instance of type <typeparamref name="T"/>.</returns>
        public static T CreateInstance<T>(string typeName, Assembly assembly) where T : class
        {
            return ActivatorUtil.CreateInstance(typeName, assembly) as T;
        }

        private static object CreateInstance(string typeName, Assembly assembly)
        {
            object instance = null;

            Type type = assembly.GetType(typeName);
            if (type != null)
            {
                instance = Activator.CreateInstance(type);
            }

            return instance;
        }
    }
}
