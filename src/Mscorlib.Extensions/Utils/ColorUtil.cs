﻿namespace System
{
    /// <summary>
    /// Util to help with color codes from the legacy codebase.
    /// </summary>
    public static class ColorUtil
    {
        /// <summary>
        /// Convert a legacy rgba integer to a hexadecimal color code. 
        /// </summary>
        /// <param name="rgba">Rgba color integer.</param>
        /// <returns>A hexadecimal color code.</returns>
        public static string RgbaToHex(int rgba)
        {
            byte red = (byte)(rgba >> 16);
            byte green = (byte)(rgba >> 8);
            byte blue = (byte)rgba;

            return $"#{red:X2}{green:X2}{blue:X2}";
        }
    }
}
