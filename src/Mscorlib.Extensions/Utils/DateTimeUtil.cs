﻿ // ReSharper disable once CheckNamespace

using System.Collections.Generic;
using System.Linq;

namespace System
{
    /// <summary>
    /// DateTime utility class
    /// </summary>
    public static class DateTimeUtil
    {
        /// <summary>
        /// Get the <see cref="TimeSpan"/> between two dates
        /// </summary>
        /// <param name="date1">The first date</param>
        /// <param name="date2">The second date</param>
        /// <returns>A <see cref="TimeSpan"/> which represents the difference between two dates.</returns>
        public static TimeSpan GetSpanBetweenDateTimes(DateTime date1, DateTime date2)
        {
            return ((date1 > date2) ? (date1 - date2) : (date2 - date1));
        }

        /// <summary>
        /// Convert a unix timestamp to a datetime object.
        /// </summary>
        /// <param name="timestamp">The unix timestamp to convert into a datetime object.</param>
        /// <returns>A <see cref="DateTime"/> representation of the unix timestamp.</returns>
        public static DateTime FromUnixTimestamp(long timestamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

            // Check for miliseconds or seconds
            int timestampLength = timestamp.ToString().Length;
            if (timestampLength >= 13)
            {
                dtDateTime = dtDateTime.AddMilliseconds(timestamp);
            }
            else
            {
                dtDateTime = dtDateTime.AddSeconds(timestamp);
            }

            return dtDateTime;
        }

        /// <summary>
        /// Convert a unix timestamp to a datetime object.
        /// </summary>
        /// <param name="timestamp">The unix timestamp to convert into a datetime object.</param>
        /// <returns>A <see cref="DateTime"/> representation of the unix timestamp.</returns>
        public static DateTime FromUnixTimestamp(string timestamp) => FromUnixTimestamp(Convert.ToInt64(timestamp));

        /// <summary>
        /// Gets the date time for time string.
        /// </summary>
        /// <param name="time">The time string.</param>
        /// <returns>A <see cref="DateTime"/> instance.</returns>
        public static DateTime GetDateTimeForTimeString(string time)
        {
            int hours = Convert.ToInt32(time.Substring(0, 2));
            int minutes = Convert.ToInt32(time.Substring(2, 2));

            DateTime toReturn = new DateTime(2000, 1, 1, hours, minutes, 0);

            return toReturn;
        }

        /// <summary>
        /// Return a DateTime with Date of the input value and Time 00:00:00
        /// </summary>
        /// <param name="date">DateTime to take Date part From</param>
        /// <returns>DateTime with time 00:00:00</returns>
        public static DateTime MakeBeginOfDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
        }

        /// <summary>
        /// Return a DateTime with Date of the input value and Time 23:59:59
        /// </summary>
        /// <param name="date">DateTime to take Date part From</param>
        /// <returns>DateTime with time 23:59:59</returns>
        public static DateTime MakeEndOfDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }

        /// <summary>
        /// Returns a DateTime with the Date set to the first day of the month.
        /// </summary>
        /// <param name="date">DateTime to take the date from.</param>
        /// <returns>A <see cref="DateTime"/> set to the first day of the month.</returns>
        public static DateTime MakeBeginOfMonth(this DateTime date)
        {
            return date.AddDays(1 - DateTime.Today.Day);
        }

        /// <summary>
        /// Returns a DateTime with the Date set to the last day of the month.
        /// </summary>
        /// <param name="date">DateTime to take the date from.</param>
        /// <returns>A <see cref="DateTime"/> set to the last day of the month.</returns>
        public static DateTime MakeEndOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }

        /// <summary>
        /// Get First Day Of Week (FIRST DAY OF WEEK MONDAY)
        /// </summary>
        /// <param name="date">Date to look for</param>
        /// <returns>First day of the week the date is part of</returns>
        public static DateTime GetFirstDayOfWeek(this DateTime date)
        {
            DateTime firstDay;

            if ((int) date.DayOfWeek == 0)
            {
                firstDay = date.AddDays(-6);
            }
            else
            {
                firstDay = date.AddDays(-1 * ((int)date.DayOfWeek - 1));
            }

            return firstDay.Date;
        }

        /// <summary>
        /// Get first day of month
        /// </summary>
        /// <param name="date">date</param>
        /// <returns>First date of month</returns>
        public static DateTime GetFirstDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        /// <summary>
        /// Get the next day of week after base date
        /// </summary>
        /// <param name="baseDateTime">The base date time, mostly the current date</param>
        /// <param name="dayOfWeek">The day of week we're looking for</param>
        /// <returns>New DateTime object set to the next date</returns>
        public static DateTime GetNextDayOfWeek(DateTime baseDateTime, DayOfWeek dayOfWeek)
        {
            // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            int daysToAdd = ((int)dayOfWeek - (int)baseDateTime.DayOfWeek + 7) % 7;
            return baseDateTime.AddDays(daysToAdd);
        }

        /// <summary>
        /// Gets a collection of all dates between a start and end date.
        /// </summary>
        /// <param name="start">The start datetime.</param>
        /// <param name="end">The end datetime.</param>
        /// <returns>A collection of all dates between two dates.</returns>
        public static IEnumerable<DateTime> GetAllDatesBetween(DateTime start, DateTime end)
        {
            return Enumerable.Range(0, 1 + end.Subtract(start).Days)
                .Select(offset => start.AddDays(offset))
                .ToArray();
        }
    }
}
