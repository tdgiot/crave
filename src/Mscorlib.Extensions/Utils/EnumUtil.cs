﻿using System.Collections.Generic;

namespace System
{
    public static class EnumUtil
    {
        /// <summary>
        /// Converts the integer representation of an enum to its enum equivalent. A return value indicates whether the conversion succeeded.
        /// </summary>
        /// <typeparam name="T">The type of the enumeration.</typeparam>
        /// <param name="value">A integer containing an enum to convert.</param>
        /// <param name="result">When this method returns, contains the enum value equivalent to the integer contained in value, if the conversion succeeded; otherwise contains the default value.</param>
        /// <returns>true if value was converted successfully; otherwise, false.</returns>
        public static bool TryParse<T>(int value, out T result)
            where T : struct, IComparable, IFormattable, IConvertible
        {
            return TryParse(value.ToString(), false, out result);
        }

        /// <summary>
        /// Converts the string representation of an enum to its enum equivalent. A return value indicates whether the conversion succeeded.
        /// </summary>
        /// <typeparam name="T">The type of the enumeration.</typeparam>
        /// <param name="value">A string containing an enum to convert.</param>
        /// <param name="result">When this method returns, contains the enum value equivalent to the string contained in value, if the conversion succeeded; otherwise contains the default value.</param>
        /// <returns>true if value was converted successfully; otherwise, false.</returns>
        public static bool TryParse<T>(string value, out T result)
            where T : struct, IComparable, IFormattable, IConvertible
        {
            return TryParse(value, false, out result);
        }

        /// <summary>
        /// Converts the string representation of an enum to its enum equivalent. A return value indicates whether the conversion succeeded.
        /// </summary>
        /// <typeparam name="T">The type of the enumeration.</typeparam>
        /// <param name="value">A string containing an enum to convert.</param>
        /// <param name="ignoreCase">If true, ignore case; otherwise, regard case.</param>
        /// <param name="result">When this method returns, contains the enum value equivalent to the string contained in value, if the conversion succeeded; otherwise contains the default value.</param>
        /// <returns>true if value was converted successfully; otherwise, false.</returns>
        public static bool TryParse<T>(string value, bool ignoreCase, out T result)
            where T : struct, IComparable, IFormattable, IConvertible
        {
            try
            {
                result = value.ToEnum<T>(ignoreCase);
                return true;
            }
            catch
            {
                result = default(T);
                return false;
            }
        }

        /// <summary>
        /// Gets a enumeration of the display names of an Enum.
        /// </summary>
        /// <typeparam name="TEnum">The enum to get the display names from.</typeparam>
        /// <returns>A <see cref="IEnumerable{T}"/> with the display names for this enum.</returns>
        public static IEnumerable<string> GetDisplayNames<TEnum>()
        {
            Type type = typeof(TEnum);
            Array values = Enum.GetValues(type);

            foreach (Enum value in values)
            {
                yield return value.GetDisplayName();
            }
        }

        /// <summary>
        /// Calculate the number of items defined in the enum.
        /// </summary>
        /// <returns>The number of items in the enums.</returns>
        public static int GetCount<TEnum>()
        {
            return Enum.GetNames(typeof(TEnum)).Length;
        }
    }
}
