﻿ // ReSharper disable once CheckNamespace
namespace System.IO
{
    public static class PathUtil
    {
        /// <summary>
        /// Checks a path for invalid characters
        /// </summary>
        /// <param name="path">The path to check</param>
        /// <returns>True if the path contains invalid path characters</returns>
        public static bool ContainsInvalidPathChars(this string path)
        {
            bool contains = false;
            foreach (char invalidChar in Path.GetInvalidPathChars())
            {
                if (path.Contains(invalidChar.ToString()))
                {
                    contains = true;
                    break;
                }
            }
            return contains;
        }

        /// <summary>
        /// Combines the path to an url without trailing slash
        /// </summary>
        /// <param name="basePath">The base path to append</param>
        /// <param name="paths">The paths to append to the base path</param>
        /// <returns>Full combined path without trailing slash</returns>
        public static string CombinePaths(this string basePath, params string[] paths)
        {
            basePath = basePath.Replace("\\", "/");

            if (paths.Length == 0)
            {
                return basePath;
            }

            string toReturn = basePath;

            foreach (string path in paths)
            {
                if (path.IsNullOrWhiteSpace())
                {
                    continue;
                }

                string p = path.Replace("\\", "/");

                if (!toReturn.EndsWith("/") && !p.StartsWith("/"))
                {
                    toReturn += "/";
                }

                toReturn += p;
            }

            if (toReturn.EndsWith("/"))
            {
                toReturn = toReturn.Substring(0, toReturn.Length - 1);
            }

            return toReturn;
        }
    }
}
