﻿ // ReSharper disable once CheckNamespace
namespace System.Resources
{
    /// <summary>
    /// Resource manager utilty class
    /// </summary>
    public static class ResourceManagerUtil
    {
        /// <summary>
        /// Returns the value of a specific resource
        /// </summary>
        /// <param name="resourceType">A type from which the resource manager derives all information for finding resource files.</param>
        /// <param name="resourceKey">The name of the resource to retrieve</param>
        /// <returns>The value of the resource</returns>
        public static string GetString(Type resourceType, string resourceKey)
        {
            ResourceManager resourceManager = new ResourceManager(resourceType);
            return resourceManager.GetString(resourceKey);
        }
    }
}
