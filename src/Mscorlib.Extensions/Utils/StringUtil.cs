﻿using System.Text;

// ReSharper disable once CheckNamespace

namespace System
{
    /// <summary>
    /// String utility class
    /// </summary>
    public static class StringUtil
    {
        /// <summary>
        /// Use this to combine infinite parts with a specified seperator
        /// </summary>
        /// <param name="seperator">Seperator</param>
        /// <param name="args">Elements to combine</param>
        /// <returns>Combined string with seperator if both a and b have a value</returns>
        public static string CombineWithSeperator(string seperator, params object[] args)
        {
            StringBuilder toReturn = new StringBuilder();

            for (int i = 0; args != null && i < args.Length; i++)
            {
                string value = string.Empty;

                if (args[i] != null)
                {
                    value = args[i].ToString();
                }

                if (value.Length > 0)
                {
                    if (toReturn.Length > 0)
                    {
                        toReturn.Append(seperator);
                        toReturn.Append(value);
                    }
                    else
                    {
                        toReturn.Append(value);
                    }
                }
            }

            return toReturn.ToString();
        }
    }
}
