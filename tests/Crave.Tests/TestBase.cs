﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NSubstitute;
using NUnit.Framework;

namespace Crave.Tests
{
    [TestFixture]
    public class TestBase
    {
        private readonly List<object> substituteList = new List<object>();

        [SetUp]
        protected void _SetUpInternal()
        {
            foreach (object o in this.substituteList)
            {
                o.ClearReceivedCalls();
            }

            this._SetUp();
        }

        protected virtual void _SetUp()
        {
            
        }

        protected T CreateSubstitute<T>() where T : class
        {
            int paramCount = 0;

            // Determine the amount of Ctor parameters this class has
            ConstructorInfo[] constructorInfos = typeof(T).GetConstructors();
            foreach (ConstructorInfo constructorInfo in constructorInfos)
            {
                int ctorParamLength = constructorInfo.GetParameters().Length;
                if (ctorParamLength == 0)
                {
                    paramCount = 0;
                    break;
                }
                if (paramCount == 0)
                {
                    paramCount = ctorParamLength;
                }
            }

            object[] constructorArguments = null;
            if (paramCount > 0)
            {
                constructorArguments = new object[paramCount];
            }

            T t = Substitute.For<T>(constructorArguments);
            this.substituteList.Add(t);

            return t;
        }
    }
}