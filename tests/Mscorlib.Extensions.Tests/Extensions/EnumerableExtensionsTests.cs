﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Mscorlib.Extensions.Tests.Extensions
{
    [TestFixture]
    public class EnumerableExtensionsTests
    {
        [Test]
        public void OrderByWithNullCheck_EmptyInput_ShouldReturnEmptyList()
        {
            List<DummyModel> input = new List<DummyModel>();
            
            IOrderedEnumerable<DummyModel> output = input.OrderByWithNullCheck(x => x.Sort);
            
            Assert.NotNull(output);
        }

        [TestCase(null)]
        public void OrderByWithNullCheck_NullInput_ShouldReturnEmptyList(List<DummyModel> input)
        {
            IOrderedEnumerable<DummyModel> output = input.OrderByWithNullCheck(x => x.Sort);

            Assert.NotNull(output);
        }

        public class DummyModel
        {
            public DummyModel(int sort, string value) => (Sort, Value) = (sort, value);
            
            public int Sort { get; } 
            public string Value { get; }
        }
    }
}